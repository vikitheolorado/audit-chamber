package com.auditchamber;

import android.content.Intent;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import in.sriraman.sharedpreferences.SharedDataProvider;
import in.sriraman.sharedpreferences.SharedHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.react.bridge.ReadableMapKeySetIterator;
import android.util.Log;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeArray;
import com.facebook.react.bridge.WritableNativeMap;

import java.util.Iterator;

import static com.nimbusds.jose.util.JSONObjectUtils.getJSONObject;


public class SqliteModule extends ReactContextBaseJavaModule {

  ReactContext reactContext;
  public SqliteModule(ReactApplicationContext reactContext){
    super (reactContext);
    this.reactContext = reactContext;
  }
  @Override
  public String getName(){
      return "SqliteModule";
  }

  @ReactMethod
  public void setItem(String key, String value) {
   SharedHandler.init(getReactApplicationContext());
   SharedDataProvider.putSharedValue(key,value);
   
  }
  @ReactMethod
  public void getItem(String key, Callback successCallback){
  SharedHandler.init(getReactApplicationContext());
  String value = SharedDataProvider.getSharedValue(key);
  successCallback.invoke(value);
  }
  @ReactMethod
  public void startTrackService(Callback successCallback) {
    Intent intent = new Intent(reactContext, myService.class);
    reactContext.startService(intent);
    successCallback.invoke("testing service");
  }

  @ReactMethod
  private static JSONArray convertArrayToJson(ReadableArray readableArray) throws JSONException {
    JSONArray array = new JSONArray();
    for (int i = 0; i < readableArray.size(); i++) {
      switch (readableArray.getType(i)) {
        case Null:
          break;
        case Boolean:
          array.put(readableArray.getBoolean(i));
          break;
        case Number:
          array.put(readableArray.getDouble(i));
          break;
        case String:
          array.put(readableArray.getString(i));
          break;
        case Array:
          array.put(convertArrayToJson(readableArray.getArray(i)));
          break;
      }
    }
    return array;
  }

  @ReactMethod
  private static WritableMap convertJsonToMap(JSONObject jsonObject) throws JSONException {
    WritableMap map = new WritableNativeMap();

    Iterator<String> iterator = jsonObject.keys();
    while (iterator.hasNext()) {
      String key = iterator.next();
      Object value = jsonObject.get(key);
      if (value instanceof JSONObject) {
        map.putMap(key, convertJsonToMap((JSONObject) value));
      } else if (value instanceof  JSONArray) {
        map.putArray(key, convertJsonToArray((JSONArray) value));
      } else if (value instanceof  Boolean) {
        map.putBoolean(key, (Boolean) value);
      } else if (value instanceof  Integer) {
        map.putInt(key, (Integer) value);
      } else if (value instanceof  Double) {
        map.putDouble(key, (Double) value);
      } else if (value instanceof String)  {
        map.putString(key, (String) value);
      } else {
        map.putString(key, value.toString());
      }
    }
    return map;
  }

  @ReactMethod
  private static WritableArray convertJsonToArray(JSONArray jsonArray) throws JSONException {
    WritableArray array = new WritableNativeArray();

    for (int i = 0; i < jsonArray.length(); i++) {
      Object value = jsonArray.get(i);
      if (value instanceof JSONObject) {
        array.pushMap(convertJsonToMap((JSONObject) value));
      } else if (value instanceof  JSONArray) {
        array.pushArray(convertJsonToArray((JSONArray) value));
      } else if (value instanceof  Boolean) {
        array.pushBoolean((Boolean) value);
      } else if (value instanceof  Integer) {
        array.pushInt((Integer) value);
      } else if (value instanceof  Double) {
        array.pushDouble((Double) value);
      } else if (value instanceof String)  {
        array.pushString((String) value);
      } else {
        array.pushString(value.toString());
      }
    }
    return array;
  }

  @ReactMethod
  public void insert(int audit, int answer, int finding, int media,String fil, String exten, int flagcompress, int flag) {
    DatabaseHandler db = new DatabaseHandler(getReactApplicationContext());

    

      db.addContact(new Contact(audit, answer, finding, media, fil, exten, flagcompress, 0));
  }

  @ReactMethod
  public void insertBulk(ReadableArray value,Callback successCallback, Callback errorCallBack){
    int AuditScheduleDetailID = 0;
    try{
      for(int i=0 ; i<value.size(); i++){
        try {
          JSONObject jobj = new JSONObject(value.getString(i));

//        Log.i("INSERTBULK", "ID "+ i +" AuditScheduleDetailID "+ String.valueOf(jobj.getInt("AuditScheduleDetailID")));
//        Log.i("INSERTBULK", "ID "+ i +" AuditAnswerId "+ String.valueOf(jobj.getInt("AuditAnswerId")));
//        Log.i("INSERTBULK", "ID "+ i +" LocalFindingID "+ String.valueOf(jobj.getInt("LocalFindingID")));
//        Log.i("INSERTBULK", "ID "+ i +" LocalMediaID "+ String.valueOf(jobj.getInt("LocalMediaID")));
//        Log.i("INSERTBULK", "ID "+ i +" Files "+ String.valueOf(jobj.getString("Files")));
//        Log.i("INSERTBULK", "ID "+ i +" ExtFiles "+ String.valueOf(jobj.getString("ExtFiles")));
//        Log.i("INSERTBULK", "ID "+ i +" FlagCompress "+ String.valueOf(jobj.getInt("FlagCompress")));
          if(i == 0){
            AuditScheduleDetailID = jobj.getInt("AuditScheduleDetailID");
          }

          insert(jobj.getInt("AuditScheduleDetailID"),
                  jobj.getInt("AuditAnswerId"),
                  jobj.getInt("LocalFindingID"),
                  jobj.getInt("LocalMediaID"),
                  jobj.getString("Files"),
                  jobj.getString("ExtFiles"),
                  jobj.getInt("FlagCompress"),
                  0);

        } catch (JSONException e) {
          e.printStackTrace();
        }
      }
      successCallback.invoke(AuditScheduleDetailID);

    }catch (Exception e){
      errorCallBack.invoke(e.getMessage());
      e.printStackTrace();
    }

  }

  // @ReactMethod
  // public void insert(ReadableArray name, int flag, Integer audit, Callback successCallback, Callback errorCallBack){
  //   int a=0;
    
  //   try{
  //     for(int i=0 ; i<name.size(); i++){
  //       DatabaseHandler db = new DatabaseHandler(getReactApplicationContext());
  //       db.addContact(new Contact(name.getString(i), a, audit.toString()));
  //     }
        
  //     successCallback.invoke(audit);
  //   }
  //   catch (Exception e){
  //     errorCallBack.invoke(e.getMessage());
  //   }
  // }

}