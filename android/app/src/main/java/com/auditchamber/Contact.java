package com.auditchamber;

public class Contact {
    int _id;
    int _schd_id;
    int _ans_id;
    int _loc_find;
    int _loc_med;
    String _file;
    String _ext_file;
    int _flag_compress;
    int _flag;

    public Contact(){

    }

    public Contact(int schd_id, int ans_id, int loc_find, int loc_med, String file, String
            ext_file,int flag_compress, int flag){
        this._schd_id = schd_id;
        this._ans_id = ans_id;
        this._loc_find = loc_find;
        this._loc_med = loc_med;
        this._file = file;
        this._ext_file = ext_file;
        this._flag_compress = flag_compress;
        this._flag = flag;
    }

    public Contact(int id, int flag_compress, int flag) {
        this._id = id;
        this._flag_compress = flag_compress;
        this._flag = flag;
    }

    public Contact(int id, int flag) {
        this._id = id;
        this._flag = flag;
    }

    // getting id
    public int getID(){
        return this._id;
    }

    // setting id
    public void setID(int id){
        this._id = id;
    }

    public int getSchedule(){
        return this._schd_id;
    }

    public void setSchedule(int schd_id){
        this._schd_id = schd_id;
    }

    public int getAnswer(){
        return this._ans_id;
    }

    public void setAnswer(int ans_id){
        this._ans_id = ans_id;
    }

    public int getLocal(){
        return this._loc_find;
    }

    public void setLocal(int loc_find){
        this._loc_find = loc_find;
    }

    public int getMedia(){
        return this._loc_med;
    }

    public void setMedia(int loc_med){
        this._loc_med = loc_med;
    }

    public String getFile(){
        return this._file;
    }

    public void setFile(String file){
        this._file = file;
    }

    public String getExt(){
        return this._ext_file;
    }

    public void setExt(String ext_file){
        this._ext_file = ext_file;
    }

    public int getFlagCompress(){
        return this._flag_compress;
    }

    public void setFlagCompress(int flag_compress){
        this._flag_compress = flag_compress;
    }

    public int getFlag(){
        return this._flag;
    }

    public void setFlag(int flag){
        this._flag = flag;
    }
}