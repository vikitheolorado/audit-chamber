package com.auditchamber;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nawadata on 20/11/2017.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 2;

    // Database Name
    private static final String DATABASE_NAME = "SendingData";

    // Contacts table name
    private static final String TABLE_CONTACTS = "Api";

    // Contacts Table Columns names
    private static final String KEY_ID = "ID";
    private static final String KEY_AuditScheduleDetailID = "AuditScheduleDetailID";
    private static final String KEY_AuditAnswerId = "AuditAnswerID";
    private static final String KEY_LocalFindingID = "LocalFindingID";
    private static final String KEY_LocalMediaID = "LocalMediaID";
    private static final String KEY_Files = "Files";
    private static final String KEY_ExtFiles = "ExtFiles";
    private static final String KEY_FLAG_COMPRESS = "FlagCompress";
    private static final String KEY_FLAG = "Flag";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_AuditScheduleDetailID + " INTEGER NOT NULL,"
                + KEY_AuditAnswerId + " INTEGER NOT NULL,"
                + KEY_LocalFindingID + " INTEGER NOT NULL,"
                + KEY_LocalMediaID + " INTEGER NOT NULL,"
                + KEY_Files + " VARCHAR NOT NULL,"
                + KEY_ExtFiles + " VARCHAR NOT NULL,"
                + KEY_FLAG_COMPRESS + " INTEGER NOT NULL,"
                + KEY_FLAG + " INTEGER"
                + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
        Log.i("Oncreate sqlite", "Berhasil");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);

        // Create tables again
        onCreate(db);
    }
    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new contact
    void addContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_AuditScheduleDetailID, contact.getSchedule());
        values.put(KEY_AuditAnswerId, contact.getAnswer());
        values.put(KEY_LocalFindingID, contact.getLocal());
        values.put(KEY_LocalMediaID, contact.getMedia());
        values.put(KEY_Files, contact.getFile());
        values.put(KEY_ExtFiles, contact.getExt());
        values.put(KEY_FLAG_COMPRESS, contact.getFlagCompress());
        values.put(KEY_FLAG, contact.getFlag());

        // Inserting Row
        db.insert(TABLE_CONTACTS, null, values);
        db.close(); // Closing database connection
    }

    // Getting All Contacts
    public List<Contact> getAllContacts() {
        List<Contact> contactList = new ArrayList<Contact>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_CONTACTS + " Where Flag = '0' and " +
                "FlagCompress = '1'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor != null && cursor.getCount() > 0){
            if (cursor.moveToFirst()) {
                do {
                    Contact contact = new Contact();
                    contact.setID(Integer.parseInt(cursor.getString(0)));
                    contact.setSchedule(Integer.parseInt(cursor.getString(1)));
                    contact.setAnswer(Integer.parseInt(cursor.getString(2)));
                    contact.setLocal(Integer.parseInt(cursor.getString(3)));
                    contact.setMedia(Integer.parseInt(cursor.getString(4)));
                    contact.setFile(cursor.getString(5));
                    contact.setExt(cursor.getString(6));
                    contact.setFlagCompress(Integer.parseInt(cursor.getString(7)));
                    contact.setFlag(Integer.parseInt(cursor.getString(8)));
                    // Adding contact to list
                    contactList.add(contact);
                } while (cursor.moveToNext());
            }
        }

        cursor.close();
        db.close();
        // return contact list
        return contactList;
    }

    public int getCountContact() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor mCount= db.rawQuery("SELECT Count(*) AS count FROM " + TABLE_CONTACTS + " Where " +
                "Flag = '0' AND FlagCompress = 1", null);
        mCount.moveToFirst();
        int count= mCount.getInt(0);
        mCount.close();
        db.close();
        // return contact list
        return count;
    }

    // Updating single contact
    public int updateContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_FLAG, contact.getFlag());

        // updating row
        return db.update(TABLE_CONTACTS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });
    }


    public List<Contact> getaudit(){
        List<Contact> contactList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT AuditScheduleDetailID, Count(AuditScheduleDetailID) AS AuditCount, Sum(Flag) AS FlagTotal FROM API  GROUP BY AuditScheduleDetailID";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Contact contact = new Contact();
                contact.setFile(cursor.getString(0));
                contact.setExt(cursor.getString(1));
                contact.setFlag(Integer.parseInt(cursor.getString(2)));
                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        // return contact list
        return contactList;
    }
    
    public List<Contact> auditid(){
        List<Contact> contactList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT x.* FROM (SELECT AuditScheduleDetailID, COUNT(AuditScheduleDetailID) AS AuditCount, SUM(Flag) AS FlagTotal FROM API GROUP BY AuditScheduleDetailID) x WHERE x.FlagTotal = x.AuditCount";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Contact contact = new Contact();
                contact.setFile(cursor.getString(0));
                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        // return contact list
        return contactList;
    }

    public int getCountFlagAuditID() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor mCount= db.rawQuery("SELECT count(*) FROM ( SELECT AuditScheduleDetailID, COUNT(AuditScheduleDetailID) AS AuditCount, SUM(Flag) AS FlagTotal FROM API GROUP BY AuditScheduleDetailID ) x WHERE  x.FlagTotal = x.AuditCount", null);
        mCount.moveToFirst();
        int count= mCount.getInt(0);
        mCount.close();
        db.close();
        // return contact list
        return count;
    }

    public void delauditid(String audit){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, "Flag = 1 AND AuditScheduleDetailID = ?", new String[] { audit });
        db.close();
    }

    public List<Contact> readCompress() {
        List<Contact> contactList = new ArrayList<Contact>();
        // Select All Query
        String selectQuery = "SELECT ID, Files FROM " + TABLE_CONTACTS + " Where " +
                "FlagCompress = '0' AND ExtFiles = 'mp4' LIMIT 1";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor != null && cursor.getCount() > 0){
            if (cursor.moveToFirst()) {
                do {
                    Contact contact = new Contact();
                    contact.setID(Integer.parseInt(cursor.getString(0)));
                    contact.setFile(cursor.getString(1));
                    // Adding contact to list
                    contactList.add(contact);
                } while (cursor.moveToNext());
            }
        }

        cursor.close();
        db.close();
        // return contact list
        return contactList;
    }
    public int updateCompress(int idnya, String newpath, int flagnya) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_Files, newpath);
        values.put(KEY_FLAG_COMPRESS, flagnya);
        
        // updating row
        return db.update(TABLE_CONTACTS, values, KEY_ID + "=" + idnya, null);
    }

    public int countCompress() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor mCount= db.rawQuery("SELECT count(*) FROM " + TABLE_CONTACTS + " where " +
                "FlagCompress = '0' AND ExtFiles = 'mp4'",
                null);
        mCount.moveToFirst();
        int count= mCount.getInt(0);
        mCount.close();
        db.close();
        // return contact list
        return count;
    }
}