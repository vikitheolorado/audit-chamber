package com.auditchamber;


import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Base64;
import android.util.Log;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

public class myService extends Service {
    public Runnable mRunnable = null;
    private boolean mRunning = false;
    Handler mHandler = new Handler();
    IBinder mBinder = new LocalBinder();
    String newPathOutputVideo = null;
    int idProcessVideo = 0;
    String log = null;
    @Inject
    FFmpeg ffmpeg;

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class LocalBinder extends Binder {
        public myService getServerInstance() {
            return myService.this;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        loadFFmpeg();
        Log.d("Service"," onstart kepanggil ga?");
        mRunnable = new Runnable() {
            @Override
            public void run() {
                /* ---New Code--- */
                Log.d("Service","SERVICE RUN");
                SharedPreferences pref = getSharedPreferences("wit_player_shared_preferences", MODE_PRIVATE);
                String servcheck = pref.getString("serviceChecker", null);
                String servcompress = pref.getString("serviceCompress", null);
                DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                int countFlagAuditID = db.getCountFlagAuditID();
                int countNeedToSend = db.getCountContact();
                int itungcompress = db.countCompress();

                if (itungcompress > 0){
                    Log.d("countNeedToCompress : ", String.valueOf(itungcompress));
                    compressTask c = new compressTask();
                    if (servcompress.equals("no")) {
                        Log.d("Service","SERVICE TRY CALL COMPRESS");
                        c.execute();
                    }
                }

                if (countNeedToSend > 0){
                    Log.d("countNeedToSend : ", String.valueOf(countNeedToSend));
                    sending a = new sending();
                    if(servcheck.equals("no")){
                        Log.d("Service","SERVICE TRY CALL SENDING");
                        a.execute();
                    }
                }

                if (countFlagAuditID > 0){
                    Log.d("countFlagAuditID : ", String.valueOf(countFlagAuditID));
                    if(servcheck.equals("no")){
                        Log.d("Service","SERVICE TRY CALL SENDGET");
                        sendget b = new sendget();
                        b.execute();
                    }
                }
                db.close();
            mHandler.postDelayed(mRunnable, 2 * 60 * 1000);
            }
        };
        mHandler.postDelayed(mRunnable, 2 * 60 * 1000);
        return START_STICKY;
    }
    private class sending extends AsyncTask<Void, Void, String >
    {
        @Override
        protected void onPreExecute() {
            Log.i("SENDING", "start sending");
            SharedPreferences pref = getSharedPreferences("wit_player_shared_preferences", MODE_PRIVATE);
            // String serv = pref.getString("serviceChecker", null);
            pref.edit().putString("serviceChecker", "yes").commit();
            // if (serv.equals("yes")){
            //     Log.i("stop service", "service di stop");
            //     stopSelf();
            // }
        }

        @Override
        protected String doInBackground(Void... params) {
            Log.i("SENDING", "BACKGROUND sending");
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            SharedPreferences pref = getSharedPreferences("wit_player_shared_preferences", MODE_PRIVATE);
            String token = pref.getString("token", null);
            String apilink = pref.getString("api", null);

            List<Contact> contac = db.getAllContacts();
            for (Contact cn : contac) {
                int idsql = cn.getID();
                Log.i("SENDING", "ID  SQLITE YANG KE SELECT " + String.valueOf(cn.getID())  +
                        " " + String.valueOf(cn.getSchedule()) +" " + String.valueOf(cn.getAnswer
                        ()) +  " " +String.valueOf(cn.getLocal()) + " " +String
                        .valueOf(cn.getMedia()) + " " +String.valueOf(cn.getFile()) + " " +String.valueOf
                        (cn.getExt()) + " " +String.valueOf(cn.getFlag() + " " +String.valueOf(cn.getFlag()
                )));

                String pathimage = cn.getFile();
                int schedule_id = cn.getSchedule();
                int answer_id = cn.getAnswer();
                int finding_id = cn.getLocal();
                int media_id = cn.getMedia();
                String imageString = null; //base64 image
                String extention = cn.getExt() ;
                String encodedString; //base64 video

                if(cn.getExt().equals("jpg")) {
                    File file = new File(pathimage);
                    try {
                        if (file.exists()) {
                            Log.i("SENDING :", "File exist Try Convert image to base 64");
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            Bitmap bitmap = BitmapFactory.decodeFile(pathimage);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                            byte[] imageBytes = baos.toByteArray();
                            imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                            Log.i("SENDING", "IDSSQL "+ String.valueOf(idsql));
                            Log.i("SENDING", "Base64 "+imageString);
                        } else{
                            Log.i("SENDING:", "image not exists");
                        }
                    } catch (Exception e){
                        Log.i("SENDING : ", "Image coonvert base64 error : " + String.valueOf(e));
                    }

                } else if (cn.getExt().equals("mp4")){
                    File fileDel = new File(cn.getFile());
                    try {
                        if (fileDel.exists()) {
                            Log.i("SENDING:", "Try Convert video to base 64");
                            InputStream inputStream = null;
                            try{
                                inputStream = new FileInputStream(fileDel);
                            }catch (Exception e){
                            }
                            byte[] videobytes;
                            byte[] buffer = new byte[2048];
                            int bytesRead;

                            ByteArrayOutputStream output = new ByteArrayOutputStream();
                            try{
                                Log.i("SENDING :", "Try in Convert video to base 64");
                                while((bytesRead = inputStream.read(buffer)) != -1){
                                    output.write(buffer, 0, bytesRead);
                                }
                            }catch (IOException e){
                                e.printStackTrace();
                            }
                            videobytes = output.toByteArray();
                            imageString = Base64.encodeToString(videobytes, Base64.DEFAULT);
                            Log.i("SENDING", "IDSQL " + String.valueOf(idsql));
                            Log.i("SENDING", "BASE64 " + imageString);
                        } else{
                            Log.i("SENDING:", "video not exists");
                        }
                    } catch (Exception e){
                        Log.i("SENDING : ", "video convert bas64 error"+ String.valueOf(e));
                    }
                }
                db.updateContact(new Contact(idsql,1));
                idsql = cn.getID();
                try {
//                    JSONObject job = new JSONObject(log);
//                    String param1 = job.getString("AuditScheduleDetailID");
//                    String param2 = job.getString("AuditAnswerId");
//                    String param3 = job.getString("LocalFindingID");
//                    String param4 = job.getString("LocalMediaID");
//                    String param5 = job.getString("Files");
//                    String param6 = job.getString("ExtFiles");

                    //URL url = new URL("https://ccaiddevauditchamber001.azurewebsites.net/api/doaudit/postauditmedia");
                    URL url = new URL(apilink + "doaudit/postauditmedia");
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setRequestProperty("Authorization", "Bearer " + token);
                    conn.setDoOutput(true);
                    conn.setDoInput(true);

                    JSONObject jsonParam = new JSONObject();
                    jsonParam.put("AuditScheduleDetailID", schedule_id);
                    jsonParam.put("AuditAnswerId", answer_id);
                    jsonParam.put("LocalFindingID", finding_id);
                    jsonParam.put("LocalMediaID", media_id);
                    jsonParam.put("Files", imageString);
                    jsonParam.put("ExtFiles", extention);

                    DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                    //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                    os.writeBytes(jsonParam.toString());
                    os.flush();
                    os.close();
                    InputStream is = null;
                    if(conn.getResponseCode() == HttpURLConnection.HTTP_OK){
                        is = conn.getInputStream();// is is inputstream
                    } else {
                        is = conn.getErrorStream();
                    }
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(
                                is, "UTF-8"), 8);
                        StringBuilder sb = new StringBuilder();
                        String line = null;
                        while ((line = reader.readLine()) != null) {
                            sb.append(line + "\n");
                        }
                        is.close();
                        String response = sb.toString();
                        //HERE YOU HAVE THE VALUE FROM THE SERVER

                        if(response.trim().equals ("0")){
                            Log.d("berhasil","keknya");
                            db.updateContact(new Contact(idsql,1));
                            deletePath(pathimage);
                        }
                        else{
                            Log.d("Error", "ubah data");
                        }
                    } catch (Exception e) {
                        Log.e("Buffer Error", "Error converting result " + e.toString());
                    }
                    Log.i("STATUS", String.valueOf(conn.getResponseCode()));
                    Log.i("Input", String.valueOf(conn.getInputStream()));
                    Log.i("MSG", conn.getResponseMessage());
                    conn.disconnect();
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }
            db.close();
            return "Done";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d("SENDING",result);
            SharedPreferences pref = getSharedPreferences("wit_player_shared_preferences", MODE_PRIVATE);
            pref.edit().putString("serviceChecker", "no").commit();
        }
    }

    /* ---New Code--- */
    
    /* ---Finish--- */

    private class sendget extends AsyncTask<Void, Void, String >
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.i("SENDGET", "start SENDGET");
            SharedPreferences pref = getSharedPreferences("wit_player_shared_preferences", MODE_PRIVATE);
            // String serv = pref.getString("serviceChecker", null);
            pref.edit().putString("serviceChecker", "yes").commit();
            // SharedPreferences pref = getSharedPreferences("wit_player_shared_preferences", MODE_PRIVATE);
            // String serv = pref.getString("serviceChecker", null);
            // if (serv.equals("yes")){
            //     Log.i("stop service", "service di stop");
            //     stopSelf();
            // }
        }

        @Override
        protected String doInBackground(Void... params) {
            Log.i("SENDGET", "BACKGROUND SENDGET");
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            
            SharedPreferences pref = getSharedPreferences("wit_player_shared_preferences", MODE_PRIVATE);
            String token = pref.getString("token", null);
            String apilink = pref.getString("api", null);

            List<Contact> contac = db.auditid();
            for (Contact cn : contac) {
                String log = cn.getFile();
                Log.i("sendget", log);
                try {
//                    //URL url = new URL("https://ccaiddevauditchamber001.azurewebsites.net/api/doaudit/postcompleteauditmedia?auditid=" + log);
//
                    URL url = new URL(apilink + "doaudit/postcompleteauditmedia?auditid=" + log);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestProperty("Authorization", "Bearer " + token);

                    Log.i("getName", log);
                    InputStream is = null;

                    if(conn.getResponseCode() == HttpURLConnection.HTTP_OK){
                        is = conn.getInputStream();// is is inputstream
                    } else {
                        is = conn.getErrorStream();
                    }
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(
                                is, "UTF-8"), 8);
                        StringBuilder sb = new StringBuilder();
                        String line = null;
                        while ((line = reader.readLine()) != null) {
                            sb.append(line + "\n");
                        }
                        is.close();
                        String response = sb.toString();
                        //HERE YOU HAVE THE VALUE FROM THE SERVER
                        Log.d("Your Data", response);
                        if(response.trim().equals ("0")){
                            Log.d("berhasil","kirim method get ke sonoh IDnya");
                            db.delauditid(log);
                        }
                        else{
                            Log.d("Error", "ubah data");
                        }
                    } catch (Exception e) {
                        Log.e("Buffer Error", "Error converting result " + e.toString());
                    }
                    Log.i("STATUS", String.valueOf(conn.getResponseCode()));
                    Log.i("Input", String.valueOf(conn.getInputStream()));
                    Log.i("MSG", conn.getResponseMessage());
                    conn.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            db.close();
            return "Done";
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d("SENDGET",result);
            SharedPreferences pref = getSharedPreferences("wit_player_shared_preferences", MODE_PRIVATE);
            pref.edit().putString("serviceChecker", "no").commit();
        }
    }

    private class compressTask extends AsyncTask<Void, Void, String>{
        final String TAG = "compressTask";
        @Override
        protected void onPreExecute(){
            Log.i(TAG, TAG+" onPreExecute");
            SharedPreferences pref = getSharedPreferences("wit_player_shared_preferences", MODE_PRIVATE);
            pref.edit().putString("serviceCompress", "yes").commit();
        }

        @Override
        protected String doInBackground(Void... params) {
            Log.i(TAG, TAG+" doInBackground");
            SharedPreferences pref = getSharedPreferences("wit_player_shared_preferences", MODE_PRIVATE);
            String curdate = DateFormat.getDateTimeInstance().format(new Date());
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            String servcheck = pref.getString("serviceChecker", null);
            String servcompress = pref.getString("serviceCompress", null);
            String path = pref.getString("mediaPath", null);
            List<Contact> contac = db.readCompress();
            for (Contact cn : contac) {
                newPathOutputVideo = path +'/'+ curdate + ".mp4";
                log = cn.getFile();
                idProcessVideo = cn.getID();
                Log.i(TAG,  "path " + log); //storage/emulated/0/Movies/Test.mp4
                Log.i(TAG, "IDSQL : " + String.valueOf(idProcessVideo));
                String[] command = {"-i", log,"-strict", "experimental","-s","160x120","-r",
                        "25", "-vcodec","mpeg4","-b:v", "100k","-ab","48000","-ac","2","-ar",
                        "22050", newPathOutputVideo};
                executeFFmpeg(command);

            }
            db.close();
            return "Done";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }

    private void loadFFmpeg() {
        ffmpeg = FFmpeg.getInstance(getApplicationContext());
        final String TAG = "Load FFmpeg";
        try {
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {

                @Override
                public void onStart() {Log.d(TAG, "Started command : ffmpeg ");}

                @Override
                public void onFailure() {Log.d(TAG, "FAILED with output : ");}

                @Override
                public void onSuccess() {Log.d(TAG, "SUCCESS with output : ");}

                @Override
                public void onFinish() {Log.d(TAG, "Finished command : ffmpeg ");}
            });
        } catch (FFmpegNotSupportedException e) {
            // Handle if FFmpeg is not supported by device
        }
    }
    private void executeFFmpeg(final String[] command) {
        final String TAG = "EXECUTEFFMPEG";
        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {
                    Log.d(TAG, "FAILED with output : " + s);
                }

                @Override
                public void onSuccess(String s) {
                    Log.d(TAG, "SUCCESS with output : " + s);
                }

                @Override
                public void onProgress(String s) {
                    Log.d(TAG, "progress : " + s);
                }

                @Override
                public void onStart() {
                    Log.d(TAG, "Started command : ffmpeg " + Arrays.toString(command));
                }

                @Override
                public void onFinish() {
                    Log.d(TAG, "Finished command : ffmpeg " + Arrays.toString(command));
                    deletePath(log);
                    DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                    db.updateCompress(idProcessVideo,newPathOutputVideo, 1);
                    Log.i(TAG, "UPDATE COMPRESSAN YANG UDH SELESAI");
                    Log.i(TAG, String.valueOf(db.readCompress().size()));
                    idProcessVideo = 0;
                    newPathOutputVideo = null;

                    SharedPreferences pref = getSharedPreferences("wit_player_shared_preferences", MODE_PRIVATE);
                    pref.edit().putString("serviceCompress", "no").commit();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // Handle if FFmpeg is already running
        }
    }

    private void deletePath(String path){
        File fileDel = new File(path);
        try {
            if (fileDel.exists()) {
                if (fileDel.delete()) {
                    Log.i("file Deleted :", path);
                } else {
                    Log.i("file not Deleted :", path);
                }
            } else{
                Log.i("file is :", "not exists");
            }
        } catch (Exception e){
            Log.i("Delete Path Error : ", String.valueOf(e));
        }
    }
}