package com.auditchamber;

import android.app.Application;

import com.avishayil.rnrestart.ReactNativeRestartPackage;
import com.brentvatne.react.ReactVideoPackage;
import com.christopherdro.htmltopdf.RNHTMLtoPDFPackage;
import com.crashlytics.android.Crashlytics;
import com.facebook.react.ReactApplication;
import com.hauvo.compress.RNCompressPackage;
import in.sriraman.sharedpreferences.RNSharedPreferencesReactPackage;
import org.wonday.pdf.RCTPdfView;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.geektime.rnonesignalandroid.ReactNativeOneSignalPackage;
import com.keyee.pdfview.PDFView;
import com.lwansbrough.RCTCamera.RCTCameraPackage;
import com.microsoft.codepush.react.CodePush;
import com.oblador.vectoricons.VectorIconsPackage;
import com.react.rnspinkit.RNSpinkitPackage;
import com.rnfs.RNFSPackage;
import com.smixx.fabric.FabricPackage;

import java.util.Arrays;
import java.util.List;

import fr.bamlab.rnimageresizer.ImageResizerPackage;
import io.fabric.sdk.android.Fabric;
import io.realm.react.RealmReactPackage;
public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {

        @Override
        protected String getJSBundleFile() {
        return CodePush.getJSBundleFile();
        }
    
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNCompressPackage(),
            new RCTPdfView(),
            new RNFetchBlobPackage(),
            new RCTCameraPackage(),
            new RNSharedPreferencesReactPackage(),
            new SqlitePackager(),
            new FabricPackage(),
            new RNHTMLtoPDFPackage(),
            new ReactNativeOneSignalPackage(),
            new CodePush("yxCPlyY76_SHb8rHLdZzPaTygUvd5c91b90e-c53c-4464-bee3-803912f31f5a",MainApplication.this, BuildConfig.DEBUG),// staging
            //new CodePush("VtdS240MrMyjmO2Aw7CEdbk8mHMG5c91b90e-c53c-4464-bee3-803912f31f5a",MainApplication.this, BuildConfig.DEBUG),// production
            new RealmReactPackage(),
            new ReactVideoPackage(),
            new VectorIconsPackage(),
            new RNSpinkitPackage(),
            new ReactNativeRestartPackage(),
            new PDFView(),
            new ImageResizerPackage(),
            new RNFSPackage()
      );
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    Fabric.with(this, new Crashlytics());
    SoLoader.init(this, /* native exopackage */ false);
  }
}
