//import CodePush from "react-native-code-push";
import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, BackAndroid } from "react-native";
import { Router, Scene, Stack } from "react-native-router-flux";
import OneSignal from "react-native-onesignal";
import ErrorReport from "./App/ErrorReport";

//Component for navigation
import HomeScreen from "./App/Containers/HomeScreen";
import CameraScreen from "./App/Containers/CameraScreen";
import Splash from "./App/Containers/Splash";
import Login from "./App/Containers/Login";
import CreateSchedule from "./App/Containers/CreateSchedule";
import QuestionScreen from "./App/Containers/QuestionScreen";
import MyAudit from "./App/Containers/MyAudit";

import ScheduleAudit from './App/Components/ScheduleAuditInc'
import CreateScheduleAudit from './App/Components/CreateScheduleInc'
import CreateNonScheduleAudit from './App/Components/CreateNonScheduleInc'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showDownloadingModal: false,
      showInstalling: false,
      downloadProgress: 0
    };
    ErrorReport.init(true);
  }
  componentDidMount() {
    console.disableYellowBox = true;
    if (Platform.OS == "android") {
      console.log("pastikan service false firsttime");
      Queries.setPersistentData("service", "false");
    }

    BackAndroid.addEventListener("hardwareBackPress", this.handleBackButton);
    // CodePush.sync({ updateDialog: true, installMode: CodePush.InstallMode.IMMEDIATE },
    //   (status) => {
    //     switch (status) {
    //       case CodePush.SyncStatus.DOWNLOADING_PACKAGE:
    //         this.setState({ showDownloadingModal: true });
    //         this._modal.open();
    //         break;
    //       case CodePush.SyncStatus.INSTALLING_UPDATE:
    //         this.setState({ showInstalling: true });
    //         break;
    //       case CodePush.SyncStatus.UPDATE_INSTALLED:
    //         this._modal.close();
    //         this.setState({ showDownloadingModal: false });
    //         break;
    //       default:
    //         break;
    //     }
    //   },
    //   ({ receivedBytes, totalBytes }) => {
    //     this.setState({ downloadProgress: (receivedBytes / totalBytes) * 100 });
    //   }
    // );
  }
  componentWillMount() {
    console.log("call app");
    OneSignal.inFocusDisplaying(0);
    OneSignal.addEventListener("ids", this.onIds);
  }

  componentWillUnmount() {
    OneSignal.removeEventListener("ids", this.onIds);
    BackAndroid.removeEventListener("hardwareBackPress", this.handleBackButton);
  }

  handleBackButton() {
    return true;
  }

  onIds(device) {
    Queries.setPersistentData("NotificationID", device.userId);
    console.log("Notif ID " + device.userId);
  }
  render() {
    if (this.state.showDownloadingModal) {
      return (
        <Container style={{ backgroundColor: theme.defaultBackgroundColor }}>
          <Content style={styles.container}>
            <Modal
              style={[styles.modal, styles.modal1]}
              backdrop={false}
              ref={c => {
                this._modal = c;
              }}
              swipeToClose={false}
            >
              <View
                style={{
                  flex: 1,
                  alignSelf: "stretch",
                  justifyContent: "center",
                  padding: 20
                }}
              >
                {this.state.showInstalling ? (
                  <Text
                    style={{
                      color: "#5067FF",
                      textAlign: "center",
                      marginBottom: 15,
                      fontSize: 15
                    }}
                  >
                    Installing update...
                  </Text>
                ) : (
                  <View
                    style={{
                      flex: 1,
                      alignSelf: "stretch",
                      justifyContent: "center",
                      padding: 20
                    }}
                  >
                    <Text
                      style={{
                        color: "#5067FF",
                        textAlign: "center",
                        marginBottom: 15,
                        fontSize: 15
                      }}
                    >
                      Downloading update...{" "}
                      {`${parseInt(this.state.downloadProgress, 10)} %`}
                    </Text>
                    <ProgressBar
                      color="#5067FF"
                      progress={parseInt(this.state.downloadProgress, 10)}
                    />
                  </View>
                )}
              </View>
            </Modal>
          </Content>
        </Container>
      );
    }
    return (
      <Router>
        <Stack panHandlers={null} key="root">
          <Scene
            panHandlers={null}
            initial
            key="Splash"
            component={Splash}
            hideNavBar={true}
          />
          <Scene
            panHandlers={null}
            key="HomeScreen"
            component={HomeScreen}
            hideNavBar={true}
          />
          <Scene
            panHandlers={null}
            key="CameraScreen"
            component={CameraScreen}
            hideNavBar={true}
          />
          <Scene
            panHandlers={null}
            key="Login"
            component={Login}
            hideNavBar={true}
          />
          <Scene
            panHandlers={null}
            key="CreateSchedule"
            component={CreateSchedule}
            hideNavBar={true}
          />
          <Scene
            panHandlers={null}
            key="QuestionScreen"
            component={QuestionScreen}
            hideNavBar={true}
          />
          <Scene
            panHandlers={null}
            key="MyAudit"
            component={MyAudit}
            hideNavBar={true}
          />
          <Scene
            panHandlers={null}
            key="ScheduleAudit"
            component={ScheduleAudit}
            hideNavBar={true}
          />
          <Scene
            panHandlers={null}
            key="CreateScheduleAudit"
            component={CreateScheduleAudit}
            hideNavBar={true}
          />
          <Scene
            panHandlers={null}
            key="CreateNonScheduleAudit"
            component={CreateNonScheduleAudit}
            hideNavBar={true}
          />
        </Stack>
      </Router>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  }
});

export default App;
