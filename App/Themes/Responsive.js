import { Dimensions, PixelRatio } from 'react-native';
const {height, width} = Dimensions.get('window')

const ratioX = width < 375 ? (width < 370 ? 0.75 : 0.875) : 1 ;
const ratioY = height < 568 ? (height < 480 ? 0.75 : 0.875) : 1 ;
const base_unit = 16;
const unit = base_unit * (width / 480);//ratioX;

function getFontRatio(){
    return PixelRatio.get() * 10;
    // if (PixelRatio.getFontScale() <= 1) {
    //     return 1;
    // }
    // else if(PixelRatio.getFontScale() <= 1.5){
    //     return 1.5;
    // }
    // else if(PixelRatio.get() <= 2){
    //     return 2;
    // }
    // else if(PixelRatio.get() <= 3){
    //     return 3;
    // }
    // else if(PixelRatio.get() <= 3.5){
    //     return 3.5;
    // }
    // else{
    //     return 5;
    // }
}

export const responsiveHeight = (h) => {
  return height*(h/100);
}

export const responsiveWidth = (w) => {
  return width*(w/100);
}

export const responsiveFontSize = (f) => {
  return Math.sqrt((height*height)+(width*width))*(f/100);
}

export const newResponsiveFontSize = (f) => {
  return unit * f;
}
