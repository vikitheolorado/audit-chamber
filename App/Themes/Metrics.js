// @flow

import {Dimensions, Platform} from 'react-native'
import { responsiveHeight, responsiveWidth } from './Responsive'

const { width, height } = Dimensions.get('window')

// Used via Metrics.baseMargin
const metrics = {
  marginTopContainer : (Platform.OS === 'ios') ? 0  : 0,
  marginIOS: (Platform.OS === 'ios') ? 20  : 0,
  marginHeader : responsiveHeight(7),

  footerHeight : responsiveHeight(12),

  footerIconHeight : responsiveWidth(10),
  footerIconWidth : responsiveWidth(10),

  footerIconPlusHeight : responsiveHeight(13),
  footerIconPlusWidth : responsiveHeight(13),

  footerIconContainerHeight :  responsiveWidth(10) + responsiveWidth(3),
  footerIconContainerWidth :  responsiveWidth(10) + responsiveWidth(3),

  modalUserHeight : responsiveHeight(70),
  modalUserWidth : responsiveWidth(85),

  // QuestionScreen
  footerHeightQuestionScreen : responsiveHeight(6),
  mainContainerHeightQuestionScreen : (width < height ? height : width) - ((Platform.OS === 'ios') ? 20  : 0) - responsiveHeight(7) - responsiveHeight(5),

  marginHorizontal: 10,
  marginVertical: 10,
  section: 25,
  baseMargin: 10,
  doubleBaseMargin: 20,
  smallMargin: 5,
  horizontalLineHeight: 1,
  searchBarHeight: 30,
  screenWidth: width < height ? width : height,
  screenHeight: width < height ? height : width,
  navBarHeight: (Platform.OS === 'ios') ? 40  : 40  ,
  buttonRadius: 4,
  icons: {
    tiny: 15,
    small: 20,
    medium: 30,
    large: 45,
    xl: 60
  },
  images: {
    small: 20,
    medium: 40,
    large: 60,
    logo: 300
  }
}

export default metrics
