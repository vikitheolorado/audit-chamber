// @flow

// leave off @2x/@3x
const images = {
  logo: require('../Images/logoquinsys.png'),
  iconuser: require('../Images/user.png'),

  myauditon : require('../Images/myauditon.png'),
  myauditoff : require('../Images/myauditoff.png'),

  resulton : require('../Images/resulton.png'),
  resultoff : require('../Images/resultoff.png'),

  plus : require('../Images/plus.png'),

  scheduleon : require('../Images/scheduleon.png'),
  scheduleoff : require('../Images/scheduleoff.png'),

  notificationon : require('../Images/New/useron.png'),
  notificationoff : require('../Images/New/useroff.png'),

  close : require('../Images/close.png'),

  camera : require('../Images/camera-white.png'),
  attach : require('../Images/attach.png'),
  addBlue : require('../Images/addblue.png'),
  lightBulb : require('../Images/light-bulb.png'),

  userprofile : require('../Images/userprofile.png'),

  power : require('../Images/power.png'),

  go : require('../Images/go.png'),

  iconcreateaudit : require('../Images/createaudit.png'), 
  iconcreatescheduled : require('../Images/createscheduled.png'), 
  iconnonschedule : require('../Images/nonschedule.png'), 

  check : require('../Images/check.png'), 

  imagenotavailable : require('../Images/photo-camera-not-available.png'), 
  detective : require('../Images/quinsy-detective-white.png'),

  flashon : require('../Images/New/flash-on-indicator.png'),
  flashoff : require('../Images/New/flash-off.png'),

  switchCamera : require('../Images/New/retweet-arrows.png'),

  closeCamera : require('../Images/New/cross.png'),
  iconVideo : require("../Images/dummyvideo.png")
}

export default images
