// @flow

import {responsiveHeight, responsiveWidth, responsiveFontSize, newResponsiveFontSize} from './Responsive';
import {Dimensions, Platform} from 'react-native'
import Constant from '../Data/Constants'

const type = {
  base: 'HelveticaNeue',
  bold: 'HelveticaNeue-Bold',
  emphasis: 'HelveticaNeue-Italic'
}

const size = {
  h1: 38, 
  h2: 34,
  h3: 30,
  h4: 26,
  h5: 20,
  h6: 19,
  input: 18,
  regular: 17,
  medium: 14,
  small: 12,
  tiny: 8.5
}

const style = {
  h1: {
    fontFamily: type.base,
    fontSize: size.h1
  },
  h2: {
    fontWeight: 'bold',
    fontSize: size.h2
  },
  h3: {
    fontFamily: type.emphasis,
    fontSize: size.h3
  },
  h4: {
    fontFamily: type.base,
    fontSize: size.h4
  },
  h5: {
    fontFamily: type.base,
    fontSize: size.h5
  },
  h6: {
    fontFamily: type.emphasis,
    fontSize: size.h6
  },
  normal: {
    fontFamily: type.base,
    fontSize: size.regular
  },
  description: {
    fontFamily: type.base,
    fontSize: size.medium
  },
  fontTitle:{
    color: Constant.main_color,
    fontWeight: 'bold',
    fontSize: newResponsiveFontSize(16)
  }
}

const font = {
  fontTitleLarge:{
    color: 'black',
    fontSize: newResponsiveFontSize(1.7),
    fontFamily: 'MyriadPro-Regular'
  },
  fontTitleNormal:{
    color: 'black',
    fontWeight: 'bold',
    fontSize: (Platform.OS === 'ios') ? newResponsiveFontSize(1.4) : newResponsiveFontSize(1.5),
    fontFamily: 'MyriadPro-Regular'
  },
  fontContentLarge:{
    color: 'black',
    fontSize: (Platform.OS === 'ios') ? newResponsiveFontSize(1.2) : newResponsiveFontSize(1.3),
    fontFamily: 'MyriadPro-Regular'
  },
  fontContentNormal:{
    color: 'black',
    fontSize: (Platform.OS === 'ios') ? newResponsiveFontSize(1.1) : newResponsiveFontSize(1.2),
    fontFamily: 'MyriadPro-Regular'
  },
  fontContentNormalPopup:{
    color:'black',
    fontSize: (Platform.OS === 'ios') ? newResponsiveFontSize(1.1) : newResponsiveFontSize(1.2),
    fontFamily: 'MyriadPro-Regular',
    fontWeight: 'bold'
  },
  fontContentNormalComboBox:{
    color: 'black',
    fontSize: (Platform.OS === 'ios') ? newResponsiveFontSize(1.5) : newResponsiveFontSize(1.5),
    fontFamily: 'MyriadPro-Regular'
  },
  fontContentSmall:{
    color: 'black',
    fontSize: (Platform.OS === 'ios') ? newResponsiveFontSize(1) : newResponsiveFontSize(1),
    fontFamily: 'MyriadPro-Regular'
  },
  fontContentSmallv1:{
    color: 'black',
    fontSize: (Platform.OS === 'ios') ? newResponsiveFontSize(0.8) : newResponsiveFontSize(0.8),
    fontFamily: 'MyriadPro-Regular'
  }
}

export default {
  type,
  size,
  style,
  font
}

