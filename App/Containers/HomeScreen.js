// @flow
import CodePush from "react-native-code-push";
import React, { Component } from 'react';
import { TextInput, NativeModules, Platform, BackAndroid, View, AppState, Image, Text, StyleSheet, Alert, Modal, TouchableHighlight, TouchableOpacity, TouchableWithoutFeedback, StatusBar } from 'react-native';
import { Metrics, Images } from '../Themes';
import { Footer, FooterTab, Button, Badge, Header, Left, Body, Right, Title, Fab, Container } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { responsiveHeight, responsiveWidth, responsiveFontSize, newResponsiveFontSize } from '../Themes/Responsive';
import MyAudit from './MyAudit';
import Notification from '../Components/Notification';
var RNFS = require('react-native-fs');
//import DbAuditChamber from '../Realm/DbAuditChamber';
import { getInstance } from '../Data/DB/DBHelper';
import Queries from '../Data/Queries';
import Constants from '../Data/Constants';
import Axios from 'axios';
import Camera from 'react-native-camera'
import ActionButton from 'react-native-circular-action-menu';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Result, CreateScheduleInc, ScheduleAuditInc, AuditReportInc, CreateNonScheduleInc, ScheduleScreen } from '../Components';
import OneSignal from 'react-native-onesignal';
import style from '../Themes/Fonts';
import DB from '../Data/DB/Db';
import ErrorReport from '../ErrorReport'
//Baru
import Swiper from 'react-native-swiper';
import Menu, { MenuContext, MenuOptions, MenuOption, MenuTrigger } from 'react-native-menu';
import Spinner from 'react-native-loading-spinner-overlay';
export default class HomeScreen extends Component {

  constructor(props) {
    //// console.log('HOME SCREEN')

    super(props);
    this.MsUser = Queries.getCurrentUser();
    this.StatusUpdate = false;
    //this.MsPlant = getInstance().objects('MsPlant').filtered("PlantID = " + this.MsUser[0].PlantID)

    this.state = {
      modalPlusVisible: false,
      modalUserVisible: false,
      authid: this.MsUser[0].Roles, //1 = 3, 2 = 2

      fullName: this.MsUser[0].FullName,
      userName: this.MsUser[0].Username,
      userPosition: this.MsUser[0].Position,
      userPlant: "",//this.MsPlant[0].PlantDesc,

      loginStatus: true,

      notification: 0,

      currentMenu: 'myaudit',

      menuMyAuditOn: true,
      menuResultOn: false,
      menuScheduleOn: false,
      menuNotificationOn: false,

      selectedAuditDetailID: -1,
      pageIndex: 0,

      value_additional_param: 0,

      // Apuy add 17 nov 17
      // change 3 sub menu from + floating action button to full pop screen
      modalScheduleAuditPop: false,
      modalCreateSchedulePop: false,
      modalCreateNonSchedulePop: false,
      loaderStatus: false,
      loadingInfo: 'Please Wait',
      insertCount: 0,
      totalInsertCount: 0,
      loadingProgress: '',

      isMasterDataDownloaded: Queries.getPersistentData('masterdata'),
      downloadingMaster: false,
      modalLoadingProgress: false,
      downloadedMasterData: [],

      showDownloadingModal: false,
      showInstalling: false,
      downloadProgress: 0,
    }

  }

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }

  componentDidMount() {
    CodePush.sync({ updateDialog: true, installMode: CodePush.InstallMode.IMMEDIATE },
      (status) => {
        switch (status) {
          case CodePush.SyncStatus.DOWNLOADING_PACKAGE:
            this.setState({ showDownloadingModal: true });
            //this._modal.open();
            break;
          case CodePush.SyncStatus.INSTALLING_UPDATE:
            this.setState({ showInstalling: true });
            break;
          case CodePush.SyncStatus.UPDATE_INSTALLED:
            //this._modal.close();
            this.setState({ showDownloadingModal: false });
            break;
          default:
            break;
        }
      },
      ({ receivedBytes, totalBytes }) => {
        this.setState({ downloadProgress: (receivedBytes / totalBytes) * 100 });
      }
    );
    if (Platform.OS == 'android') {
      RNFS.exists(Constants.PDFPath)
        .then((res) => {
          if (!res) {
            RNFS.mkdir(Constants.PDFPath).then(res => {
              console.log('success create pdf dir')
            })
              .catch(err => {
                console.log('error create pdf dir')
              })
          }
        })

      RNFS.exists(Constants.MediaPath)
      .then((res) => {
        if (!res) {
          RNFS.mkdir(Constants.MediaPath)
            .then(res => {
              console.log("success create media dir");
            })
            .catch(err => {
              console.log("error create media dir");
            });
        }
      })

      let dataAdd = NativeModules.SqliteModule;

      dataAdd.setItem("token", Queries.getPersistentData('token'));
      dataAdd.setItem("api", Constants.APILink);
      dataAdd.setItem("mediaPath", Constants.MediaPath);
      /* ---New Code--- */
      dataAdd.setItem("serviceChecker", "no");
      dataAdd.setItem("serviceCompress", "no");
      console.log("serviceCheck");
      dataAdd.getItem("serviceChecker", function (value) {
          console.log(value);
      });
      /* ---Finish--- */

      // dataAdd.getItem("token", function (value) {
      //   console.log(value);
      // })

      var isServiceRunning = Queries.getPersistentData('service')
      console.log("isServiceRunning " + isServiceRunning);

      if(isServiceRunning == "false"){
        dataAdd.startTrackService(function (value) {
          console.log("Service callback " + value);
          Queries.setPersistentData('service', 'true')
        })
      }
        
    } else {
      var Service = NativeModules.Service;
      var TokenCoy = Queries.getPersistentData('token');
      var URLAPI = Constants.APILink;
      Service.addEvent([], TokenCoy, URLAPI, (callback) => {
        if (callback) {
          console.log("Service Dari React Native => " + callback);
        }
      });
    }

    AppState.addEventListener('change', this._handleAppStateChange.bind(this));
    if (Platform.OS == "android") {
      listener = BackAndroid.addEventListener("hardwareBackPress", () => {
        //console.log('masuk ya')
        return true;
      })
    }

    if (this.state.isMasterDataDownloaded == '0') {
      this.setState({
        loaderStatus: true
        //modalLoadingProgress : true
      })

      this.getMasterDataAllBackup()
        .then((res) => {
          Alert.alert(
            'Information',
            'Master data success downloaded',
            [
              {
                text: 'OK', onPress: () => {
                  Queries.setPersistentData('masterdata', '1');
                  this.setState({
                    isMasterDataDownloaded: '1',
                    loaderStatus: false,
                    //modalLoadingProgress : false,
                    loadingInfo: 'Please Wait'
                  })
                }
              },
            ],
            { cancelable: false }
          )
        })
        .catch((err) => {
          console.log(err)
          Alert.alert(
            'Information',
            'Failed download Master data',
            [
              {
                text: 'OK', onPress: () => this.setState({
                  loaderStatus: false,
                  //modalLoadingProgress : false,
                  loadingInfo: 'Please Wait'
                })
              },
            ],
            { cancelable: false }
          )
        })
    }
  }


  _changeCreateScheduleParameter(param) {
    console.log(param)
    this.setState({
      value_additional_param: param
    })
  }

  // mainContainer() {
  //   //// console.log(this.state.currentMenu)
  //   if (this.state.currentMenu === 'myaudit') {
  //     return (
  //       <View style={[styles.mainContainer]}>
  //         <MyAudit />
  //       </View>
  //     )
  //   } else if (this.state.currentMenu === 'result') {
  //     return (
  //       // <View style={{backgroundColor: '#636264', height:Metrics.screenHeight - Metrics.marginTopContainer - responsiveHeight(11) - responsiveHeight(5)}}>
  //       // </View>
  //       <View style={[styles.mainContainer]}>
  //         <Result />
  //       </View>
  //     )
  //   }
  //   else if (this.state.currentMenu === 'schedule') {
  //     return (
  //       <View style={[styles.mainContainer]}>
  //         <ScheduleScreen />
  //       </View>
  //     )
  //   }
  //   else if (this.state.currentMenu === 'notification') {
  //     return (
  //       <View style={[styles.mainContainer]}>
  //         <Notification userName={this.state.userName} moveToMyAudit={this.tabMenuPress.bind(this)} readNotification={function () { this.setState({ notification: (this.state.notification > 1 ? this.state.notification - 1 : 0) }) }.bind(this)} />
  //       </View>
  //     )
  //   }
  //   else if (this.state.currentMenu === 'createauditschedule') {
  //     return (
  //       <View style={[styles.mainContainer]}>
  //         <CreateScheduleInc />
  //       </View>
  //     )
  //   }
  //   else if (this.state.currentMenu === 'createauditnonschedule') {
  //     return (
  //       <View style={[styles.mainContainer]}>
  //         <CreateNonScheduleInc />
  //       </View>
  //     )
  //   }
  //   else if (this.state.currentMenu === 'scheduleaudit') {
  //     return (
  //       <View style={[styles.mainContainer]}>
  //         <ScheduleAuditInc PK_AuditScheduleDetailID={this.state.selectedAuditDetailID} />
  //       </View>
  //     )
  //   }
  //   else if (this.state.currentMenu === 'auditreport') {
  //     return (
  //       <View style={[styles.mainContainer]}>
  //         <AuditReportInc />
  //       </View>
  //     )
  //   }
  // }

  plusOnPress = () => {
    // console.log('press');
    value = this.state.modalPlusVisible;
    this.setState({ modalPlusVisible: !value })
  }

  createAuditScheduleOnPress() {
    this.disableTabMenu();
    this.setState({ 
      modalPlusVisible: false, 
      //modalCreateSchedulePop: true 
    },()=>{
      Actions.CreateScheduleAudit({ type: 'replace' })
    });


  }

  createAuditNonScheduleOnPress() {
    this.disableTabMenu();
    this.setState({ 
      modalPlusVisible: false, 
      //modalCreateNonSchedulePop: true 
    },()=>{
      Actions.CreateNonScheduleAudit({ type: 'replace' })
    });
  }

  scheduleAudit() {
    this.disableTabMenu();
    this.setState({ 
      //modalPlusVisible: false, 
      selectedAuditDetailID: -1,
      //modalScheduleAuditPop: true 
    });

    Actions.ScheduleAudit({ type: 'replace' })
  }

  nonScheduleOnPress() {
    this.setState({ currentMenu: 'nonscheduleaudit', modalPlusVisible: false })
  }

  auditreport() {
    this.setState({ currentMenu: 'auditreport', modalPlusVisible: false })
  }


  modalPlus() {
    if (this.state.authid === 3) {
      return (
        <View>
          <TouchableHighlight onPress={this.createAuditScheduleOnPress.bind(this)}>
            <Text style={styles.modalPlusText} >Create Audit Schedule</Text>
          </TouchableHighlight>
          <TouchableHighlight onPress={this.scheduleAudit.bind(this)}>
            <Text style={styles.modalPlusText} >Schedule Audit</Text>
          </TouchableHighlight>
          <TouchableHighlight onPress={this.createAuditNonScheduleOnPress.bind(this)}>
            <Text style={styles.modalPlusText} >Non Schedule</Text>
          </TouchableHighlight>

        </View>)
    }
    else {
      return (
        <View>
          <TouchableHighlight onPress={this.scheduleAudit.bind(this)}>
            <Text style={styles.modalPlusText} >Schedule Audit</Text>
          </TouchableHighlight>
          <TouchableHighlight onPress={this.nonScheduleOnPress.bind(this)}>
            <Text style={styles.modalPlusText} >Non Schedule</Text>
          </TouchableHighlight>
        </View>)
    }
  }

  disableTabMenu() {
    this.setState({
      menuResultOn: false,
      menuMyAuditOn: false,
      menuScheduleOn: false,
      menuNotificationOn: false,
    })
  }

  tabMenuPress(value, auditDetailID = null) {
    // // console.log("tabMenuPress " + value);
    // this.disableTabMenu();
    if (value === 'myaudit') {
      this.setState({
        currentMenu: value,
        menuMyAuditOn: true,
      })
    } else if (value === 'result') {
      this.setState({
        currentMenu: value,
        menuResultOn: true,
      })
    } else if (value === 'schedule') {
      this.setState({
        currentMenu: value,
        menuScheduleOn: true,
      })
    } else if (value === 'notification') {
      this.setState({
        currentMenu: value,
        menuNotificationOn: true,
      });
    }
    else if (value === 'auditreport') {
      this.setState({
        currentMenu: value,
        menuNotificationOn: true,
      })
    } else if (value === 'scheduleaudit') {
      this.setState({ currentMenu: 'scheduleaudit', modalPlusVisible: false, selectedAuditDetailID: auditDetailID })
    }
  }

  formatDate(date) {
    let monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ];

    let day = date.getDate();
    let monthIndex = date.getMonth();
    let year = date.getFullYear();

    return day + ' ' + monthNames[monthIndex] + ' ' + year;
  }

  RefreshLocalNotification() {
    // console.log('CCAAPI')
    console.log(Constants.APILink + 'notif/GetNotification?username=' + this.state.userName + `apuytoken`)
    if (Queries.getPersistentData('token') != '') {
      Axios.get(Constants.APILink + 'notif/GetNotification?username=' + this.state.userName, {
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer " + Queries.getPersistentData('token'),
        }
      })
        .then(function (result) {
          Queries.syncNotifications(result.data);
          this.setState({
            notification: result.data.length
          });
        }.bind(this))
        .catch((error) => {


          if (error && error.response && error.response.status == "401") {
            // console.log("Information - HomeScreen - GetNotification - 401")
            Queries.sessionHabis(error.response.status);
          }

        });
    }

  }

  componentWillMount() {
    OneSignal.addEventListener('received', this.onReceived.bind(this));

    // let notifications = Queries.getNotifications(this.state.userName);
    // this.setState({
    //   notification: notifications.length
    // });

    this.RefreshLocalNotification();
  }

  _onPressNothing() {

  }

  floatingactionbutton() {

    if (this.state.authid === 3) {
      return (
        <ActionButton size={Metrics.footerIconPlusHeight} buttonColor="#c00000" image={Images.plus} bgColor={'rgba(0,0,0,0.6)'} radius={responsiveHeight(17)}>
          <ActionButton.Item onPress={() => this._onPressNothing()} />

          <ActionButton.Item buttonColor='transparent' size={responsiveHeight(10)} title="New Task" onPress={() => this.scheduleAudit()}>
            {/* Create Non Schedule Audit
              <Icon name="android-create" style={styles.actionButtonIcon} />*/}
            <Image source={Images.iconcreateaudit} style={{ height: responsiveHeight(10), width: responsiveHeight(10) }} />
          </ActionButton.Item>
          <ActionButton.Item buttonColor='transparent' size={responsiveHeight(10)} title="Notifications" onPress={() => this.createAuditScheduleOnPress()}>
            {/*  Create Schedule Audit */}
            <Image source={Images.iconcreatescheduled} style={{ height: responsiveHeight(10), width: responsiveHeight(10) }} />
          </ActionButton.Item>
          <ActionButton.Item buttonColor='transparent' size={responsiveHeight(10)} title="All Tasks" onPress={() => this.createAuditNonScheduleOnPress()}>
            {/* Conduct Audit*/}
            <Image source={Images.iconnonschedule} style={{ height: responsiveHeight(10), width: responsiveHeight(10) }} />
          </ActionButton.Item>

          <ActionButton.Item onPress={() => this._onPressNothing()} />
        </ActionButton>
      )
    } else if (this.state.authid === 1) {
      return (
        <ActionButton size={Metrics.footerIconPlusHeight} buttonColor="#c00000" image={Images.plus} bgColor={'rgba(0,0,0,0.6)'} radius={responsiveHeight(17)}>
          <ActionButton.Item onPress={() => this._onPressNothing()} />
          <ActionButton.Item buttonColor='transparent' size={responsiveHeight(10)} title="New Task" onPress={() => this.scheduleAudit()}>
            {/* Create Non Schedule Audit
              <Icon name="android-create" style={styles.actionButtonIcon} />*/}
            <Image source={Images.iconcreateaudit} style={{ height: responsiveHeight(10), width: responsiveHeight(10) }} />
          </ActionButton.Item>
          <ActionButton.Item onPress={() => this._onPressNothing()} />
          <ActionButton.Item buttonColor='transparent' size={responsiveHeight(10)} title="All Tasks" onPress={() => this.createAuditNonScheduleOnPress()}>
            {/* Conduct Audit*/}
            <Image source={Images.iconnonschedule} style={{ height: responsiveHeight(10), width: responsiveHeight(10) }} />
          </ActionButton.Item>
          <ActionButton.Item onPress={() => this._onPressNothing()} />
        </ActionButton>
      )
    } else if (this.state.authid === 3) {
      return (
        <ActionButton size={Metrics.footerIconPlusHeight} buttonColor="rgba(231,76,60,1)">
          <ActionButton.Item onPress={() => this._onPressNothing()} />
          <ActionButton.Item onPress={() => this._onPressNothing()} />
          <ActionButton.Item buttonColor='#9b59b6' size={responsiveHeight(10)} title="New Task" >
            {/*<Icon name="android-create" style={styles.actionButtonIcon} />*/}
            <Image source={Images.iconuser} style={{ height: responsiveHeight(10), width: responsiveHeight(10) }} />
          </ActionButton.Item>
          <ActionButton.Item onPress={() => this._onPressNothing()} />
          <ActionButton.Item onPress={() => this._onPressNothing()} />
        </ActionButton>
      )
    }

  }

  _doLogout() {
    Queries.setAllUserNotLogin();
    this.setState({ modalUserVisible: false })
    Actions.Login({ type: 'replace' })
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange(nextAppState) {
    if (nextAppState === 'active') {
      this.RefreshLocalNotification();
    }
  }

  onReceived(notification) {
    this.RefreshLocalNotification();
  }

  changePage(param) {
    try {
      if (param == 0) {

        if (this.state.pageIndex == 1) {
          this.child.scrollBy(-1, true);
          this.tabMenuPress('myaudit');
        }
        else if (this.state.pageIndex == 2) {
          this.child.scrollBy(-2, true);
          this.tabMenuPress('myaudit');
        }
        else if (this.state.pageIndex == 3) {
          this.child.scrollBy(-3, true);
          this.tabMenuPress('myaudit');
        }
      }
      else if (param == 1) {

        if (this.state.pageIndex == 0) {
          this.child.scrollBy(1, true);
          this.tabMenuPress('result');
        }
        else if (this.state.pageIndex == 2) {
          this.child.scrollBy(-1, true);
          this.tabMenuPress('result');
        }
        else if (this.state.pageIndex == 3) {
          this.child.scrollBy(-2, true);
          this.tabMenuPress('result');
        }
      }
      else if (param == 2) {

        if (this.state.pageIndex == 0) {
          this.child.scrollBy(2, true);
          this.tabMenuPress('schedule');
        }
        else if (this.state.pageIndex == 1) {
          this.child.scrollBy(1, true);
          this.tabMenuPress('schedule');
        }
        else if (this.state.pageIndex == 3) {
          this.child.scrollBy(-1, true);
          this.tabMenuPress('schedule');
        }
      }
      else if (param == 3) {

        if (this.state.pageIndex == 0) {
          this.child.scrollBy(3, true);
          this.tabMenuPress('notification');
        }
        else if (this.state.pageIndex == 1) {
          this.child.scrollBy(2, true);
          this.tabMenuPress('notification');
        }
        else if (this.state.pageIndex == 2) {
          this.child.scrollBy(1, true);
          this.tabMenuPress('notification');
        }
      }
    }
    catch (ex) {
      // console.log(ex)
    }

  }

  SwipeViewAudit() { if (this.state.pageIndex == 0) { return (<MyAudit />) } }
  SwipeViewResult() { if (this.state.pageIndex == 1) { return (<Result />) } }
  SwipeViewSchedule() { if (this.state.pageIndex == 2) { return (<ScheduleScreen />) } }
  SwipeViewNotification() {
    if (this.state.pageIndex == 3) {
      return (
        <Notification userName={this.state.userName} moveToMyAudit={this.tabMenuPress.bind(this)} readNotification={function () { this.setState({ notification: (this.state.notification > 1 ? this.state.notification - 1 : 0) }) }.bind(this)} />
      )
    }
  }
  SwipeView() {
    if (this.state.currentMenu === 'createauditschedule') {
      return (
        <View>
          <View style={[styles.mainContainer]}>
            <CreateScheduleInc
              _changeParam={this._changeCreateScheduleParameter.bind(this)}
              onRef={ref => (this.createSchedule = ref)}
            />
          </View>
        </View>
      )
    }
    else if (this.state.currentMenu === 'createauditnonschedule') {
      return (
        <View>
          <View style={[styles.mainContainer]}>
            <CreateNonScheduleInc />
          </View>
        </View>
      )
    }
    else if (this.state.currentMenu === 'scheduleaudit') {
      console.log("Schedule Audit Inc - Home Screen")
      return (
        <View>
          <View style={[styles.mainContainer]}>
            <ScheduleAuditInc PK_AuditScheduleDetailID={this.state.selectedAuditDetailID} />
          </View>
        </View>
      )
    }
  }

  closeModal(submenu) {
    // case submenu value 1, conduct schedule
    // case submenu value 2, create schedule
    // case submenu value 3, adhoc schedule
    console.log("Test close modal " + submenu)

    if (submenu == 1) {
      this.setState({ modalScheduleAuditPop: false })
    } else if (submenu == 2) {
      this.setState({ modalCreateSchedulePop: false })
    } else if (submenu == 3) {
      this.setState({ modalCreateNonSchedulePop: false })
    }


  }

  marginTopForIOS() {
    if (Platform.OS === 'ios') {
      return (
        <View style={{ height: 20, backgroundColor: '#c00' }}>
        </View>
      )
    }
  }

  getMasterDataAll() {
    return new Promise((resolve, reject) => {
      console.log('CCAAPI')
      Axios.get(Constants.APILink + `Master/GetMasterDataAll?username=` + Queries.getPersistentData('email'), {
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer " + Queries.getPersistentData('token'),
        }, timeout: 30000
      })
        .then(res => {
          console.log(res.data)
          this.setState({
            downloadedMasterData: res.data,
            downloadingMaster: true,
            insertCount: 0
          })
        })
        .catch(err => {
          reject(new Error(err))
        })
      // this.setState({ loaderStatus: true })
    })
  }

  getMasterDataAllBackup() {
    return new Promise((resolve, reject) => {
      //Axios.get(Constants.APILink + `Master/GetMasterDataAllFilterByPlant?username=` + Queries.getPersistentData('email'), {
      Axios.get(Constants.APILink + `Master/GetMasterDataAll?username=` + Queries.getPersistentData('email'), {
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer " + Queries.getPersistentData('token'),
        }, timeout: 30000
      })
        .then(res => {
          console.log(res.data.ObjTemplateCategory)
          // let totalCount = 0;
          // totalCount += res.data.ObjPlant.length
          // totalCount += res.data.ObjGroupLine.length
          // totalCount += res.data.ObjUser.length
          // totalCount += res.data.ObjUserAuditee.length
          // totalCount += res.data.ObjArea.length
          // totalCount += res.data.ObjLine.length
          // totalCount += res.data.ObjTemplate.length
          // totalCount += res.data.ObjTemplateQuestion.length

          // console.log(totalCount)


          DB.write(() => {
            getInstance().delete(getInstance().objects('MsPlant'));
            getInstance().delete(getInstance().objects('MsGroupLine'));
            getInstance().delete(getInstance().objects('MsUser'));
            getInstance().delete(getInstance().objects('MsUserAuditee'));
            getInstance().delete(getInstance().objects('MsArea'));
            getInstance().delete(getInstance().objects('MsLine'));
            getInstance().delete(getInstance().objects('MsTemplateHeader'));
            getInstance().delete(getInstance().objects('TemplateQuestion'));
            getInstance().delete(getInstance().objects('TemplateCategory'));
            getInstance().delete(getInstance().objects('TemplateAnswer'));
          })

          console.log("data plant")
          try {
            DB.write(() => {
              res.data.ObjPlant.map(function (data, i) {
                DB.create('MsPlant', { PlantID: data.PlantID, PlantDesc: data.PlantDesc, active: data.Active });
              })
            })
          } catch (error) {
            console.log(error)
            DB.write(() => {
              res.data.ObjPlant.map(function (data, i) {
                DB.create('MsPlant', { PlantID: data.PlantID, PlantDesc: data.PlantDesc, active: data.Active }, true);
              })
            })
          }
          // res.data.ObjPlant.map(function (data, i) {
          //   //console.log(data)
          //   try {
          //     DB.write(() => {
          //       DB.create('MsPlant', { PlantID: data.PlantID, PlantDesc: data.PlantDesc, active: data.Active });
          //     })

          //   } catch (error) {
          //     DB.write(() => {
          //       DB.create('MsPlant', { PlantID: data.PlantID, PlantDesc: data.PlantDesc, active: data.Active }, true);
          //     })

          //     ErrorReport.log(error);
          //   }
          // })
          console.log("data MsGroupLine")
          try {
            DB.write(() => {
              res.data.ObjGroupLine.map(function (data, i) {
                DB.create('MsGroupLine', { GroupLineID: data.GroupLineID, GroupLineIDDesc: data.GroupLineDesc, PlantID: data.PlantID, GroupLineIDAbb: data.GroupLineAbb, active: data.Active });
              })
            })
          } catch (error) {
            DB.write(() => {
              res.data.ObjGroupLine.map(function (data, i) {
                DB.create('MsGroupLine', { GroupLineID: data.GroupLineID, GroupLineIDDesc: data.GroupLineDesc, PlantID: data.PlantID, GroupLineIDAbb: data.GroupLineAbb, active: data.Active }, true);
              })
            })
          }


          // console.log("data 2")
          // res.data.ObjGroupLine.map(function (data, i) {
          //   try {
          //     DB.write(() => {
          //       DB.create('MsGroupLine', { GroupLineID: data.GroupLineID, GroupLineIDDesc: data.GroupLineDesc, PlantID: data.PlantID, GroupLineIDAbb: data.GroupLineAbb, active: data.Active });
          //     })
          //   } catch (error) {
          //     DB.write(() => {
          //       DB.create('MsGroupLine', { GroupLineID: data.GroupLineID, GroupLineIDDesc: data.GroupLineDesc, PlantID: data.PlantID, GroupLineIDAbb: data.GroupLineAbb, active: data.Active }, true);
          //     })
          //     ErrorReport.log(error);
          //   }
          // })
          console.log("data MsUser")
          try {
            DB.write(() => {
              res.data.ObjUser.map(function (data, i) {
                DB.create('MsUser', { UserID: data.UserID, Username: data.Username, FullName: data.FullName, Email: data.Email, Roles: data.Roles, isLogin: data.isLogin, PlantID: data.PlantID, Position: data.Position, active: data.Active });
              })
            })
          } catch (error) {
            DB.write(() => {
              res.data.ObjUser.map(function (data, i) {
                DB.create('MsUser', { UserID: data.UserID, Username: data.Username, FullName: data.FullName, Email: data.Email, Roles: data.Roles, isLogin: data.isLogin, PlantID: data.PlantID, Position: data.Position, active: data.Active }, true);
              })
            })
          }

          // console.log("data 3")
          // res.data.ObjUser.map(function (data, i) {
          //   try {
          //     DB.write(() => {
          //       DB.create('MsUser', { UserID: data.UserID, Username: data.Username, FullName: data.FullName, Email: data.Email, Roles: data.Roles, isLogin: data.isLogin, PlantID: data.PlantID, Position: data.Position, active: data.Active });
          //     })
          //   } catch (error) {
          //     DB.write(() => {
          //       DB.create('MsUser', { UserID: data.UserID, Username: data.Username, FullName: data.FullName, Email: data.Email, Roles: data.Roles, isLogin: data.isLogin, PlantID: data.PlantID, Position: data.Position, active: data.Active }, true);
          //     })
          //     ErrorReport.log(error);
          //   }
          // })
          console.log("data MsUserAuditee")
          try {
            DB.write(() => {
              res.data.ObjUserAuditee.map(function (data, i) {
                DB.create('MsUserAuditee', { PK_MsUserAuditee_ID: (i + 1), UserID: data.UserID, Username: data.Username, FullName: data.FullName, Email: data.Email, PlantID: data.PlantID, GroupLineID: data.GroupLineID, active: data.Active });
              })
            })

          } catch (error) {
            DB.write(() => {
              res.data.ObjUserAuditee.map(function (data, i) {
                DB.create('MsUserAuditee', { PK_MsUserAuditee_ID: (i + 1), UserID: data.UserID, Username: data.Username, FullName: data.FullName, Email: data.Email, PlantID: data.PlantID, GroupLineID: data.GroupLineID, active: data.Active });
              })
            })

          }

          // res.data.ObjUserAuditee.map(function (data, i) {
          //   try {
          //     DB.write(() => {
          //       DB.create('MsUserAuditee', { PK_MsUserAuditee_ID: (i + 1), UserID: data.UserID, Username: data.Username, FullName: data.FullName, Email: data.Email, PlantID: data.PlantID, GroupLineID: data.GroupLineID, active: data.Active });
          //     })
          //   } catch (error) {
          //     DB.write(() => {
          //       DB.create('MsUserAuditee', { PK_MsUserAuditee_ID: (i + 1), UserID: data.UserID, Username: data.Username, FullName: data.FullName, Email: data.Email, PlantID: data.PlantID, GroupLineID: data.GroupLineID, active: data.Active }, true);
          //     })
          //     ErrorReport.log(error);
          //   }
          // })
          console.log("data MsArea")
          try {
            DB.write(() => {
              res.data.ObjArea.map(function (data, i) {
                DB.create('MsArea', { groupLineID: data.groupLineID, lineID: data.lineID, areaID: data.areaID, areaDesc: data.areaDesc, Active: data.Active })
              })
            })
          } catch (error) {
            DB.write(() => {
              res.data.ObjArea.map(function (data, i) {
                DB.create('MsArea', { groupLineID: data.groupLineID, lineID: data.lineID, areaID: data.areaID, areaDesc: data.areaDesc, active: data.Active }, true)
              })
            })
          }

          // res.data.ObjArea.map(function (data, i) {
          //   try {
          //     DB.write(() => {
          //       DB.create('MsArea', { groupLineID: data.groupLineID, lineID: data.lineID, areaID: data.areaID, areaDesc: data.areaDesc, Active: data.Active })
          //     })
          //   } catch (error) {
          //     DB.write(() => {
          //       DB.create('MsArea', { groupLineID: data.groupLineID, lineID: data.lineID, areaID: data.areaID, areaDesc: data.areaDesc, active: data.Active }, true)
          //     })
          //     ErrorReport.log(error);
          //   }
          // })
          console.log("data MsLine")
          try {
            DB.write(() => {
              res.data.ObjLine.map(function (data, i) {
                DB.create('MsLine', { PK_LineID: (i + 1), LineID: data.LineID, LineIDDesc: data.LineDesc, GroupLineID: data.GroupLineID, active: data.Active });
              })
            })

          } catch (error) {
            DB.write(() => {
              res.data.ObjLine.map(function (data, i) {
                DB.create('MsLine', { PK_LineID: (i + 1), LineID: data.LineID, LineIDDesc: data.LineDesc, GroupLineID: data.GroupLineID, active: data.Active }, true);
              })
            })

          }

          // res.data.ObjLine.map(function (data, i) {
          //   try {
          //     DB.write(() => {
          //       DB.create('MsLine', { PK_LineID: (i + 1), LineID: data.LineID, LineIDDesc: data.LineDesc, GroupLineID: data.GroupLineID, active: data.Active });
          //     })
          //   } catch (error) {
          //     DB.write(() => {
          //       DB.create('MsLine', { PK_LineID: (i + 1), LineID: data.LineID, LineIDDesc: data.LineDesc, GroupLineID: data.GroupLineID, active: data.Active }, true);
          //     })
          //     ErrorReport.log(error);
          //   }
          // })
          console.log("data PersistentData")
          try {
            DB.write(() => {
              res.data.ObjSystemParameter.map(function (data, i) {
                DB.create('PersistentData', { Key: data.key, Value: data.value }, true);
              })
            })

          } catch (error) {
            DB.write(() => {
              res.data.ObjSystemParameter.map(function (data, i) {
                DB.create('PersistentData', { Key: data.key, Value: data.value }, true);
              })
            })

          }

          // res.data.ObjSystemParameter.map(function (data, i) {
          //   try {
          //     DB.write(() => {
          //       DB.create('PersistentData', { Key: data.key, Value: data.value }, true);
          //     })
          //   } catch (error) {
          //     DB.write(() => {
          //       DB.create('PersistentData', { Key: data.key, Value: data.value }, true);
          //     })
          //     ErrorReport.log(error);
          //   }
          // })

          try {
            DB.write(() => {
              res.data.ObjTemplate.map(function (data, i) {
                DB.create('MsTemplateHeader', { TemplateHeaderId: data.TemplateHeaderId, AuditTypeId: data.AuditTypeId, TemplateCode: data.TemplateCode, TemplateStatus: data.TemplateStatus }, true);
              })
            })

          } catch (error) {
            DB.write(() => {
              res.data.ObjTemplate.map(function (data, i) {
                DB.create('MsTemplateHeader', { TemplateHeaderId: data.TemplateHeaderId, AuditTypeId: data.AuditTypeId, TemplateCode: data.TemplateCode, TemplateStatus: data.TemplateStatus }, true);
              })
            })

          }

          // res.data.ObjTemplate.map(function (data, i) {
          //   try {
          //     DB.write(() => {
          //       DB.create('MsTemplateHeader', { TemplateHeaderId: data.TemplateHeaderId, AuditTypeId: data.AuditTypeId, TemplateCode: data.TemplateCode, TemplateStatus: data.TemplateStatus }, true);
          //     })
          //   } catch (error) {
          //     DB.write(() => {
          //       DB.create('MsTemplateHeader', { TemplateHeaderId: data.TemplateHeaderId, AuditTypeId: data.AuditTypeId, TemplateCode: data.TemplateCode, TemplateStatus: data.TemplateStatus }, true);
          //     })
          //     ErrorReport.log(error);
          //   }
          // })

          try {
            DB.write(() => {
              res.data.ObjTemplateQuestion.map(function (data, i) {
                DB.create('TemplateQuestion', {
                  templateCode: data.TemplateCode,
                  categoryID: data.CategoryId,
                  questionFill: data.QuestionFill,
                  questionOrder: data.QuestionOrder,
                  weight: data.Weight,
                  isRequired: data.IsRequired,
                  templateQuestionID: data.TemplateQuestionId
                },
                  true);

                DB.create('TemplateAnswer', {
                templateQuestionID: data.TemplateQuestionId,
                answerID:1,
                answerType:'YES'
              });

                DB.create('TemplateAnswer', {
                templateQuestionID: data.TemplateQuestionId,
                answerID:2,
                answerType:'NO'
              });

                DB.create('TemplateAnswer', {
                templateQuestionID: data.TemplateQuestionId,
                answerID:3,
                answerType:'N/A'
              });

              })
            })
          } catch (error) {
            DB.write(() => {
              res.data.ObjTemplateQuestion.map(function (data, i) {
                DB.create('TemplateQuestion', {
                  templateCode: data.TemplateCode,
                  categoryID: data.CategoryId,
                  questionFill: data.QuestionFill,
                  questionOrder: data.QuestionOrder,
                  weight: data.Weight,
                  isRequired: data.IsRequired,
                  templateQuestionID: data.TemplateQuestionId
                },
                  true);

                  DB.create('TemplateAnswer', {
                templateQuestionID: data.TemplateQuestionId,
                answerID:1,
                answerType:'YES'
              });

                DB.create('TemplateAnswer', {
                templateQuestionID: data.TemplateQuestionId,
                answerID:2,
                answerType:'NO'
              });

                DB.create('TemplateAnswer', {
                templateQuestionID: data.TemplateQuestionId,
                answerID:3,
                answerType:'N/A'
              });

              })
            })
          }

          // template category
          try {
            DB.write(() => {
              res.data.ObjTemplateCategory.map(function (data, i) {
                DB.create('TemplateCategory', {
                  templateCode: data.TemplateCode,
                  auditType:  data.AuditTypeId,
                  templateStatus: data.TemplateStatus,
                  releasedDate: new Date(data.ReleaseDate),
                  categoryID: data.CategoryId,
                  categoryDesc: data.CategoryDescription,
                  evidenceID: data.EvidenceId,
                  evidenceType: data.EvidenceType,
                  categoryOrder:data.CategoryOrder
                },
                  true);
              })
            })
          } catch (error) {
            DB.write(() => {
              res.data.ObjTemplateCategory.map(function (data, i) {
                DB.create('TemplateCategory', {
                 templateCode: data.TemplateCode,
                  auditType:  data.AuditTypeId,
                  templateStatus: data.TemplateStatus,
                  releasedDate: new Date(data.ReleaseDate),
                  categoryID: data.CategoryId,
                  categoryDesc: data.CategoryDescription,
                  evidenceID: data.EvidenceId,
                  evidenceType: data.EvidenceType,
                  categoryOrder:data.CategoryOrder
                },
                  true);
              })
            })
          }

          // res.data.ObjTemplateQuestion.map(function (data, i) {
          //   try {
          //     DB.write(() => {
          //       DB.create('TemplateQuestion', {
          //         templateCode: data.TemplateCode,
          //         categoryID: data.CategoryId,
          //         questionFill: data.QuestionFill,
          //         questionOrder: data.QuestionOrder,
          //         weight: data.Weight,
          //         isRequired: data.IsRequired,
          //         templateQuestionID: data.TemplateQuestionId
          //       },
          //         true);
          //     })
          //   } catch (error) {
          //     DB.write(() => {
          //       DB.create('TemplateQuestion', {
          //         templateCode: data.TemplateCode,
          //         categoryID: data.CategoryId,
          //         questionFill: data.QuestionFill,
          //         questionOrder: data.QuestionOrder,
          //         weight: data.Weight,
          //         isRequired: data.IsRequired,
          //         templateQuestionID: data.TemplateQuestionId
          //       }, true);
          //     })
          //     ErrorReport.log(error);
          //   }
          // })

          //Cek database
          console.log(Queries.getPersistentData('email'))
          var result_filter = getInstance().objects('MsUser').filtered('Email ==[c] "' + Queries.getPersistentData('email') + '"');

          Queries.setAllUserNotLogin();
          getInstance().write(() => {
            getInstance().create('MsUser', { UserID: result_filter[0].UserID, isLogin: 1 }, true);
          });

          // this.setState({ loaderStatus: false, loadingInfo: 'Please Wait' })
          resolve()

        })
        .catch(err => {
          reject(new Error(err))
        })
    })
    // this.setState({ loaderStatus: true })
  }

  async InsertToTablePlant() {
    console.log('InsertToTablePlant')
    DB.write(() => {
      getInstance().delete(getInstance().objects('MsPlant'));
    })

    this.state.downloadedMasterData.ObjPlant.map(function (data, i) {
      console.log("PLANT")
      try {
        DB.write(() => {
          DB.create('MsPlant', { PlantID: data.PlantID, PlantDesc: data.PlantDesc, active: data.Active });
        })

      } catch (error) {
        DB.write(() => {
          DB.create('MsPlant', { PlantID: data.PlantID, PlantDesc: data.PlantDesc, active: data.Active }, true);
        })

        ErrorReport.log(error);
      }
    })

    setTimeout(() => {
      this.setState({
        insertCount: this.state.insertCount + 1
      })
    }, 200)
  }

  async InsertToTableGroupLine(value) {
    console.log('InsertToTableGroupLine')
    DB.write(() => {
      getInstance().delete(getInstance().objects('MsGroupLine'));
    })

    this.state.downloadedMasterData.ObjGroupLine.map(function (data, i) {
      console.log("GROUP LINE")
      //console.log(data)
      try {
        DB.write(() => {
          DB.create('MsGroupLine', { GroupLineID: data.GroupLineID, GroupLineIDDesc: data.GroupLineDesc, PlantID: data.PlantID, GroupLineIDAbb: data.GroupLineAbb, active: data.Active });
        })
      } catch (error) {
        DB.write(() => {
          DB.create('MsGroupLine', { GroupLineID: data.GroupLineID, GroupLineIDDesc: data.GroupLineDesc, PlantID: data.PlantID, GroupLineIDAbb: data.GroupLineAbb, active: data.Active }, true);
        })
        ErrorReport.log(error);
      }
    }.bind(this))

    setTimeout(() => {
      this.setState({
        insertCount: this.state.insertCount + 1
      })
    }, 200)
  }

  SettingOnPress(value) {
    if (value == 1) {
      this.setState({
        loaderStatus: true
      })

      this.getMasterDataAllBackup()
        .then((res) => {
          Alert.alert(
            'Information',
            'Master data success downloaded',
            [
              {
                text: 'OK', onPress: () => {
                  Queries.setPersistentData('masterdata', '1');
                  this.setState({
                    loaderStatus: false,
                    loadingInfo: 'Please Wait',
                    // downloadingMaster: false 
                    //modalLoadingProgress : false,
                  })
                }
              },
            ],
            { cancelable: false }
          )
        })
        .catch((err) => {
          console.log(err)
          Alert.alert(
            'Information',
            'Failed download master data',
            [
              {
                text: 'OK', onPress: () => this.setState({
                  loaderStatus: false,
                  loadingInfo: 'Please Wait',
                  // downloadingMaster: false 
                  //modalLoadingProgress : false,
                })
              },
            ],
            { cancelable: false }
          )
        })


    } else {
      this._doLogout()
    }

  }

  TopNavigation() {
    console.log('render top navigation')
    return (
      <View style={[styles.header, { paddingLeft: responsiveWidth(3), paddingRight: responsiveWidth(3) }]}>

        <Text style={[style.font.fontTitleLarge, { flex: 3, color: 'white' }]}>Profile</Text>
        <Menu onSelect={(value) => this.SettingOnPress(value)}>
          <MenuTrigger>
            <Image source={require('../Images/settings_white.png')} style={{ height: responsiveWidth(7), width: responsiveWidth(7) }}></Image>
          </MenuTrigger>
          <MenuOptions>
            <MenuOption value={1}>
              <Text style={style.font.fontContentNormal}>Synchronize</Text>
            </MenuOption>
            <MenuOption value={2}>
              <Text style={style.font.fontContentNormal}>Logout</Text>
            </MenuOption>
          </MenuOptions>
        </Menu>

      </View >
    )
  }

  render() {
    return (
      <Container>
        <Spinner visible={this.state.loaderStatus} textContent={this.state.downloadingMaster ? this.state.insertCount + ' of 8' : this.state.loadingInfo} textStyle={{ color: '#fff' }} cancelable={false} />
        <StatusBar
          backgroundColor='#c00'
          barStyle="light-content"
        />

        {this.marginTopForIOS()}
        <MenuContext style={{ flex: 1 }}>
          <View style={[styles.container]}>
            {this.state.showDownloadingModal == true ?
              <Modal
                style={[styles.modal, styles.modal1]}
                backdrop={false}
                ref={(c) => { this._modal = c; }}
                swipeToClose={false}
              >
                <View
                  style={{ flex: 1, alignSelf: 'stretch', justifyContent: 'center', padding: 20 }}
                >
                  {this.state.showInstalling ?
                    <Text
                      style={ [style.font.fontContentLarge, {
                        color: '#5067FF',
                        textAlign: 'center'
                      }]}
                    >
                      Installing update...
                  </Text> :
                    <View
                      style={{
                        flex: 1,
                        alignSelf: 'stretch',
                        justifyContent: 'center',
                        padding: 20,
                      }}
                    >
                      <Text
                        style={[style.font.fontContentLarge, {
                          color: '#5067FF',
                          textAlign: 'center'
                        }]}
                      >
                        Downloading update... {`${parseInt(this.state.downloadProgress, 10)} %`}
                      </Text>
                    </View>
                  }
                </View>
              </Modal> : null
            }
            {this.state.modalLoadingProgress == true ?
              <Modal
                transparent={true}
                animationType="fade"
                visible={this.state.modalLoadingProgress}
                onRequestClose={() => this.setState({ modalLoadingProgress: false })}>
                <View
                  behavior='padding'
                  style={{
                    flex: 1,
                    backgroundColor: 'rgba(0, 0, 0, 0.5)',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}>
                  <Text style={[style.font.fontContentLarge, { color: 'white' }]}>{this.state.insertCount} of {this.state.totalInsertCount} </Text>
                  <TextInput ref={component => this._valueProgress = component} style={[style.font.fontContentLarge, { color: 'white' }]} value={''} />
                </View>
              </Modal> : null}

            {this.state.modalScheduleAuditPop == true ?
              <Modal
                transparent={true}
                animationType="fade"
                visible={this.state.modalScheduleAuditPop}
                onRequestClose={() => this.setState({ modalScheduleAuditPop: false })}>
                <View
                  behavior='padding'
                  style={{
                    flex: 1,
                    backgroundColor: 'white'
                  }}>
                  {/*<ScheduleAuditInc PK_AuditScheduleDetailID={this.state.selectedAuditDetailID} CloseModal={this.closeModal.bind(this)} />*/}
                  <ScheduleAuditInc PK_AuditScheduleDetailID={this.state.selectedAuditDetailID} CloseModal={this.closeModal.bind(this)} />
                </View>
              </Modal> : null}

            {this.state.modalCreateSchedulePop == true ?
              <Modal
                transparent={true}
                animationType="fade"
                visible={this.state.modalCreateSchedulePop}
                onRequestClose={() => this.setState({ modalCreateSchedulePop: false })}>
                <View
                  behavior='padding'
                  style={{
                    flex: 1,
                    backgroundColor: 'white'
                  }}>
                  <CreateScheduleInc
                    _changeParam={this._changeCreateScheduleParameter.bind(this)}
                    onRef={ref => (this.createSchedule = ref)}
                    CloseModal={this.closeModal.bind(this)}
                  />
                </View>
              </Modal> : null}

            {this.state.modalCreateNonSchedulePop == true ?

              <Modal
                transparent={true}
                animationType="fade"
                visible={this.state.modalCreateNonSchedulePop}
                onRequestClose={() => this.setState({ modalCreateNonSchedulePop: false })}>
                <View
                  behavior='padding'
                  style={{
                    flex: 1,
                    backgroundColor: 'white'
                  }}>
                  <CreateNonScheduleInc CloseModal={this.closeModal.bind(this)} />
                </View>
              </Modal> : null}

            {this.state.modalPlusVisible == true ?
              <Modal
                transparent={true}
                animationType="fade"
                visible={this.state.modalPlusVisible}
                onRequestClose={() => this.setState({ modalPlusVisible: false })}>
                <TouchableWithoutFeedback onPress={() => this.setState({ modalPlusVisible: false })}>
                  <View
                    behavior='padding'
                    style={{
                      flex: 1,
                      backgroundColor: 'transparent'
                    }}>
                    <View
                      style={{
                        height: Metrics.screenHeight,
                        backgroundColor: 'rgba(0,0,0,0.5)'
                      }}
                    >
                      <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                          {this.modalPlus()}
                        </View>
                      </View>
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              </Modal> : null}

            {this.state.modalUserVisible == true ?

              <Modal
                transparent={true}
                animationType="fade"
                visible={this.state.modalUserVisible}
                onRequestClose={() => this.setState({ modalUserVisible: false })}>
                <TouchableWithoutFeedback onPress={() => this.setState({ modalUserVisible: false })}>
                  <View
                    behavior='padding'
                    style={{
                      flex: 1,
                      justifyContent: 'center',
                      backgroundColor: 'rgba(0, 0, 0, 0.5)',

                      alignItems: 'center'
                    }}>
                    <TouchableWithoutFeedback>
                      <View
                        style={{
                          borderRadius: 10,
                          backgroundColor: '#fff',
                          height: Metrics.modalUserHeight,
                          width: Metrics.modalUserWidth,
                        }} >

                        <View style={{ flex: 1, justifyContent: 'space-between' }}>
                          <View style={{ alignItems: 'center' }}>
                            <Text style={{ fontFamily: 'MyriadPro-Regular', fontWeight: 'bold', fontSize: responsiveFontSize(3), marginTop: responsiveHeight(2) }}> My Profile </Text>
                            <Image source={Images.userprofile} style={{ height: responsiveHeight(20), width: responsiveHeight(20), marginTop: responsiveHeight(5) }} />
                            <View style={{ flexDirection: 'row', marginTop: responsiveHeight(5) }}>
                              <View>
                                <Text style={{ fontFamily: 'MyriadPro-Regular', marginBottom: 10, fontSize: responsiveFontSize(2.5) }}>Name</Text>
                                <Text style={{ fontFamily: 'MyriadPro-Regular', marginBottom: 10, fontSize: responsiveFontSize(2.5) }}>Position</Text>
                                <Text style={{ fontFamily: 'MyriadPro-Regular', marginBottom: 10, fontSize: responsiveFontSize(2.5) }}>Plant</Text>
                              </View>
                              <View>
                                <Text style={{ fontFamily: 'MyriadPro-Regular', marginBottom: 10, fontSize: responsiveFontSize(2.5) }}> : {this.state.fullName}</Text>
                                <Text style={{ fontFamily: 'MyriadPro-Regular', marginBottom: 10, fontSize: responsiveFontSize(2.5) }}> : {this.state.userPosition}</Text>
                                <Text style={{ fontFamily: 'MyriadPro-Regular', marginBottom: 10, fontSize: responsiveFontSize(2.5) }}> : {this.state.userPlant}</Text>
                              </View>
                            </View>
                          </View>
                          <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                            <TouchableOpacity style={{ backgroundColor: '#cecece', alignItems: 'center', justifyContent: 'center', marginBottom: responsiveHeight(2), height: responsiveHeight(7), width: Metrics.modalUserWidth - responsiveWidth(20), borderRadius: responsiveHeight(1) }}
                              onPress={() => this._doLogout.bind(this)}>
                              <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
                                <Image source={require('../Images/New/setting.png')} style={{ width: responsiveHeight(5), height: responsiveHeight(5), marginRight: 10 }} />
                                <Text style={{ fontFamily: 'MyriadPro-Regular', fontSize: newResponsiveFontSize(1.5), textAlign: 'center' }}>Log Out</Text>
                              </View>
                            </TouchableOpacity>
                          </View>
                        </View>
                      </View>
                    </TouchableWithoutFeedback>
                  </View>
                </TouchableWithoutFeedback>
              </Modal> : null}


            {
              // <View style={styles.header}>
              //   <Text style={{ fontFamily: 'MyriadPro-Regular', flex: 2, color: 'white', fontSize: 25, textAlign: 'center' }}>Audit Chamber</Text>
              // </View>
            }

            {
              // <View style={styles.header}>
              //   <Image source={require('../Images/cancel-music.png')} style={{resizeMode: 'contain', flex: 1, height: responsiveHeight(4), marginLeft: responsiveWidth(2) }}></Image>
              //   <Image source={require('../Images/quinsy-detective-white.png')} style={{resizeMode: 'contain', flex: 1, height: responsiveHeight(7), width: responsiveHeight(10) }}></Image>
              //   <Text style={{ fontFamily: 'MyriadPro-Regular', flex: 5, color: 'white', fontSize: 25, textAlign: 'right', marginRight:responsiveWidth(2) }}>Create Schedule Audit</Text>
              // </View>
            }

            {
              // <View style={styles.header}>
              //   <Image source={require('../Images/cancel-music.png')} style={{resizeMode: 'contain', flex: 1, height: responsiveHeight(4), marginLeft: responsiveWidth(2) }}></Image>
              //   <Image source={require('../Images/quinsy-detective-white.png')} style={{resizeMode: 'contain', flex: 1, height: responsiveHeight(7), width: responsiveHeight(10) }}></Image>
              //   <Text style={{ fontFamily: 'MyriadPro-Regular', flex: 5, color: 'white', fontSize: 25, textAlign: 'right', marginRight:responsiveWidth(2) }}>FINISH</Text>
              // </View>
            }

            {
              this.state.currentMenu == 'notification' ? this.TopNavigation()
                /* <View style={[styles.header, { paddingLeft: responsiveWidth(3), paddingRight: responsiveWidth(3) }]}>

                  <Text style={[style.font.fontTitleLarge, { flex: 3, color: 'white' }]}>Profile</Text>
                  <TouchableOpacity onPress={() => this._doLogout()}>
                    <Image source={require('../Images/settings_white.png')} style={{ resizeMode: 'contain', flex: 1, height: responsiveWidth(7), width: responsiveWidth(7) }}></Image>
                  </TouchableOpacity>
                </View>:
              this.state.currentMenu == 'createauditschedule' && this.state.value_additional_param == 'preview' ?
                <View style={styles.header}>
                  <TouchableOpacity style={{ flex: 1, alignItems: 'center' }} onPress={() => this.createSchedule._backToCreate()}>
                    <Image source={require('../Images/cancel-music.png')} style={{ resizeMode: 'contain', height: responsiveHeight(4), marginLeft: responsiveWidth(4) }}></Image>
                  </TouchableOpacity>
                  <Image source={require('../Images/quinsy-detective-white.png')} style={{ resizeMode: 'contain', flex: 1, height: responsiveHeight(7), width: responsiveHeight(10) }}></Image>
                  <TouchableOpacity style={{ flex: 5, padding: responsiveWidth(2) }} onPress={() => this.createSchedule._onCreateSchedule()}>
                    <Text style={{ fontFamily: 'MyriadPro-Regular', flex: 5, color: 'white', fontSize: newResponsiveFontSize(2), textAlign: 'right', marginRight: responsiveWidth(2) }}>Finish</Text>
                  </TouchableOpacity>
                </View> :
                this.state.currentMenu == 'createauditschedule' && this.state.value_additional_param == 'finish' ?
                  <View style={styles.header}>
                  </View> :
                  this.state.currentMenu == 'createauditschedule' ?
                    <View style={styles.header}>
                      <Image source={require('../Images/cancel-music.png')} style={{ resizeMode: 'contain', flex: 1, height: responsiveHeight(4), marginLeft: responsiveWidth(2) }}></Image>
                      <Image source={require('../Images/quinsy-detective-white.png')} style={{ resizeMode: 'contain', flex: 1, height: responsiveHeight(7), width: responsiveHeight(10) }}></Image>
                      <TouchableOpacity style={{ flex: 5, padding: responsiveWidth(2) }} onPress={() => this.createSchedule._onPreviewSchedule()}>
                        <Text style={{ fontFamily: 'MyriadPro-Regular', flex: 5, color: 'white', fontSize: newResponsiveFontSize(2), textAlign: 'right' }}>Create Schedule Audit</Text>
                      </TouchableOpacity>
                    </View> */:
                <View style={[styles.header, { justifyContent: 'space-between', paddingLeft: responsiveWidth(2), paddingRight: responsiveWidth(2) }]}>
                  <Image source={require('../Images/Quinsys-logo-putih.png')} style={{ height: responsiveHeight(7), width: responsiveWidth(25) }}></Image>
                  <Text style={[style.font.fontTitleLarge, { color: 'white', textAlign: 'center' }]}>{Constants.Environment == "DEV" ? "AuditChamber Dev" : "Audit Chamber" }</Text>
                </View>
            }


            <View style={[styles.mainContainer]}>
              {this.state.isMasterDataDownloaded == '0' ? null :
                <Swiper loop={false} index={this.state.pageIndex} onRef={ref => (this.child = ref)} showsButtons={false} showsPagination={false} onIndexChanged={(index) => {
                  //Alert.alert(index.toString())
                  this.setState({
                    pageIndex: index
                  })
                  if (index == 0) {
                    this.setState({
                      currentMenu: "myaudit"
                    })
                    this.setState({
                      menuMyAuditOn: true,
                      menuNotificationOn: false,
                      menuResultOn: false,
                      menuScheduleOn: false
                    })
                  }
                  else if (index == 1) {
                    this.setState({
                      currentMenu: "result"
                    })
                    this.setState({
                      menuMyAuditOn: false,
                      menuNotificationOn: false,
                      menuResultOn: true,
                      menuScheduleOn: false
                    })
                  }
                  else if (index == 2) {
                    this.setState({
                      currentMenu: "schedule"
                    })
                    this.setState({
                      menuMyAuditOn: false,
                      menuNotificationOn: false,
                      menuResultOn: false,
                      menuScheduleOn: true
                    })
                  }
                  else if (index == 3) {
                    this.setState({
                      currentMenu: "notification"
                    })
                    this.setState({
                      menuMyAuditOn: false,
                      menuNotificationOn: true,
                      menuResultOn: false,
                      menuScheduleOn: false
                    })
                  }
                }}>
                  <View>
                    <View style={[styles.mainContainer]}>
                      {this.SwipeViewAudit()}
                    </View>
                  </View>
                  <View>
                    <View style={[styles.mainContainer]}>
                      {this.SwipeViewResult()}
                    </View>
                  </View>
                  <View>
                    <View style={[styles.mainContainer]}>
                      {this.SwipeViewSchedule()}
                    </View>
                  </View>
                  <View>
                    <View style={[styles.mainContainer]}>
                      {this.SwipeViewNotification()}
                    </View>
                  </View>
                </Swiper>
              }
            </View>


            <View style={{ flexDirection: 'row', height: responsiveHeight(10), justifyContent: 'space-between', backgroundColor: '#fff', borderTopColor: '#c3c3c3', borderTopWidth: 1, paddingBottom: responsiveHeight(1.5) }}>
              <TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.changePage(0)}>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={this.state.menuMyAuditOn === true ? Images.myauditon : Images.myauditoff} style={styles.footerIcon}></Image>
                  {/*<Text style={{ fontSize: 10, width: Metrics.footerIconWidth + responsiveWidth(6), textAlign: 'center' }}>My Audits</Text>*/}
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.changePage(1)}>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={this.state.menuResultOn === true ? Images.resulton : Images.resultoff} style={styles.footerIcon}></Image>
                  {/*<Text style={{ fontSize: 10, width: Metrics.footerIconWidth + responsiveWidth(6), textAlign: 'center' }}>Results</Text>*/}
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <View style={{ alignItems: 'center' }}>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.changePage(2)}>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={this.state.menuScheduleOn === true ? Images.scheduleon : Images.scheduleoff} style={styles.footerIcon}></Image>
                  {/*<Text style={{ fontSize: 10, width: Metrics.footerIconWidth + responsiveWidth(6), textAlign: 'center' }}>Schedule</Text>*/}
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.changePage(3)}>
                <View style={{ alignItems: 'center' }}>
                  <Image source={this.state.menuNotificationOn === true ? Images.notificationon : Images.notificationoff} style={styles.footerIcon}></Image>
                  {/*<Text style={{ fontSize: responsiveFontSize(1.4), width: Metrics.footerIconWidth + responsiveWidth(6), }}>Notifications</Text>*/}
                </View>
                {
                  this.state.notification > 0 ?
                    (
                      <Badge style={{ position: 'absolute', right: responsiveWidth(2.5), top: responsiveHeight(1), backgroundColor: 'red', width: responsiveWidth(5), height: responsiveWidth(5), marginLeft: 5 }}>
                        <Text style={{ fontSize: responsiveFontSize(2), color: 'white', width: responsiveWidth(6), marginLeft: -responsiveWidth(2), textAlign: 'center', fontFamily: 'MyriadPro-Regular', backgroundColor: 'transparent' }}>
                          {this.state.notification}
                        </Text>
                      </Badge>
                    )
                    :
                    (
                      <View></View>
                    )
                }
              </TouchableOpacity>
            </View>
          </View>
          {/*<TouchableHighlight onPress={this.plusOnPress} style={styles.addButton}>
          <Image source={Images.plus} style={{ height: Metrics.footerIconPlusHeight, width: Metrics.footerIconPlusWidth }}></Image>
        </TouchableHighlight>*/}
          {this.floatingactionbutton()}
        </MenuContext>
      </Container>
    )
  }
}


var styles = StyleSheet.create({
  container: {
    marginTop: Metrics.marginTopContainer,
    backgroundColor: "#fff",
  },
  header: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: '#c00',
    height: Metrics.marginHeader
  },
  footer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#cecece',
    height: responsiveHeight(11),
    borderColor: '#c6c6c6',
    borderWidth: 1,
  },
  mainContainer: {
    height: Metrics.screenHeight - Metrics.marginTopContainer - Metrics.footerHeight - Metrics.marginHeader,
    // height : 560.29,\
  },
  footerText: {
    fontSize: 5,
    textAlign: 'center',
  },
  footerIcon: {
    width: Metrics.footerIconWidth,
    height: Metrics.footerIconHeight,
  },
  footerIconContainer: {
    width: Metrics.footerIconWidth + responsiveHeight(2),
    height: Metrics.footerIconHeight + responsiveHeight(2),
    borderColor: 'red',
    borderWidth: 1
  },

  addButton: {
    backgroundColor: '#fff',
    borderColor: '#fff',
    borderWidth: 2,
    height: Metrics.footerIconPlusHeight,
    width: Metrics.footerIconPlusHeight,
    borderRadius: Metrics.footerIconPlusHeight / 2,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: responsiveHeight(1),
    right: (Metrics.screenWidth / 2) - (Metrics.footerIconPlusHeight / 2)
  },
  modalPlusText: {
    fontSize: responsiveFontSize(2),
    width: responsiveWidth(70),
    backgroundColor: 'white',
    height: responsiveHeight(6),
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    borderColor: '#cecece',
    borderWidth: 1,
    paddingTop: responsiveHeight(1.5),
    fontFamily: 'MyriadPro-Regular'
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },

});