import React, { Component } from 'react';
import { Container, Content, Body, ListItem, Text, CheckBox, Button, Segment, Icon, Separator, Spinner } from 'native-base';
import { Image, View, ScrollView, Dimensions, Alert, TouchableOpacity } from 'react-native';
var dateFormat = require('dateformat');
import Moment from 'moment';
import { responsiveFontSize, responsiveHeight, responsiveWidth, newResponsiveFontSize } from '../Themes/Responsive'
//import DbAuditChamber from '../Realm/DbAuditChamber';
import { Actions } from 'react-native-router-flux';
import { getInstance } from '../Data/DB/DBHelper'
import axios from 'axios';
import DB from '../Data/DB/Db';
import Queries from '../Data/Queries'
import Constants from '../Data/Constants'
import SpinnerFull from 'react-native-loading-spinner-overlay';
import style from '../Themes/Fonts';

const { height, width } = Dimensions.get('window')

var arr = [];
var tempTime = "";
export default class MyAudit extends Component {

    constructor(props) {
        super(props);

        this.users = Queries.getCurrentUser();
        this.AuditScheduleHeader = getInstance().objects('AuditScheduleHeader');
        this.AuditScheduleDetail = getInstance().objects('AuditScheduleDetail');
        this.MsGroupLine = getInstance().objects('MsGroupLine');
        this.MsUser = getInstance().objects('MsUser');
        var rowAudit, rowAuditSelected;
        this.flagDelete = 0;
        this.state = {
            tipe: '1',
            backColor: "white",
            textColor: "grey",
            listAudit: <Spinner />,
            listAuditSelected: <Spinner />,
            listAuditSelectedCompleted: <View />,
            tempStatus: 0,
            loaderStatus: false
        };
    }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }

    _onPressButton(param, e) {

        // console.log("masuk onpress Inprogress")
        // console.log(param)
        this.setState({
            loaderStatus: true
        })

        requestAnimationFrame(() => {
            var tempParam;
            if (param.auditType === 'GMP1') {
                tempParam = 2
            }
            else {
                tempParam = 3
            }
            //console.log(param);
            //Actions.QuestionScreen({auditScheduleDetailID : 1, auditType : 'GMP', templateCode : 1, groupLineID: 4});
            Actions.QuestionScreen({
                plantid: param.plantid,
                grouplinedesc: param.grouplinedesc,
                week: param.week,
                year: param.year,
                auditScheduleDetailID: param.auditScheduleDetailID,
                auditType: tempParam,
                templateCode: param.templateCode,
                groupLineID: param.groupLineID,
                isComplete: false,
                scheduleCode: param.scheduleCode,
                isOverdue: param.isOverdue
            })

            this.setState({
                loaderStatus: false
            })
        })


    }

    _onPressButtonCompleted(param, e) {

        this.setState({
            loaderStatus: true
        })

        requestAnimationFrame(() => {
            var tempParam;
            if (param.auditType === 'GMP1') {
                tempParam = 2
            }
            else {
                tempParam = 3
            }
            // console.log(JSON.stringify({
            //     plantid : param.plantid,
            //     grouplinedesc : param.grouplinedesc,
            //     week : param.week,
            //     year : param.year,
            //     auditScheduleDetailID : param.auditScheduleDetailID,
            //     auditType : tempParam,
            //     templateCode : param.templateCode,
            //     groupLineID : param.groupLineID,
            //     isComplete : true,
            //     scheduleCode : param.scheduleCode
            // }));
            //Actions.QuestionScreen({auditScheduleDetailID : 1, auditType : 'GMP', templateCode : 1, groupLineID: 4});
            Actions.QuestionScreen({
                plantid: param.plantid,
                grouplinedesc: param.grouplinedesc,
                week: param.week,
                year: param.year,
                auditScheduleDetailID: param.auditScheduleDetailID,
                auditType: tempParam,
                templateCode: param.templateCode,
                groupLineID: param.groupLineID,
                isComplete: true,
                scheduleCode: param.scheduleCode,
                isOverdue: param.isOverdue

            })

            this.setState({
                loaderStatus: false
            })
        })
    }

    componentWillMount() {
        //console.log("widthnya ni " + width)
        console.log("token = " + Queries.getPersistentData('token'))

        var statusExpire = false;

        axios
            .get(Constants.APILink + `Schedule/GetAllAuditScheduleDetailByScheduleStatus?IntScheduleStatus=1`, {
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + Queries.getPersistentData('token'),
                },
                timeout: Constants.Timeout
            })
            .then(res => {
                //console.log("berhasil")
                //if(res.data.length > 0){

                if (this.flagDelete === 0) {
                    console.log("delete 1")
                    DB.write(() => {
                        getInstance().delete(getInstance().objects('AuditScheduleHeader'));
                        getInstance().delete(getInstance().objects('AuditScheduleDetail'));
                    })
                    this.flagDelete = 1;
                }

                res.data.map(function (data, index) {
                    console.log("masuk 1")
                    //console.log(data)
                    DB.write(() => {
                        console.log("masuk 2")
                        DB.create('AuditScheduleHeader', { ScheduleCode: data.AuditScheduleCode, PlantID: data.PlantId, PlantDesc: data.PlantDesc, PeriodStart: data.PeriodStart, PeriodEnd: data.PeriodEnd, WeekToDateStart: data.WeekToDateStart, WeekToDateEnd: data.WeekToDateEnd, YearStart: data.YearStart, YearEnd: data.YearEnd, AuditType: data.AuditType, TemplateCode: data.TemplateCode }, true);
                    });


                    DB.write(() => {
                        console.log("masuk 3")
                        data.ObjTrAuditScheduleDetail.map(function (data, index) {
                            // console.log(data)
                            DB.create('AuditScheduleDetail', { PK_AuditScheduleDetailID: parseInt(data.AuditScheduleDetailId), ScheduleCode: data.AuditScheduleCode, AuditorID: data.AuditorId, GroupLineID: data.GroupLineId, CompleteTask: data.CompletedTask, TotalTask: data.TotalTask, AuditeeSignStatus: data.AuditeeSignStatus, LastUpdateDate: data.LastUpdateDate, ScheduleStatus: data.ScheduleStatus, StartDateTime: data.StartAuditDate, EndDateTime: data.EndAuditDate, TotalFindings: data.TotalFindings }, true);
                        })
                    });
                })

                tempTime = '';

                var dataAuditScheduleDetail = []

                getInstance().objects('AuditScheduleDetail').filtered('AuditorID = "' + this.users[0].Username + '"').map(function (data, i) {

                    //Check overdue
                    var tempAuditScheduleHeader = getInstance().objects('AuditScheduleHeader').filtered('ScheduleCode = "' + data.ScheduleCode + '"');
                    if (new Date(dateFormat(Queries.stringToDateFormat(tempAuditScheduleHeader[0].PeriodEnd), "yyyy/mm/dd")) < new Date(dateFormat(new Date(), "yyyy/mm/dd")) && data.ScheduleStatus == '1') {
                        //data.ScheduleStatus = '2';
                        let db = getInstance();
                        db.write(() => {
                            db.create('AuditScheduleDetail', {
                                PK_AuditScheduleDetailID: data.PK_AuditScheduleDetailID,
                                ScheduleStatus: '2'
                            }, true);
                        });
                    }
                    else {
                        dataAuditScheduleDetail.push(data)
                    }

                })

                dataAuditScheduleDetail = this._funcSortingDate(dataAuditScheduleDetail, 'desc')

                rowAuditSelected = dataAuditScheduleDetail.map(function (data, i) {
                    if (data.ScheduleStatus === this.state.tipe) {
                        var tempAuditScheduleHeader = getInstance().objects('AuditScheduleHeader').filtered('ScheduleCode = "' + data.ScheduleCode + '"');
                        var tempMsGroupLine = this.MsGroupLine.filtered('GroupLineID = "' + data.GroupLineID + '"');
                        let isOverdue = false;

                        //Data untuk onpress
                        var arrDataOnPress = {
                            plantid: tempAuditScheduleHeader[0].PlantID,
                            grouplinedesc: tempMsGroupLine[0].GroupLineIDDesc,
                            week: tempAuditScheduleHeader[0].WeekToDateStart,
                            year: tempAuditScheduleHeader[0].YearStart,
                            auditScheduleDetailID: data.PK_AuditScheduleDetailID,
                            auditType: tempAuditScheduleHeader[0].AuditType,
                            templateCode: tempAuditScheduleHeader[0].TemplateCode,
                            groupLineID: data.GroupLineID,
                            scheduleCode: data.ScheduleCode,
                            isOverdue: isOverdue
                        }

                        const formattedDT = Queries.stringToDateFormat(data.LastUpdateDate);

                        var timeData = dateFormat(formattedDT, "mmm dd, yyyy");
                        var tempImage;
                        if (this.state.tipe === '1') {
                            tempImage = <Image style={{ resizeMode: 'cover', width: responsiveWidth(15), height: responsiveWidth(15) }} source={require('../Images/MyGMPProgress.png')} />
                        }
                        else {
                            tempImage = <Image style={{ resizeMode: 'cover', width: responsiveWidth(15), height: responsiveWidth(15) }} source={require('../Images/MyGMPCompleted.png')} />
                        }

                        if (tempTime !== timeData) {
                            tempTime = timeData;
                            return (
                                <View style={{ backgroundColor: 'white', paddingLeft:responsiveWidth(1), paddingRight:responsiveWidth(1) }} key={"rowAuditSelected" + i}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderBottomColor:'black', borderBottomWidth:1}}>
                                        <Text style={[style.font.fontTitleNormal, { color: 'black' }]}>{timeData}</Text>
                                        <Text style={[style.font.fontTitleNormal, { color: '#696969', textAlign: 'right', flex: 1 }]}>My Audits</Text>
                                    </View>
                                    <TouchableOpacity style={{  flexDirection:'row', padding:responsiveWidth(1), paddingBottom:responsiveHeight(1), paddingTop:responsiveHeight(1), paddingLeft:responsiveWidth(2), paddingRight:responsiveWidth(2)}} onPress={data.ScheduleStatus === '1' ? this._onPressButton.bind(this, arrDataOnPress) : this._onPressButtonCompleted.bind(this, arrDataOnPress)}>
                                        {tempImage}
                                        <View style={{ flex:1}} >
                                            <Text style={ [style.font.fontContentLarge, {color:'black', fontWeight:'bold'}] }>{tempAuditScheduleHeader[0].AuditType + " " + tempMsGroupLine[0].GroupLineIDDesc + " - Week " + tempAuditScheduleHeader[0].WeekToDateStart + " " + tempAuditScheduleHeader[0].YearStart}</Text>
                                            <View style={{ flexDirection: 'row', justifyContent:'space-between' }}>
                                                <Text style={ [ style.font.fontTitleLarge, {flex: 1, color: '#FC6A03', fontWeight: 'bold'}]}>{data.CompleteTask + '/' + data.TotalTask}</Text>
                                                <View>
                                                    <Text style={ [style.font.fontContentNormal, { color: '#696969',textAlign:'right'}]}>Last Updated :</Text>
                                                    <Text style={ [style.font.fontContentNormal, { color: '#696969',textAlign:'right'}]}>{dateFormat(formattedDT, "dd mmmm yyyy HH:MM")}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            );
                        }
                        else {
                            return (
                                <View style={{ backgroundColor: 'white' }} key={"rowAuditSelected" + i}>
                                    <TouchableOpacity style={{  flexDirection:'row', padding:responsiveWidth(1), paddingBottom:responsiveHeight(1), paddingTop:responsiveHeight(1), paddingLeft:responsiveWidth(2), paddingRight:responsiveWidth(2)}} onPress={data.ScheduleStatus === '1' ? this._onPressButton.bind(this, arrDataOnPress) : this._onPressButtonCompleted.bind(this, arrDataOnPress)}>
                                        {tempImage}
                                        <View style={{ flex:1}} >
                                            <Text style={ [style.font.fontContentLarge, {color:'black', fontWeight:'bold'}] }>{tempAuditScheduleHeader[0].AuditType + " " + tempMsGroupLine[0].GroupLineIDDesc + " - Week " + tempAuditScheduleHeader[0].WeekToDateStart + " " + tempAuditScheduleHeader[0].YearStart}</Text>
                                            <View style={{ flexDirection: 'row', justifyContent:'space-between' }}>
                                                <Text style={ [ style.font.fontTitleLarge, {flex: 1, color: '#FC6A03', fontWeight: 'bold'}]}>{data.CompleteTask + '/' + data.TotalTask}</Text>
                                                <View>
                                                    <Text style={ [style.font.fontContentNormal, { color: '#696969',textAlign:'right'}]}>Last Updated :</Text>
                                                    <Text style={ [style.font.fontContentNormal, { color: '#696969',textAlign:'right'}]}>{dateFormat(formattedDT, "dd mmmm yyyy HH:MM")}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            );
                        }
                    }
                }.bind(this));

                this.setState({
                    //listAudit: rowAudit,
                    listAuditSelected: rowAuditSelected,
                    listAuditSelectedCompleted: <Spinner />,
                    tempStatus: 1
                })

                requestAnimationFrame(() => {
                    console.log(Constants.APILink + `Schedule/GetAllAuditScheduleDetailByScheduleStatus?IntScheduleStatus=2 apuytoken`)
                    axios
                        //.get(`http://ccaaudit.azurewebsites.net/api/Schedule/GetAllAuditScheduleDetail`, {
                        .get(Constants.APILink + `Schedule/GetAllAuditScheduleDetailByScheduleStatus?IntScheduleStatus=2`, {
                            headers: {
                                "Accept": "application/json",
                                "Content-Type": "application/json",
                                "Authorization": "Bearer " + Queries.getPersistentData('token'),
                            }
                        })
                        .then(res => {
                            //console.log("berhasil")
                            //if(res.data.length > 0){
                            if (this.flagDelete === 0) {
                                //console.log("delete 2")
                                DB.write(() => {
                                    getInstance().delete(getInstance().objects('AuditScheduleHeader'));
                                    getInstance().delete(getInstance().objects('AuditScheduleDetail'));
                                })
                                this.flagDelete = 1;
                            }

                            res.data.map(function (data, index) {
                                //console.log(data)
                                DB.write(() => {
                                    DB.create('AuditScheduleHeader', { ScheduleCode: data.AuditScheduleCode, PlantID: data.PlantId, PlantDesc: data.PlantDesc, PeriodStart: data.PeriodStart, PeriodEnd: data.PeriodEnd, WeekToDateStart: data.WeekToDateStart, WeekToDateEnd: data.WeekToDateEnd, YearStart: data.YearStart, YearEnd: data.YearEnd, AuditType: data.AuditType, TemplateCode: data.TemplateCode }, true);
                                });


                                DB.write(() => {
                                    data.ObjTrAuditScheduleDetail.map(function (data, index) {
                                        //console.log(data)
                                        DB.create('AuditScheduleDetail', { PK_AuditScheduleDetailID: parseInt(data.AuditScheduleDetailId), ScheduleCode: data.AuditScheduleCode, AuditorID: data.AuditorId, GroupLineID: data.GroupLineId, CompleteTask: data.CompletedTask, TotalTask: data.TotalTask, AuditeeSignStatus: data.AuditeeSignStatus, LastUpdateDate: data.LastUpdateDate, ScheduleStatus: data.ScheduleStatus, StartDateTime: data.StartAuditDate, EndDateTime: data.EndAuditDate, TotalFindings: data.TotalFindings }, true);
                                    })
                                });
                            })
                            //}

                            console.log(Constants.APILink + `Schedule/GetAllAuditScheduleDetailByScheduleStatus?IntScheduleStatus=3 apuytoken`)
                            axios
                                //.get(`http://ccaaudit.azurewebsites.net/api/Schedule/GetAllAuditScheduleDetail`, {
                                .get(Constants.APILink + `Schedule/GetAllAuditScheduleDetailByScheduleStatus?IntScheduleStatus=3`, {
                                    headers: {
                                        "Accept": "application/json",
                                        "Content-Type": "application/json",
                                        "Authorization": "Bearer " + Queries.getPersistentData('token'),
                                    }
                                })
                                .then(res => {
                                    //console.log("berhasil")
                                    //if(res.data.length > 0){
                                    if (this.flagDelete === 0) {
                                        //console.log("delete 2")
                                        DB.write(() => {
                                            getInstance().delete(getInstance().objects('AuditScheduleHeader'));
                                            getInstance().delete(getInstance().objects('AuditScheduleDetail'));
                                        })
                                        this.flagDelete = 1;
                                    }

                                    res.data.map(function (data, index) {
                                        //console.log(data)
                                        DB.write(() => {
                                            DB.create('AuditScheduleHeader', { ScheduleCode: data.AuditScheduleCode, PlantID: data.PlantId, PlantDesc: data.PlantDesc, PeriodStart: data.PeriodStart, PeriodEnd: data.PeriodEnd, WeekToDateStart: data.WeekToDateStart, WeekToDateEnd: data.WeekToDateEnd, YearStart: data.YearStart, YearEnd: data.YearEnd, AuditType: data.AuditType, TemplateCode: data.TemplateCode }, true);
                                        });


                                        DB.write(() => {
                                            data.ObjTrAuditScheduleDetail.map(function (data, index) {
                                                //console.log(data)
                                                DB.create('AuditScheduleDetail', { PK_AuditScheduleDetailID: parseInt(data.AuditScheduleDetailId), ScheduleCode: data.AuditScheduleCode, AuditorID: data.AuditorId, GroupLineID: data.GroupLineId, CompleteTask: data.CompletedTask, TotalTask: data.TotalTask, AuditeeSignStatus: data.AuditeeSignStatus, LastUpdateDate: data.LastUpdateDate, ScheduleStatus: data.ScheduleStatus, StartDateTime: data.StartAuditDate, EndDateTime: data.EndAuditDate, TotalFindings: data.TotalFindings }, true);
                                            })
                                        });

                                        // console.log("sebelum panggil")
                                        // this._changeType.bind(this,1)

                                    })
                                    console.log("sebelum panggil2")
                                    this._changeType(2)
                                })
                                .catch((error) => {
                                    console.log("Information - Myaudit - GetAllAuditScheduleDetailByScheduleStatus")
                                    if (error && error.response && error.response.status == "401") {
                                        Queries.sessionHabis(error.response.status);
                                    }
                                });
                        })
                        .catch((error) => {
                            if (error && error.response && error.response.status == "401") {
                                Queries.sessionHabis(error.response.status);
                            }
                        });
                })


            })
            .catch((error) => {
                //console.log("Information - Myaudit - willmount - GetAllAuditScheduleDetailByScheduleStatus")
                console.log(error)
                Alert.alert("An error accured, please check your connetion.")
                if (error.response != undefined) {
                    if (error && error.response && error.response.status == "401") {
                        // console.log("Information - HomeScreen - GetNotification - 401")
                        Queries.sessionHabis(error.response.status);
                    }
                } else {
                    tempTime = '';

                    var dataAuditScheduleDetail = []

                    getInstance().objects('AuditScheduleDetail').filtered('AuditorID = "' + this.users[0].Username + '"').map(function (data, i) {

                        //Check overdue
                        var tempAuditScheduleHeader = getInstance().objects('AuditScheduleHeader').filtered('ScheduleCode = "' + data.ScheduleCode + '"');
                        if (new Date(dateFormat(Queries.stringToDateFormat(tempAuditScheduleHeader[0].PeriodEnd), "yyyy/mm/dd")) < new Date(dateFormat(new Date(), "yyyy/mm/dd")) && data.ScheduleStatus == '1') {
                            //data.ScheduleStatus = '2';
                            let db = getInstance();
                            db.write(() => {
                                db.create('AuditScheduleDetail', {
                                    PK_AuditScheduleDetailID: data.PK_AuditScheduleDetailID,
                                    ScheduleStatus: '2'
                                }, true);
                            });
                        }
                        else {
                            dataAuditScheduleDetail.push(data)
                        }

                    })

                    dataAuditScheduleDetail = this._funcSortingDate(dataAuditScheduleDetail, 'desc')

                    rowAuditSelected = dataAuditScheduleDetail.map(function (data, i) {
                        if (data.ScheduleStatus === this.state.tipe) {
                            var tempAuditScheduleHeader = getInstance().objects('AuditScheduleHeader').filtered('ScheduleCode = "' + data.ScheduleCode + '"');
                            var tempMsGroupLine = this.MsGroupLine.filtered('GroupLineID = "' + data.GroupLineID + '"');
                            let isOverdue = false;

                            // console.log("data header")
                            // console.log(tempAuditScheduleHeader)

                            //Data untuk onpress
                            var arrDataOnPress = {
                                plantid: tempAuditScheduleHeader[0].PlantID,
                                grouplinedesc: tempMsGroupLine[0].GroupLineIDDesc,
                                week: tempAuditScheduleHeader[0].WeekToDateStart,
                                year: tempAuditScheduleHeader[0].YearStart,
                                auditScheduleDetailID: data.PK_AuditScheduleDetailID,
                                auditType: tempAuditScheduleHeader[0].AuditType,
                                templateCode: tempAuditScheduleHeader[0].TemplateCode,
                                groupLineID: data.GroupLineID,
                                scheduleCode: data.ScheduleCode,
                                isOverdue: isOverdue
                            }


                            //const formattedDT = Moment(data.LastUpdateDate, Moment.ISO_8601).format();//Moment(dateTime).format('d MMM') //2 May
                            const formattedDT = Queries.stringToDateFormat(data.LastUpdateDate);

                            var timeData = dateFormat(formattedDT, "dd mmmm yyyy");
                            var tempImage;
                            if (this.state.tipe === '1') {
                                tempImage = <Image style={{ resizeMode: 'cover', width: responsiveWidth(15), height: responsiveWidth(15) }} source={require('../Images/MyGMPProgress.png')} />
                            }
                            else {
                                tempImage = <Image style={{ resizeMode: 'cover', width: responsiveWidth(15), height: responsiveWidth(15) }} source={require('../Images/MyGMPCompleted.png')} />
                            }

                            // console.log(tempTime)
                            // console.log(timeData)

                            if (tempTime !== timeData) {
                                tempTime = timeData;
                                return (
                                    <View style={{ backgroundColor: 'white' }} key={"rowAuditSelected" + i}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={[style.font.fontTitleNormal, { color: 'black' }]}>{timeData}</Text>
                                            <Text style={[style.font.fontTitleNormal, { color: '#696969', textAlign: 'right', flex: 1 }]}>My Audits</Text>
                                        </View>
                                        <TouchableOpacity style={{  flexDirection:'row', padding:responsiveWidth(1), paddingBottom:responsiveHeight(1), paddingTop:responsiveHeight(1), paddingLeft:responsiveWidth(2), paddingRight:responsiveWidth(2)}} onPress={data.ScheduleStatus === '1' ? this._onPressButton.bind(this, arrDataOnPress) : this._onPressButtonCompleted.bind(this, arrDataOnPress)}>
                                            {tempImage}
                                            <View style={{ flex:1}} >
                                            <Text style={ [style.font.fontContentLarge, {color:'black', fontWeight:'bold'}] }>{tempAuditScheduleHeader[0].AuditType + " " + tempMsGroupLine[0].GroupLineIDDesc + " - Week " + tempAuditScheduleHeader[0].WeekToDateStart + " " + tempAuditScheduleHeader[0].YearStart}</Text>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Text style={ [ style.font.fontTitleLarge, {flex: 1, color: '#FC6A03', fontWeight: 'bold'}]}>{data.CompleteTask + '/' + data.TotalTask}</Text>
                                                    <View>
                                                        <Text style={ [style.font.fontContentNormal, { color: '#696969',textAlign:'right'}]}>Last Updated :</Text>
                                                        <Text style={ [style.font.fontContentNormal, { color: '#696969',textAlign:'right'}]}>{dateFormat(formattedDT, "dd mmmm yyyy HH:MM")}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                );
                            }
                            else {
                                return (
                                    <View style={{ backgroundColor: 'white' }} key={"rowAuditSelected" + i}>
                                        <TouchableOpacity style={{  flexDirection:'row', padding:responsiveWidth(1), paddingBottom:responsiveHeight(1), paddingTop:responsiveHeight(1), paddingLeft:responsiveWidth(2), paddingRight:responsiveWidth(2)}} onPress={data.ScheduleStatus === '1' ? this._onPressButton.bind(this, arrDataOnPress) : this._onPressButtonCompleted.bind(this, arrDataOnPress)}>
                                            {tempImage}
                                            <View style={{ flex:1}} >
                                            <Text style={ [style.font.fontContentLarge, {color:'black', fontWeight:'bold'}] }>{tempAuditScheduleHeader[0].AuditType + " " + tempMsGroupLine[0].GroupLineIDDesc + " - Week " + tempAuditScheduleHeader[0].WeekToDateStart + " " + tempAuditScheduleHeader[0].YearStart}</Text>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Text style={ [ style.font.fontTitleLarge, {flex: 1, color: '#FC6A03', fontWeight: 'bold'}]}>{data.CompleteTask + '/' + data.TotalTask}</Text>
                                                    <View>
                                                        <Text style={ [style.font.fontContentNormal, { color: '#696969',textAlign:'right'}]}>Last Updated :</Text>
                                                        <Text style={ [style.font.fontContentNormal, { color: '#696969',textAlign:'right'}]}>{dateFormat(formattedDT, "dd mmmm yyyy HH:MM")}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                );
                            }
                        }
                    }.bind(this));

                    console.log("Process completed")
                    this.setState({
                        //listAudit: rowAudit,
                        listAuditSelected: rowAuditSelected,
                        listAuditSelectedCompleted: null,
                        tempStatus: 1
                    })
                }
            });


    }

    _funcSortingDate(array, type) {

        if (type == 'asc') {
            array.sort(function (a, b) {
                // Turn your strings into dates, and then subtract them
                // to get a value that is either negative, positive, or zero.
                return new Date(Queries.stringToDateFormat(a.LastUpdateDate)) - new Date(Queries.stringToDateFormat(b.LastUpdateDate));
            });
        }
        else {
            array.sort(function (a, b) {
                // Turn your strings into dates, and then subtract them
                // to get a value that is either negative, positive, or zero.
                return new Date(Queries.stringToDateFormat(b.LastUpdateDate)) - new Date(Queries.stringToDateFormat(a.LastUpdateDate));
            });
        }

        return array
    }

    _changeType(param, e) {
        try {
            console.log("param " + param);
            this.setState({
                tipe: param,
                //listAudit: <Spinner />,
                listAuditSelectedCompleted: <Spinner />,
            })

            if (param === '1') {
                this.setState({
                    backColor: "white",
                    textColor: "grey"
                })
            }
            else {
                this.setState({
                    backColor: "grey",
                    textColor: "white"
                })
            }

            //console.log("masuk data")

            requestAnimationFrame(() => {

                tempTime = '';

                var dataAuditScheduleDetail = []

                
                getInstance().objects('AuditScheduleDetail').filtered('AuditorID = "' + this.users[0].Username + '"').map(function (data, i) {

                    //Check overdue
                    var tempAuditScheduleHeader = getInstance().objects('AuditScheduleHeader').filtered('ScheduleCode = "' + data.ScheduleCode + '"');
                    if (new Date(dateFormat(Queries.stringToDateFormat(tempAuditScheduleHeader[0].PeriodEnd), "yyyy/mm/dd")) < new Date(dateFormat(new Date(), "yyyy/mm/dd")) && data.ScheduleStatus == '1') {
                        //data.ScheduleStatus = '2';
                        
                        let db = getInstance();
                        db.write(() => {
                            db.create('AuditScheduleDetail', {
                                PK_AuditScheduleDetailID: data.PK_AuditScheduleDetailID,
                                ScheduleStatus: '2'
                            }, true);
                        });
                    }
                    else {
                            dataAuditScheduleDetail.push(data);


                         }

                })

                dataAuditScheduleDetail = this._funcSortingDate(dataAuditScheduleDetail, 'desc')

                rowAuditSelected = dataAuditScheduleDetail.map(function (data, i) {
                    console.log("Masuk 13")
                    //console.log(data)

                    //console.log("start rowAuditSelected => " + new Date());
                    if (data.ScheduleStatus == param || (data.ScheduleStatus == '3' && param == '2')) {
                        console.log("Masuk 14")

                        var tempAuditScheduleHeader = getInstance().objects('AuditScheduleHeader').filtered('ScheduleCode = "' + data.ScheduleCode + '"');
                        var tempMsGroupLine = this.MsGroupLine.filtered('GroupLineID = "' + data.GroupLineID + '"');
                        let isOverdue = false;

                        if (data.AuditeeSignStatus == '0' && data.ScheduleStatus == '2') {
                            isOverdue = true
                        }

                        //Data untuk onpress
                        var arrDataOnPress = {
                            plantid: tempAuditScheduleHeader[0].PlantID,
                            grouplinedesc: tempMsGroupLine[0].GroupLineIDDesc,
                            week: tempAuditScheduleHeader[0].WeekToDateStart,
                            year: tempAuditScheduleHeader[0].YearStart,
                            auditScheduleDetailID: data.PK_AuditScheduleDetailID,
                            auditType: tempAuditScheduleHeader[0].AuditType,
                            templateCode: tempAuditScheduleHeader[0].TemplateCode,
                            groupLineID: data.GroupLineID,
                            scheduleCode: data.ScheduleCode,
                            isOverdue: isOverdue
                        }

                        //const formattedDT = Moment(data.LastUpdateDate, Moment.ISO_8601).format();//Moment(dateTime).format('d MMM') //2 May
                        const formattedDT = Queries.stringToDateFormat(data.LastUpdateDate);

                        var timeData = dateFormat(formattedDT, "mmm dd, yyyy");
                        var tempImage;
                        if (param === '1') {
                            tempImage = <Image style={{ resizeMode: 'cover', width: responsiveWidth(15), height: responsiveWidth(15) }} source={require('../Images/MyGMPProgress.png')} />
                        }
                        else {
                            tempImage = <Image style={{ resizeMode: 'cover', width: responsiveWidth(15), height: responsiveWidth(15) }} source={require('../Images/MyGMPCompleted.png')} />
                        }
                        // else if(data.ScheduleStatus === '3'){
                        //     tempImage = <Image style={{ resizeMode: 'cover', width: responsiveWidth(15), height: responsiveWidth(15)}} source={require('../Images/All Gov Progress.png')} />
                        // }

                        var additionalText = null ;

                        if (data.ScheduleStatus === '3') {
                            additionalText = <Text style={ [style.font.fontContentSmall, {  fontStyle: 'italic' } ]}>(Uploading Attachment)</Text>
                        }

                        // console.log(tempTime)
                        // console.log(timeData)

                        if (tempTime !== timeData) {
                            tempTime = timeData;
                            return (
                                <View style={{ paddingLeft:responsiveWidth(1), paddingRight:responsiveWidth(1)}} key={"rowAuditSelected" + data.PK_AuditScheduleDetailID}>
                                    <View style={{ flexDirection: 'row', borderBottomWidth:1,borderBottomColor:'black' }}>
                                        <Text style={[style.font.fontTitleNormal, { color: 'black' }]}>{timeData}</Text>
                                        <Text style={[style.font.fontTitleNormal, { color: '#696969', textAlign: 'right', flex: 1 }]}>My Audits</Text>
                                    </View>
                                     <TouchableOpacity style={{  flexDirection:'row', padding:responsiveWidth(1), paddingBottom:responsiveHeight(1), paddingTop:responsiveHeight(1), paddingLeft:responsiveWidth(2), paddingRight:responsiveWidth(2)}} onPress={data.ScheduleStatus === '1' ? this._onPressButton.bind(this, arrDataOnPress) : this._onPressButtonCompleted.bind(this, arrDataOnPress)}>

                                        {tempImage}

                                         <View style={{ flex:1 }} >
                                            <Text style={ [style.font.fontContentLarge, {color:'black', fontWeight:'bold'}] }>{tempAuditScheduleHeader[0].AuditType + " " + tempMsGroupLine[0].GroupLineIDDesc + " - Week " + tempAuditScheduleHeader[0].WeekToDateStart + " " + tempAuditScheduleHeader[0].YearStart}</Text>
                                            <View style={{ flexDirection: 'row', justifyContent:'space-between' }}>
                                                {
                                                    param == 1 ?    <Text style={ [ style.font.fontTitleLarge, {flex: 1, color: '#FC6A03', fontWeight: 'bold'}]}>{data.CompleteTask + '/' + data.TotalTask}</Text> :
                                                           <Text style={ [ style.font.fontContentLarge, {flex: 1, color: data.TotalFindings == 0 ? '#0b6623' : "#c00"}]}>{data.TotalFindings} Finding(s)</Text>
                                                }
                                                <View>
                                                    <Text style={ [style.font.fontContentNormal, { color: '#696969',textAlign:'right'}]}>Last Updated :</Text>
                                                    <Text style={ [style.font.fontContentNormal, { color: '#696969',textAlign:'right'}]}>{dateFormat(formattedDT, "dd mmmm yyyy HH:MM")}</Text>
                                                </View>
                                            </View>

                                            {additionalText}
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            );
                        }
                        else {
                            return (
                                <View style={{ backgroundColor: 'white' }} key={"rowAuditSelected" + data.PK_AuditScheduleDetailID}>
                                    <TouchableOpacity style={{  flexDirection:'row', padding:responsiveWidth(1), paddingBottom:responsiveHeight(1), paddingTop:responsiveHeight(1), paddingLeft:responsiveWidth(2), paddingRight:responsiveWidth(2)}} onPress={data.ScheduleStatus === '1' ? this._onPressButton.bind(this, arrDataOnPress) : this._onPressButtonCompleted.bind(this, arrDataOnPress)}>
                                        {tempImage}
                                        <View style={{ flex:1}} >
                                            <Text style={ [style.font.fontContentLarge, { fontWeight:'bold'}] }>{tempAuditScheduleHeader[0].AuditType + " " + tempMsGroupLine[0].GroupLineIDDesc + " - Week " + tempAuditScheduleHeader[0].WeekToDateStart + " " + tempAuditScheduleHeader[0].YearStart}</Text>
                                            <View style={{ flexDirection: 'row', justifyContent:'space-between' }}>
                                                {
                                                    param == 1 ? <Text style={ [ style.font.fontTitleLarge, {flex: 1, color: '#FC6A03', fontWeight: 'bold'}]}>{data.CompleteTask + '/' + data.TotalTask}</Text> :
                                                        <Text style={ [ style.font.fontContentLarge, {flex: 1, color: data.TotalFindings == 0 ? '#0b6623' : "#c00"}]}>{data.TotalFindings} Finding(s)</Text>
                                                }
                                                <View>
                                                    <Text style={ [style.font.fontContentNormal, { color: '#696969',textAlign:'right'}]}>Last Updated :</Text>
                                                    <Text style={ [style.font.fontContentNormal, { color: '#696969',textAlign:'right'}]}>{dateFormat(formattedDT, "dd mmmm yyyy HH:MM")}</Text>
                                                </View>
                                            </View>
                                            {additionalText}
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            );
                        }
                    }
                    //console.log("end rowAuditSelected => " + new Date());
                }.bind(this));

                console.log("change completed")
                this.setState({
                    //listAudit: rowAudit,
                    listAuditSelectedCompleted: rowAuditSelected,
                    tempStatus: 1
                })
            });
        }
        catch (err) {
            this.setState({
                //listAudit: rowAudit,
                listAuditSelectedCompleted: <View />,
                tempStatus: 1
            })
        }




    }

    setCurrentReadOffset = (event) => {
        // Log the current scroll position in the list in pixels
        //console.log(event.nativeEvent.contentOffset.y);
    }

    render() {
        //console.log(this.state.textColor)

        //console.log(this.state.backColor)
        var styleDot = (<View style={{ backgroundColor: '#fff', borderColor: 'grey', borderWidth: 2, width: responsiveWidth(5), height: responsiveWidth(5), borderRadius: 14, marginLeft: responsiveWidth(1), marginRight: responsiveWidth(1), marginTop: responsiveWidth(1), marginBottom: responsiveHeight(3), }} />)
        var styleActiveDot = (<View style={{ backgroundColor: 'grey', width: responsiveWidth(5), height: responsiveWidth(5), borderRadius: 14, marginLeft: responsiveWidth(1), marginRight: responsiveWidth(1), marginTop: responsiveWidth(1), marginBottom: responsiveHeight(3), }} />)
        return (

            <Container>
                {
                    // <Segment style={{ backgroundColor: "white", paddingTop: responsiveHeight(7), paddingBottom: responsiveHeight(5), borderColor: "#fff" }}>
                    //     <Button bordered={true} first style={{ borderColor: "grey", borderBottomLeftRadius: 5, borderTopLeftRadius: 5, backgroundColor: this.state.textColor, fontFamily: 'MyriadPro-Regular' }} onPress={this._changeType.bind(this, '1')}><Text style={{ color: this.state.backColor, fontFamily: 'MyriadPro-Regular' }}>In Progress</Text></Button>
                    //     <Button bordered={true} style={{ borderColor: "grey", borderBottomRightRadius: 5, borderTopRightRadius: 5, backgroundColor: this.state.backColor, fontFamily: 'MyriadPro-Regular' }} onPress={this._changeType.bind(this, '2')}><Text style={{ color: this.state.textColor, fontFamily: 'MyriadPro-Regular' }}>Completed</Text></Button>
                    // </Segment>
                }
                <Content onScroll={this.setCurrentReadOffset}>
                    <SpinnerFull visible={this.state.loaderStatus} textContent={"Please Wait"} textStyle={{ color: '#fff' }} cancelable={false} />
                    <View>
                        <ScrollView style={{ paddingTop: responsiveHeight(1)}}>
                            {this.state.listAuditSelected}
                            {
                                this.state.listAuditSelectedCompleted
                            }
                            <View style={{ backgroundColor: '#fff', padding: responsiveHeight(2) }}>

                            </View>
                        </ScrollView>
                    </View>
                </Content>
            </Container>


        );
    }
}
