// @flow

import React from 'react';
import { View, Text, Switch, Alert, TouchableHighlight } from 'react-native';
import Camera from 'react-native-camera';
import {Metrics} from '../Themes'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from '../Themes/Responsive';
import { Actions } from 'react-native-router-flux';


export default class CameraScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isVideo : false,
      captureMode : Camera.constants.CaptureMode.still,
      isRecording: false,
      isLight : false,
      isFront : false,
      cameraRender: true,
      timerID: -1,
    }
  }

  changeVideoPicture(){
    var value = !this.state.isVideo
    this.setState({
      isVideo:value,
    });
  }

  takePicture() {
    this.camera.capture()
      .then((data) => {
        this.setState({cameraRender: false});
        //this.setState({ path: data.path })
        //Alert.alert(data.path.substring(data.path.length - 20));
        Actions.pop({refresh: {status: true, assetExt: data.path.split('.').pop(), assetType: 'image', assetPath: data.path}});
      })
      .catch(err => console.error(err));
  }

  startRecording = () => {
    if (this.camera) {
      let timerID = setTimeout(this.stopRecording, 60000);
      this.setState({
        isRecording: true,
        timerID: timerID
      });
      this.camera.capture({mode: Camera.constants.CaptureMode.video})
      .then((data) => {
        Actions.pop({refresh: {status: true, assetExt: data.path.split('.').pop(), assetType: 'video', assetPath: data.path}})
      })
      .catch(err => console.error(err));
    }
  }

  stopRecording = () => {
    if (this.camera) {
      this.camera.stopCapture();
      this.setState({
        isRecording: false,
        cameraRender: false
      });
    }
  }

  capture(){
    if (this.state.isVideo) {
      if (this.state.isRecording) {
        clearTimeout(this.state.timerID);
        this.stopRecording();
      }else {
        this.startRecording();
      }
    }else {
      this.takePicture();
    }
  }

  componentWillReceiveProps(nextProps){
    this.setState({cameraRender: true});
  }

  render () {
    return (
      <View style={{flex:1}}>
        {(this.state.cameraRender ? (
          <Camera
            ref={(cam) => {
              this.camera = cam;
            }}
            captureQuality={this.state.isVideo ? Camera.constants.CaptureQuality.low : Camera.constants.CaptureQuality.medium}
            aspect={Camera.constants.Aspect.fill}
            captureMode = {this.state.CaptureMode}
            captureTarget = {Camera.constants.CaptureTarget.disk}
            flashMode = {(this.state.isLight ? (this.state.isVideo ? Camera.constants.FlashMode.off : Camera.constants.FlashMode.on) : Camera.constants.FlashMode.off)}
            torchMode = {(this.state.isLight ? (this.state.isVideo ? Camera.constants.TorchMode.on : Camera.constants.TorchMode.off) : Camera.constants.TorchMode.off)}
            type = {this.state.isFront ? Camera.constants.Type.front : Camera.constants.Type.back}>
              <View style={{height:Metrics.screenHeight, width:Metrics.screenWidth}}>
                <View style={{height:responsiveHeight(10), backgroundColor:'rgba(0, 0, 0, 0.5)', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                  {
                    this.state.isRecording ? null :
                    (
                      <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                          <Text style={{fontFamily:'MyriadPro-Regular', fontSize:responsiveFontSize(2), color: 'white'}}>Light</Text>
                          <Switch
                            value={this.state.isLight}
                            onValueChange={() => this.setState({isLight : !this.state.isLight})} />
                        </View>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                          <Text style={{fontFamily:'MyriadPro-Regular', fontSize:responsiveFontSize(2), color: 'white'}}>Front</Text>
                          <Switch
                            value={this.state.isFront}
                            onValueChange={() => this.setState({isFront : !this.state.isFront, isLight: false})} />
                        </View>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                          <TouchableHighlight onPress={() => {this.setState({cameraRender: false}); Actions.pop({refresh: {status: false}})}}>
                            <Text style={{fontFamily:'MyriadPro-Regular', fontSize:responsiveFontSize(2), color: 'white'}}>Close</Text>
                          </TouchableHighlight>
                        </View>
                      </View>
                    )
                  }
                </View>
                <View style={{height:Metrics.screenHeight - responsiveHeight(10) - 20 - responsiveHeight(15)}}>

                </View>
                <View style={{height:responsiveHeight(15), backgroundColor:'rgba(0, 0, 0, 0.5)', flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
                  <View style={{flex: 1}}>
                  </View>
                  <TouchableHighlight style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} onPress={this.capture.bind(this)}>
                    <View style={{ width: responsiveHeight(14), height: responsiveHeight(14), borderRadius: responsiveHeight(7), backgroundColor: (this.state.isRecording ? 'lightblue' : 'white'), justifyContent: 'center', alignItems: 'center' }}>
                      <Text style={{fontFamily:'MyriadPro-Regular', fontSize:responsiveFontSize(2), color: 'black'}}>{this.state.isVideo ? 'Rec' : 'Capture'}</Text>
                    </View>
                  </TouchableHighlight>
                  <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                  {
                    this.state.isRecording ? null :
                    (
                      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{fontFamily:'MyriadPro-Regular', fontSize:responsiveFontSize(2), color: 'white'}}>Camera - Video</Text>
                        <Switch
                          value={this.state.isVideo}
                          onValueChange={() => this.setState({captureMode: !this.state.isVideo === true ? Camera.constants.CaptureMode.video : Camera.constants.CaptureMode.still, isVideo: !this.state.isVideo, isLight: false})} />
                      </View>
                    )
                  }
                  </View>
                </View>
              </View>
          </Camera>
        ) : (null))}
      </View>
    )
  }
}