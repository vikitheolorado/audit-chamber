// @flow

import React, { Component } from 'react';
import { View, Image, Text, StyleSheet, Alert, Modal, TouchableHighlight, TouchableOpacity, TouchableWithoutFeedback, StatusBar } from 'react-native';
import { Metrics, Images } from '../Themes';
import { Footer, FooterTab, Button, Badge, Header, Left, Body, Right, Title, Fab, Container } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from '../Themes/Responsive'; 
//import ScheduleAuditInc from './ScheduleAuditInc';

import Camera from 'react-native-camera'

var styles = StyleSheet.create({
  container: {
    marginTop: Metrics.marginTopContainer,
  },
  header: {
    alignItems: 'center', 
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#c00',
    height: Metrics.marginHeader,
  },
  footer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#cecece',
    height: responsiveHeight(11),
    borderColor: '#c6c6c6',
    borderWidth: 1,
  },
  mainContainer: {
    height: Metrics.screenHeight - Metrics.marginTopContainer - Metrics.footerHeight - Metrics.marginHeader,
    // height : 560.29,\
  },
  footerText: {
    fontSize: 5,
    textAlign: 'center',
  },
  footerIcon: {
    width: Metrics.footerIconWidth,
    height: Metrics.footerIconHeight,
  },
  footerIconContainer: {
    width: Metrics.footerIconWidth + responsiveHeight(3),
    height: Metrics.footerIconHeight + responsiveHeight(3),
  },

  addButton: {
    backgroundColor: '#ff5722',
    borderColor: '#ff5722',
    borderWidth: 1,
    height: Metrics.footerIconPlusHeight,
    width: Metrics.footerIconPlusHeight,
    borderRadius: Metrics.footerIconPlusHeight / 2,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: responsiveHeight(1),
    right: (Metrics.screenWidth / 2) - (Metrics.footerIconPlusHeight / 2)
  }

});

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalPlusVisible: false,
      modalUserVisible: false,
      roleid: 2,

      userName: 'Fendiyono',
      userPosition: 'Auditor',
      userPlant: 'Cikedokan',

      active: true,
    }

  }

  mainContainer() {
    return (
      <View style={[styles.mainContainer]}>
      </View>
    )
  }

  plusOnPress() {
    if (this.state.roleid === 1) {
      this.setState({ modalPlusVisible: true })
    }
    else {
      // new adhoc audit
      // Alert.alert('user bukan manager')
      return (
        Actions.CameraScreen()
      )
    }
  }

  createAuditScheduleOnPress() {
    Alert.alert('createAuditScheduleOnPress')
  }

  nonScheduleOnPress() {
    Alert.alert('nonScheduleOnPress')
  }


  render() {
    return (
      <Container>

        <StatusBar
              backgroundColor='#c00'
              barStyle="default"
          />
        <View style={styles.container}>

          <Modal
            transparent={true}
            animationType="fade"
            visible={this.state.modalPlusVisible}>
            <View
              behavior='padding'
              style={{
                flex: 1,
                justifyContent: 'center',
                backgroundColor: 'rgba(0, 0, 0, 0.5)',

              }}>
              <View
                style={{
                  backgroundColor: '#fff',
                  padding: 10,
                  top: 100
                }} >
                <View>
                  <View>
                    <TouchableHighlight onPress={this.createAuditScheduleOnPress.bind(this)}>
                      <Text style={{ textAlign: 'center' }} >Create Audit Schedule</Text>
                    </TouchableHighlight>
                    <TouchableHighlight onPress={this.nonScheduleOnPress.bind(this)}>
                      <Text style={{ textAlign: 'center' }} >Non Schedule</Text>
                    </TouchableHighlight>
                  </View>
                </View>
              </View>
            </View>
          </Modal>

          <Modal
            transparent={true}
            animationType="fade"
            visible={this.state.modalUserVisible}>
            <View
              behavior='padding'
              style={{
                flex: 1,
                justifyContent: 'center',
                backgroundColor: 'rgba(0, 0, 0, 0.5)',
                borderWidth: 1,
                padding: 20
              }}>
              <View
                style={{
                  borderRadius: 10,
                  backgroundColor: '#fff',
                  padding: 20,
                  alignItems: 'center',
                  justifyContent: 'center'
                }} >
                <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                  <View>
                    <Text> My Profile
                  </Text>
                  </View>

                  <View>
                    <TouchableHighlight onPress={() => this.setState({ modalUserVisible: false })}>
                      <Image source={Images.close} style={{ height: responsiveHeight(5), width: responsiveHeight(5), right: 0 }} />
                    </TouchableHighlight>
                  </View>
                </View>


                <Image source={Images.userprofile} style={{ height: responsiveHeight(20), width: responsiveHeight(20) }} />
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                  <View>
                    <Text>Name</Text>
                    <Text>Position</Text>
                    <Text>Plant</Text>
                  </View>
                  <View>
                    <Text>: {this.state.userName}</Text>
                    <Text>: {this.state.userPosition}</Text>
                    <Text>: {this.state.userPlant}</Text>
                  </View>
                </View>

                <Button style={{ backgroundColor: '#c3c3c3', marginTop: 10 }}>
                  <Image source={Images.power} style={{ width: responsiveHeight(5), height: responsiveHeight(5), marginRight: 10 }} />
                  <Text>Log Out</Text>
                </Button>
              </View>
            </View>
          </Modal>

          <View style={styles.header}>
            <Image source={require('../Images/quinsys.png')} style={{ height: responsiveHeight(5), width: responsiveHeight(10), marginLeft: 10 }}></Image>
            <Text style={{color:'white', fontSize: 25}}>Audit Chamber</Text>
            <TouchableOpacity onPress={() => this.setState({ modalUserVisible: true })}>
              <Image source={Images.iconuser} style={{ height: responsiveHeight(5), width: responsiveHeight(5), marginRight: 10 }}></Image>
            </TouchableOpacity>
          </View>

          <View style={styles.mainContainer}>
            {this.mainContainer()}
          </View>
          <View style={{ flexDirection: 'row', height: Metrics.footerHeight, justifyContent: 'space-between', backgroundColor: '#c0c0c0' }}>

            <TouchableHighlight style={styles.footerIconContainer}>
              <View style={{alignItems:'center', justifyContent:'center'}}>
                <Text />
                <Image source={Images.myauditon} style={styles.footerIcon}></Image>
                <Text style={{ fontSize: 10, width: Metrics.footerIconWidth + responsiveHeight(3), textAlign:'center'}}>My Audits</Text>
              </View>
            </TouchableHighlight>
            <TouchableHighlight style={styles.footerIconContainer}>
              <View  style={{alignItems:'center', justifyContent:'center'}}>
                <Text />
                <Image source={Images.resultoff} style={styles.footerIcon}></Image>
                <Text style={{ fontSize: 10,  width: Metrics.footerIconWidth + responsiveHeight(3), textAlign:'center'}}>Results</Text>
              </View>
            </TouchableHighlight>
            <TouchableHighlight style={styles.footerIconContainer}>
              <View  style={{alignItems:'center'}}>
                <Text />
              </View>
            </TouchableHighlight>
            <TouchableHighlight style={styles.footerIconContainer}>
              <View  style={{alignItems:'center', justifyContent:'center'}}>
                <Text />
                <Image source={Images.scheduleon} style={styles.footerIcon}></Image>
                <Text style={{ fontSize: 10,  width: Metrics.footerIconWidth + responsiveHeight(3), textAlign:'center'}}>Schedule</Text>
              </View>
            </TouchableHighlight>
            <TouchableHighlight style={styles.footerIconContainer}>
              <View style={{ alignItems: 'center' }}>
                <Badge style={{ backgroundColor: 'red', width: responsiveHeight(3), height: responsiveHeight(3), marginLeft:5 }}>
                  <Text style={{ fontSize: 14, color: 'white', width: responsiveHeight(3), marginLeft: -5, textAlign: 'center' }}>
                    51
                  </Text>
                </Badge>
                <Image source={Images.notificationon} style={[styles.footerIcon, { marginTop: -10 }]}></Image>
                <Text style={{ fontSize: 10,  width: Metrics.footerIconWidth + responsiveHeight(2), }}>Notifications</Text>
              </View>
            </TouchableHighlight>


          </View>
        </View>
        <TouchableHighlight onPress={this.plusOnPress.bind(this)} style={styles.addButton}>
          <Image source={Images.plus} style={{ height: Metrics.footerIconPlusHeight, width: Metrics.footerIconPlusWidth }}></Image>
        </TouchableHighlight>
      </Container>
    )
  }
}
