import React, { Component } from "react";
import {
  Container,
  Content,
  Body,
  ListItem,
  Text,
  CheckBox,
  Button,
  Segment,
  Input,
  Item,
  Left,
  Thumbnail
} from "native-base";
import {
  ScrollView,
  NativeModules,
  Image,
  Alert,
  View,
  Dimensions,
  Keyboard,
  DeviceEventEmitter,
  StatusBar
} from "react-native";
const { width, height } = Dimensions.get("window");
import {
  responsiveFontSize,
  responsiveHeight,
  responsiveWidth,
  newResponsiveFontSize
} from "../Themes/Responsive";
import { Actions } from "react-native-router-flux";
import Icon from "react-native-vector-icons/FontAwesome";
import { getInstance } from "../Data/DB/DBHelper";
import Queries from "../Data/Queries";
import Constants from "../Data/Constants";
import Axios from "axios";
import DB from "../Data/DB/Db";
import { ReactNativeAD, ADLoginView, Logger } from "react-native-azure-ad";
import ErrorReport from "../ErrorReport";
import Spinner from "react-native-loading-spinner-overlay";
import ModalProgress from "../Components/ModalProgress";
//import res.data from '../Data/ExampleJson.json'

var _keyboardWillShowSubscription;
var _keyboardWillHideSubscription;

Logger.setLevel("VERBOSE");
const config = {
  client_id: "6244083d-01e9-48e9-9584-36beafab5fdf",
  //client_id: '7972f0b6-a779-4f4d-9b3b-953f696f4430',
  redirectUrl: "http://ccaiddevauditchamber001.azurewebsites.net",
  authorityHost: "https://login.microsoftonline.com/CCAMATIL1.onmicrosoft.com",
  tenant: "31f6eb2e-90b9-4668-9645-fec7390e62c6",
  //client_secret: 'Hkt2SPeA7E+M21IrXftr78TtyAPwali6FeCQluLg8UE=',
  client_secret: "x1nZUbPY5oKOpvf05DsgqgDPsEeib2E/ve3oue1pS7Q=",
  resources: [
    //'https://CCAMATIL1.onmicrosoft.com/ccaiddevauditchamber001'
    "https://graph.windows.net"
  ]
};

var arr = [];
var tempTime = "";
export default class Login extends Component {
  constructor(props) {
    super(props);
    new ReactNativeAD(config);
    this.MsUser = getInstance().objects("MsUser");
    this.AuditScheduleHeader = getInstance().objects("AuditScheduleHeader");
    this.AuditScheduleDetail = getInstance().objects("AuditScheduleDetail");
    this.MsPlant = getInstance().objects("MsPlant");
    this.MsGroupLine = getInstance().objects("MsGroupLine");
    this.MsLine = getInstance().objects("MsLine");

    if (
      getInstance()
        .objects("MsUser")
        .filtered("isLogin = 1").length
    ) {
      Actions.HomeScreen({ type: "replace" });
    }

    this.state = {
      userid: "",
      password: "",
      invalid: <View />,
      loaderStatus: false,
      logoutStatus: true,
      loadingInfo: "Please Wait",
      count: 0,
      showModalProgress: true
    };
  }

  _loginProcess(email, token) {
    console.log("_loginprocess");
    Axios.get(Constants.APILink + `Master/GetCurrentUser?username=` + email, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + Queries.getPersistentData("token")
      },
      timeout: 30000
    })
      .then(res => {
        console.log("log success Master/GetCurrentUser?username=");
        console.log(res);

        DB.write(() => {
          // getInstance().delete(getInstance().objects('MsPlant'));
          // getInstance().delete(getInstance().objects('MsGroupLine'));
          getInstance().delete(getInstance().objects("MsUser"));
          // getInstance().delete(getInstance().objects('MsUserAuditee'));
          // getInstance().delete(getInstance().objects('MsArea'));
          // getInstance().delete(getInstance().objects('MsLine'));
          // getInstance().delete(getInstance().objects('MsTemplateHeader'));
        });

        // 2.0.6 perbaikan insert data ke DB
        console.log("data current user");
        // res.data.map(function (data, i) {
        //     console.log(data)
        //     try {
        //         DB.write(() => {
        //             DB.create('MsUser', { UserID: data.UserID, Username: data.Username, FullName: data.FullName, Email: data.Email, Roles: data.Roles, isLogin: data.isLogin, PlantID: data.PlantID, Position: data.Position, active: data.Active });
        //         })
        //     } catch (error) {
        //         // console.log(data.Username)
        //         DB.write(() => {
        //             DB.create('MsUser', { UserID: data.UserID, Username: data.Username, FullName: data.FullName, Email: data.Email, Roles: data.Roles, isLogin: data.isLogin, PlantID: data.PlantID, Position: data.Position, active: data.Active }, true);
        //         })
        //         // console.log(error);
        //         ErrorReport.log(error);
        //     }
        // })
        try {
          DB.write(() => {
            res.data.map(function(data, i) {
              DB.create("MsUser", {
                UserID: data.UserID,
                Username: data.Username,
                FullName: data.FullName,
                Email: data.Email,
                Roles: data.Roles,
                isLogin: data.isLogin,
                PlantID: data.PlantID,
                Position: data.Position,
                active: data.Active
              });
            });
          });
        } catch (error) {
          DB.write(() => {
            res.data.map(function(data, i) {
              DB.create(
                "MsUser",
                {
                  UserID: data.UserID,
                  Username: data.Username,
                  FullName: data.FullName,
                  Email: data.Email,
                  Roles: data.Roles,
                  isLogin: data.isLogin,
                  PlantID: data.PlantID,
                  Position: data.Position,
                  active: data.Active
                },
                true
              );
            });
          });
        }

        //Cek database
        this.result_filter = this.MsUser.filtered(
          'Email ==[c] "' + email + '"'
        );

        Queries.setAllUserNotLogin();
        getInstance().write(() => {
          getInstance().create(
            "MsUser",
            { UserID: this.result_filter[0].UserID, isLogin: 1 },
            true
          );
        });

        let notificationID = Queries.getPersistentData("NotificationID");
        console.log(
          Constants.APILink +
            "notif/PostNotificationID?username=" +
            this.result_filter[0].Username +
            "&notificationid=" +
            notificationID +
            `apuytoken`
        );
        Axios.get(
          Constants.APILink +
            "notif/PostNotificationID?username=" +
            this.result_filter[0].Username +
            "&notificationid=" +
            notificationID,
          {
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Bearer " + Queries.getPersistentData("token")
            }
          }
        )
          .then(result => {})
          .catch(exception => {
            ErrorReport.log(JSON.stringify(exception));
          });

        this.setState({
          loaderStatus: false
        });

        //console.log("data 8")
        // 2.0.6
        
        Actions.HomeScreen({ type: "replace" });
      })
      .catch(err => {
        console.log("log gagal");
        console.log(err);
        Alert.alert(email + " is not registered");
        this.setState({
          invalid: (
            <Button
              iconLeft
              danger
              style={{ width: responsiveWidth(75), backgroundColor: "#f08080" }}
            >
              <Icon name="times" style={{ color: "#c00", paddingRight: 10 }} />
              <Text
                style={{
                  fontFamily: "MyriadPro-Regular",
                  textAlign: "left",
                  width: responsiveWidth(65)
                }}
              >
                The username / password combination is not valid.
              </Text>
            </Button>
          )
        });

        this.setState({
          loaderStatus: false,
          logoutStatus: true
        });
      });
  }

  _loginProcessBackUp(email) {
    Axios.get(Constants.APILink + `Master/GetMasterDataAll?username=` + email, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        //console.log("log berhasil")
        //console.log(res)

        DB.write(() => {
          getInstance().delete(getInstance().objects("MsPlant"));
          getInstance().delete(getInstance().objects("MsGroupLine"));
          getInstance().delete(getInstance().objects("MsUser"));
          getInstance().delete(getInstance().objects("MsUserAuditee"));
          getInstance().delete(getInstance().objects("MsArea"));
          getInstance().delete(getInstance().objects("MsLine"));
        });

        //console.log("data 1")
        res.data.ObjPlant.map(function(data, i) {
          //console.log("datanya")
          //console.log(data)
          DB.write(() => {
            DB.create("MsPlant", {
              PlantID: data.PlantID,
              PlantDesc: data.PlantDesc
            });
          });
        });

        //console.log("data 2")
        res.data.ObjGroupLine.map(function(data, i) {
          //console.log("datanya")
          //console.log(data)
          DB.write(() => {
            DB.create("MsGroupLine", {
              GroupLineID: data.GroupLineID,
              GroupLineIDDesc: data.GroupLineDesc,
              PlantID: data.PlantID,
              GroupLineIDAbb: data.GroupLineAbb
            });
          });
        });

        //console.log("data 3")
        res.data.ObjUser.map(function(data, i) {
          // console.log("datanya")
          // console.log(data)
          DB.write(() => {
            DB.create("MsUser", {
              UserID: data.UserID,
              Username: data.Username,
              FullName: data.FullName,
              Email: data.Email,
              Roles: data.Roles,
              isLogin: data.isLogin,
              PlantID: data.PlantID,
              Position: data.Position
            });
          });
        });

        //console.log("data 3")
        res.data.ObjUserAuditee.map(function(data, i) {
          // console.log("datanya")
          // console.log(data)
          DB.write(() => {
            DB.create("MsUserAuditee", {
              PK_MsUserAuditee_ID: i + 1,
              UserID: data.UserID,
              Username: data.Username,
              FullName: data.FullName,
              Email: data.Email,
              PlantID: data.PlantID,
              GroupLineID: data.GroupLineID
            });
          });
        });

        //console.log("data 4")
        res.data.ObjArea.map(function(data, i) {
          // console.log("datanya")
          // console.log(data)
          DB.write(() => {
            DB.create("MsArea", {
              groupLineID: data.groupLineID,
              lineID: data.lineID,
              areaID: data.areaID,
              areaDesc: data.areaDesc
            });
          });
        });

        //console.log("data 5")
        res.data.ObjLine.map(function(data, i) {
          // console.log("datanya")
          // console.log(data)
          DB.write(() => {
            DB.create("MsLine", {
              PK_LineID: i + 1,
              LineID: data.LineID,
              LineIDDesc: data.LineDesc,
              GroupLineID: data.GroupLineID
            });
          });
        });

        //console.log("data 6")
        //console.log("data 6")
        res.data.ObjSystemParameter.map(function(data, i) {
          // console.log("datanya")
          console.log(data);
          try {
            DB.write(() => {
              DB.create(
                "PersistentData",
                { Key: data.key, Value: data.value },
                true
              );
            });
          } catch (error) {
            DB.write(() => {
              DB.create(
                "PersistentData",
                { Key: data.key, Value: data.value },
                true
              );
            });
            ErrorReport.log(error);
          }
        });
        // this.setState({
        //     invalid: (<View></View>)
        // })

        //console.log("data 7")

        //Cek database
        this.result_filter = this.MsUser.filtered(
          'Email ==[c] "' + email + '"'
        );

        Queries.setAllUserNotLogin();
        getInstance().write(() => {
          getInstance().create(
            "MsUser",
            { UserID: this.result_filter[0].UserID, isLogin: 1 },
            true
          );
        });

        let notificationID = Queries.getPersistentData("NotificationID");
        console.log("CCAAPI");
        console.log(
          Constants.APILink +
            "notif/PostNotificationID?username=" +
            this.result_filter[0].Username +
            "&notificationid=" +
            notificationID
        );

        Axios.get(
          Constants.APILink +
            "notif/PostNotificationID?username=" +
            this.result_filter[0].Username +
            "&notificationid=" +
            notificationID,
          {
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json"
            }
          }
        );
        // .then((result) => { Actions.HomeScreen(); })
        // .catch((exception) => { Alert.alert('Error', JSON.stringify(exception)) });

        this.setState({
          loaderStatus: false
        });

        //console.log("data 8")
        Actions.HomeScreen({ type: "replace" });
      })
      .catch(err => {
        console.log("log gagal2");
        console.log(err);
        Alert.alert(email + " is not registered");
        this.setState({
          invalid: (
            <Button
              iconLeft
              danger
              style={{ width: responsiveWidth(75), backgroundColor: "#f08080" }}
            >
              <Icon name="times" style={{ color: "#c00", paddingRight: 10 }} />
              <Text
                style={{
                  fontFamily: "MyriadPro-Regular",
                  textAlign: "left",
                  width: responsiveWidth(65)
                }}
              >
                The username / password combination is not valid.
              </Text>
            </Button>
          )
        });

        this.setState({
          loaderStatus: false,
          logoutStatus: true
        });
      });
  }

  _onSuccessAzureAd(cred) {
    console.log("_onSuccessAzureAd");
    try {
      this.setState({
        loaderStatus: true,
        logoutStatus: false
      });

      console.log("call api data/login?token=");
      Axios.get(
        Constants.APILink +
          "data/login?token=" +
          cred["https://graph.windows.net"].access_token,
        {}
      )
        .then(res => {
          console.log("Information - Login - success - login viki");
          console.log(res);

          if (res.data.result == 1) {
            console.log(
              "Information - Login - success - login viki - result = 1"
            );
            console.log(res.data.email);
            console.log(res.data.token);

            Queries.setPersistentData("masterdata", "0");
            Queries.setPersistentData("token", res.data.token);
            Queries.setPersistentData("email", res.data.email);

            //Cek database
            // this.result_filter = this.MsUser.filtered('Email ==[c] "' + res.data.email + '"');

            // Queries.setAllUserNotLogin();
            // getInstance().write(() => {
            //     getInstance().create('MsUser', { UserID: this.result_filter[0].UserID, isLogin: 1 }, true);
            // });

            // let notificationID = Queries.getPersistentData('NotificationID');
            // console.log('CCAAPI')
            // console.log(Constants.APILink + 'notif/PostNotificationID?username=' + this.result_filter[0].Username + '&notificationid=' + notificationID + `apuytoken`)
            // Axios.get(Constants.APILink + 'notif/PostNotificationID?username=' + this.result_filter[0].Username + '&notificationid=' + notificationID, {
            //     headers: {
            //         "Accept": "application/json",
            //         "Content-Type": "application/json",
            //         "Authorization": "Bearer " + Queries.getPersistentData('token'),
            //     }
            // })
            //     .then((result) => { })
            //     .catch((exception) => { ErrorReport.log(JSON.stringify(exception)) });

            // this.setState({
            //     loaderStatus: false
            // })

            // //console.log("data 8")
            // Actions.HomeScreen({ type: 'replace' });
            this._loginProcess(res.data.email, res.data.token);
          } else if(res.data.result == '2'){
            this.setState({
              loaderStatus: false,
              logoutStatus: true
            }, ()=>{
              Alert.alert(
                "Information",
                "User are not registered, please close the app and contact your administrator.",
                [{ text: "Ok" }],
                { cancelable: false }
              ); 
            });
          }else{
            this.setState({
              loaderStatus: false,
              logoutStatus: true
            }, ()=>{
              Alert.alert(
                "Information",
                "Cannot login, please contact your administrator.",
                [{ text: "Ok" }],
                { cancelable: false }
              );
            });
            
          }
          // //console.log(customData.mailNickname)
          // Alert.alert('Mencoba Mengambil Data User')
          //Alert.alert('ID ANDA : ' + res.data.mailNickname)
        })
        .catch(err => {
          console.log("Information - Login - error - login viki");
          console.log(err);
          //   this.setState({ loaderStatus: false })
        });
    } catch (e) {
      //Alert.alert(JSON.stringify(e));
      Alert.alert("Error getting Email");
      this.setState({
        loaderStatus: false,
        logoutStatus: true
      });
    }
  }

  _onPressButtonBackUp(e) {
    // console.log("pressx");
    // console.log(this.state.userid)
    // console.log(this.state.password)

    this.setState({
      loaderStatus: true
    });

    requestAnimationFrame(() => {
      //console.log(getInstance().objects('MsLine'));

      if (this.state.password !== "1234") {
        this.setState({
          invalid: (
            <Button
              iconLeft
              danger
              style={{ width: responsiveWidth(75), backgroundColor: "#f08080" }}
            >
              <Icon name="times" style={{ color: "#c00", paddingRight: 10 }} />
              <Text
                style={{
                  fontFamily: "MyriadPro-Regular",
                  textAlign: "left",
                  width: responsiveWidth(65)
                }}
              >
                The username / password combination is not valid.
              </Text>
            </Button>
          )
        });

        this.setState({
          loaderStatus: false
        });
      } else {
        //Check user on server
        console.log(
          Constants.APILink +
            `Master/GetMasterDataAll?username=` +
            this.state.userid +
            `apuytoken`
        );
        Axios.get(
          Constants.APILink +
            `Master/GetMasterDataAll?username=` +
            this.state.userid,
          {
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Bearer " + Queries.getPersistentData("token")
            }
          }
        )
          .then(res => {
            //console.log("log berhasil")
            //console.log(res)

            DB.write(() => {
              getInstance().delete(getInstance().objects("MsPlant"));
              getInstance().delete(getInstance().objects("MsGroupLine"));
              getInstance().delete(getInstance().objects("MsUser"));
              getInstance().delete(getInstance().objects("MsUserAuditee"));
              getInstance().delete(getInstance().objects("MsArea"));
              getInstance().delete(getInstance().objects("MsLine"));
            });

            //console.log("data 1")
            res.data.ObjPlant.map(function(data, i) {
              //console.log("datanya")
              //console.log(data)
              DB.write(() => {
                DB.create(
                  "MsPlant",
                  { PlantID: data.PlantID, PlantDesc: data.PlantDesc },
                  true
                );
              });
            });

            //console.log("data 2")
            res.data.ObjGroupLine.map(function(data, i) {
              //console.log("datanya")
              //console.log(data)
              DB.write(() => {
                DB.create("MsGroupLine", {
                  GroupLineID: data.GroupLineID,
                  GroupLineIDDesc: data.GroupLineDesc,
                  PlantID: data.PlantID,
                  GroupLineIDAbb: data.GroupLineAbb
                });
              });
            });

            //console.log("data 3")
            res.data.ObjUser.map(function(data, i) {
              // console.log("datanya")
              // console.log(data)
              DB.write(() => {
                DB.create("MsUser", {
                  UserID: data.UserID,
                  Username: data.Username,
                  FullName: data.FullName,
                  Email: data.Email,
                  Roles: data.Roles,
                  isLogin: data.isLogin,
                  PlantID: data.PlantID,
                  Position: data.Position
                });
              });
            });

            //console.log("data 3")
            res.data.ObjUserAuditee.map(function(data, i) {
              // console.log("datanya")
              // console.log(data)
              DB.write(() => {
                DB.create("MsUserAuditee", {
                  PK_MsUserAuditee_ID: i + 1,
                  UserID: data.UserID,
                  Username: data.Username,
                  FullName: data.FullName,
                  Email: data.Email,
                  PlantID: data.PlantID,
                  GroupLineID: data.GroupLineID
                });
              });
            });

            //console.log("data 4")
            res.data.ObjArea.map(function(data, i) {
              // console.log("datanya")
              // console.log(data)
              DB.write(() => {
                DB.create("MsArea", {
                  groupLineID: data.groupLineID,
                  lineID: data.lineID,
                  areaID: data.areaID,
                  areaDesc: data.areaDesc
                });
              });
            });

            //console.log("data 5")
            res.data.ObjLine.map(function(data, i) {
              // console.log("datanya")
              // console.log(data)
              DB.write(() => {
                DB.create("MsLine", {
                  PK_LineID: i + 1,
                  LineID: data.LineID,
                  LineIDDesc: data.LineDesc,
                  GroupLineID: data.GroupLineID
                });
              });
            });

            //console.log("data 6")
            // this.setState({
            //     invalid: (<View></View>)
            // })
            res.data.ObjSystemParameter.map(function(data, i) {
              // console.log("datanya")
              console.log(data);
              try {
                DB.write(() => {
                  DB.create(
                    "PersistentData",
                    { Key: data.key, Value: data.value },
                    true
                  );
                });
              } catch (error) {
                DB.write(() => {
                  DB.create(
                    "PersistentData",
                    { Key: data.key, Value: data.value },
                    true
                  );
                });
                ErrorReport.log(error);
              }
            });

            console.log("data 7");

            //Cek database
            this.result_filter = this.MsUser.filtered(
              'Email ==[c] "' + this.state.userid + '"'
            );

            Queries.setAllUserNotLogin();
            getInstance().write(() => {
              getInstance().create(
                "MsUser",
                { UserID: this.result_filter[0].UserID, isLogin: 1 },
                true
              );
            });

            let notificationID = Queries.getPersistentData("NotificationID");
            console.log("CCAAPI");
            console.log(
              Constants.APILink +
                "notif/PostNotificationID?username=" +
                this.result_filter[0].Username +
                "&notificationid=" +
                notificationID +
                `apuytoken`
            );

            Axios.get(
              Constants.APILink +
                "notif/PostNotificationID?username=" +
                this.result_filter[0].Username +
                "&notificationid=" +
                notificationID,
              {
                headers: {
                  Accept: "application/json",
                  "Content-Type": "application/json",
                  Authorization: "Bearer " + Queries.getPersistentData("token")
                }
              }
            );
            // .then((result) => { Actions.HomeScreen(); })
            // .catch((exception) => { Alert.alert('Error', JSON.stringify(exception)) });

            this.setState({
              loaderStatus: false
            });

            //console.log("data 8")
            Actions.HomeScreen({ type: "replace" });
          })
          .catch(err => {
            //console.log("log gagal")
            //console.log(err)
            this.setState({
              invalid: (
                <Button
                  iconLeft
                  danger
                  style={{
                    width: responsiveWidth(75),
                    backgroundColor: "#f08080"
                  }}
                >
                  <Icon
                    name="times"
                    style={{ color: "#c00", paddingRight: 10 }}
                  />
                  <Text
                    style={{
                      fontFamily: "MyriadPro-Regular",
                      textAlign: "left",
                      width: responsiveWidth(65)
                    }}
                  >
                    The username / password combination is not valid.
                  </Text>
                </Button>
              )
            });

            this.setState({
              loaderStatus: false
            });
          });

        // //Cek database
        // this.result_filter = this.MsUser.filtered('Username ==[c] "'+ this.state.userid +'"');

        // //console.log(this.result_filter.length);

        // if(!this.result_filter.length){
        //     this.setState({
        //         invalid: (<Button iconLeft danger style={{width: responsiveWidth(75), backgroundColor:'#f08080'}}>
        //                     <Icon name="times" style={{color:'#c00', paddingRight:10}} />
        //                     <Text style={{fontFamily:'MyriadPro-Regular', textAlign:'left', width:responsiveWidth(65)}}>The username / password combination is not valid.</Text>
        //                 </Button>)
        //     })
        // }
        // else{
        //     this.setState({
        //         invalid: (<View></View>)
        //     })

        //     Queries.setAllUserNotLogin();
        //     getInstance().write(() => {
        //         getInstance().create('MsUser', {UserID: this.result_filter[0].UserID, isLogin: 1}, true);
        //     });

        //     let notificationID = Queries.getPersistentData('NotificationID');
        //     Axios.get(Constants.APILink + 'notif/PostNotificationID?username=' + this.state.userid + '&notificationid=' + notificationID)
        //     // .then((result) => { Actions.HomeScreen(); })
        //     // .catch((exception) => { Alert.alert('Error', JSON.stringify(exception)) });

        //     Actions.HomeScreen();
        // }

        // //this.result_filter = this.MsUser.filtered('UserID = "Adhitya"');
        // //console.log(this.result_filter)
      }
    });
  }

  _onPressButton(e) {
    console.log("apuy test");
    this.setState({
      loaderStatus: true
    });

    requestAnimationFrame(() => {
      //console.log(getInstance().objects('MsLine'));

      if (this.state.password !== "1234") {
        this.setState({
          invalid: (
            <Button
              iconLeft
              danger
              style={{ width: responsiveWidth(75), backgroundColor: "#f08080" }}
            >
              <Icon name="times" style={{ color: "#c00", paddingRight: 10 }} />
              <Text
                style={{
                  fontFamily: "MyriadPro-Regular",
                  textAlign: "left",
                  width: responsiveWidth(65)
                }}
              >
                The username / password combination is not valid.
              </Text>
            </Button>
          )
        });

        this.setState({
          loaderStatus: false
        });
      } else {
        console.log("CCAAPI");
        console.log(
          Constants.APILink +
            `Master/GetMasterDataAll?username=` +
            this.state.userid +
            `apuytoken`
        );
        //Check user on server
        Axios.get(
          Constants.APILink +
            `Master/GetMasterDataAll?username=` +
            this.state.userid,
          {
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Bearer " + Queries.getPersistentData("token")
            }
          }
        )
          .then(res => {
            //console.log("log berhasil")
            //console.log(res)

            DB.write(() => {
              getInstance().delete(getInstance().objects("MsPlant"));
              getInstance().delete(getInstance().objects("MsGroupLine"));
              getInstance().delete(getInstance().objects("MsUser"));
              getInstance().delete(getInstance().objects("MsUserAuditee"));
              getInstance().delete(getInstance().objects("MsArea"));
              getInstance().delete(getInstance().objects("MsLine"));
            });

            //console.log("data 1")
            res.data.ObjPlant.map(function(data, i) {
              //console.log("datanya")
              //console.log(data)
              try {
                DB.write(() => {
                  DB.create("MsPlant", {
                    PlantID: data.PlantID,
                    PlantDesc: data.PlantDesc
                  });
                });
              } catch (error) {
                DB.write(() => {
                  DB.create(
                    "MsPlant",
                    { PlantID: data.PlantID, PlantDesc: data.PlantDesc },
                    true
                  );
                });

                ErrorReport.log(error);
              }
            });

            //console.log("data 2")
            res.data.ObjGroupLine.map(function(data, i) {
              //console.log("datanya")
              //console.log(data)
              try {
                DB.write(() => {
                  DB.create("MsGroupLine", {
                    GroupLineID: data.GroupLineID,
                    GroupLineIDDesc: data.GroupLineDesc,
                    PlantID: data.PlantID,
                    GroupLineIDAbb: data.GroupLineAbb
                  });
                });
              } catch (error) {
                DB.write(() => {
                  DB.create(
                    "MsGroupLine",
                    {
                      GroupLineID: data.GroupLineID,
                      GroupLineIDDesc: data.GroupLineDesc,
                      PlantID: data.PlantID,
                      GroupLineIDAbb: data.GroupLineAbb
                    },
                    true
                  );
                });
                ErrorReport.log(error);
              }
            });

            //console.log("data 3")
            res.data.ObjUser.map(function(data, i) {
              // console.log(data)
              try {
                DB.write(() => {
                  DB.create("MsUser", {
                    UserID: data.UserID,
                    Username: data.Username,
                    FullName: data.FullName,
                    Email: data.Email,
                    Roles: data.Roles,
                    isLogin: data.isLogin,
                    PlantID: data.PlantID,
                    Position: data.Position
                  });
                });
              } catch (error) {
                DB.write(() => {
                  DB.create(
                    "MsUser",
                    {
                      UserID: data.UserID,
                      Username: data.Username,
                      FullName: data.FullName,
                      Email: data.Email,
                      Roles: data.Roles,
                      isLogin: data.isLogin,
                      PlantID: data.PlantID,
                      Position: data.Position
                    },
                    true
                  );
                });
                //console.log(error);
                ErrorReport.log(error);
              }
            });

            //console.log("data 3")
            res.data.ObjUserAuditee.map(function(data, i) {
              // console.log("datanya")
              // console.log(data)
              try {
                DB.write(() => {
                  DB.create("MsUserAuditee", {
                    PK_MsUserAuditee_ID: i + 1,
                    UserID: data.UserID,
                    Username: data.Username,
                    FullName: data.FullName,
                    Email: data.Email,
                    PlantID: data.PlantID,
                    GroupLineID: data.GroupLineID
                  });
                });
              } catch (error) {
                DB.write(() => {
                  DB.create(
                    "MsUserAuditee",
                    {
                      PK_MsUserAuditee_ID: i + 1,
                      UserID: data.UserID,
                      Username: data.Username,
                      FullName: data.FullName,
                      Email: data.Email,
                      PlantID: data.PlantID,
                      GroupLineID: data.GroupLineID
                    },
                    true
                  );
                });
                ErrorReport.log(error);
              }
            });

            console.log("data 4");
            res.data.ObjArea.map(function(data, i) {
              // console.log("datanya")
              // console.log(data)
              try {
                DB.write(() => {
                  DB.create("MsArea", {
                    groupLineID: data.groupLineID,
                    lineID: data.lineID,
                    areaID: data.areaID,
                    areaDesc: data.areaDesc
                  });
                });
              } catch (error) {
                DB.write(() => {
                  DB.create(
                    "MsArea",
                    {
                      groupLineID: data.groupLineID,
                      lineID: data.lineID,
                      areaID: data.areaID,
                      areaDesc: data.areaDesc
                    },
                    true
                  );
                });
                ErrorReport.log(error);
              }
            });

            console.log("data 5");
            res.data.ObjLine.map(function(data, i) {
              // console.log("datanya")
              // console.log(data)
              try {
                DB.write(() => {
                  DB.create("MsLine", {
                    PK_LineID: i + 1,
                    LineID: data.LineID,
                    LineIDDesc: data.LineDesc,
                    GroupLineID: data.GroupLineID
                  });
                });
              } catch (error) {
                DB.write(() => {
                  DB.create(
                    "MsLine",
                    {
                      PK_LineID: i + 1,
                      LineID: data.LineID,
                      LineIDDesc: data.LineDesc,
                      GroupLineID: data.GroupLineID
                    },
                    true
                  );
                });
                ErrorReport.log(error);
              }
            });

            //console.log("data 6")
            // this.setState({
            //     invalid: (<View></View>)
            // })
            console.log("data 6");
            res.data.ObjSystemParameter.map(function(data, i) {
              console.log("datanya");
              console.log(data.key);
              try {
                Queries.setPersistentData(data.key, data.value);
              } catch (error) {
                ErrorReport.log(error);
              }
            });

            console.log("data 7");

            //Cek database
            this.result_filter = this.MsUser.filtered(
              'Email ==[c] "' + this.state.userid + '"'
            );

            Queries.setAllUserNotLogin();
            getInstance().write(() => {
              getInstance().create(
                "MsUser",
                { UserID: this.result_filter[0].UserID, isLogin: 1 },
                true
              );
            });

            let notificationID = Queries.getPersistentData("NotificationID");
            console.log("CCAAPI");
            console.log(
              Constants.APILink +
                "notif/PostNotificationID?username=" +
                this.result_filter[0].Username +
                "&notificationid=" +
                notificationID +
                `apuytoken`
            );

            Axios.get(
              Constants.APILink +
                "notif/PostNotificationID?username=" +
                this.result_filter[0].Username +
                "&notificationid=" +
                notificationID,
              {
                headers: {
                  Accept: "application/json",
                  "Content-Type": "application/json",
                  Authorization: "Bearer " + Queries.getPersistentData("token")
                }
              }
            );
            // .then((result) => { Actions.HomeScreen(); })
            // .catch((exception) => { Alert.alert('Error', JSON.stringify(exception)) });

            this.setState({
              loaderStatus: false
            });
            console.log("data 8");
            Actions.HomeScreen({ type: "replace" });
          })
          .catch(err => {
            console.log("log gagal");
            console.log(err);
            this.setState({
              invalid: (
                <Button
                  iconLeft
                  danger
                  style={{
                    width: responsiveWidth(75),
                    backgroundColor: "#f08080"
                  }}
                >
                  <Icon
                    name="times"
                    style={{ color: "#c00", paddingRight: 10 }}
                  />
                  <Text
                    style={{
                      fontFamily: "MyriadPro-Regular",
                      textAlign: "left",
                      width: responsiveWidth(65)
                    }}
                  >
                    The username / password combination is not valid.
                  </Text>
                </Button>
              )
            });

            this.setState({
              loaderStatus: false
            });
          });

        // //Cek database
        // this.result_filter = this.MsUser.filtered('Username ==[c] "'+ this.state.userid +'"');

        // //console.log(this.result_filter.length);

        // if(!this.result_filter.length){
        //     this.setState({
        //         invalid: (<Button iconLeft danger style={{width: responsiveWidth(75), backgroundColor:'#f08080'}}>
        //                     <Icon name="times" style={{color:'#c00', paddingRight:10}} />
        //                     <Text style={{fontFamily:'MyriadPro-Regular', textAlign:'left', width:responsiveWidth(65)}}>The username / password combination is not valid.</Text>
        //                 </Button>)
        //     })
        // }
        // else{
        //     this.setState({
        //         invalid: (<View></View>)
        //     })

        //     Queries.setAllUserNotLogin();
        //     getInstance().write(() => {
        //         getInstance().create('MsUser', {UserID: this.result_filter[0].UserID, isLogin: 1}, true);
        //     });

        //     let notificationID = Queries.getPersistentData('NotificationID');
        //     Axios.get(Constants.APILink + 'notif/PostNotificationID?username=' + this.state.userid + '&notificationid=' + notificationID)
        //     // .then((result) => { Actions.HomeScreen(); })
        //     // .catch((exception) => { Alert.alert('Error', JSON.stringify(exception)) });

        //     Actions.HomeScreen();
        // }

        // //this.result_filter = this.MsUser.filtered('UserID = "Adhitya"');
        // //console.log(this.result_filter)
      }
    });
  }

  _keyboardWillShow(e) {
    console.log("keyboard showed");
    if (Queries.getPersistentData("keyboardHeight") == null) {
      console.log("insert to local db");
      Queries.setPersistentData(
        "keyboardHeight",
        e.endCoordinates.height.toString()
      );
    }
  }

  componentDidMount() {
    _keyboardWillShowSubscription = Keyboard.addListener("keyboardDidShow", e =>
      this._keyboardWillShow(e)
    );
  }
  componentWillUnmount() {
    console.log("keyboard removed");
    _keyboardWillShowSubscription.remove();
  }

  render() {
    return (
      // <Container backgroundColor='white'>
      //     <StatusBar
      //         backgroundColor='white'
      //         barStyle="default"
      //     />
      //     <Content keyboardShouldPersistTaps="always">
      //         <View style={{ justifyContent: 'center', alignItems: 'center', alignSelf: 'center', height: responsiveHeight(90) }}>
      //             <Spinner visible={this.state.loaderStatus} textContent={"Please Wait"} textStyle={{ color: '#fff' }} cancelable={false} />
      //             <Image style={{ resizeMode: 'center', width: responsiveWidth(35), height: responsiveWidth(35) }} source={require('../Images/quinsy-detective-black.png')} />
      //             <Text style={{ fontSize: responsiveFontSize(4), fontFamily: 'MyriadPro-Regular' }}>Audit Chamber</Text>
      //             <View>
      //                 {this.state.invalid}
      //             </View>
      //             <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: responsiveHeight(2) }}>
      //                 <Image style={{ width: responsiveWidth(8), height: responsiveWidth(8) }} source={require('../Images/user-login.png')} />
      //                 <Input onChange={(event) => this.setState({ userid: event.nativeEvent.text })} style={{ backgroundColor: '#cecece', color: 'black', width: responsiveWidth(50), marginLeft: responsiveWidth(1), paddingTop: responsiveHeight(1.5), height: responsiveHeight(7), fontSize: responsiveFontSize(2) }} placeholder='UserID' />
      //             </View>
      //             <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: responsiveHeight(2) }}>
      //                 <Image style={{ width: responsiveWidth(8), height: responsiveWidth(8) }} source={require('../Images/key.png')} />
      //                 <Input onChange={(event) => this.setState({ password: event.nativeEvent.text })} secureTextEntry={true} style={{ backgroundColor: '#cecece', color: 'black', width: responsiveWidth(50), marginLeft: responsiveWidth(1), paddingTop: responsiveHeight(1.5), height: responsiveHeight(7), fontSize: responsiveFontSize(2) }} placeholder='Password' />
      //             </View>

      //             <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: responsiveHeight(1), marginBottom: responsiveHeight(4) }}>
      //                 <Button style={{ backgroundColor: '#CC0000', marginTop: responsiveHeight(1), height: responsiveHeight(8), width: responsiveWidth(75), alignItems: 'center', justifyContent: 'center' }} onPress={this._onPressButton.bind(this)}>
      //                     <Image style={{ width: responsiveWidth(11), height: responsiveHeight(7) }} source={require('../Images/quinsy-detective-white.png')} />
      //                     <Text style={{ fontWeight: 'bold', height: responsiveHeight(7), textAlignVertical: 'center', fontSize: newResponsiveFontSize(1.7), fontFamily: 'MyriadPro-Regular' }}>Login</Text>
      //                 </Button>
      //             </View>

      //             <Image style={{ width: responsiveWidth(30), height: responsiveWidth(13) }} source={require('../Images/quinsys.png')} />

      //             <Text style={{ marginTop: responsiveWidth(2), fontFamily: 'MyriadPro-Regular' }}> Copyright <Icon name="copyright" style={{ color: '#000', paddingRight: responsiveWidth(2) }} /> 2017 Coca Cola Amatil </Text>

      //         </View>
      //     </Content>
      // </Container>
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "#F5FCFF"
        }}
      >
        <Spinner
          visible={this.state.loaderStatus}
          textContent={"Please Wait"}
          textStyle={{ color: "#fff" }}
          cancelable={false}
        />
        <ADLoginView
          context={ReactNativeAD.getContext(config.client_id)}
          needLogout={true}
          hideAfterLogin={true}
          onSuccess={this._onSuccessAzureAd.bind(this)}
        />
      </View>
    );
  }
}
