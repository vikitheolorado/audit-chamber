import React, { Component } from 'react';
import { Container, Content, Body, ListItem, Text, CheckBox, Button, Segment, Input, Item, Left, Thumbnail } from 'native-base';
import { Image, View, Dimensions, StatusBar } from 'react-native';
const { width, height } = Dimensions.get('window');
import { Actions } from 'react-native-router-flux';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from '../Themes/Responsive';
const timer = require('react-native-timer');
var Spinner = require('react-native-spinkit');

var arr = [];
var tempTime = "";

export default class Splash extends Component {

    constructor(props) {
        super(props);
        this.state = {

        };
    }
    
    componentWillMount() {
        timer.setTimeout(
            this, 'hideMsg', () => this._timer(), 1000
        )
    }

    _timer() {
        console.log("panggil");
        Actions.Login()
    }


    render() {
        return (
            <View backgroundColor='#c00' style={{justifyContent:'center', alignItems:'center', height:responsiveHeight(100), width:responsiveWidth(100)}}>
                <StatusBar
                    backgroundColor='#c00'
                    barStyle="default"
                />
                <Image style={{ resizeMode: 'contain', width: responsiveWidth(60), height: responsiveWidth(60) }} source={require('../Images/quinsy-detective-white.png')} />
                <Spinner type={'ThreeBounce'} color={'white'} size={responsiveWidth(20)} />
            </View>
        );
    }
}