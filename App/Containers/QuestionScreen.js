// @flow

import React, { Component } from "react";
import {
  WebView,
  NativeModules,
  Platform,
  Dimensions,
  Switch,
  Keyboard,
  View,
  Image,
  Text,
  StyleSheet,
  Alert,
  Modal,
  KeyboardAvoidingView,
  TouchableHighlight,
  TouchableWithoutFeedback,
  TouchableOpacity,
  ScrollView,
  ListView,
  Picker,
  TextInput
} from "react-native";
import { Metrics, Images } from "../Themes";
import { Actions } from "react-native-router-flux";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
  newResponsiveFontSize
} from "../Themes/Responsive";
import Camera from "react-native-camera";
import Accordion from "react-native-accordion";
import { ListItem, Input, Button } from "native-base";
import ListItemMedia from "../Components/ListItemMedia";
import { PDFAuditReport } from "../Components/PDFResult";
import Timer from "../Components/Timer";
import { getInstance } from "../Data/DB/DBHelper";
import Queries from "../Data/Queries";
import PDFView from "react-native-pdf-view";
import RNHTMLtoPDF from "react-native-html-to-pdf";
var dateFormat = require("dateformat");
import Moment from "moment";
var RNFS = require("react-native-fs");
import axios from "axios";
import Constants from "../Data/Constants";
import Spinner from "react-native-loading-spinner-overlay";
const timer = require("react-native-timer");
import Icon from "react-native-vector-icons/FontAwesome";
import Toast from "react-native-smart-toast";
import ImageResizer from "react-native-image-resizer";
import AccordionNew from "react-native-collapsible/Accordion";
import ModalPicker from "react-native-modal-picker";
const { width, height } = Dimensions.get("window");
import style from "../Themes/Fonts";
import Pdf from "react-native-pdf";
import Video from "react-native-video";
import RNCompress from "react-native-compress";

export default class QuestionScreen extends Component {
  constructor(props) {
    super(props);
    this.Base64Files = [];
    this.MsUser = Queries.getCurrentUser();
    this.MsPlant = getInstance()
      .objects("MsPlant")
      .filtered("PlantID = " + this.MsUser[0].PlantID + " AND active = true");
    //console.log(this.props);
    var currentplant = Queries.getPlantDescByID(this.props.plantid);
    var keyboardHeight = Queries.getPersistentData("keyboardHeight");

    var pictureHeight = Queries.getPersistentData("pictureHeight");
    var pictureWidth = Queries.getPersistentData("pictureWidth");
    var pictureQuality = Queries.getPersistentData("pictureQuality");

    this.state = {
      pathPDF: "",
      isCompleted: this.props.isComplete,
      auditScheduleDetailID: this.props.auditScheduleDetailID,
      isOverdue: this.props.isOverdue,
      scheduleCode: this.props.scheduleCode,
      isAuditHasBeenComplete: false,
      currentPDFPath: "",
      listofarea: [],
      modalUserVisible: false,
      modalSignOffView: false,
      modalSignOffAuditeeView: false,
      modalSignOffAzureADView: false,
      modalQuitSave: false,
      modalFindingPicture: false,
      modalPDFPreview: false,
      fullName: this.MsUser[0].FullName,
      userName: this.MsUser[0].Username,
      userPosition: this.MsUser[0].Position,
      userPlant: this.MsPlant[0].PlantDesc,
      questionFooterStatus: this.props.isComplete
        ? "exitsubmit"
        : "exitsummary",
      plant: currentplant,
      groupline: this.props.grouplinedesc,
      week: "Week " + this.props.week,
      year: this.props.year,
      //Should be state
      // auditScheduleDetailID : 1,
      // auditType : 'GMP',
      // templateCode : 1,
      // groupLineID: 4,

      startDate: new Date(),
      endDate: new Date(),
      finalScore: 0.0,
      totalScore: 0.0,
      calculatedScore: 0.0,
      currentCompletion: 0,
      totalCompletion: 0,
      selectedAttachment: [],
      savedAttachment: [],
      questionData: Queries.getArrayQuestion(
        this.props.auditType,
        this.props.templateCode,
        this.props.auditScheduleDetailID
      ),
      viewedSectionID: -1,
      viewedQuestionID: -1,
      viewedQuestionIndex: -1,
      choosedAnswerID: -1,
      findingState: 0,
      listArea: [],
      selectedArea: "",
      notesText: "",
      editedPictureIndex: -1,
      listCategory: "",
      totalFinding: 0,
      arrayFinding: [],
      signOffDate: new Date(),
      fullnameAuditee: "",
      passwordAuditor: "",
      usernameAuditee: "",
      passwordAuditee: "",
      emailAuditee: "",
      loaderStatus: false,
      finishText: "Finish",
      isVideo: false,
      captureMode: Camera.constants.CaptureMode.still,
      isRecording: false,
      isLight: false,
      isFront: false,
      cameraRender: false,
      timerID: -1,
      keyboardHeight: keyboardHeight == null ? 200 : parseInt(keyboardHeight),
      pictureHeight: pictureHeight == null ? 600 : parseInt(pictureHeight),
      pictureWidth: pictureWidth == null ? 400 : parseInt(pictureWidth),
      pictureQuality: pictureQuality == null ? 50 : parseInt(pictureQuality),
      arrAuditee: [],
      selectedAuditee: "",
      cameraPreview: false,
      pathCameraPreview: "",
      listFindings: <View />,
      viewDeleteFindingList: false,
      viewEditFindingList: false,
      editCategoryLabel: "",
      editAreaLabel: "",
      editCommentLabel: "",
      auditFindingListIDMedia: "",
      finishSaveToNative: false,
      buttonSaveFindingColor: "#888888",
      buttonCameraColor: "rgb(237,125,49)",
      indexListMediaToDelete: "",
      maxRecordTime: 30000
    };

    this.selectedAuditeeLabel = "";

    // this.intervalID = setInterval(() => {
    //   this.setState({
    //     secondFromStart: this.state.secondFromStart + 1,
    //   });
    // }, 1000);
  }

  _renderHeader(section) {
    return <View>{section.title}</View>;
  }

  _renderContent(section) {
    return <View>{section.content}</View>;
  }

  _doLogout(e) {
    Queries.setAllUserNotLogin();
    this.setState({ modalUserVisible: false });
    Actions.Login({ type: "replace" });
  }

  async PDFAuditReportYa(value) {
    console.log("masuk generate pdf 9 Feb 18");
    // console.log(value)
    var string_result = "";

    //opening tag html & body, dan head
    var stringHead = `
        <!DOCTYPE html>
        <html>

        <head>
            <style>
                body {
                    margin: 15px;
                    font-family: FrutigerLTStd-Roman;
                }
                .header {
                    align-content: stretch;
                    font-size: 16px;
                    font-family: "Myriad Pro";
                    font-weight: bold;
                }
                .content {
                    font-size: 12px;
                    font-family: "Myriad Pro";
                }
                .content-total {
                    font-size: 32px;
                    font-family: "Myriad Pro";
                    font-weight: bold;
                }
                .content-total-over {
                    font-size: 32px;
                    font-family: "Myriad Pro";
                    color: forestgreen;
                    font-weight: bold;
                }
                .content-total-less {
                    font-size: 32px;
                    font-family: "Myriad Pro";
                    color: red;
                    font-weight: bold;
                }
                .content-response-over {
                    font-size: 12px;
                    font-family: "Myriad Pro";
                    background-color: forestgreen;
                    font-weight: bold;
                }
                .content-response-less {
                    font-size: 12px;
                    font-family: "Myriad Pro";
                    background-color: #c00;
                    font-weight: bold;
                }
                .content-border {
                    font-size: 12px;
                    font-family: "Myriad Pro";
                }
                .logo {
                    text-align: left;
                    margin-left: 10px;
                }
                .table-border-empty {
                    border: none;
                    border-width: 1px;
                }
            </style>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        </head>

        <body>`;

    //closing tag
    var stringClosing = "</table></body></html>";

    //opening tag table, logo, dan judul report
    var stringTitle = `
    <table style="width:100%; table-layout:fixed" cellspacing="0" cellpadding="2">
    <tr>
        <td colspan="1" rowspan="2" style="text-align:center"><img class="logo" style="display: table-cell; width: 60%" src="https://ccaidauesprdquinrsg002.blob.core.windows.net/auditchamber/quinsys.png" />
        </td>
        <td colspan="5" rowspan="1" style="text-align:center" align="center">
            <div class="header">QUINSYS - AUDIT CHAMBER </div>
        </td>
    </tr>
    <tr>
        <td colspan="5" rowspan="1" style="text-align:center" align="center">
            <div class="header">GMP/6S ` + value.audit_type + ` Audit</div>
        </td>
    </tr>
    <tr>
        <td width="15%"></td>
        <td width="25%"></td>
        <td width="10%"></td>
        <td width="20%"></td>
        <td width="15%" style="text-align:center" align="center">
            <div class="content">Score</div>
        </td>
        <td width="15%" style="text-align:center" align="center">
            <div class="content">Last ` + value.periode_type + `</div>
        </td>
    </tr>

    `;
    //define 'Conducted On', score saat ini, score audit sebelumnya
    var stringHeader =
      `
      <tr>
          <td colspan="1">
              <div class="content">Conducted On</div>
          </td>
          <td colspan="2" style="border:solid 1px;text-align:left;">
              <div class="content-border">` +
      value.conducted_date +
      `</div>
          </td>
          <td colspan="1">
              <div class="content"></div>
          </td>
          <td colspan="1" rowspan="4" style="border:solid;border-right:none;border-width:1px;word-wrap:break-word;text-align:center" align="center" valign="middle">
              <div class="content-total-over">` +
      value.current_score +
      `</div>
          </td>
          <td colspan="1" rowspan="4" style="border:solid;border-width:1px;word-wrap:break-word;text-align:center" align="center" valign="middle">
              <div class="content-total">` +
      (value.last_score == null ? '-' : value.last_score) +
      `</div>
          </td>
      </tr>
    `;

    //define informasi header yang lain (Prepared By, Plant, Line)
    var stringHeader2 =
      ` <tr>
          <td colspan="1">
              <div class="content">Prepared By</div>
          </td>
          <td colspan="2" style="border:solid 1px;text-align:left;">
              <div class="content-border">` +
      value.prepared_by +
      `</div>
          </td>
      </tr>
      <tr>
          <td colspan="1">
              <div class="content">Plant</div>
          </td>
          <td colspan="2" style="border:solid 1px;text-align:left;">
              <div class="content-border">` +
      value.plant_data +
      `</div>
          </td>
      </tr>
      <tr>
          <td colspan="1">
              <div class="content">Line</div>
          </td>
          <td colspan="2" style="border:solid 1px;text-align:left;">
              <div class="content-border">` +
      value.line_data +
      `</div>
          </td>
      </tr>
    `;

    //informasi score dan judul table detail
    var stringHeader3 =
      `
      <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td style="text-align:center" align="center">
              <div class="content">` +
      value.current_count +
      `</div>
          </td>
          <td style="text-align:center" align="center">
              <div class="content">` +
      value.last_count +
      `</div>
          </td>
      </tr>
      <tr>
          <td colspan="6" rowspan="1" style="text-align:center" align="center">
              <div class="content">Audit Details - ` +
      value.current_count +
      `</div>
          </td>
      </tr>
      <tr>
          <td colspan="3" rowspan="1" style="border:solid;border-right:none;border-width:1px;background:#d9d9d9;text-align:center;" align="center">
              <div class="content">Question</div>
          </td>
          <td colspan="1" rowspan="1" style="border:solid;border-right:none;border-width:1px;background:#d9d9d9;text-align:center;" align="center">
              <div class="content">Response</div>
          </td>
          <td colspan="2" rowspan="1" style="border:solid;border-width:1px;background:#d9d9d9;text-align:center;" align="center">
              <div class="content">Details</div>
          </td>
      </tr>
    `;

    //table sign off
    var stringFooter =
      `
      <tr>
          <td colspan="6" rowspan="1"><br /><br /><br /></td>
      </tr>
      <tr>
          <td colspan="3" rowspan="1" style="border:solid;border-width:1px;background:#d9d9d9" align="center">
              <div class="content">Sign Off</div>
          </td>
      </tr>
      <tr>
          <td colspan="1" rowspan="2" style="border:solid;border-top:none;border-right:none;border-width:1px;word-wrap:break-word" align="center">
              <div class="content">Auditor</div>
          </td>
          <td colspan="2" rowspan="1" style="border:solid;border-top:none;border-width:1px;word-wrap:break-word" align="center">
              <div class="content" align="left">` +
      value.auditor_name +
      `</div>
          </td>
      </tr>
      <tr>
          <td colspan="2" rowspan="1" style="border:solid;border-top:none;border-width:1px;word-wrap:break-word" align="center">
              <div class="content" align="left">` +
      value.auditor_signoff_date +
      `</div>
          </td>
      </tr>
      <tr>
          <td colspan="1" rowspan="2" style="border:solid;border-top:none;border-right:none;border-width:1px;word-wrap:break-word" align="center">
              <div class="content">Auditee</div>
          </td>
          <td colspan="2" rowspan="1" style="border:solid;border-top:none;border-width:1px;word-wrap:break-word" align="center">
              <div class="content" align="left">` +
      value.auditee_name +
      `</div>
          </td>
      </tr>
      <tr>
          <td colspan="2" rowspan="1" style="border:solid;border-top:none;border-width:1px;word-wrap:break-word" align="center">
              <div class="content" align="left">` +
      value.auditee_signoff_date +
      `</div>
          </td>
      </tr>
    `;

    var stringDetails = "";

    value.arr_summary.map(function(summary, index) {
      // console.log('masuk parse')
      ////insert row informasi kategori ('RINGKAS', 'RAPI', dll) dan total score
      stringDetails +=
        `
        <tr>
            <td colspan="2" rowspan="1" style="border-left:solid 1px;word-wrap:break-word;text-align:left;" align="center">
                <div class="content" style="padding:15px 0px 15px 35px;">` +
        summary.name +
        `</div>
            </td>
            <td colspan="4" rowspan="1" style="border-right:solid 1px;word-wrap:break-word;text-align:right;" align="center">
                <div class="content-border" style="padding:15px 35px 15px 0px;">` +
        summary.summary_score +
        `</div>
            </td>
        </tr> 
      `;

      var arr_length = summary.result.length;
      var back_answer = "";
      if (arr_length > 0) {
        for (i = 0; i < arr_length; i++) {
          ////untuk warna background
          var style = "";
          if (summary.result[i].response == "Yes") {
            style = "content-response-over";
            back_answer = "forestgreen";
          } else {
            style = "content-response-less";
            back_answer = "#c00";
          }
          ////pertanyaan
          stringDetails +=
            `<tr>
                <td colspan="3" rowspan="1" style="border:solid;border-right:none;border-width:1px;word-wrap:break-word" align="center">
                    <div align="left" class="content" style="padding: 5px 0px 5px 0px">` +
            summary.result[i].question +
            `</div>
                </td>`;
          ////response jawaban (yes/no)
          stringDetails +=
            `
            <td colspan="1" rowspan="1" style="border:solid;border-right:none;border-width:1px;word-wrap:break-word;text-align:center;color:white;background:` +
            back_answer +
            `" align="center">
                <div class="` +
            style +
            `">` +
            summary.result[i].response +
            `</div>
            </td>
          `;
          ////keterangan

          stringDetails += `<td colspan="2" rowspan="1" style="border:solid;border-width:1px;word-wrap:break-word" align="center">
            <div align="left" class="content">
          `;

          summary.result[i].detail.map(function(result, key) {
            stringDetails +=
              "<div>" +
              result.comments +
              "(AREA : " +
              result.area +
              ")" +
              "</div>";
          });

          stringDetails += "</div></td></tr>";
        }
      }
    });

    string_result =
      stringHead +
      stringTitle +
      stringHeader +
      stringHeader2 +
      stringHeader3 +
      stringDetails +
      stringFooter +
      stringClosing;
    // console.log(stringHead + stringTitle + stringHeader + stringHeader2)
    // console.log(stringHeader3)
    // console.log(stringDetails)
    // console.log(stringFooter + stringClosing)
    let options = {
      html: string_result,
      directory: "docs",
      fileName: this.props.scheduleCode + "-" + this.props.grouplinedesc,
      base64: true,
      padding: 24
    };

    return new Promise(async (resolve, reject) => {
      try {
        const results = await RNHTMLtoPDF.convert(options).then(data => {
          console.log("PDF Created");
          console.log(data.filePath);
          this.setState({ 
            currentPDFPath: data.filePath
          });

          // return new Promise((resolve, reject)=>{
          //   console.log('ayamhaha')
          //   resolve(data.filePath);
          // })
          resolve(data.filePath);
        });
      } catch (err) {
        console.log("PDF Error");
        console.error(err);
        reject(err);
      }
    });
  }

  generatePDF(isFinish) {
    console.log("start generatePDF");
    // Kurang psot data groupline
    let scheduleCode = this.props.scheduleCode;
    let groupLineId = this.props.groupLineID;

    let lastWeekScore = "";
    let completedTask = "";
    let totalTask = "";

    return new Promise((resolve, reject) => {
      // console.log(Constants.APILink + 'Schedule/GetLastWeekAuditScore?AuditScheduleCode='+scheduleCode+'&GroupLineID='+groupLineId)
      console.log("CCAAPI 1");
      console.log(
        Constants.APILink +
          "Schedule/GetLastWeekAuditScore?AuditScheduleCode=" +
          scheduleCode +
          "&GroupLineID=" +
          groupLineId +
          `apuytoken`
      );
      axios
        .get(
          Constants.APILink +
            "Schedule/GetLastWeekAuditScore?AuditScheduleCode=" +
            scheduleCode +
            "&GroupLineID=" +
            groupLineId,
          {
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Bearer " + Queries.getPersistentData("token")
            },
            timeout: Constants.GetTimeout
          }
        )
        .then(
          function(response) {
            console.log("berhasil");
            console.log(response);

            if (response.data.length == 0) {
              console.log("data null");
              lastWeekScore = "-";
              completedTask = "-";
              totalTask = "-";
            } else {
              lastWeekScore =
                response.data.AuditScore == "NULL"
                  ? "-"
                  : response.data.AuditScore;
              completedTask = response.data.CompletedTask;
              totalTask = response.data.TotalTask;
            }

            let newSummary = [];
            this.state.questionData.map((value, key) => {
              let result = [];
              newSummary[newSummary.length] = {
                name: value.categoryDesc,
                id: value.categoryID,
                summary_score:
                  value.questions.filter(item => {
                    return item.answer.answerID == 1;
                  }).length +
                  "/" +
                  value.questions.filter(item => {
                    return item.answer.answerID != 3;
                  }).length +
                  " Score: " +
                  //value.questions.filter((item) => { return item.answer.answerID != 3 }).length == 0 ? '0' : (value.questions.filter((item) => { return item.answer.answerID == 1 }).length * 100 / value.questions.filter((item) => { return item.answer.answerID != 3 }).length) + '%',
                  (value.questions.filter(item => {
                    return item.answer.answerID != 3; // all answer NA so 0 %
                  }).length == 0
                    ? "100"
                    : (value.questions.filter(item=>{
                      console.log('apakah required '+item.isRequired)
                      return item.answer.answerID == 2 && item.isRequired == 1; 
                    })).length > 0 
                    ? "0" :
                    Math.round(
                        value.questions.reduce((accumulator, currentValue) => {return accumulator + ((currentValue.answer.answerID == 1 ? 1 : 0) * currentValue.weight)}, 0) * 10000 /
                          value.questions.reduce((accumulator, currentValue) => {return accumulator + (currentValue.answer.answerID != 3 ? 1 : 0) * currentValue.weight}, 0)
                        // value.questions.filter(item => {
                        //   return item.answer.answerID == 1;
                        // }).length *
                        //   100 /
                        //   value.questions.filter(item => {
                        //     return item.answer.answerID != 3;
                        //   }).length *
                        //   100
                      ) / 100) + "%",
                result: result
              };

              value.questions
                .filter(item => {
                  return item.answer.answerID !== 3;
                })
                .map((item, i) => {
                  let detail = [];
                  result[result.length] = {
                    question: item.questionDesc,
                    response: item.answer.answerID == 1 ? "Yes" : "No",
                    detail: detail
                  };
                  item.answer.findings.map((result, index) => {
                    detail[detail.length] = {
                      comments: result.comments,
                      area: this.testgetAreaDescById(result.area)
                    };
                  });
                });
            });

            let value = {
              file_name: this.props.templateCode + this.props.grouplinedesc,
              path_image_logo: "https://ccaidauesprdquinrsg002.blob.core.windows.net/auditchamber/quinsys.png",
              audit_type: "Weekly",
              periode_type: "Week",
              conducted_date: dateFormat(this.signOffDate, "dddd, d mmmm yyyy"),
              prepared_by: isFinish
                ? this.state.usernameAuditee.toUpperCase()
                : " ",
              plant_data: this.state.plant,
              line_data: this.props.grouplinedesc,
              current_score: (Math.round(
                this.state.finalScore * 1000 / this.state.totalScore
              ) / 10).toString(),
              last_score: lastWeekScore,
              current_count:
                this.state.currentCompletion + "/" + this.state.totalCompletion,
              last_count: (completedTask == null ? '-' : completedTask) + "/" + (totalTask == null ? '-' : totalTask),
              //last_count: completedTask == null ? '0' : completedTask + "/" + totalTask == null ? '0' : totalTask,
              auditor_name: isFinish ? this.state.userName : " ",
              auditor_signoff_date: isFinish
                ? dateFormat(this.state.signOffDate, "dd mmmm yyyy HH:MM")
                : " ",
              auditee_name: isFinish
                ? this.state.usernameAuditee.toUpperCase()
                : " ",
              auditee_signoff_date: isFinish
                ? dateFormat(this.state.signOffDate, "dd mmmm yyyy HH:MM")
                : " ",
              arr_summary: newSummary
            }; //this.state.fullnameAuditee,

            this.PDFAuditReportYa(value)
              .then(res => { 
                resolve(res);
              })
              .catch(res => {
                console.log(res);
                reject(res);
              });
          }.bind(this)
        )
        .catch(function(error) {
            console.log(error);
            if (error && error.response && error.response.status == "401") {
              console.log("Information - Result - GetResultPage - 401");
              Queries.sessionHabis(error.response.status);
            }

          console.log(error);
          console.log('Failed get last week data');

          lastWeekScore = "N/A";
          completedTask = "N/A";
          totalTask = "N/A";

          let newSummary = [];
          this.state.questionData.map((value, key) => {
            let result = [];
            newSummary[newSummary.length] = {
              name: value.categoryDesc,
              id: value.categoryID,
              summary_score:
                value.questions.filter(item => {
                  return item.answer.answerID == 1;
                }).length +
                "/" +
                value.questions.filter(item => {
                  return item.answer.answerID != 3;
                }).length +
                " Score: " +
                (value.questions.filter(item => {
                  return item.answer.answerID != 3;
                }).length == 0
                  ? "0" :
                  Math.round(
                        value.questions.reduce((accumulator, currentValue) => {return accumulator + ((currentValue.answer.answerID == 1 ? 1 : 0) * currentValue.weight)}, 0) * 10000 /
                          value.questions.reduce((accumulator, currentValue) => {return accumulator + (currentValue.answer.answerID != 3 ? 1 : 0) * currentValue.weight}, 0)
                        // value.questions.filter(item => {
                        //   return item.answer.answerID == 1;
                        // }).length *
                        //   100 /
                        //   value.questions.filter(item => {
                        //     return item.answer.answerID != 3;
                        //   }).length *
                        //   100
                      ) / 100) + "%",
              result: result
            };

            value.questions
              .filter(item => {
                return item.answer.answerID !== 3;
              })
              .map((item, i) => {
                let detail = [];
                result[result.length] = {
                  question: item.questionDesc,
                  response: item.answer.answerID == 1 ? "Yes" : "No",
                  detail: detail
                };
                item.answer.findings.map((result, index) => {
                  detail[detail.length] = {
                    comments: result.comments,
                    area: this.testgetAreaDescById(result.area)
                  };
                });
              });
          });


          let value = {
            file_name: this.props.templateCode + this.props.grouplinedesc,
            path_image_logo: "https://ccaidauesprdquinrsg002.blob.core.windows.net/auditchamber/quinsys.png",
            audit_type: "Weekly",
            periode_type: "Week",
            conducted_date: dateFormat(this.signOffDate, "dddd, d mmmm yyyy"),
            prepared_by: this.state.fullnameAuditee.toUpperCase(),
            plant_data: this.state.plant,
            line_data: this.props.grouplinedesc,
            current_score: (Math.round(
              this.state.finalScore * 1000 / this.state.totalScore
            ) / 10).toString(),
            last_score: lastWeekScore,
            current_count:
              this.state.currentCompletion + "/" + this.state.totalCompletion,
            last_count: completedTask + "/" + totalTask,
            auditor_name: isFinish ? this.state.userName : " ",
            auditor_signoff_date: isFinish
              ? dateFormat(this.state.signOffDate, "dd mmmm yyyy HH:MM")
              : " ",
            auditee_name: isFinish
              ? this.state.usernameAuditee.toUpperCase()
              : " ",
            auditee_signoff_date: isFinish
              ? dateFormat(this.state.signOffDate, "dd mmmm yyyy HH:MM")
              : " ",
            arr_summary: newSummary
          };
          this.PDFAuditReportYa(value)
            .then(res => {
              console.log("success - after AuditReportYa - failed getlastweekscore")
              resolve(res);
            })
            .catch(res => {
              console.log("error - after AuditReportYa - failed getlastweekscore")
              console.log(res);
              reject(res);
            });

          reject(error);
        }.bind(this));
    });
  }

  _onPressQuestion(param, index) {
    this.setState({
      questionFooterStatus: "backnext",
      viewedSectionID: param.categoryID,
      viewedQuestionID: param.questions[index].questionID,
      viewedQuestionIndex: index,
      choosedAnswerID: Queries.getArrayQuestion(
        this.props.auditType,
        this.props.templateCode,
        this.props.auditScheduleDetailID
      ).filter(
        function(item) {
          return item.categoryID == param.categoryID;
        }.bind(this)
      )[0].questions[index].answer.answerID
    });
  }

  _getListQuestion(param) {
    console.log("Question List ID = " + param.categoryID);
    let dataList = Queries.getArrayQuestion(
      this.props.auditType,
      this.props.templateCode,
      this.props.auditScheduleDetailID
    ).filter(
      function(item) {
        return item.categoryID == param.categoryID;
      }.bind(this)
    );
    //let dataList = this.state.questionData
    //console.log(this.state.questionData)
    let listQuestion = dataList[0].questions.map(
      function(data, i) {
        console.log("Question List");
        //console.log(data)
        return (
          <ListItem
            style={{
              backgroundColor: "transparent",
              borderBottomColor: "black",
              borderBottomWidth: 1
            }}
          >
            <TouchableOpacity
              onPress={() => this._onPressQuestion(param, i)}
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center"
              }}
            >
              <View
                style={{
                  width: responsiveFontSize(2),
                  height: responsiveFontSize(2),
                  borderRadius: responsiveFontSize(1),
                  margin: responsiveWidth(3),
                  backgroundColor:
                    data.answer.answerID == 1
                      ? "green"
                      : data.answer.answerID == 2
                        ? "red"
                        : data.answer.answerID == 3
                          ? "orange"
                          : data.answer.answerID == -1 ? "grey" : "grey"
                }}
              />
              <Text
                style={{
                  flex: 5,
                  fontFamily: "MyriadPro-Regular",
                  color: "black",
                  fontSize: responsiveFontSize(2.5)
                }}
              >
                {data.questionDesc}
              </Text>
            </TouchableOpacity>
          </ListItem>
        );
      }.bind(this)
    );

    return listQuestion;
  }

  getRenderListSection(questionData) {
    // return questionData.map(function (data, i) {
    //   return (
    //     <View>
    //     <TouchableOpacity onPress={() => { this._onPressSection(data) }}  >
    //       <View style={[styles.groupQuestion, { height: responsiveHeight(7) }]}>
    //         <View style={this.setIndicator(data.questions.filter((item) => { return item.answer.answerID != -1 && item.answer.answerID !== 3; }).length, data.questions.filter((item) => { return item.answer.answerID !== 3; }).length)} />
    //         <Text style={{ fontFamily: 'MyriadPro-Regular', color: '#AAAAAA', fontSize: responsiveFontSize(3), flex: 8 }}>{data.categoryDesc}</Text>

    //         <Text style={{ fontFamily: 'MyriadPro-Regular', color: '#AAAAAA', fontSize: responsiveFontSize(3), flex: 2 }}>{data.questions.filter((item) => { return item.answer.answerID != -1 && item.answer.answerID !== 3; }).length}/{data.questions.filter((item) => { return item.answer.answerID !== 3; }).length}</Text>
    //         <Icon name='chevron-right' size={responsiveWidth(6)} color="#AAAAAA" />
    //       </View>
    //     </TouchableOpacity>
    //     <View style={{ backgroundColor: 'transparent', paddingLeft: responsiveWidth(2), paddingRight: responsiveWidth(2) }}>
    //         {this._getListQuestion(data)}
    //     </View>

    //     </View>
    //   );
    // }.bind(this));

    let SectionData = [];
    questionData.map(
      function(data, i) {
        let tempData = {
          title: (
            <View
              style={[styles.groupQuestion, { height: responsiveHeight(7) }]}
            >
              <View
                style={this.setIndicator(
                  data.questions.filter(item => {
                    return (
                      item.answer.answerID != -1 && item.answer.answerID !== 3
                    );
                  }).length,
                  data.questions.filter(item => {
                    return item.answer.answerID !== 3;
                  }).length
                )}
              />
              <Text
                style={{
                  fontFamily: "MyriadPro-Regular",
                  color: "black",
                  fontSize: responsiveFontSize(3),
                  flex: 8
                }}
              >
                {data.categoryDesc}
              </Text>

              <Text
                style={{
                  fontFamily: "MyriadPro-Regular",
                  color: "black",
                  fontSize: responsiveFontSize(3),
                  flex: 2
                }}
              >
                {
                  data.questions.filter(item => {
                    return (
                      item.answer.answerID != -1 && item.answer.answerID !== 3
                    );
                  }).length
                }/{
                  data.questions.filter(item => {
                    return item.answer.answerID !== 3;
                  }).length
                }
              </Text>
              <Icon
                name="chevron-right"
                size={responsiveWidth(3)}
                color="black"
              />
            </View>
          ),
          content: (
            <View
              style={{
                backgroundColor: "transparent",
                paddingLeft: responsiveWidth(2),
                paddingRight: responsiveWidth(2)
              }}
            >
              {this._getListQuestion(data)}
            </View>
          )
        };

        SectionData.push(tempData);
      }.bind(this)
    );

    return (
      <AccordionNew
        sections={SectionData}
        underlayColor={"transparent"}
        renderHeader={this._renderHeader}
        renderContent={this._renderContent}
      />
    );
  }

  _onPressSection(rowData) {
    if (this.state.questionFooterStatus == "exitsummary") {
      console.log("run = exitsummary");
      this.setState({
        questionFooterStatus: "backnext",
        viewedSectionID: rowData.categoryID,
        viewedQuestionID: rowData.questions[0].questionID,
        viewedQuestionIndex: 0,
        choosedAnswerID: this.state.questionData.filter(
          function(item) {
            return item.categoryID == rowData.categoryID;
          }.bind(this)
        )[0].questions[0].answer.answerID
      });
    } else if (this.state.questionFooterStatus == "backnext") {
      console.log("run = backnext");
      let newTotalCompletion = this.state.totalCompletion;
      let newCurrentCompletion = this.state.currentCompletion;
      let newFinalScore = this.state.finalScore;
      let newTotalScore = this.state.totalScore;

      let viewedSection = this.state.questionData.filter(
        function(item) {
          return item.categoryID == this.state.viewedSectionID;
        }.bind(this)
      )[0];
      if (
        !(
          this.state.choosedAnswerID == 2 &&
          viewedSection.questions[this.state.viewedQuestionIndex].answer
            .findings.length == 0
        )
      ) {
        if (
          viewedSection.questions[this.state.viewedQuestionIndex].answer
            .answerID != this.state.choosedAnswerID
        ) {
          viewedSection.questions[
            this.state.viewedQuestionIndex
          ].answer.answerID = this.state.choosedAnswerID;
          viewedSection.questions[
            this.state.viewedQuestionIndex
          ].answer.answerDate = new Date();

          let scoreResult = this.CalculateScore(this.state.questionData);

          newTotalCompletion = scoreResult.totalCompletion;
          newCurrentCompletion = scoreResult.currentCompletion;
          newFinalScore = scoreResult.countFinalScore;
          newTotalScore = scoreResult.countTotalScore;
          /*
          let prevSectionScore = 0;
          let currSectionScore = 0;
          viewedSection.questions.every((value, index) => {
            if (value.answer.answerID == 1) {
              prevSectionScore += value.weight;
            } else if (value.answer.answerID == 2) {
              if (value.isRequired) {
                prevSectionScore = 0;
                return false;
              }
            }

            return true;
          });

          viewedSection.questions.every((value, index) => {
            if ((index == this.state.viewedQuestionIndex ? this.state.choosedAnswerID : value.answer.answerID) == 1) {
              currSectionScore += value.weight;
            } else if ((index == this.state.viewedQuestionIndex ? this.state.choosedAnswerID : value.answer.answerID) == 2) {
              if (value.isRequired) {
                currSectionScore = 0;
                return false;
              }
            }

            return true;
          });

          newFinalScore -= prevSectionScore;
          newFinalScore += currSectionScore;
          if (viewedSection.questions[this.state.viewedQuestionIndex].answer.answerID == 1 || viewedSection.questions[this.state.viewedQuestionIndex].answer.answerID == 2) {
            newCurrentCompletion--;
          } else if (viewedSection.questions[this.state.viewedQuestionIndex].answer.answerID == 3) {
            newTotalCompletion++;
            newTotalScore += viewedSection.questions[this.state.viewedQuestionIndex].weight;
          }

          if (this.state.choosedAnswerID == 1 || this.state.choosedAnswerID == 2) {
            newCurrentCompletion++;
          } else if (this.state.choosedAnswerID == 3) {
            newTotalCompletion--;
            newTotalScore -= viewedSection.questions[this.state.viewedQuestionIndex].weight;
          }
          */
        }

        this.SaveUpdateAudit();

        let rows = this.getRenderListSection(this.state.questionData);
        this.setState({
          currentCompletion: newCurrentCompletion,
          totalCompletion: newTotalCompletion,
          finalScore: newFinalScore,
          totalScore: newTotalScore,
          calculatedScore:
            Math.round(newFinalScore * 1000 / newTotalScore) / 10,
          questionFooterStatus: "exitsummary",
          listCategory: rows,
          findingState: 0,
          buttonCameraColor: "rgb(237,125,49)"
        });
      }
    }

    requestAnimationFrame(() => {
      this.setState({ loaderStatus: false });
    });
  }

  _onPressAnswer(answerID) {
    this.setState({ choosedAnswerID: answerID });
  }

  _onPressAddFinding() {
    this.setState({ findingState: 1 });
  }

  _onPressSaveFinding() {
    //console.log(this.state.selectedArea)
    if (this.state.selectedArea) {
      if (this.state.savedAttachment.length > 0) {
        let questionData = this.state.questionData;
        questionData
          .filter(
            function(item) {
              return item.categoryID == this.state.viewedSectionID;
            }.bind(this)
          )[0]
          .questions[this.state.viewedQuestionIndex].answer.findings.push({
            auditFindingListID: -1,
            comments: this.state.notesText,
            area: this.state.selectedArea,
            attachment: this.state.savedAttachment,
            findingDate: new Date()
          });
        this.setState({
          questionData: this.state.questionData,
          findingState: 0,
          notesText: "",
          savedAttachment: [],
          buttonCameraColor: "rgb(237,125,49)"
        });
      }
    }
  }

  _onPressAddPicture(index) {
    if (this.state.selectedAttachment.length > index) {
      console.log(
        "1 onPressAddPicture " + this.state.selectedAttachment.length
      );
      this.setState({
        editedPictureIndex: index
      });
    } else {
      console.log(
        "2 onPressAddPicture " + this.state.selectedAttachment.length
      );
      this.refs.timer.stopCounter();
      this.setState(
        {
          editedPictureIndex: index,
          modalFindingPicture: false
        },
        function() {
          this.setState({ cameraRender: true });
        }
      );
      //this.setState({ editedPictureIndex: -1 });
    }
  }

  setIndicator(answerCount, questionCount) {
    return {
      width: responsiveFontSize(2),
      height: responsiveFontSize(2),
      borderRadius: responsiveFontSize(1),
      margin: 5,
      backgroundColor: answerCount >= questionCount ? "#00FF00" : "#FF0000"
    };
  }

  showFindingField() {
    if (this.state.choosedAnswerID != 2) {
      return <Text />;
    } else {
      if (this.state.findingState == 0) {
        return (
          <View>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  flex: 1.3,
                  fontStyle: "italic",
                  fontFamily: "MyriadPro-Regular",
                  color: "black",
                  fontSize: responsiveFontSize(2)
                }}
              >
                {" "}
                {
                  this.state.questionData.filter(
                    function(item) {
                      return item.categoryID == this.state.viewedSectionID;
                    }.bind(this)
                  )[0].questions[this.state.viewedQuestionIndex].answer.findings
                    .length
                }{" "}
                Findings Recorded
              </Text>
              <Button
                style={{
                  flex: 1,
                  backgroundColor: "#FFA500",
                  borderRadius: responsiveWidth(0),
                  justifyContent: "space-around"
                }}
                onPress={this._onPressAddFinding.bind(this)}
              >
                <Text
                  style={{
                    textAlign: "center",
                    fontWeight: "bold",
                    fontSize: responsiveFontSize(2.2),
                    color: "white",
                    fontFamily: "MyriadPro-Regular"
                  }}
                >
                  + Add Finding
                </Text>
              </Button>
            </View>
          </View>
        );
      } else if (this.state.findingState == 1) {
        return (
          <View>
            <View style={styles.groupQuestion}>
              <Text
                style={{
                  fontStyle: "italic",
                  fontFamily: "MyriadPro-Regular",
                  color: "#AAAAAA",
                  fontSize: responsiveFontSize(2)
                }}
              >
                {
                  this.state.questionData.filter(
                    function(item) {
                      return item.categoryID == this.state.viewedSectionID;
                    }.bind(this)
                  )[0].questions[this.state.viewedQuestionIndex].answer.findings
                    .length
                }{" "}
                Findings Recorded
              </Text>
            </View>
            <View
              style={{ flexDirection: "column", justifyContent: "flex-start" }}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "space-around",
                  alignItems: "center",
                  paddingBottom: responsiveHeight(2)
                }}
              >
                <Text
                  style={{
                    fontFamily: "MyriadPro-Regular",
                    fontSize: responsiveFontSize(2)
                  }}
                >
                  Area
                </Text>
                {/*
                <View style={{ backgroundColor: 'blue', flex: 1, borderWidth: 1, borderColor: 'red', borderRadius: responsiveWidth(2), marginTop: responsiveHeight(1), marginBottom: responsiveHeight(1) }}>
                    Platform == 'ios' ? <PickerIOS selectedValue={this.state.selectedArea} onValueChange={(area) => { this.setState({ selectedArea: area }) }} style={{ alignItems: 'flex-end', width: responsiveWidth(69) }}>
                    {this.state.listArea}
                  </PickerIOS> : 
                  <Picker selectedValue={this.state.selectedArea} onValueChange={(area) => { this.setState({ selectedArea: area }) }} style={{ alignItems: 'flex-end', width: responsiveWidth(69) }}>
                    {this.state.listArea}
                  </Picker>
                </View>
                */}
                <ModalPicker
                  data={this.state.listofarea}
                  initValue={this.state.selectedAreaLabel}
                  onChange={area => {
                    console.log(area);
                    this.setState({
                      selectedAreaLabel: area.label,
                      selectedArea: area.key
                    });
                  }}
                  style={{ alignItems: "flex-end", width: responsiveWidth(69) }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      backgroundColor: "white",
                      justifyContent: "space-between"
                    }}
                  >
                    <Text
                      style={[
                        style.font.fontContentNormalComboBox,
                        {
                          height: responsiveHeight(5),
                          width: responsiveWidth(50),
                          backgroundColor: "white",
                          paddingLeft: responsiveWidth(2),
                          paddingTop: responsiveHeight(0.5)
                        }
                      ]}
                      editable={false}
                    >
                      {this.state.selectedAreaLabel}
                    </Text>
                    <Icon
                      size={responsiveWidth(6)}
                      style={{
                        backgroundColor: "white",
                        height: responsiveHeight(5),
                        paddingTop: responsiveHeight(0.5),
                        paddingRight: responsiveWidth(1)
                      }}
                      name="caret-down"
                    />
                  </View>
                </ModalPicker>
              </View>

              <View style={{ borderWidth:2, borderColor:'lightgrey', borderRadius:responsiveWidth(2)}}>
                <TextInput
                  value={this.state.notesText}
                  multiline={true}
                  onChangeText={text => this.setState({ notesText: text })}
                  placeholder="Add comments"
                  style={[style.font.fontContentLarge, { fontSize:newResponsiveFontSize(1.6), height: responsiveHeight(10), paddingLeft: responsiveWidth(2) }]}
                />
              </View>

              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  marginTop: responsiveHeight(1)
                }}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        modalFindingPicture: true,
                        selectedAttachment: this.state.savedAttachment.slice()
                      });
                    }}
                    style={{
                      height: responsiveWidth(10),
                      width: responsiveWidth(10),
                      flex: 1
                    }}
                  >
                    <Image
                      source={Images.camera}
                      style={{
                        backgroundColor: this.state.buttonCameraColor,
                        flex: 1,
                        borderRadius: responsiveWidth(3),
                        height: responsiveWidth(13),
                        width: responsiveWidth(13)
                      }}
                    />
                  </TouchableOpacity>
                </View>
                <View style={{ flex: 1 }} />
                <TouchableOpacity
                  onPress={() => {
                    this._onPressSaveFinding();
                  }}
                  disabled={
                    this.state.notesText.length > 0 &&
                    this.state.selectedAttachment.length > 0 ? (
                      false
                    ) : (
                      true
                    )
                  }
                  style={{
                    borderRadius: responsiveWidth(2),
                    justifyContent: "center",
                    alignItems: "center",
                    width: responsiveWidth(30),
                    height: responsiveWidth(10),
                    flex: 1,
                    backgroundColor:
                      this.state.notesText.length > 0 && this.state.savedAttachment.length > 0 ? "#c00" : "#888888"
                      // this.state.buttonCameraColor == "#c00"
                      //   ? "#c00"
                      //   : "#888888"
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "MyriadPro-Regular",
                      fontSize: responsiveFontSize(2),
                      color:
                        this.state.notesText.length > 0 &&
                        this.state.selectedAttachment.length > 0
                          ? "white"
                          : "black"
                    }}
                  >
                    Save Findings
                  </Text>
                </TouchableOpacity>
                {/*<View style={{ marginBottom: responsiveHeight(1), width: responsiveWidth(30), height: responsiveHeight(5), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                  
                </View>*/}
              </View>
            </View>
          </View>
        );
      }
    }
  }

  changeVideoPicture() {
    var value = !this.state.isVideo;
    this.setState({ isVideo: value });
  }

  takePicture() {
    this.camera
      .capture()
      .then(data => {
        /*this.refs.timer.startCounter();*/
        this.setState({
          //cameraRender: false,
          cameraPreview: true,
          pathCameraPreview: data.path
        });
      this.setState({ isLight: false });        
        //this.pictureTaken({ status: true, assetExt: data.path.split('.').pop(), assetType: 'image', assetPath: data.path });
      })
      .catch(err => console.error(err));
  }

  startRecording = () => {
    if (this.camera) {
      let timerID = setTimeout(this.stopRecording, this.state.maxRecordTime);
      this.setState({
        isRecording: true,
        timerID: timerID
      });
      this.camera
        .capture({
          mode: Camera.constants.CaptureMode.video
        })
        .then(data => {
          this.pictureTaken({
            status: true,
            assetExt: data.path.split(".").pop(),
            assetType: "video",
            assetPath: data.path
          });
        })
        .catch(err => console.error(err));
    }
  };

  stopRecording = () => {
    if (this.camera) {
      this.camera.stopCapture();
      /*this.refs.timer.startCounter();*/
      this.setState({
        isRecording: false,
        cameraRender: false
      });
    }
  };

  capture() {
    console.log('capture')
    if (this.state.isVideo) {
      if (this.state.isRecording) {
        clearTimeout(this.state.timerID);
        this.stopRecording();

        this.setState({ isLight: false });
      } else {
        var flagVideoExist = false;

        this.state.selectedAttachment.filter((val, index)=>{
          if(index != this.state.editedPictureIndex){
            return true;
          }else{
            return false;
          }
        })
        .map((item,i)=>{
          console.log(item); 
          if (item.ext == "mov" || item.ext == "mp4" || item.ext == "3gp") {
              flagVideoExist = true;
          }
        });
        
        if(!flagVideoExist){
          console.log()
          this.startRecording();
        }else{
          Alert.alert('Information', "Only 1 video allowed.")
        }
      }
    } else {
      this.takePicture();

    }
  }

  previewMedia(i) {
    Alert.alert(i);
  }

  // _functLoop(){
  // return this.state.arrayFinding.map(function(data, i){
  //   return (
  //     <ListItem>
  //       <View style={{flex:1, flexDirection:'row'}}>
  //         <View style={{flex:1,  }}>
  //           <Text style={{fontFamily:'myriadpro-bold'}}>{data.category}</Text>
  //           <Text style={{marginTop:responsiveHeight(1)}}>{data.comment}</Text>
  //         </View>
  //         <View style={{flex:0.5, }}>
  //           <Text style={{fontFamily:'myriadpro-bold'}}>Area</Text>
  //           <Text style={{marginTop:responsiveHeight(1)}}>{data.area}</Text>
  //         </View>
  //       </View>
  //       <View style={{flex:0.2}}>
  //         <TouchableHighlight>
  //           <Image source={{uri:data.blob[0]}} style={{ height: responsiveWidth(15), width: responsiveWidth(15)}} />
  //         </TouchableHighlight>
  //       </View>
  //     </ListItem>
  //   )
  // })
  // }

  setModalMediaView() {
    Alert.alert("a");
  }

  viewPDFAuditReport() {
    //console.log('viewPDFAuditReport')
    try{
        if (this.props.isOverdue) {
          this._toast.show({
            position: Toast.constants.gravity.top,
            children: "Schedule was overdue."
          });

          return;
        }

        if (this.props.isComplete) { 
          this.setState({
            loaderStatus:true
          })
          try {
            console.log("CCAAPI");
            axios
              .get(
                Constants.APILink +
                  "DoAudit/GetUrlAttachmentResult?AuditScheduleDetailId=" +
                  this.props.auditScheduleDetailID,
                {
                  headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization:
                      "Bearer " + Queries.getPersistentData("token")
                  },
                  timeout: Constants.GetTimeout
                }
              )
              .then(function(response) {
                  console.log(response.data);
                  if (response.data == "waiting") {
                    Alert.alert(
                          "Information",
                          "Uploading Attachment",
                          [{ text:"Ok", onPress:()=>{
                            this.setState({
                                loaderStatus: false
                              })
                          }}]
                        );  
                  } else {
                    //console.log("data pdf output")
                    //console.log(response)
                    //console.log(RNFS.DocumentDirectoryPath + '/' + this.props.scheduleCode + '-' + this.props.grouplinedesc + '.pdf')
                    if (this.props.isComplete) {
                      const options = { fromUrl: response.data[0].UrlAttachmentResult, toFile: "/storage/emulated/0/Documents" + "/" + this.props.scheduleCode + "-" + this.props.grouplinedesc + ".pdf" };

                      if (Platform.OS == "android") {
                        RNFS.downloadFile(options)
                          .promise.then(res => {
                            //console.log("download berhasil")
                            //console.log(res)

                            this.setState({ modalPDFPreview: true });
                            this.setState({ loaderStatus: false });
                          })
                          .catch(err => {
                            //console.log("download gagal")
                            //console.log(err);
                            this.setState({ loaderStatus: false });
                          });
                      } else {
                        this.setState({
                          modalPDFPreview: true,
                          currentPDFPath:
                            response.data[0].UrlAttachmentResult
                        });
                        this.setState({ loaderStatus: false });
                      }
                    } else {
                      this.PDFAuditReportYa(response.data[0])
                        .then(res => {
                          //console.log(res)
                          this.setState({ modalPDFPreview: true });
                          this.setState({ loaderStatus: false });
                        })
                        .catch(err => {
                          this.setState({ loaderStatus: false });
                        });
                    }
                  }
                }.bind(this))
              .catch(function(error) {
                  this.setState({ loaderStatus: false });
                  // Alert.alert("Connection Failed!", "Catch!");

                  console.log(error);
                  if (error && error.response && error.response.status == "401") {
                    console.log("Information - Result - GetResultPage - 401");
                    Queries.sessionHabis(error.response.status);
                  } else {
                    Alert.alert("Connection Failed!", "Unable to retrieve data. Please check your connection.");
                  }
                  // console.log(error);
                  //Alert.alert(JSON.stringify(error))
                }.bind(this));
          } catch (err) {
            console.log(err);
            this.setState({ loaderStatus: false });
          }
        } else {
          console.log("try generate PDF in draft mode")

          if (this.state.currentCompletion != this.state.totalCompletion) {
            Alert.alert(
              "Information",
              "Cannot preview draft PDF cause there is some question not answered.", 
              [{ text: "Ok"}], 
              { cancelable: false }
            ); 

            return;
          }

          this.setState({ loaderStatus : true})
          

          this.generatePDF(false)
            .then(res => {
              console.log("done generatePDF");
              console.log(res);
              this.setState({ loaderStatus: false }, () => {
                this.setState({ modalPDFPreview: true });
              });
              console.log("tampil");
            })
            .catch(res => {
              console.log("error geneeate PDF");
              this.setState({ loaderStatus: false }, () => {
                this.setState({ modalPDFPreview: true });
              });
            });
        }
    }
    catch(err){
        this.setState({ loaderStatus: false });
    }
    
  }

  showBlankViewKeyboard() {
    // if (this.state.keyboardShowed) {
    //   return(
    //     <View style={{height:this.state.keyboardHeight/2}}>
    //     </View>
    //   )
    // }else
    // return(
    //   null
    // )
    return <View style={{ height: this.state.keyboardHeight }} />;
  }

  _procDeleteFinding(index) {
    let tempArrayFinding = this.state.arrayFinding;
    console.log("array finding awal");
    this.setState(
      {
        arrayFinding: []
      },
      function() {
        console.log("delete finding " + index);
        //update array
        let tempAnswer = Queries.getDataAnswer(
          tempArrayFinding[index].auditFindingListID
        ).trxAuditAnswerID;
        //console.log("tempObj")
        //console.log(tempObj)
        console.log(this.state.questionData);
        let tempAnswerID = Queries.deleteFindingList(
          tempArrayFinding[index].auditFindingListID
        );

        let questions = Queries.getArrayQuestion(
          this.props.auditType,
          this.props.templateCode,
          this.props.auditScheduleDetailID
        );

        if (tempAnswerID == -1) {
          let countAnswered = 0;
          let countQuestion = 0;
          let countFinalScore = 0.0;
          let countTotalScore = 0.0;

          let scoreResult = this.CalculateScore(questions);
          countFinalScore = scoreResult.countFinalScore;
          countTotalScore = scoreResult.countTotalScore;
          countAnswered = scoreResult.currentCompletion;
          countQuestion = scoreResult.totalCompletion;

          let rows = this.getRenderListSection(questions);

          this.setState(
            {
              questionData: questions,
              currentCompletion: countAnswered,
              totalCompletion: countQuestion,
              finalScore: countFinalScore,
              totalScore: countTotalScore,
              listCategory: rows,
              calculatedScore:
                Math.round(countFinalScore * 1000 / countTotalScore) / 10
            },
            () => {
              this.state.questionData.map((value, key) => {
                value.questions.map((data, index) => {
                  if (data.answer.trxAuditAnswerID == tempAnswer) {
                    data.answer.answerID = tempAnswerID;
                  }
                });
              });

              let tempVar = tempArrayFinding.splice(index, 1);
              this.setState({
                arrayFinding: tempArrayFinding,
                totalFinding: this.state.totalFinding - 1,
                viewDeleteFindingList: false
              });
              console.log("array finding");
              console.log(tempArrayFinding);
            }
          );
        } else {
          this.state.questionData.map((value, key) => {
            value.questions.map((data, index) => {
              if (data.answer.trxAuditAnswerID == tempAnswer) {
                data.answer.answerID = tempAnswerID;
              }
            });
          });

          let tempVar = tempArrayFinding.splice(index, 1);
          this.setState({
            questionData: questions,
            arrayFinding: tempArrayFinding,
            totalFinding: this.state.totalFinding - 1,
            viewDeleteFindingList: false
          });
        }
      }
    );
  }

  _procEditFinding() {
    console.log("proc edit finding")
    console.log(this.state.arrayFinding);
    console.log("index " + this.state.indexListMediaToDelete);
    let tempArrayFinding = this.state.arrayFinding;
    this.setState(
      {
        arrayFinding: []
      },
      function() {
        Queries.editComment(
          tempArrayFinding[this.state.indexListMediaToDelete]
            .auditFindingListID,
          this.state.editCommentLabel
        );
        tempArrayFinding[
          this.state.indexListMediaToDelete
        ].comment = this.state.editCommentLabel;
        this.setState({
          arrayFinding: tempArrayFinding,
          viewEditFindingList: false
        });
        console.log("array finding");
        console.log(tempArrayFinding);
        
        let newData = Queries.getArrayQuestion(
          this.props.auditType,
          this.props.templateCode,
          this.props.auditScheduleDetailID
        );

        this.setState({ questionData: newData });
      }
    );
  }

  _deleteFinding(index) {
    this.setState({
      viewDeleteFindingList: true,
      indexListMediaToDelete: index
    });
  }

  _editFinding(index, category, area, comment) {
    console.log("tes edit finding")
    this.setState({
      indexListMediaToDelete: index,
      viewEditFindingList: true,
      editCategoryLabel: category,
      editAreaLabel: area,
      editCommentLabel: comment
    });
  }

  _generateViewFindingList(condition) {
    let tempFindingsList = this.state.arrayFinding.map(
      function(data, i) {
        console.log("_generateviewfinding list data ke " + i);
        console.log(data);
        return (
          <ListItemMedia
            key={i}
            data={data}
            isComplete={condition}
            index={i}
            _deleteFinding={this._deleteFinding.bind(this)}
            _editFinding={this._editFinding.bind(this)}
            _editMedia={this.editListItemMedia.bind(this)}
          />
        );
      }.bind(this)
    );

    this.setState({
      listFindings: tempFindingsList
    });
  }

  mainContainer() {
    let textScore =
      this.state.questionFooterStatus == "exitsummary"
        ? "CURRENT SCORE"
        : "FINAL SCORE";
    if (this.state.questionFooterStatus == "exitsubmit") {
      var header =
        this.state.totalFinding == 0 ? (
          <View
            style={{ backgroundColor: "#636264", justifyContent: "center" }}
          >
            <Text
              style={{
                padding: responsiveWidth(4),
                fontFamily: "MyriadPro-Regular",
                fontSize: responsiveFontSize(3),
                color: "white"
              }}
            >
              {" "}
              No Finding
            </Text>
          </View>
        ) : (
          <View
            style={{
              backgroundColor: "#636264",
              justifyContent: "center",
              flexDirection: "row"
            }}
          >
            <Text
              style={{
                flex: 8,
                padding: responsiveWidth(4),
                fontFamily: "MyriadPro-Regular",
                fontSize: responsiveFontSize(3),
                color: "white",
                textAlign: "left"
              }}
            >
              {" "}
              {this.state.totalFinding} Total Findings
            </Text>
            <Icon
              style={{ flex: 1, paddingTop: responsiveWidth(4) }}
              name="chevron-down"
              size={responsiveWidth(6)}
              color="white"
            />
          </View>
        );

      // medialist = this.state.arrayFinding.map(function (data, i) {
      //   console.log("datanya ini")
      //   console.log(data)
      //   return (
      //     <ListItem>
      //       <View style={{ flex: 1, flexDirection: 'row' }}>
      //         <View style={{ flex: 1, }}>
      //           <Text style={{ fontFamily: 'MyriadPro-Bold' }}>{data.nama}</Text>
      //           <Text style={{ marginTop: responsiveHeight(1), fontFamily: 'MyriadPro-Regular' }}>{data.comment}</Text>
      //         </View>
      //         <View style={{ flex: 0.5, }}>
      //           <Text style={{ fontFamily: 'MyriadPro-Bold' }}>Area</Text>
      //           <Text style={{ marginTop: responsiveHeight(1), fontFamily: 'MyriadPro-Regular' }}>{data.area}</Text>
      //         </View>
      //       </View>
      //       <View style={{ flex: 0.2 }}>
      //         <TouchableHighlight onPress={() => { console.log('media on press'); this.setState({ modalMediaView: true }) } }>
      //           <Image source={{ uri: data.blob[0].source }} style={{ height: responsiveWidth(15), width: responsiveWidth(15) }} />
      //         </TouchableHighlight>
      //       </View>
      //     </ListItem>)
      // }.bind(this))
      var condition = this.props.isComplete;
      var content =
        this.state.totalFinding == 0 ? (
          <View />
        ) : (
          <ScrollView
            style={{
              backgroundColor: "white",
              maxHeight: responsiveHeight(45)
            }}
          >
            {
              //medialist
            }
            {this.state.arrayFinding.length > 0 ? (
              this.state.arrayFinding.map(
                function(data, i) {
                  console.log("media list data ke " + i);
                  console.log(data);
                  return (
                    <ListItemMedia
                      key={i}
                      data={data}
                      isComplete={condition}
                      index={i}
                      _deleteFinding={this._deleteFinding.bind(this)}
                      _editFinding={this._editFinding.bind(this)}
                      _editMedia={this.editListItemMedia.bind(this)}
                    />
                  );
                }.bind(this)
              )
            ) : (
              <View />
            )
            //this._generateViewFindingList(condition)
            }
            {
              //this.state.listFindings
            }
          </ScrollView>
        );

      let SectionDataMedia = [];

      let tempData = {
        title: (
          <View
            style={{
              backgroundColor: "#636264",
              justifyContent: "center",
              flexDirection: "row"
            }}
          >
            <Text
              style={{
                flex: 8,
                padding: responsiveWidth(4),
                fontFamily: "MyriadPro-Regular",
                fontSize: responsiveFontSize(3),
                color: "white",
                textAlign: "left"
              }}
            >
              {" "}
              {this.state.totalFinding} Total Findings
            </Text>
            <Icon
              style={{ flex: 1, paddingTop: responsiveWidth(4) }}
              name="chevron-down"
              size={responsiveWidth(6)}
              color="white"
            />
          </View>
        ),
        content: (
          <ScrollView
            style={{
              backgroundColor: "white",
              maxHeight: responsiveHeight(45)
            }}
          >
            {
              //medialist
            }
            {this.state.arrayFinding.length > 0 ? (
              this.state.arrayFinding.map(
                function(data, i) {
                  console.log("media list data ke " + i);
                  console.log(data);
                  return (
                    <ListItemMedia
                      key={i}
                      data={data}
                      isComplete={condition}
                      index={i}
                      _deleteFinding={this._deleteFinding.bind(this)}
                      _editFinding={this._editFinding.bind(this)}
                      _editMedia={this.editListItemMedia.bind(this)}
                    />
                  );
                }.bind(this)
              )
            ) : (
              <View />
            )
            //this._generateViewFindingList(condition)
            }
            {
              //this.state.listFindings
            }
          </ScrollView>
        )
      };

      SectionDataMedia.push(tempData);

      return (
        <View style={{ flex: 1, paddingTop: responsiveHeight(1) }}>
          <View
            style={{
              flex: 0.4,
              paddingLeft: responsiveWidth(2),
              paddingRight: responsiveWidth(2)
            }}
          >
            <Text
              style={{
                textAlign: "center",
                fontFamily: "MyriadPro-Regular",
                fontWeight: "bold",
                fontSize: responsiveFontSize(3),
                color: "#000"
              }}
            >
              {this.state.plant} Plant - {this.state.groupline}
            </Text>
            <Text
              style={{
                textAlign: "center",
                fontFamily: "MyriadPro-Regular",
                color: "#AAAAAA",
                fontSize: responsiveFontSize(2.5)
              }}
            >
              {this.state.week} {this.state.year}
            </Text>
          </View>
          <View
            style={{
              flex: 0.6,
              paddingLeft: responsiveWidth(2),
              paddingRight: responsiveWidth(2),
              borderColor: "red"
            }}
          >
            <View
              style={{ flex: 1, flexDirection: "row", alignItems: "center" }}
            >
              <View
                style={{
                  flex: 1,
                  backgroundColor: "transparent",
                  alignItems: "center"
                }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    color: "black",
                    fontFamily: "MyriadPro-Regular",
                    fontSize: responsiveFontSize(2)
                  }}
                >
                  {textScore}
                </Text>
                <Text
                  style={{
                    textAlign: "center",
                    color: "green",
                    fontFamily: "MyriadPro-Regular",
                    fontSize: newResponsiveFontSize(3.5)
                  }}
                >
                  {this.state.isOverdue ? (
                    "-"
                  ) : this.state.totalCompletion == 0 ? (
                    "-"
                  ) : (
                    this.state.calculatedScore
                  )}%
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  alignItems: "center",
                  borderColor: "grey",
                  borderLeftWidth: 1
                }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    color: "black",
                    fontFamily: "MyriadPro-Regular",
                    fontSize: responsiveFontSize(2)
                  }}
                >
                  Audit Completion
                </Text>
                <Text
                  style={{
                    textAlign: "center",
                    color:
                      this.state.currentCompletion == this.state.totalCompletion
                        ? "green"
                        : "orange",
                    fontFamily: "MyriadPro-Regular",
                    fontSize: newResponsiveFontSize(3.5),
                    marginLeft: responsiveWidth(1)
                  }}
                >
                  {this.state.isOverdue == true ? (
                    "-/-"
                  ) : (
                    this.state.currentCompletion +
                    "/" +
                    this.state.totalCompletion
                  )}
                </Text>
              </View>
            </View>
            <View
              style={{
                flex: 0.4,
                alignItems: "flex-end",
                marginTop: responsiveWidth(2),
                marginBottom: responsiveWidth(3)
              }}
            >
              <Text
                onPress={this.viewPDFAuditReport.bind(this)}
                style={{
                  fontFamily: "MyriadPro-Regular",
                  fontSize: responsiveFontSize(2.1),
                  padding: responsiveWidth(3),
                  backgroundColor: "orange",
                  color: "white"
                }}
              >
                PDF Report
              </Text>
            </View>
          </View>

          <View style={{ flex: 1.6, marginTop: responsiveHeight(3) }}>
            <AccordionNew
              sections={SectionDataMedia}
              underlayColor={"transparent"}
              renderHeader={this._renderHeader}
              renderContent={this._renderContent}
            />
          </View>
        </View>
      );
    } else if (this.state.questionFooterStatus == "backnext") {
      //console.log("Question Data")
      //console.log(this.state.questionData)
      return (
        <ScrollView
          ref="scroll"
          keyboardDismissMode={"on-drag"}
          keyboardShouldPersistTaps={"always"}
          automaticallyAdjustContentInsets={false}
          style={{ paddingTop: responsiveHeight(1) }}
        >
          <View
            style={{
              paddingLeft: responsiveWidth(2),
              paddingRight: responsiveWidth(2),
              borderWidth: 1,
              borderColor: "transparent"
            }}
          >
            <Text
              style={{
                textAlign: "center",
                fontFamily: "MyriadPro-Regular",
                fontWeight: "bold",
                fontSize: responsiveFontSize(3),
                color: "#000"
              }}
            >
              {this.state.plant} Plant - {this.state.groupline}
            </Text>
            <Text
              style={{
                textAlign: "center",
                fontFamily: "MyriadPro-Regular",
                color: "#AAAAAA",
                fontSize: responsiveFontSize(2.5)
              }}
            >
              {this.state.week} {this.state.year}
            </Text>
          </View>
          <View style={{ marginTop: responsiveHeight(1) }}>
            <TouchableOpacity
              onPress={() => {
                this.setState({
                  loaderStatus: true
                });
                requestAnimationFrame(() => {
                  this._onPressSection(
                    this.state.questionData.filter(
                      function(item) {
                        return item.categoryID == this.state.viewedSectionID;
                      }.bind(this)
                    )[0]
                  );
                });
              }}
            >
              <View
                style={[styles.groupQuestion, { height: responsiveHeight(7) }]}
              >
                <View
                  style={this.setIndicator(
                    this.state.questionData
                      .filter(
                        function(item) {
                          return item.categoryID == this.state.viewedSectionID;
                        }.bind(this)
                      )[0]
                      .questions.filter(item => {
                        return (
                          item.answer.answerID != -1 &&
                          item.answer.answerID !== 3
                        );
                      }).length,
                    this.state.questionData
                      .filter(
                        function(item) {
                          return item.categoryID == this.state.viewedSectionID;
                        }.bind(this)
                      )[0]
                      .questions.filter(item => {
                        return item.answer.answerID !== 3;
                      }).length
                  )}
                />
                <Text
                  style={{
                    fontFamily: "MyriadPro-Regular",
                    color: "black",
                    fontSize: responsiveFontSize(3),
                    flex: 8
                  }}
                >
                  {
                    this.state.questionData.filter(
                      function(item) {
                        return item.categoryID == this.state.viewedSectionID;
                      }.bind(this)
                    )[0].categoryDesc
                  }
                </Text>

                <Text
                  style={{
                    fontFamily: "MyriadPro-Regular",
                    color: "black",
                    fontSize: responsiveFontSize(3),
                    flex: 2
                  }}
                >
                  {
                    this.state.questionData
                      .filter(
                        function(item) {
                          return item.categoryID == this.state.viewedSectionID;
                        }.bind(this)
                      )[0]
                      .questions.filter(item => {
                        return (
                          item.answer.answerID != -1 &&
                          item.answer.answerID !== 3
                        );
                      }).length
                  }/{
                    this.state.questionData
                      .filter(
                        function(item) {
                          return item.categoryID == this.state.viewedSectionID;
                        }.bind(this)
                      )[0]
                      .questions.filter(item => {
                        return item.answer.answerID !== 3;
                      }).length
                  }
                </Text>
                {/*<Icon name='chevron-right' size={responsiveWidth(6)} color="#c00" />*/}
              </View>
            </TouchableOpacity>
          </View>

          <View
            style={{
              backgroundColor: "transparent",
              paddingLeft: responsiveWidth(2),
              paddingRight: responsiveWidth(2)
            }}
          >
            <Text
              style={{
                fontFamily: "MyriadPro-Regular",
                color: "black",
                fontSize: responsiveFontSize(2.5)
              }}
            >
              {
                this.state.questionData.filter(
                  function(item) {
                    return item.categoryID == this.state.viewedSectionID;
                  }.bind(this)
                )[0].questions[this.state.viewedQuestionIndex].questionDesc
              }
            </Text>
            <View
              style={{
                flex: 1,
                marginTop: responsiveHeight(4),
                marginBottom: responsiveHeight(2),
                flexDirection: "row",
                justifyContent: "flex-start"
              }}
            >
              {this.getAnswerList()}
            </View>
            {this.showFindingField()}
          </View>
          {this.showBlankViewKeyboard()}
        </ScrollView>
      );
    } else if (this.state.questionFooterStatus == "exitsummary") {
      return (
        <View style={{ flex: 1, paddingTop: responsiveHeight(1) }}>
          <View
            style={{
              flex: 0.4,
              paddingLeft: responsiveWidth(2),
              paddingRight: responsiveWidth(2),
              borderWidth: 1,
              borderColor: "transparent"
            }}
          >
            <Text
              style={{
                textAlign: "center",
                fontFamily: "MyriadPro-Regular",
                fontWeight: "bold",
                fontSize: responsiveFontSize(3),
                color: "#000"
              }}
            >
              {this.state.plant} Plant - {this.state.groupline}
            </Text>
            <Text
              style={{
                textAlign: "center",
                fontFamily: "MyriadPro-Regular",
                color: "#AAAAAA",
                fontSize: responsiveFontSize(2.5)
              }}
            >
              {this.state.week} {this.state.year}
            </Text>
          </View>
          <View
            style={{
              flex: 0.4,
              paddingLeft: responsiveWidth(2),
              paddingRight: responsiveWidth(2),
              borderWidth: 1,
              borderColor: "transparent"
            }}
          >
            <View
              style={{ flex: 1, flexDirection: "row", alignItems: "center" }}
            >
              <View
                style={{
                  flex: 1,
                  backgroundColor: "transparent",
                  alignItems: "center"
                }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    color: "black",
                    fontFamily: "MyriadPro-Regular",
                    fontSize: responsiveFontSize(2),
                    fontWeight: "bold"
                  }}
                >
                  CURRENT SCORE
                </Text>
                <Text
                  style={{
                    textAlign: "center",
                    color: "green",
                    fontFamily: "MyriadPro-Regular",
                    fontWeight: "bold",
                    fontSize: newResponsiveFontSize(3.5)
                  }}
                >
                  {this.state.totalCompletion == 0 ? (
                    "-"
                  ) : (
                    this.state.calculatedScore
                  )}%
                </Text>
              </View>

              <View
                style={{
                  flex: 1,
                  backgroundColor: "transparent",
                  alignItems: "center",
                  borderColor: "grey",
                  borderLeftWidth: 1
                }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    color: "black",
                    fontFamily: "MyriadPro-Regular",
                    fontSize: responsiveFontSize(2),
                    fontWeight: "bold"
                  }}
                >
                  AUDIT COMPLETION
                </Text>
                <Text
                  style={{
                    textAlign: "center",
                    color:
                      this.state.currentCompletion == this.state.totalCompletion
                        ? "green"
                        : "orange",
                    fontWeight: "bold",
                    fontFamily: "MyriadPro-Regular",
                    fontSize: newResponsiveFontSize(3.5),
                    marginLeft: responsiveWidth(1)
                  }}
                >
                  {this.state.currentCompletion}/{this.state.totalCompletion}
                </Text>
              </View>
            </View>
          </View>
          <View
            style={{
              flex: 2,
              borderWidth: 1,
              borderColor: "transparent",
              height:
                Metrics.mainContainerHeightQuestionScreen - responsiveHeight(30)
            }}
          >
            <ScrollView>
              <View style={{ flex: 1 }}>{this.state.listCategory}</View>
            </ScrollView>
          </View>
        </View>
      );
    }
  }

  SaveUpdateAudit() {
    console.log("viki SaveUpdateAudit");
    return new Promise((resolve, reject) => {
      this.state.questionData.map((value, key) => {
        value.questions.map((data, index) => {
          let trxAuditAnswerID = Queries.saveAnswer(
            this.props.auditScheduleDetailID,
            data.questionID,
            data.answer.answerID,
            data.answer.findings.length,
            data.answer.answerDate
          );
          if (data.answer.answerID == 2) {
            //let tempDataFindings = data.answer.findings
            //console.log("tempDataFindings 1")
            //console.log(data.answer)
            // let tempItemFindings = []

            data.answer.findings.map((item, i) => {
              if (item.auditFindingListID == -1) {
                let auditFindingListID = Queries.saveAuditFindingList(
                  trxAuditAnswerID,
                  item.comments,
                  item.area,
                  item.findingDate
                );
                item.auditFindingListID = auditFindingListID;
                if (auditFindingListID != -1) {
                  item.attachment.map((content, id) => {
                    if (content.attachmentID == -1) {
                      let attachmentID = Queries.saveAttachment(
                        auditFindingListID,
                        content.url,
                        content.ext,
                        content.type,
                        "",
                        new Date()
                      );
                      content.attachmentID = attachmentID;
                    }
                  });
                  // check db yang belum keinsert dan update
                }
                // let newArray = {
                //   auditFindingListID: auditFindingListID,
                //   nama : item.nama,
                //   comment: item.comment,
                //   area: item.area
                // }

                // tempItemFindings.push(newArray)
              }
              // else{
              //   let newArray = {
              //     auditFindingListID: item.auditFindingListID,
              //     nama : item.nama,
              //     comment: item.comment,
              //     area: item.area
              //   }

              //   tempItemFindings.push(newArray)
              // }
            });

            //console.log("tempDataFindings 2")
            //console.log(data.answer)
          }
        });
      });

      resolve(true);
    });
  }

  leftOnPress() {
    if (this.state.questionFooterStatus == "exitsummary") {
      console.log("log leftonpress 1");
      this.setState({ loaderStatus: true });
    } else if (this.state.questionFooterStatus == "exitsubmit") {
      console.log("Left on press exit submit");
      // this.setState({
      //   loaderStatus: true
      // }, () => {
      //   if (!this.props.isComplete) {
      //     //requestAnimationFrame(() => {
      //       console.log('change to exit summary')
      //       this.setState({
      //         questionFooterStatus: 'exitsummary',
      //         loaderStatus: false
      //       })
      //     //})
      //   }
      // })
      if (!this.props.isComplete) {
        this.setState({
          questionFooterStatus: "exitsummary",
          loaderStatus: false
        });
        return;
      }
    } else if (this.state.questionFooterStatus == "submitauditee" && this.state.isAuditHasBeenComplete ) {
      this.setState({ loaderStatus: false });
      Actions.HomeScreen({ type: "replace" });
    } else if (this.state.questionFooterStatus == "submitauditee") {
      console.log("submit auditee");

      this.setState(
        {
          questionFooterStatus: "exitsubmit"
        },
        () => {
          requestAnimationFrame(() => {
            this.setState({
              loaderStatus: false
            });
          });
        }
      );

      return;
    }

    requestAnimationFrame(() => {
      console.log("log leftonpress 2");
      // Alert.alert('left on press')
      // API -> this.props.auditScheduleDetailID
      //        this.props.groupLineID
      // auditType : 'GMP',
      // templateCode : 1,
      if (
        this.state.questionFooterStatus == "exitsummary" ||
        this.state.questionFooterStatus == "exitsubmit"
      ) {
        console.log("Left on press 3");
        if (this.props.isComplete) {
          console.log("Left on press 4");
          this.setState({ loaderStatus: false });
          Actions.HomeScreen({ type: "replace" });
        } else {
          this.refs.timer.stopCounter();
          let result = this.formatDraftResult();
          Queries.saveProgress(
            this.props.auditScheduleDetailID,
            this.state.calculatedScore,
            this.state.currentCompletion,
            this.state.totalCompletion
          );
          // loader
          //clearInterval(this.intervalID);
          this.SaveUpdateAudit()
            .then(res => {
              console.log("CCAAPI");
              console.log(
                Constants.APILink + "DoAudit/PostUpdateAudit",
                result + `apuytoken`
              );
              axios
                .post(
                  Constants.APILink + "DoAudit/PostUpdateAudit",
                  result,
                  {
                    headers: {
                      Accept: "application/json",
                      "Content-Type": "application/json",
                      Authorization:
                        "Bearer " +
                        Queries.getPersistentData("token")
                    },
                    timeout: Constants.GetTimeout
                  }
                )
                .then(res => {
                  console.log("AFTER" + JSON.stringify(res));
                  this.setState({ loaderStatus: false });
                  Actions.HomeScreen({ type: "replace" });
                })
                .catch(function(error) {
                    // Alert.alert("Connection Failed!", "Catch!");

                    console.log(error);
                    if (error && error.response && error.response.status == "401") {
                      console.log("Information - Result - GetResultPage - 401");
                      Queries.sessionHabis(error.response.status);
                    } else {
                      Alert.alert(
                        "Connection Failed!", 
                        "Unable to retrieve data. Please check your connection.",
                        [{text:'Ok', onPress:()=>{
                            Actions.HomeScreen({
                              type: "replace"
                            });
                            this.setState({ loaderStatus: false });
                          }}
                        ]);
                    }
                    // console.log(error);
                    //Alert.alert(JSON.stringify(error))
                  }.bind(this));

            })
            .catch(res => {
              this.setState({
                loaderStatus: false
              });
            });
        }
      } else if (this.state.questionFooterStatus == "backnext") {
        let viewedSection = this.state.questionData.filter(
          function(item) {
            return item.categoryID == this.state.viewedSectionID;
          }.bind(this)
        )[0];
        this._onPressSection(viewedSection);
        // let viewedSection = this.state.questionData.filter(function (item) { return item.categoryID == this.state.viewedSectionID; }.bind(this))[0];
        // if (!(this.state.choosedAnswerID == 2 && viewedSection.questions[this.state.viewedQuestionIndex].answer.findings.length == 0)) {
        //   if (this.state.viewedQuestionIndex <= 0) {
        //     this._onPressSection(viewedSection);
        //   } else {
        //     viewedSection.questions[this.state.viewedQuestionIndex].answer.answerID = this.state.choosedAnswerID;
        //     viewedSection.questions[this.state.viewedQuestionIndex].answer.answerDate = new Date();

        //     this.SaveUpdateAudit();

        //     let scoreResult = this.CalculateScore(this.state.questionData);

        //     let newTotalCompletion = scoreResult.totalCompletion;
        //     let newCurrentCompletion = scoreResult.currentCompletion;
        //     let newFinalScore = scoreResult.countFinalScore;
        //     let newTotalScore = scoreResult.countTotalScore;

        //     this.setState({
        //       currentCompletion: newCurrentCompletion,
        //       totalCompletion: newTotalCompletion,
        //       finalScore: newFinalScore,
        //       totalScore: newTotalScore,
        //       calculatedScore: Math.round(newFinalScore * 1000 / newTotalScore) / 10,
        //       viewedQuestionID: viewedSection.questions[this.state.viewedQuestionIndex - 1],
        //       choosedAnswerID: viewedSection.questions[this.state.viewedQuestionIndex - 1].answer.answerID,
        //       viewedQuestionIndex: this.state.viewedQuestionIndex - 1,
        //     });
        //   }
        // }

        // this.setState({
        //   loaderStatus: false
        // })
      }
    });
  }

  testgetAreaDescById(areaid) {
    // console.log('masuk test')
    return Queries.getAreaDescByID(areaid);
  }

  checkExists(path, arr, idx, blobidx, url, type, attachmentID) {
    console.log("Viki Check Exists");
    console.log(path);
    return new Promise((resolve, reject) => {
      RNFS.exists(path).then(res => {
        if (res) {
          console.log("res");
          // arr.idx.blob.push({
          //   source: url,
          //   type: type,
          //   attachmentID: attachmentID
          // })
          arr[idx].blob[blobidx] = {
            source: url,
            type: type,
            attachmentID: attachmentID
          };
        } else {
          console.log("else not res");
          // arr.idx.blob.push({
          //   source: url,
          //   type: type,
          //   attachmentID: attachmentID
          // })
          arr[idx].blob[blobidx] = {
            source: null,
            type: "image",
            attachmentID: attachmentID
          };
        }
        resolve(res);
      });
      console.log("Viki end Check Exists");
    });
  }

  convertToArrayFindings() {
    let promises = [];
    let arrResult = [];
    console.log("apu array finding");
    //console.log(this.state.questionData)

    this.SaveUpdateAudit().then(res => {
      let newData = Queries.getArrayQuestion(
        this.props.auditType,
        this.props.templateCode,
        this.props.auditScheduleDetailID
      );
      this.setState({ questionData: newData });

      console.log("viki array finding");
      console.log(newData);

      newData.map((value, key) => {
        value.questions.map((data, index) => {
          if (data.answer.answerID == 2 && data.answer.findings.length > 0) {
            data.answer.findings.map((item, i) => {
              let idx = arrResult.length;
              arrResult[idx] = {
                auditFindingListID: item.auditFindingListID,
                nama: value.categoryDesc,
                comment: item.comments,
                area: this.testgetAreaDescById(item.area),
                blob: []
              };
              item.attachment.map((content, id) => {
                //let prom = RNFS.exists(content.url);
                let prom = this.checkExists(
                  content.url,
                  arrResult,
                  idx,
                  id,
                  content.url,
                  content.type,
                  content.attachmentID
                );
                console.log("prom");
                promises.push(prom);

                // prom.then((res) => {
                //   if (res) {
                //     arrResult[idx].blob[arrResult[idx].blob.length] = {
                //       source: content.url,
                //       type: content.type,
                //       attachmentID: content.attachmentID
                //     };
                //   } else {
                //     arrResult[idx].blob[arrResult[idx].blob.length] = {
                //       source: null,
                //       type: 'image',
                //       attachmentID: content.attachmentID
                //     };
                //   }
                // })
                //   .catch(err => {
                //     console.log('Information - QuestionScreen - converttoarrayfinding - prom.then(')
                //   })
              });
            });
          }
        });
      });

      Promise.all(promises).then(() => {
        console.log("Information - QuestionScreen convert array finding");
        this.state.arrayFinding = arrResult;
        console.log(JSON.stringify(arrResult));
        this.setState({
          totalFinding: arrResult.length
        });
      });
    });
  }

  convertToSummary() {
    let arrResult = [];
    this.state.questionData.map((value, key) => {
      value.questions.map((data, index) => {
        if (data.answer.answerID !== 3) {
          data.answer.findings.map((item, i) => {
            arrResult[arrResult.length] = {
              category: value.categoryDesc,
              comment: item.comments,
              area: this.testgetAreaDescById(item.area),
              blob: []
            };
            item.attachment.map((content, id) => {
              arrResult[arrResult.length - 1].blob[
                arrResult[arrResult.length - 1].blob.length
              ] = { source: content.url, type: content.type };
            });
          });
        }
      });
    });
  }

  rightOnPress() {
    if (this.state.questionFooterStatus == "exitsummary") {
      console.log("exitsummary");
      this.keyboardDidShowListener.remove();
      if (this.state.currentCompletion == this.state.totalCompletion) {
        this.setState({
          questionFooterStatus: "exitsubmit"
        });
        this.convertToArrayFindings();
      } else {
        Alert.alert(
          "Information",
          "Cannot go to summary page cause there is some question not answered.", [{ text: "Ok", onPress : ()=>{ this.setState({ loaderStatus : false})} }], { cancelable: false }); 
          return
      }
    } else if (this.state.questionFooterStatus == "backnext") {
      let viewedSection = this.state.questionData.filter(
        function(item) {
          return item.categoryID == this.state.viewedSectionID;
        }.bind(this)
      )[0];
      if (
        !(
          this.state.choosedAnswerID == 2 &&
          viewedSection.questions[this.state.viewedQuestionIndex].answer
            .findings.length == 0
        )
      ) {
        if (
          this.state.viewedQuestionIndex >=
          viewedSection.questions.length - 1
        ) {
          this._onPressSection(viewedSection);
        } else {
          viewedSection.questions[
            this.state.viewedQuestionIndex
          ].answer.answerID = this.state.choosedAnswerID;
          viewedSection.questions[
            this.state.viewedQuestionIndex
          ].answer.answerDate = new Date();



          this.SaveUpdateAudit();

          let scoreResult = this.CalculateScore(this.state.questionData);

          let newTotalCompletion = scoreResult.totalCompletion;
          let newCurrentCompletion = scoreResult.currentCompletion;
          let newFinalScore = scoreResult.countFinalScore;
          let newTotalScore = scoreResult.countTotalScore;

          this.setState({
            notesText:'',
            savedAttachment: [],
            currentCompletion: newCurrentCompletion,
            totalCompletion: newTotalCompletion,
            finalScore: newFinalScore,
            totalScore: newTotalScore,
            calculatedScore:
              Math.round(newFinalScore * 1000 / newTotalScore) / 10,
            viewedQuestionID:
              viewedSection.questions[this.state.viewedQuestionIndex + 1],
            choosedAnswerID:
              viewedSection.questions[this.state.viewedQuestionIndex + 1].answer
                .answerID,
            viewedQuestionIndex: this.state.viewedQuestionIndex + 1,
            findingState: 0,
            buttonCameraColor: "rgb(237,125,49)"
          });
        }
      }
    } else if (this.state.questionFooterStatus == "exitsubmit") {
      if (this.state.currentCompletion == this.state.totalCompletion) {
        if (!this.props.isComplete) {
          //this.setState({ modalSignOffView: true, modalSignOffAzureADView: true,  })

          this.setState({
            //modalSignOffAuditeeView: true,
            questionFooterStatus: "submitauditee"
          });
        }
      } else {
        Alert.alert(
          "Information",
          "Cannot go to submit page cause there is some question not answered.",
                                 [{text: "Ok"}], 
                                 {cancelable:false} 
        );
      }
      // this.setState({ modalSignOffView: true })
    } else if (
      this.state.questionFooterStatus == "exitsubmit" &&
      this.props.isFromComplete == true
    ) {
      Alert.alert("null");
    } else if (this.state.questionFooterStatus == "submitauditee") {
      this.finishOnPressAuditee();
    }

    this.setState({ loaderStatus: false });
  }

  questionFooter() {
    let left = "";
    let right = "";
    let enabled = true;
    let colorRight = "transparent";
    if (this.state.questionFooterStatus == "exitsummary") {
      left = "EXIT";
      right = "SUMMARY";

      if (
        this.state.currentCompletion == this.state.totalCompletion &&
        right == "Summary"
      ) {
        colorRight = "rgba(0, 0, 0, 0.5)";
      } else {
        colorRight = "rgba(255, 255, 255, 0.5)";
      }
    } else if (this.state.questionFooterStatus == "backnext") {
      left = "BACK";
      right = "NEXT";
      colorRight = "transparent";
    } else if (this.state.questionFooterStatus == "exitsubmit") {
      left = "BACK";
      right = "END";

      if (this.props.isComplete) {
        colorRight = "rgba(0, 0, 0, 0.5)";
      } else {
        if (
          this.state.currentCompletion == this.state.totalCompletion &&
          right == "Submit"
        ) {
          // console.log('ini andi') transparent
          colorRight = "rgba(0, 0, 0, 0.5)";
        } else {
          // console.log('ini budi') abu2
          colorRight = "rgba(255, 255, 255, 0.5)";
        }
      }
    } else if (
      this.state.questionFooterStatus == "submitauditee" &&
      this.state.isAuditHasBeenComplete
    ) {
      left = "BACK";
      right = "";
      colorRight = "transparent";
    } else if (this.state.questionFooterStatus == "submitauditee") {
      left = "BACK";
      right = "FINISH";
      colorRight = "transparent";
    }

    if (this.state.isCompleted) {
      right = "";
    }

    return (
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          backgroundColor: "transparent",
          justifyContent: "space-between"
        }}
      >
        <TouchableOpacity
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            height: Metrics.marginHeader
          }}
          onPress={() => {
            this.setState({ loaderStatus: true });
            this.leftOnPress();
          }}
        >
          <Text style={[style.font.fontContentLarge, { color: "white" }]}>
            {left}
          </Text>
        </TouchableOpacity>

        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            borderLeftColor: "white",
            borderLeftWidth: 2,
            borderRightColor: "white",
            borderRightWidth: 2
          }}
        >
          {this.state.isAuditHasBeenComplete ? (
            <View />
          ) : (
            <Timer
              ref="timer"
              startDate={this.state.startDate}
              endDate={this.state.endDate}
              isComplete={this.props.isComplete}
            />
          )}
        </View>

        <TouchableOpacity
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            height: Metrics.marginHeader
          }}
          onPress={() => {
            this.setState({ loaderStatus: true });
            requestAnimationFrame(() => {
              this.rightOnPress();
            });
          }}
        >
          <Text style={[style.font.fontContentLarge, { color: "white" }]}>
            {right}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  getAnswerList() {
    return this.state.questionData
      .filter(
        function(item) {
          return item.categoryID == this.state.viewedSectionID;
        }.bind(this)
      )[0]
      .questions[this.state.viewedQuestionIndex].options.map(
        function(data, i) {
          return (
            <TouchableHighlight
              key={i}
              onPress={() => {
                this._onPressAnswer(data.optionID);
              }}
              style={{
                flex: 1,
                height: responsiveHeight(13),
                marginTop: responsiveHeight(0.5),
                marginBottom: responsiveHeight(0.5),
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                backgroundColor:
                  data.optionID == this.state.choosedAnswerID
                    ? data.optionID == 1
                      ? "rgb(112,173,71)"
                      : data.optionID == 2 ? "rgb(192,0,0)" : "rgb(237,125,49)"
                    : "rgb(68,84,106)"
              }}
            >
              <Text
                style={{
                  fontFamily: "MyriadPro-Regular",
                  color: "white",
                  fontSize: responsiveFontSize(2.5)
                }}
              >
                {data.optionDesc}
              </Text>
            </TouchableHighlight>
          );
        }.bind(this)
      );
  }

  // 8 kotak plus button
  getAttachedImage(index) {
    if (this.state.selectedAttachment.length > index) {
      if(this.state.selectedAttachment[index].type == "image"){
        return (
          <Image
            source={{ uri: this.state.selectedAttachment[index].url }}
            style={{ width: responsiveWidth(23), height: responsiveWidth(23) }}
          />
        );
      } else if (this.state.selectedAttachment[index].type == "video" && Platform.OS == "ios") {
        return (
          <Image
            source={Images.iconVideo}
            style={{ width: responsiveWidth(23), height: responsiveWidth(23) }}
          />
        );
      }else{
        return (
          <Image
            source={{ uri: this.state.selectedAttachment[index].url }}
            style={{ width: responsiveWidth(23), height: responsiveWidth(23) }}
          />
        );
      }
    } else {
      return (
        <Image
          source={Images.addBlue}
          style={{ width: responsiveWidth(23), height: responsiveWidth(23) }}
        />
      );
    }
  }

  // kotak gede preview
  getBigImage() {
    if ( this.state.editedPictureIndex != -1 && this.state.selectedAttachment.length > this.state.editedPictureIndex ) {
      if(this.state.selectedAttachment[this.state.editedPictureIndex].type == "image"){
        return <Image source={{ uri: this.state.selectedAttachment[this.state.editedPictureIndex].url }} style={{ flex: 1 }} />;  
      } else if (this.state.selectedAttachment[this.state.editedPictureIndex].type == "video" && Platform.OS == "ios") {
        return(
          <View style={{ flex: 1, width: responsiveWidth(100) }}>
              <Video
                ref={(ref) => {
                  this.player = ref
                }}
                source={{ uri: this.state.selectedAttachment[this.state.editedPictureIndex].url }}
                style={{ flex:1}}
                rate={1.0}
                paused={false}
                volume={1.0}
                muted={false}
                resizeMode='contain'
                repeat={true} /> 
            </View>
        )
      }else if (this.state.selectedAttachment[this.state.editedPictureIndex].type == "video" && Platform.OS == "android") {
        return(
          <View style={{ flex: 1, width: responsiveWidth(100) }}>
              <Video
                ref={(ref) => {
                  this.player = ref
                }}
                source={{ uri: this.state.selectedAttachment[this.state.editedPictureIndex].url }}
                style={{ flex:1}}
                rate={1.0}
                paused={false}
                volume={1.0}
                muted={false}
                resizeMode='contain'
                repeat={true} /> 
            </View>
        )
      } else{
        return <Image source={{ uri: this.state.selectedAttachment[this.state.editedPictureIndex].url }} style={{ flex: 1 }} />;  
      }
    } else {
      return (
        <Image
          source={Images.addBlue}
          resizeMode="stretch"
          style={{ flex: 1, width: responsiveWidth(100) }}
        />
      );
    }
  }

  formatDraftResult() {
    let result = {
      AuditScheduleDetailID: this.props.auditScheduleDetailID,
      AuditScore: this.state.calculatedScore,
      CompletedTask: this.state.currentCompletion,
      TotalTask: this.state.totalCompletion,
      ScheduleStatus: 1,
      StartAuditDate: this.state.startDate,
      LastUpdateDate: new Date(),
      LastUpdateBy: this.state.userName
    };
    console.log(result);
    return result;
  }

  convertBase64(path) {
    console.log("function convertbase64");
    return new Promise((resolve, reject) => {
      let prom = RNFS.exists(path);
      prom.then(res => {
        if (res == true) {
          console.log("Information - Question Screen - please - file exists");
          let proms = RNFS.readFile(path, "base64");
          proms.then(function(result) {
            console.log(
              "Information - Question Screen - please - file exists - converted"
            );
            resolve(result);
          });
        } else {
          console.log(
            "Information - Question Screen - please - file not exists"
          );
          let proms = RNFS.readFileAssets(Images.imagenotavailable, "base64");
          proms.then(function(result) {
            console.log(
              "Information - Question Screen - please - file not exists - converted"
            );
            resolve(result);
          });
        }
      });
    });
  }

  please() {
    //console.log('plase')
    let Base64Files = {};
    let promises = [];
    let AuditAnswer = [];
    let questions = Queries.getArrayQuestion(this.props.auditType, this.props.templateCode, this.props.auditScheduleDetailID);
    //this.state.questionData.map((value, key) => {
      questions.map((value, key) => {
      value.questions.map((item, index) => {
        let FindingList = [];
        Base64Files[index] = [];
        if (item.answer.answerID == 2) {
          item.answer.findings.map((val, i) => {
            let FindingMedia = [];
            Base64Files[index][i] = [];
            val.attachment.map((row, inc) => {
              FindingMedia[inc] = {
                LocalID: row.attachmentID,
                Files: "NOTHING",
                ExtFiles: row.ext
              };
              // console.log(row.url)
              // console.log(row.ext)

              //let path = row.url.substring(5, row.url.length);
              var path = Platform.OS == "ios" ? row.url : row.url.substring(7, row.url.length);
              // if (Platform.OS == "ios") {
              //   let prom = this.convertBase64(path);
              //   promises.push(prom);
              //   prom.then(res => {
              //     FindingMedia[inc].Files = res;
              //   });
              // } else {
              //   FindingMedia[inc].Files = path;
              // }

              FindingMedia[inc].Files = path;

              // FindingMedia[FindingMedia.length] = {
              // Files: RNFS.readFile(row.url, 'base64'),
              // Files : RNFS.readFile(row.url, 'base64'),
              // Files: 'NOTHING',
              // ExtFiles: row.ext
              // };
              // console.log('keluar')
            });

            FindingList[FindingList.length] = {
              LocalID: val.auditFindingListID,
              AreaID: val.area,
              Comment: val.comments,
              CreatedBy: this.state.userName,
              CreatedDate: val.findingDate,
              FindingMedia: FindingMedia
            };
          });
        }

        AuditAnswer[AuditAnswer.length] = {
          // AuditAnswerId: item.questionID,
          AuditAnswerId: item.questionID,
          TotalFinding:
            item.answer.answerID == 2 ? item.answer.findings.length : 0,
          AnswerID: item.answer.answerID,
          AnswerBy: this.state.userName,
          AnswerDate: item.answer.answerDate,
          FindingList: FindingList
        };
      });
    });

    return new Promise((resolve, reject) => {
      Promise.all(promises).then(() => {
        // AuditAnswer.forEach((value, key) => {
        //   value.FindingList.forEach((item, index) => {
        //     item.FindingMedia.forEach(function (content, counter) {
        //       content.Files = Base64Files[key][index][counter];
        //     });
        //   });
        // });
        resolve(AuditAnswer);
      });
    });

    // console.log('please keluar')

    // return AuditAnswer;
  }

  formatCompleteResult(pdfBase64) {
    let arrayBase64 = [];

    return new Promise((resolve, reject) => {
      this.please().then(res => {
        console.log(res);
        let result = [
          {
            // AuditScheduleDetailID: this.props.auditScheduleDetailID,
            // ResultFiles: pdfBase64,
            // AuditScore: this.state.finalScore,
            // CompletedTask: this.state.currentCompletion,
            // TotalTask: this.state.totalCompletion,
            // ScheduleStatus: 2,
            // AuditeeID: this.state.auditee,
            // AuditeeSignStatus: 1,
            // AuditeeSignDate: this.state.signOffDate,
            // StartAuditDate: this.state.startDate,
            // EndAuditDate: new Date(),
            // LastUpdateDate: new Date(),
            // LastUpdateBy: this.state.userName,
            // AuditAnswer: res

            AuditScheduleDetailID: this.props.auditScheduleDetailID,
            ResultFiles: pdfBase64,
            AuditScore: this.state.calculatedScore,
            CompletedTask: this.state.currentCompletion,
            TotalTask: this.state.totalCompletion,
            ScheduleStatus: 2, // AuditeeID: this.state.usernameAuditee,
            AuditeeID: this.state.selectedAuditee,
            AuditeeSignStatus: 1,
            AuditeeSignDate: this.state.signOffDate,
            StartAuditDate: this.state.startDate,
            EndAuditDate: new Date(),
            LastUpdateDate: new Date(),
            LastUpdateBy: this.state.userName
          },
          {
            AuditScheduleDetailID: this.props.auditScheduleDetailID,
            ResultFiles: pdfBase64,
            AuditScore: this.state.calculatedScore,
            CompletedTask: this.state.currentCompletion,
            TotalTask: this.state.totalCompletion,
            ScheduleStatus: 2, // AuditeeID: this.state.usernameAuditee,
            AuditeeID: this.state.selectedAuditee,
            AuditeeSignStatus: 1,
            AuditeeSignDate: this.state.signOffDate,
            StartAuditDate: this.state.startDate,
            EndAuditDate: new Date(),
            LastUpdateDate: new Date(),
            LastUpdateBy: this.state.userName,
            AuditAnswer: res
          }
        ];

        // Promise.all(arrayBase64.map(Promise.all)).then(function(values) {
        //     console.log(values.base64value);
        // });

        // Promise.all(arrayBase64).then((res)=>{
        //   console.log('done')
        //   // res.map((resBase64, count) => {
        //   //   console.log(resBase64)
        //   //   FindingMedia[count].Files = resBase64
        //   // });
        //   return result;
        // });
        resolve(result);
      });
    });
  }

  componentDidMount() {
    if (Platform.OS == "ios") {
      Camera.checkDeviceAuthorizationStatus().then(
        res => {
          console.log(res);
        }
      );
      Camera.checkVideoAuthorizationStatus().then(
        res => {
          console.log(res);
        }
      );
      Camera.checkAudioAuthorizationStatus().then(
        res => {
          console.log(res);
        }
      );
    }
    //console.log('componentDidMount')
    //console.log(Queries.getAllPersistentData())
    console.log(this.props.auditScheduleDetailID);

    // console.log(this.props.isComplete)
    if (this.props.isComplete) {
      console.log("Question Screen - DidMount - prop complete");
      try {
        console.log("CCAAPI");
        console.log(Constants.APILink + "DoAudit/GetAuditResultDetailById?AuditScheduleDetailId=" + this.props.auditScheduleDetailID + `apuytoken`);

        axios
          .get(
            Constants.APILink +
              "DoAudit/GetAuditResultDetailById?AuditScheduleDetailId=" +
              this.props
                .auditScheduleDetailID,
            {
              headers: {
                Accept:
                  "application/json",
                "Content-Type":
                  "application/json",
                Authorization:
                  "Bearer " +
                  Queries.getPersistentData(
                    "token"
                  )
              },
              timeout: Constants.GetTimeout
            }
          )
          .then(function(response) {
              console.log("lalal blob");
              console.log(response);
              this.setState({
                arrayFinding:
                  response.data,
                totalFinding:
                  response.data.length
              });
            }.bind(this))
          .catch(function(error) {
            this.setState({ loaderStatus: false });
            if (error && error.response && error.response.status == "401"){
              Queries.sessionHabis(res.response.status);
            } else {
              Alert.alert("Connection Failed!", "Unable to retrieve data. Please check your connection.",
              [{text: "Ok"}], 
              {cancelable:false} );
            }
          }.bind(this));
      } catch (err) {
        console.log(err);
      }

      console.log("CCAAPI");
      console.log(Constants.APILink + "DoAudit/GetAnswerDetailById?AuditScheduleDetailId=" + this.props.auditScheduleDetailID + `apuytoken`);
      try {
        axios
          .get(
            Constants.APILink +
              "DoAudit/GetAnswerDetailById?AuditScheduleDetailId=" +
              this.props
                .auditScheduleDetailID,
            {
              headers: {
                Accept:
                  "application/json",
                "Content-Type":
                  "application/json",
                Authorization:
                  "Bearer " +
                  Queries.getPersistentData(
                    "token"
                  )
              },
              timeout:
                Constants.GetTimeout
            }
          )
          .then(function(response) {
              console.log("Get Answer Detail By TrAuditScheduleDetailId");
              console.log(response);

              // let currentcountapi = response.data.current_count.toString();
              // console.log(currentcountapi)
              //console.log(response.data[0].current_count.substring(0, response.data[0].current_count.indexOf("/")));
              //console.log(response.data[0].current_count.substring(response.data[0].current_count.indexOf("/") + 1, response.data[0].current_count.length));

              this.setState({
                calculatedScore: response.data.AuditScore,
                currentCompletion: response.data.CompletedTask,
                totalCompletion: response.data.TotalTask,
                loaderStatus: false
              });
            }.bind(this))
          .catch(function(error) {
              this.setState({
                loaderStatus: false
              });
              // Alert.alert("Connection Failed!", "Catch!");

              console.log(error);
              if (error && error.response && error.response.status == "401") {
                console.log("Information - Result - GetResultPage - 401");
                Queries.sessionHabis(error.response.status);
              } else {
                Alert.alert("Connection Failed!", "Unable to retrieve data. Please check your connection.",
              [{text: "Ok"}], 
              {cancelable:false} );
              }
              // console.log(error);
              //Alert.alert(JSON.stringify(error))
            }.bind(this));
      } catch (err) {
        console.log(err);
      }
    }
  }

  pictureTaken(props) {
    console.log("pictureTaken 20 maret 18"); 
    console.log(props)

    if (props.status) {
      if (props.assetType == "image") {
        ImageResizer.createResizedImage( props.assetPath, this.state.pictureWidth, this.state.pictureHeight, "JPEG", this.state.pictureQuality )
          .then(resizedImageUri => {
            console.log("assets ");
            console.log(props.assetPath);
            console.log(resizedImageUri);

            var convPath = Platform.OS == "ios" ? resizedImageUri.uri : resizedImageUri.path;

            RNFS.unlink(props.assetPath).then(res => {
              RNFS.copyFile(convPath, Platform.OS == "android" ? "file://" + Constants.MediaPath + "/" + resizedImageUri.name : props.assetPath.replace("file:", "")).then(//props.assetPath.replace("file:", "")
                res => {
                  console.log("copy file");
                  let attachments = this.state.selectedAttachment;
                  if (attachments.length > this.state.editedPictureIndex) {
                    attachments[this.state.editedPictureIndex] = { attachmentID: attachments[this.state.editedPictureIndex].attachmentID, ext: props.assetExt, type: props.assetType, url: Platform.OS == "android" ? "file://" + Constants.MediaPath + "/" + resizedImageUri.name : props.assetPath };
                  } else {
                    attachments.push({
                      attachmentID: -1,
                      ext: props.assetExt,
                      type: props.assetType,
                      url:
                        Platform.OS == "android"
                          ? "file://" +
                            Constants.MediaPath +
                            "/" +
                            resizedImageUri.name
                          : props.assetPath
                    });
                  }
                  this.setState({
                    modalFindingPicture: true,
                    selectedAttachment: attachments
                  });

                  console.log("Information - Question Screen - pictureTaken - try unlink");
                  RNFS.unlink(convPath)
                    .then(res => {
                      console.log("Information - Question Screen - pictureTaken - try unlinked - success");
                    })
                    .catch(err => {
                      console.log("Information - Question Screen - pictureTaken - try unlinked - failed");
                    });
                });
            });
          })
          .catch(err => {
            console.log(err);
          });
      }else if (props.assetType == "video" && Platform.OS == "android"){
          console.log("assets video");
          console.log(props.assetPath);

          var convPath = Platform.OS == "ios" ? props.assetPath : props.assetPath.replace("file:", "");
          
          RNFS.copyFile(convPath, Constants.MediaPath +'/'+ props.assetPath.split('/').pop()).then(res => {
            let attachments = this.state.selectedAttachment;
            if (attachments.length > this.state.editedPictureIndex) {
              attachments[this.state.editedPictureIndex] = { 
                attachmentID: attachments[this.state.editedPictureIndex].attachmentID, 
                ext: props.assetExt, 
                type: props.assetType, 
                url: 'file://' + Constants.MediaPath + '/'+props.assetPath.split('/').pop() 
               };
            } else {
              attachments.push({
                attachmentID: -1,
                ext: props.assetExt,
                type: props.assetType,
                url: Platform.OS == "android" ? 'file://'+Constants.MediaPath + '/'+props.assetPath.split('/').pop() : props.assetPath 
              });
            }
            this.setState({
              modalFindingPicture: true,
              selectedAttachment: attachments
            });

            console.log("Information - Question Screen - pictureTaken - try unlink");
            RNFS.unlink(convPath)
              .then(res => {
                console.log("Information - Question Screen - pictureTaken - try unlinked - success");
              })
              .catch(err => {
                console.log("Information - Question Screen - pictureTaken - try unlinked - failed");
              });
          });
          
      } else if (props.assetType == "video" && Platform.OS == "ios"){
        console.log("assets video 20 maret 18ss");
        this.setState({
          loaderStatus:true
        }, function(){
        // console.log(props.assetPath); 
        //'/var/mobile/Containers/Data/Application/1B50D868-822A-4272-86AE-2929A690AA1F/Documents/148B2B15-107A-427F-AFEE-56547DBB36E7.mov'
        //'/var/mobile/Containers/Data/Application/1B50D868-822A-4272-86AE-2929A690AA1F/Library/Caches/7A5E2F79-9080-4070-909B-491D455541D7-363-0000005341513113.mp4'

        var oriFileName = props.assetPath.split("/").pop();
        // console.log("Ori File Name " + oriFileName)
        var destinationPath = props.assetPath.substring(0, oriFileName.length)
        // console.log("Dest Path " + destinationPath)

        var convPath = Platform.OS == "ios" ? props.assetPath : props.assetPath.replace("file:", "");

        // console.log("before convert")
        // RNFS.stat(convPath).then((res)=>{
        //   console.log(res)
        // })

        RNCompress.compressVideo(convPath, "low")
          .then(res => {
            console.log("success compress video");
            console.log(res);

            var newConvPath = res.path

            // RNFS.stat(newConvPath).then(res => {
            //   console.log("after convert");
            //   console.log(res);
            // });

            var newVideoFileName = newConvPath.split("/").pop();

            // console.log("new video file name" + RNFS.DocumentDirectoryPath + newVideoFileName)

            RNFS.copyFile(newConvPath, RNFS.DocumentDirectoryPath +"/"+ newVideoFileName).then(res => {
              console.log("after copy")
              console.log(RNFS.DocumentDirectoryPath + "/" + newVideoFileName);
              let attachments = this.state.selectedAttachment;
              if (attachments.length > this.state.editedPictureIndex) {
                attachments[this.state.editedPictureIndex] = { 
                  attachmentID: attachments[this.state.editedPictureIndex].attachmentID, 
                  ext: props.assetExt, 
                  type: props.assetType,
                  url:  RNFS.DocumentDirectoryPath +"/"+ newVideoFileName
                };
              } else {
                attachments.push({
                  attachmentID: -1,
                  ext: props.assetExt,
                  type: props.assetType,
                  url: Platform.OS == "android" ? 'file://'+Constants.MediaPath + '/'+props.assetPath.split('/').pop() : RNFS.DocumentDirectoryPath +"/"+ newVideoFileName
                });
              }
              // this.setState({
              //   modalFindingPicture: true,
              //   selectedAttachment: attachments
              // });

              console.log("Information - Question Screen - pictureTaken - try unlink");
              RNFS.unlink(convPath)
                .then(res => {
                  console.log("Information - Question Screen - pictureTaken - try unlinked - success");
                  RNFS.unlink(newConvPath)
                  .then(res =>{
                    console.log("Information - Question Screen - pictureTaken - try unlinked - success");
                    
                    this.setState({ loaderStatus: false }, ()=>{ setTimeout(function(){this.setState(
                                                                                         {
                                                                                           modalFindingPicture: true,
                                                                                           selectedAttachment: attachments
                                                                                         },
                                                                                         () => {
                                                                                           console.log(
                                                                                             "modal finding true"
                                                                                           );
                                                                                         }
                                                                                       );}.bind(this), 100); console.log("set loader false ")} );
                  })
                  .catch(err => {
                    console.log("Information - Question Screen - pictureTaken - try unlinked - failed");
                    this.setState({ loaderStatus: false }, ()=>{ this.setState({ modalFindingPicture: true,selectedAttachment: attachments }, ()=>{ console.log("modal finding true")}); console.log("set loader false ")} );
                  });
                })
                .catch(err => {
                  console.log("Information - Question Screen - pictureTaken - try unlinked - failed");
                  this.setState({ loaderStatus: false }, ()=>{ this.setState({ modalFindingPicture: true,selectedAttachment: attachments }, ()=>{ console.log("modal finding true")}); console.log("set loader false ")} );
                });
            })
          })
          .catch(err => {
            console.log(err);
            this.setState({ loaderStatus: false }, ()=>{ this.setState({ modalFindingPicture: true,selectedAttachment: attachments }, ()=>{ console.log("modal finding true")}); console.log("set loader false ")} );
          });
        }.bind(this));
      } else {
        let attachments = this.state.selectedAttachment;
        if (attachments.length > this.state.editedPictureIndex) {
          attachments[this.state.editedPictureIndex] = {
            attachmentID:
              attachments[this.state.editedPictureIndex].attachmentID,
            ext: props.assetExt,
            type: props.assetType,
            url: props.assetPath
          };
        } else {
          attachments.push({
            attachmentID: -1,
            ext: props.assetExt,
            type: props.assetType,
            url: props.assetPath
          });
        }
        this.setState({
          modalFindingPicture: true,
          selectedAttachment: attachments
        });
      }
    } else {
      this.setState({
        modalFindingPicture: true
      });
    }
  }

  keyboardDidShow(e) {
    // let newSize = Dimensions.get('window').height - e.endCoordinates.height;
    // console.log('KEYBOARD SHOW')
    // console.log(Queries.getAllPersistentData())
    // console.log(Queries.getPersistentData('keyboardHeight'))
    if (Queries.getPersistentData("keyboardHeight") == null) {
      console.log("insert to local db");
      Queries.setPersistentData(
        "keyboardHeight",
        e.endCoordinates.height.toString()
      );
    }
    this.setState({ keyboardShowed: true });
    if (this.state.questionFooterStatus == 'backnext' && Platform.OS == 'ios' && this.refs.scroll) { 
      this.refs.scroll.scrollToEnd({ animated: true });
    }
  }
  keyboardDidHide(e) {
    //console.log('KEYBOARD HIDE')
    this.setState({ keyboardShowed: false });
  }

  CalculateScore(questions) {
    let countFinalScore = 0.0;
    let countTotalScore = 0.0;
    let currentCompletion = 0;
    let totalCompletion = 0;

    questions.forEach((item, index) => {
      let tempScore = 0.0;
      let isBoom = false;
      let originalTotal = 0.0;
      let currentTotal = 0.0;
      let isAllNA = true;

      originalTotal = item.questions.reduce((a, b) => a + b.weight, 0.0);
      currentTotal = item.questions
        .filter(item => {
          return item.answer.answerID !== 3;
        })
        .reduce((a, b) => a + b.weight, 0.0);

      item.questions.forEach((value, key) => {
        if (value.answer.answerID == 1 || value.answer.answerID == 2) {
          currentCompletion++;
          totalCompletion++;
          isAllNA = false;
        } else if (value.answer.answerID == -1) {
          totalCompletion++;
          isAllNA = false;
        }

        if (!isBoom) {
          if (value.answer.answerID == 1) {
            tempScore += value.weight;
          } else if (value.answer.answerID == 2 && value.isRequired) {
            tempScore = 0;
            isBoom = true;
          }
        }
      });

      if (!isAllNA) {
        countTotalScore += originalTotal;
      }

      countFinalScore +=
        currentTotal == 0 ? 0 : tempScore * originalTotal / currentTotal;
    });

    return {
      countFinalScore: countFinalScore,
      countTotalScore: countTotalScore,
      currentCompletion: currentCompletion,
      totalCompletion: totalCompletion
    };
  }

  componentWillUnmount() {
    //clearInterval(this.intervalID);

    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  componentWillMount() {
    console.log("Question Screen WillMount");
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this.keyboardDidShow.bind(this)
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this.keyboardDidHide.bind(this)
    );

    let startDate = Queries.getStartDate(this.props.auditScheduleDetailID);
    let currentDate = Queries.getEndDate(this.props.auditScheduleDetailID);
    let areas = Queries.getArrayAreaActive(this.props.groupLineID);
    let listofarea = [];
    console.log(this.props);
    let questions = Queries.getArrayQuestion(
      this.props.auditType,
      this.props.templateCode,
      this.props.auditScheduleDetailID
    );
    console.log(questions);
    let countAnswered = 0;
    let countQuestion = 0;
    let countFinalScore = 0.0;
    let countTotalScore = 0.0;

    let scoreResult = this.CalculateScore(questions);
    countFinalScore = scoreResult.countFinalScore;
    countTotalScore = scoreResult.countTotalScore;
    countAnswered = scoreResult.currentCompletion;
    countQuestion = scoreResult.totalCompletion;

    let rows = this.getRenderListSection(questions);

    console.log("LIST AREA");
    console.log(areas);
    areas.map((value, key) => {
      console.log("value");
      console.log(value);
      listofarea[key] = { label: value.areaDesc, key: value.areaID };
    });

    {
      /*
         let listArea = areas.map((value, key) => {
      listofarea[key] = { areaDesc: value.areaDesc, areaID: value.areaID }
        if(Platform == 'ios'){
        return (
        <PickerItemIOS label={value.areaDesc} value={value.areaID} />
        );
      }else{
        return (
        <Picker.Item label={value.areaDesc} value={value.areaID} />
        );
      }
    });*/
    }

    console.log("WILLMOUNT");

    this.setState({
      questionData: questions,
      currentCompletion: countAnswered,
      totalCompletion: countQuestion,
      finalScore: countFinalScore,
      totalScore: countTotalScore,
      calculatedScore:
        Math.round(countFinalScore * 1000 / countTotalScore) / 10,
      listCategory: rows,
      //listArea: listArea,
      listofarea: listofarea,
      selectedArea: areas.length > 0 ? areas[0].areaID : "",
      startDate: startDate,
      endDate: currentDate
      //secondFromStart: Math.round((currentDate - startDate) / 1000),
    });

    let arrAuditee = [];
    this.selectedAuditeeLabel = "Select an Auditee";
    this.setState({
      selectedAuditee: "Select an Auditee"
    });
    let tempAuditee = { label: "Select an Auditee", key: "Select an Auditee" };
    arrAuditee.push(tempAuditee);
    Queries.getListAuditeeActive(
      this.props.plantid,
      this.props.groupLineID
    ).map(
      function(data, index) {
        // if (index === 0) {
        //     this.selectedAuditeeLabel = data.Username;
        //     this.setState({
        //         selectedAuditee: data.Username
        //     })
        // }
        let tempAuditee = { label: data.Username, key: data.Username };
        arrAuditee.push(tempAuditee);
      }.bind(this)
    );

    this.setState({ arrAuditee: arrAuditee });

    if (this.props.isComplete) {
      this.setState({ loaderStatus: true });
    }
  }

  checkisAuditeeRegistered() {
    console.log(this.props.plantid);
    console.log(this.props.groupLineID);
    console.log(this.props.usernameAuditee);
    return Queries.isAuditeeRegistered(
      this.props.plantid,
      this.props.groupLineID,
      this.state.usernameAuditee
    );
  }

  convertToBase64(path) {
    console.log("convert pdf to base64");
    console.log("file://" + path);
    return new Promise((resolve, reject) => {
      RNFS.readFile("file://" + path, "base64").then(function(result) {
        resolve(result);
      });
    });
  }

  deleteAuditResult() {
    let newData = Queries.getArrayQuestion(
      this.props.auditType,
      this.props.templateCode,
      this.props.auditScheduleDetailID
    );

    let dataIdFindingList = [];
    let dataIdAuditAnswer = [];

    newData.map((value, key) => {
      value.questions.map((data, index) => {
        console.log("question");
        console.log(data);
        data.answer.findings.map((item, i) => {
          item.attachment.map((inneritem, i) => {
            console.log("deleteAttachmentById - " + inneritem.attachmentID);
            Queries.deleteAttachmentById(inneritem.attachmentID);
          });
          console.log(
            "deleteMediaByFindingListID - " + item.auditFindingListID
          );
          Queries.deleteMediaByFindingListID(item.auditFindingListID);
        });
        console.log(
          "deleteFindingListByAuditAnswerID - " + data.answer.trxAuditAnswerID
        );
        Queries.deleteFindingListByAuditAnswerID(data.answer.trxAuditAnswerID);
      });
    });
    console.log(
      "deleteAuditAnswerByAuditScheduleDetailID - " +
        this.props.auditScheduleDetailID
    );
    Queries.deleteAuditAnswerByAuditScheduleDetailID(
      this.props.auditScheduleDetailID
    );
  }

  sendHeader(header, mediaListFinding) {
    this.setState({ loaderStatus: true });
    console.log(
      Constants.APILink + `DoAudit/PostCompleteAuditSchedulev1`,
      header + `token`
    );
    axios
      .post(
        Constants.APILink + `DoAudit/PostCompleteAuditSchedulev1`,
        header,
        {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + Queries.getPersistentData("token")
          },
          timeout: Constants.GetTimeout
        }
      )
      .then(res => {
        if (mediaListFinding.length > 0) {
          if (Platform.OS == "ios") {
            this.sendToNativeDeviceIOS(mediaListFinding);
          } else {
            mediaListFinding.map((item, i) => {
              if (item.ExtFiles == "mp4") {
                item.FlagCompress = 0;
              } else {
                item.FlagCompress = 1;
              }
            });

            console.log("after edit");
            console.log(mediaListFinding);

            this.sendToNativeDeviceAndroid(mediaListFinding);
          }
        } else {
          this.sendDetail();
        }
      })
      .catch(function(error) {
          this.setState({ loaderStatus: false });
          // Alert.alert("Connection Failed!", "Catch!");

          console.log(error);
          if (error && error.response && error.response.status == "401") {
            console.log("Information - Result - GetResultPage - 401");
            Queries.sessionHabis(error.response.status);
          } else {
            Alert.alert(
              "Connection Failed!", 
              "Unable to retrieve data. Please check your connection."),
              [
                {
                  text: "Ok",
                  onPress: () => {
                    this.setState({
                      loaderStatus: false
                    });
                  }
                }
              ], 
              {cancelable:false} ;
          }
          // console.log(error);
          //Alert.alert(JSON.stringify(error))
        }.bind(this));
  }

  sendDetail() {
    this.setState({ loaderStatus: true });

    axios
      .get(
        Constants.APILink +
          `DoAudit/PostCompleteAuditMedia?auditid=` +
          this.props.auditScheduleDetailID,
        {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + Queries.getPersistentData("token")
          },
          timeout: Constants.GetTimeout
        }
      )
      .then(res => {
        this.deleteAuditResult();
        this.refs.timer.stopCounter();
        this.setState({
          isAuditHasBeenComplete: true,
          modalSignOffAuditeeView: false,
          loaderStatus: false
        });
      })
      .catch(function(error) {
          this.setState({ loaderStatus: false });
          // Alert.alert("Connection Failed!", "Catch!");

          console.log(error);
          if (error && error.response && error.response.status == "401") {
            console.log("Information - Result - GetResultPage - 401");
            Queries.sessionHabis(error.response.status);
          } else {
            Alert.alert("Connection Failed!", "Unable to retrieve data. Please check your connection.");
          }
          // console.log(error);
          //Alert.alert(JSON.stringify(error))
        }.bind(this));
  }

  sendToNativeDeviceIOS(mediaListFinding) {
    this.setState({ loaderStatus: true });

    var objData = [];
    mediaListFinding.map((item, i) => {
      objData.push(JSON.stringify(item));
    });

    var Service = NativeModules.Service;
    var TokenCoy = Queries.getPersistentData("token");
    var URLAPI = Constants.APILink;
    Service.addEvent(objData, TokenCoy, URLAPI, callback => {
      console.log("blabla " + callback);

      if (callback == "No") {
        this.setState({
          loaderStatus: false
        });

        setTimeout(() => {
          Alert.alert(
            "Information",
            `Failed to send data to Native`,
            [
              {
                text: "Retry",
                onPress: () => {
                  this.sendToNativeDeviceIOS(mediaListFinding);
                }
              }
            ],
            { cancelable: false }
          );
        }, 200);
      } else if (callback == "Ok") {
        this.deleteAuditResult();

        this.refs.timer.stopCounter();
        this.setState({
          isAuditHasBeenComplete: true,
          modalSignOffAuditeeView: false,
          loaderStatus: false
        });
      }
    });
  }

  sendToNativeDeviceAndroid(mediaListFinding) {
    this.setState({ loaderStatus: true });
    var objData = [];
    mediaListFinding.map((item,i)=>{
      objData.push(JSON.stringify(item))
    });

    let dataAdd = NativeModules.SqliteModule;

    //dataAdd.insertBulk(objData);

    dataAdd.insertBulk(
      objData,
      function(value) {
        if (value == this.props.auditScheduleDetailID) {
          console.log("success send data to native JAVA " + value);

          this.deleteAuditResult();

          this.refs.timer.stopCounter();
          this.setState({
            isAuditHasBeenComplete: true,
            modalSignOffAuditeeView: false,
            loaderStatus: false
          });
        }
      }.bind(this),
      function(err) {
        console.log("error send data to native JAVA " + err);
        this.setState({ loaderStatus: false });

        Alert.alert("Information", `Failed to send data to Native`, [
          {
            text: "Retry",
            onPress: function() {
              this.sendToNativeDeviceAndroid(mediaListFinding);
            }.bind(this)
          }
        ]);
      }.bind(this)
    );
  }

  finishOnPressAuditee() {
    if (this.selectedAuditeeLabel == "Select an Auditee") {
      Alert.alert("Please select an Auditee");
      return;
    }

    this.setState({ loaderStatus: true });
    

    this.formatCompleteResult("").then(resultMedia => {
      //console.log(resultMedia)

      let header = resultMedia[1];
      let mediaListFinding = [];
      let auditid = header.AuditScheduleDetailID;

      header.AuditAnswer.map(function(headerItemAuditAnswer, i) {
        headerItemAuditAnswer.FindingList.map(function(headerItemFindingList,j) {
          headerItemFindingList.FindingMedia.map(function(headerItemFindingMedia,k) {
            console.log("media for upload");

            let mediaForUpload = {
              AuditScheduleDetailID: header.AuditScheduleDetailID,
              AuditAnswerId: headerItemAuditAnswer.AuditAnswerId,
              LocalFindingID: headerItemFindingList.LocalID,
              LocalMediaID: headerItemFindingMedia.LocalID,
              Files: headerItemFindingMedia.Files,
              ExtFiles: headerItemFindingMedia.ExtFiles
            };
            mediaListFinding.push(mediaForUpload);

            headerItemFindingList.FindingMedia[k] = [];
          });
        });
      });

      console.log(mediaListFinding);

      
      this.sendHeader(header, mediaListFinding)
    });
  }

  finishOnPressAuditeeBackup() {
    this.setState(
      {
        questionData: Queries.getArrayQuestion(
          this.props.auditType,
          this.props.templateCode,
          this.props.auditScheduleDetailID
        )
      },
      function() {
        // let email = this.state.emailAuditee;
        // email = email.trim();

        // if (email == '') {
        //   Alert.alert('Email must not be null')
        //   return;
        // }

        if (this.selectedAuditeeLabel == "Select an Auditee") {
          Alert.alert("Please select an Auditee");
          return;
        }

        this.setState({
          loaderStatus: true
        });

        // let result = Queries.isAuditeeRegistered(this.props.plantid, this.props.groupLineID, email)
        // if (result !== null) {
        console.log("auditee exist apuy");

        this.formatCompleteResult("").then(resultMedia => {
          console.log("kirim");

          // resultMedia[0].AuditeeID = result;
          // resultMedia[1].AuditeeID = result;

          console.log(resultMedia);

          // try {
          //   //console.log(Constants.APILink + `DoAudit/PostCompleteAuditHeaderFirstUpload`);
          //   // Header
          //   //console.log(JSON.stringify(resultMedia[0]));
          //   console.log('CCAAPI')
          //   console.log(Constants.APILink + `DoAudit/PostCompleteAuditHeaderFirstUpload`, resultMedia[0] +`token`)
          //   axios.post(Constants.APILink + `DoAudit/PostCompleteAuditHeaderFirstUpload`, resultMedia[0], {
          //     headers: {
          //       "Accept": "application/json",
          //       "Content-Type": "application/json",
          //       "Authorization": "Bearer " + Queries.getPersistentData('token')
          //     }
          //   })
          //     .then(res => {
          //       //console.log("berhasil POST API HEADER")
          //       //console.log(res)
          //       console.log(res)
          //       if (res.data == 0) {
          //         // jika sudah berhasil harus start run on background
          //         // DETAIl              http://ccaaudit.azurewebsites.net/api/DoAudit/PostSubmitAudit
          //         //console.log(JSON.stringify(resultMedia[1]));
          //         var path = '/storage/emulated/0/Documents/test.txt';
          //         RNFS.writeFile(path, JSON.stringify(resultMedia[1]), 'utf8')
          //           .then((success) => {
          //             console.log('FILE WRITTEN!');
          //           })
          //           .catch((err) => {
          //             console.log(err.message);
          //           });

          //           console.log(Constants.APILink + `DoAudit/PostCompleteAuditSchedule`, resultMedia[1] + `apuytoken`)
          //         axios.post(Constants.APILink + `DoAudit/PostCompleteAuditSchedule`, resultMedia[1], {
          //           headers: {
          //             "Accept": "application/json",
          //             "Content-Type": "application/json",
          //             "Authorization": "Bearer " + Queries.getPersistentData('token')
          //           }
          //         })
          //           .then(res => {
          //             //console.log("berhasil POST API DETAIL")
          //             //console.log(res)
          //             //clearInterval(this.intervalID)

          //             this.state.arrayFinding.map(function (data, i) {
          //               for (let i = 0; i < data.blob.length; i++) {
          //                 if (data.blob[i].source != null) {
          //                   RNFS.exists(data.blob[i].source.replace('file:', '')).then(res => {
          //                     RNFS.unlink(data.blob[i].source.replace('file:', ''))
          //                   })
          //                 }
          //               }
          //             })

          //             this.refs.timer.stopCounter();
          //             this.setState({
          //               isAuditHasBeenComplete: true,
          //               modalSignOffAuditeeView: false,
          //               loaderStatus: false
          //             })
          //           })
          //           .catch((error) => {
          //             if (error.response.status == '401') {
          //               this.setState({
          //                 loaderStatus: false
          //               })
          //               Queries.sessionHabis(error.response.status)
          //             } else {
          //               Alert.alert("Failed transmiting files detail")
          //               this.setState({
          //                 loaderStatus: false
          //               })
          //             }
          //             console.log("error submit detail")
          //             console.log(error)

          //           });
          //       }
          //     })
          //     .catch((error) => {
          //       if (error.response.status == '401') {
          //         this.setState({
          //           loaderStatus: false
          //         })
          //         Queries.sessionHabis(error.response.status)

          //       } else {
          //         Alert.alert("Failed transmiting files header")
          //         this.setState({
          //           loaderStatus: false
          //         })
          //       }
          //       console.log("error submit header")
          //       console.log(error)

          //     });
          // } catch (error) {
          //   //console.log(error)
          //   this.setState({
          //     loaderStatus: false
          //   })
          // }
        });
        // } else {
        //   Alert.alert('Auditee not valid')
        //   this.setState({
        //     loaderStatus: false
        //   })
        // }
      }
    );
  }

  finishOnPressAzureAD(emailauditee) {
    {
      let result = Queries.isAuditeeRegistered(
        this.props.plantid,
        this.props.groupLineID,
        emailauditee
      );
      if (result !== null) {
        //console.log('auditee exist')

        this.formatCompleteResult("").then(resultMedia => {
          //console.log('kirim');
          try {
            //console.log(Constants.APILink + `DoAudit/PostCompleteAuditHeaderFirstUpload`);
            // Header
            console.log(
              Constants.APILink + `DoAudit/PostCompleteAuditHeaderFirstUpload`,
              resultMedia[0] + `apuytoken`
            );
            axios
              .post(
                Constants.APILink +
                  `DoAudit/PostCompleteAuditSchedulev1`,
                resultMedia[0],
                {
                  headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization:
                      "Bearer " + Queries.getPersistentData("token")
                  },
                  timeout: Constants.GetTimeout
                }
              )
              .then(res => {
                console.log(res);
                //console.log("berhasil POST API HEADER")
                if (res.data == 0) {
                  // jika sudah berhasil harus start run on background
                  // DETAIl              http://ccaaudit.azurewebsites.net/api/DoAudit/PostSubmitAudit
                  console.log(Constants.APILink + `DoAudit/PostCompleteAuditSchedule`, resultMedia[1] + `apuytoken`);
                  axios
                    .post(
                      Constants.APILink +
                        `DoAudit/PostCompleteAuditSchedule`,
                      resultMedia[1],
                      {
                        headers: {
                          Accept: "application/json",
                          "Content-Type": "application/json",
                          Authorization:
                            "Bearer " +
                            Queries.getPersistentData("token")
                        },
                        timeout: Constants.GetTimeout
                      }
                    )
                    .then(res => {
                      //console.log("berhasil POST API")
                      //console.log(res)
                      //clearInterval(this.intervalID)
                      this.refs.timer.stopCounter();
                      this.setState({
                        isAuditHasBeenComplete: true,
                        modalSignOffAzureADView: false,
                        loaderStatus: false
                      });
                    })
                    .catch(function(error) {
                        this.setState({
                          loaderStatus: false
                        });
                        // Alert.alert("Connection Failed!", "Catch!");

                        console.log(error);
                        if (error && error.response && error.response.status == "401") {
                          console.log("Information - Result - GetResultPage - 401");
                          Queries.sessionHabis(error.response.status);
                        } else {
                          Alert.alert("Connection Failed!", "Unable to retrieve data. Please check your connection.");
                        }
                        // console.log(error);
                        //Alert.alert(JSON.stringify(error))
                      }.bind(this));
                }
              })
              .catch(function(error) {
                  this.setState({ loaderStatus: false });
                  // Alert.alert("Connection Failed!", "Catch!");

                  console.log(error);
                  if (error && error.response && error.response.status == "401") {
                    console.log("Information - Result - GetResultPage - 401");
                    Queries.sessionHabis(error.response.status);
                  } else {
                    Alert.alert("Connection Failed!", "Unable to retrieve data. Please check your connection.");
                  }
                  // console.log(error);
                  //Alert.alert(JSON.stringify(error))
                }.bind(this));
          } catch (error) {
            //console.log(error)
            this.setState({
              loaderStatus: false
            });
          }
        });
      } else {
        Alert.alert("Auditee not valid");
        this.setState({ loaderStatus: false });
      }
    }
  }

  finishOnPress() {
    //console.log('press ' + this.props.auditScheduleDetailID);
    this.setState({ loaderStatus: true });

    if (this.state.finishText == "Finish") {
      if (
        this.state.passwordAuditor == "" ||
        this.state.usernameAuditee == "" ||
        this.state.passwordAuditee == ""
      ) {
        Alert.alert("Value cannot be null");
        this.setState({ loaderStatus: false });
      } else if (this.state.usernameAuditee.length < 3) {
        Alert.alert("Username not valid");
        this.setState({ loaderStatus: false });
      } else if (
        this.state.passwordAuditor !== "1234" ||
        this.state.passwordAuditee !== "1234"
      ) {
        Alert.alert("Password did not match");
        this.setState({ loaderStatus: false });
      } else {
        let result = Queries.isAuditeeRegistered(
          this.props.plantid,
          this.props.groupLineID,
          this.state.usernameAuditee
        );
        if (result !== null) {
          // this.setState({ isAuditHasBeenComplete: true }).bind(this)
          //console.log('auditee exist')

          //this.setState({ finishText: 'On Process' });
          // if (this.state.currentPDFPath == '') {
          //   console.log('path not exist')
          //   this.generatePDF().then(res => {
          //     console.log(res)
          //     this.convertToBase64(res).then((resultconvertbase64) => {
          //       console.log('dapat res')
          //       console.log(resultconvertbase64)
          //     })
          //   })
          // } else {
          //   console.log('path exist')
          // }

          this.formatCompleteResult("").then(resultMedia => {
            //console.log('kirim');
            try {
                  //console.log(Constants.APILink + `DoAudit/PostCompleteAuditHeaderFirstUpload`);
                  // Header
                  console.log(Constants.APILink + `DoAudit/PostCompleteAuditHeaderFirstUpload`, resultMedia[0] + `apuytoken`);
                  axios
                    .post(
                      Constants.APILink +
                        `DoAudit/PostCompleteAuditHeaderFirstUpload`,
                      resultMedia[0],
                      {
                        headers: {
                          Accept: "application/json",
                          "Content-Type": "application/json",
                          Authorization:
                            "Bearer " +
                            Queries.getPersistentData("token")
                        },
                        timeout: Constants.GetTimeout
                      }
                    )
                    .then(res => {
                      //console.log("berhasil POST API HEADER")
                      if (res.data == 0) {
                                           // jika sudah berhasil harus start run on background
                                           // DETAIl              http://ccaaudit.azurewebsites.net/api/DoAudit/PostSubmitAudit
                                           console.log(Constants.APILink + `DoAudit/PostCompleteAuditSchedule`, resultMedia[1] + `apuytoken`);
                                           axios
                                             .post(
                                               Constants.APILink +
                                                 `DoAudit/PostCompleteAuditSchedule`,
                                               resultMedia[1],
                                               {
                                                 headers: {
                                                   Accept:
                                                     "application/json",
                                                   "Content-Type":
                                                     "application/json",
                                                   Authorization:
                                                     "Bearer " +
                                                     Queries.getPersistentData(
                                                       "token"
                                                     )
                                                 },
                                                 timeout:
                                                   Constants.GetTimeout
                                               }
                                             )
                                             .then(
                                               res => {
                                                 //console.log("berhasil POST API")
                                                 //console.log(res)
                                                 //clearInterval(this.intervalID)
                                                 this.setState(
                                                   {
                                                     isAuditHasBeenComplete: true,
                                                     modalSignOffView: false,
                                                     modalSignOffAzureADView: false,
                                                     passwordAuditee:
                                                       "",
                                                     passwordAuditor:
                                                       "",
                                                     usernameAuditee:
                                                       "",
                                                     finishText:
                                                       "Finish",
                                                     loaderStatus: false
                                                   }
                                                 );
                                               }
                                             )
                                             .catch(function(error) {
                                                 this.setState(
                                                   {
                                                     finishText:
                                                       "Finish",
                                                     loaderStatus: false
                                                   }
                                                 );
                                                 // Alert.alert("Connection Failed!", "Catch!");

                                                 console.log(error);
                                                 if (error && error.response && error.response.status == "401") {
                                                   console.log("Information - Result - GetResultPage - 401");
                                                   Queries.sessionHabis(error.response.status);
                                                 } else {
                                                   Alert.alert("Connection Failed!", "Unable to retrieve data. Please check your connection.");
                                                 }
                                                 // console.log(error);
                                                 //Alert.alert(JSON.stringify(error))
                                               }.bind(this));
                                         }
                    })
                    .catch(function(error) {
                        this.setState({
                          finishText: "Finish",
                          loaderStatus: false
                        });
                        // Alert.alert("Connection Failed!", "Catch!");

                        console.log(error);
                        if (error && error.response && error.response.status == "401") {
                          console.log("Information - Result - GetResultPage - 401");
                          Queries.sessionHabis(error.response.status);
                        } else {
                          Alert.alert("Connection Failed!", "Unable to retrieve data. Please check your connection.");
                        }
                        // console.log(error);
                        //Alert.alert(JSON.stringify(error))
                      }.bind(this));
                } catch (error) {
              //console.log(error)
              this.setState({
                finishText: "Finish",
                loaderStatus: false
              });
            }
          });

          // this.generatePDF(true).then(res => {
          //   console.log(res)
          //   this.convertToBase64(res).then((resultconvertbase64) => {
          //     console.log('dapat res')
          //     //console.log(resultconvertbase64)

          //     this.formatCompleteResult(resultconvertbase64).then((resultMedia) => {
          //       // console.log(JSON.stringify(resultMedia));

          //       console.log('kirim');
          //       //console.log(Constants.APILink + `DoAudit/PostCompleteAudit`);
          //       try {
          //         // write the file
          //         // error apabila dengan media 8MB
          //         // var path = '/storage/emulated/0/Documents' + '/test.txt';
          //         // RNFS.writeFile(path, JSON.stringify(resultMedia[1]), 'ascii')
          //         //   .then((success) => {
          //         //     console.log('FILE WRITTEN!');
          //         //     console.log(path)
          //         //   })
          //         //   .catch((err) => {
          //         //     console.log(err.message);
          //         //   });

          //         //console.log(Constants.APILink + `DoAudit/PostCompleteAuditHeaderFirstUpload`);
          //         // Header
          //         axios.post(Constants.APILink + `DoAudit/PostCompleteAuditHeaderFirstUpload`, resultMedia[0], {
          //           headers: {
          //             "Accept": "application/json",
          //             "Content-Type": "application/json"
          //           }
          //         })
          //           .then(res => {
          //             console.log("berhasil POST API HEADER")
          //             if (res.data == 0) {
          //               // jika sudah berhasil harus start run on background
          //               // DETAIl              http://ccaaudit.azurewebsites.net/api/DoAudit/PostSubmitAudit
          //               axios.post(Constants.APILink + `DoAudit/PostCompleteAuditSchedule`, resultMedia[1], {
          //                 headers: {
          //                   "Accept": "application/json",
          //                   "Content-Type": "application/json"
          //                 }
          //               })
          //                 .then(res => {
          //                   console.log("berhasil POST API")
          //                   console.log(res)
          //                   clearInterval(this.intervalID)
          //                   this.setState({
          //                     isAuditHasBeenComplete: true,
          //                     modalSignOffView: false,
          //                     passwordAuditee: '',
          //                     passwordAuditor: '',
          //                     usernameAuditee: '',
          //                     finishText: 'Finish',
          //                     loaderStatus: false
          //                   })
          //                 })
          //                 .catch((error) => {
          //                   console.log("error13")
          //                   console.log(error)
          //                   //Alert.alert("Error transmiting files")
          //                   this.setState({
          //                     finishText: 'Finish',
          //                     loaderStatus: false
          //                   })
          //                 });
          //             }
          //           })
          //           .catch((error) => {
          //             console.log(error)
          //             //Alert.alert("Error transmiting files Header")
          //             this.setState({
          //               finishText: 'Finish',
          //               loaderStatus: false
          //             })
          //           });
          //       } catch (error) {
          //         console.log(error)
          //         this.setState({
          //           finishText: 'Finish',
          //           loaderStatus: false
          //         })
          //       }
          //     });
          //   })
          // })

          // this.formatCompleteResult().then( (resultMedia) => {
          //   console.log(JSON.stringify(resultMedia));
          //   console.log('kirim')

          //    axios.post(`http://192.168.2.132:9565/api/DoAudit/PostCompleteAudit`, resultMedia, {
          //       headers: {
          //         "Accept": "application/json",
          //         "Content-Type": "application/json"
          //       }
          //     })
          //     .then(res => {
          //         console.log("berhasil")
          //         console.log(res)
          //     })
          //     .catch((error) => {
          //         console.log(error)
          //     });
          // });
        } else {
          Alert.alert("Auditee not valid");
          this.setState({
            finishText: "Finish",
            loaderStatus: false
          });
        }
      }
    }
  }

  mainOuterContainer() {
    if (this.state.isAuditHasBeenComplete) {
      //if (true) {
      // timer.setTimeout(
      //   this, 'hideMsg', () => Actions.HomeScreen({ type: 'replace' }), 5000
      // )
      return (
        <TouchableHighlight
          onPress={() => {
            this.leftOnPress();
          }}
          style={{ flex: 1 }}
        >
          <View
            style={{
              backgroundColor: "#c00",
              flex: 1,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Image
              style={{
                resizeMode: "contain",
                width: responsiveWidth(70),
                height: responsiveWidth(70)
              }}
              source={require("../Images/New/completed.png")}
            />

            <Text
              style={[
                style.font.fontContentSmall,
                {
                  color: "white",
                  marginTop: responsiveHeight(5)
                }
              ]}
            >
              {" "}
              Tap to dismiss{" "}
            </Text>
          </View>
        </TouchableHighlight>
      );
    } else if (this.state.questionFooterStatus == "submitauditee") {
      return (
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            alignSelf: "center",
            marginTop: responsiveHeight(13)
          }}
        >
          <Image
            style={{
              resizeMode: "cover",
              width: responsiveWidth(40),
              height: responsiveWidth(40)
            }}
            source={require("../Images/New/verification.png")}
          />
          <Text
            style={{
              textAlign: "center",
              fontSize: newResponsiveFontSize(2),
              padding: responsiveWidth(5)
            }}
          >
            Auditee
          </Text>
          <ModalPicker
            data={this.state.arrAuditee}
            initValue={this.selectedAuditeeLabel}
            onChange={option => {
              this.selectedAuditeeLabel = option.label;
              this.setState({
                selectedAuditee: option.key
              });
            }}
            style={{
              width: responsiveWidth(80),
              backgroundColor: "transparent"
            }}
          >
            <View
              style={{
                flexDirection: "row",
                backgroundColor: "white",
                justifyContent: "space-between"
              }}
            >
              <Text
                style={[
                  style.font.fontContentNormalComboBox,
                  {
                    height: responsiveHeight(5),
                    width: responsiveWidth(50),
                    backgroundColor: "white",
                    paddingLeft: responsiveWidth(2),
                    paddingTop: responsiveHeight(0.5)
                  }
                ]}
                editable={false}
              >
                {this.selectedAuditeeLabel}
              </Text>
              <Icon
                size={responsiveWidth(5)}
                style={{
                  backgroundColor: "white",
                  height: responsiveHeight(5),
                  paddingTop: responsiveHeight(0.5),
                  paddingRight: responsiveWidth(1)
                }}
                name="chevron-down"
              />
            </View>
          </ModalPicker>
        </View>
      );
    } else {
      return <View style={{ flex: 1 }}>{this.mainContainer()}</View>;
    }
  }

  // componentDidUpdate() {
  //   console.log('component did update')
  //   // console.log(this.props.auditScheduleDetailID)
  //   // if (this.state.keyboardShowed) {
  //   //   this.refs.scroll.scrollToEnd({ animated: true });
  //   // }
  // }

  shouldComponentUpdate(nextProps, nextState) {
    console.log("shouldComponentUpdate tanya adhit / viki"); 
    // console.log(this.state.keyboardShowed)
    // if (this.state.keyboardShowed) {
    //   console.log(this.state.keyboardShowed)
    //   this.refs.scroll.scrollToEnd({animated: true});
    // }

    //Check overdue
    var tempAuditScheduleHeader = getInstance()
      .objects("AuditScheduleHeader")
      .filtered('ScheduleCode = "' + this.state.scheduleCode + '"');
    var tempAuditScheduleDetail = getInstance()
      .objects("AuditScheduleDetail")
      .filtered(
        "PK_AuditScheduleDetailID = " + this.state.auditScheduleDetailID + ""
      );

    // console.log(this.state.auditScheduleDetailID)

    if (
      tempAuditScheduleHeader != undefined &&
      tempAuditScheduleHeader != null &&
      tempAuditScheduleHeader != ""
    ) {
      //console.log("masuk cek overdue")
      // console.log(Queries.stringToDateFormat(tempAuditScheduleHeader[0].PeriodEnd))
      // console.log(new Date())
      //console.log(tempAuditScheduleHeader)
      if (
        new Date(
          dateFormat(
            Queries.stringToDateFormat(tempAuditScheduleHeader[0].PeriodEnd),
            "yyyy/mm/dd"
          )
        ) < new Date(dateFormat(new Date(), "yyyy/mm/dd")) &&
        tempAuditScheduleDetail[0].ScheduleStatus == "1"
      ) {
        //data.ScheduleStatus = '2';
        let db = getInstance();
        db.write(() => {
          db.create(
            "AuditScheduleDetail",
            {
              PK_AuditScheduleDetailID:
                tempAuditScheduleDetail[0].PK_AuditScheduleDetailID,
              ScheduleStatus: "2"
            },
            true
          );
        });

        //console.log("overdue ni")
        Actions.HomeScreen({ type: "replace" });
        return false;
      } else {
        //console.log("gak overdue ni")
        return true;
      }
      //return true;
    } else {
      return true;
    }

    // return true;
  }

  // componentDidMount(){
  //   console.log(Queries.getAllPersistentData())
  // }

  _tesAction() {
    console.log("log baru");
  }

  marginTopForIOS() {
    if (Platform.OS === "ios") {
      return <View style={{ height: 20, backgroundColor: "#c00" }} />;
    }
  }

  editListItemMedia(arrayOfAttachment, id) {
    this.setState({
      modalFindingPicture: true,
      selectedAttachment: arrayOfAttachment,
      auditFindingListIDMedia: id
    });
  }

  DoneOnPress() {
    if (this.state.questionFooterStatus == "backnext") {
      this.setState({
        modalFindingPicture: false,
        savedAttachment: this.state.selectedAttachment,
        buttonCameraColor: this.state.selectedAttachment.length > 0 ?  "#c00" : "rgb(237,125,49)"
      });
    } else if (this.state.questionFooterStatus == "exitsubmit") {
      if (this.state.selectedAttachment.length == 0) {
        setTimeout(() => {
          Alert.alert("Information", "At least 1 photo should be submitted.");
        }, 200);
      } else {
        var deleteTemp = [];

        Queries.getAllAttachmentByFindingListId(
          this.state.auditFindingListIDMedia
        ).forEach(
          function(val) {
            let exist = false;
            console.log("DoneOnPress loop 1 " + val.attachmentID);
            this.state.selectedAttachment.forEach(function(innerval) {
              console.log("DoneOnPress loop 2" + innerval.attachmentID);
              if (val.attachmentID == innerval.attachmentID) {
                exist = true;
              }
            });

            if (exist == false) {
              console.log("masuk untuk di delete " + val.attachmentID);
              deleteTemp.push(val.attachmentID);
            }
          }.bind(this)
        );

        for (i = 0; i < deleteTemp.length; i++) {
          console.log("DoneOnPress delete " + deleteTemp[i]);
          Queries.deleteAttachmentById(deleteTemp[i]);
        }

        this.state.selectedAttachment.forEach(
          function(val) {
            console.log("DoneOnPress update attachment - " + val.attachmentID);
            Queries.updateAttachment(
              val.attachmentID,
              this.state.auditFindingListIDMedia,
              val.url,
              val.ext,
              val.type,
              Queries.getCurrentUser()[0].FullName,
              new Date()
            );
          }.bind(this)
        );

        let newData = Queries.getArrayQuestion(
          this.props.auditType,
          this.props.templateCode,
          this.props.auditScheduleDetailID
        );
        console.log("DoneOnPress final ");
        this.setState(
          {
            questionData: newData,
            modalFindingPicture: false
          },
          function(res) {
            console.log("DoneOnPress try convert array finding");
            this.convertToArrayFindings();
          }
        );
      }
    }
  }

  viewPDF() {
    console.log("view PDF");
    if (Platform.OS == "ios") {
      console.log("view PDF IOS");
      //console.log("file://" + this.state.currentPDFPath);

      //return <WebView source={{ uri: this.state.currentPDFPath }} />;
      return <Pdf source={ {uri : this.state.currentPDFPath}} onLoadComplete={(numberOfPages, filePath) => {
            console.log(`number of pages: ${numberOfPages}`);
          }} onPageChanged={(page, numberOfPages) => {
            console.log(`current page: ${page}`);
          }} onError={error => {
            console.log(error);
          }} style={{
            flex:1,
            width:Dimensions.get('window').width
          }} />;
    } else {
      return (
        <PDFView
          ref={pdf => {
            this.pdfView = pdf;
          }} //src={this.state.currentPDFPath}
          src={
            "/storage/emulated/0/Documents" +
            "/" +
            this.props.scheduleCode +
            "-" +
            this.props.grouplinedesc +
            ".pdf"
          }
          onLoadComplete={pageCount => {
            this.pdfView.setNativeProps({
              zoom: 1
            });
          }}
          style={{ flex: 1 }}
        />
      );
    }
  }

  compressVideo() {
        const options = {
            width: this.state.pictureWidth,
            height: this.state.pictureHeight,
            bitrateMultiplier: 3,
            minimumBitrate: 300000
        };
        this.videoPlayerRef.compress(options)
            .then((newSource) => console.log(newSource))
            .catch(console.warn);
    }

   

  render() {
    var heightStatusBar;
    Platform.OS === "ios" ? (heightStatusBar = 0) : (heightStatusBar = 20);

    return this.state.cameraRender ? <Modal transparent={true} animationType="fade" visible={this.state.cameraRender} onRequestClose={() => {
          /*this.refs.timer.startCounter();*/ 
          // this.setState({
          //   cameraRender: false
          // });
          this.pictureTaken({ status: false });
        }}>
        <View style={{ flex: 1 }}>
          {this.marginTopForIOS()}
          {this.state.cameraPreview == true && this.state.pathCameraPreview != "" ? <Image style={{ resizeMode: "cover", flex: 1 }} source={{ uri: this.state.pathCameraPreview }} /> : <Camera ref={cam => {
                this.camera = cam;
              }} 
              captureQuality={this.state.isVideo ? Camera.constants.CaptureQuality.high : Platform.OS == 'android' ? Camera.constants.CaptureQuality.medium : Camera.constants.CaptureQuality.high} 
              aspect={Camera.constants.Aspect.fill} 
              captureMode={this.state.CaptureMode} 
              captureTarget={Camera.constants.CaptureTarget.disk} 
              flashMode={(this.state.isLight ? (this.state.isVideo ? Camera.constants.FlashMode.off : Camera.constants.FlashMode.on)  : Camera.constants.FlashMode.off)}
              torchMode={(this.state.isLight ? (this.state.isVideo ? Camera.constants.TorchMode.on : Camera.constants.TorchMode.off) : Camera.constants.TorchMode.off)}
              //flashMode={this.state.isLight ? Camera.constants.FlashMode.on : Camera.constants.FlashMode.off} 
              type={this.state.isFront ? Camera.constants.Type.front : Camera.constants.Type.back}>
              <View style={{ height: Metrics.screenHeight, width: Metrics.screenWidth }}>
                <View style={{ height: responsiveHeight(10), backgroundColor: "rgba(0, 0, 0, 0.5)", flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                  {this.state.isRecording ? null : <View style={{ flex: 1, flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
                      <TouchableOpacity onPress={() => this.setState({
                            isLight: !this.state.isLight
                          })} style={{ flex: 1, alignItems: "center", justifyContent: "center", height: responsiveHeight(10)}}>
                        <Image source={this.state.isLight ? Images.flashon : Images.flashoff} style={{ height: responsiveHeight(4), width: responsiveHeight(2) }} />
                      </TouchableOpacity>
                      {/*<Switch
                              value={this.state.isLight}
                              onValueChange={() => this.setState({ isLight: !this.state.isLight })} />*/}

                      <TouchableOpacity onPress={() => this.setState({
                            isFront: !this.state.isFront,
                            isLight: false
                          })} style={{ flex: 1, alignItems: "center", justifyContent: "center",height: responsiveHeight(10) }}>
                        <Image source={Images.switchCamera} style={{ height: responsiveHeight(3), width: responsiveHeight(5) }} />
                      </TouchableOpacity>
                      {/*<Text style={{ fontFamily: 'MyriadPro-Regular', fontSize: responsiveFontSize(2), color: 'white' }}>Front</Text>*/}
                      {/*
                            <Switch
                              value={this.state.isFront}
                              onValueChange={() => this.setState({ isFront: !this.state.isFront, isLight: false })} />
                            */}

                      <TouchableOpacity onPress={() => {
                          /*this.refs.timer.startCounter();*/ this.setState(
                            {
                              cameraRender: false
                            }
                          );
                          this.pictureTaken({ status: false });
                        }} style={{ flex: 1, alignItems: "center", justifyContent: "center",height: responsiveHeight(10) }}>
                        <Image source={Images.closeCamera} style={{ height: responsiveHeight(3), width: responsiveHeight(3) }} />
                      </TouchableOpacity>
                    </View>}
                </View>
                <View style={{ height: Metrics.screenHeight - responsiveHeight(10) - 20 - responsiveHeight(15) }} />
                <View style={{ height: responsiveHeight(15), backgroundColor: "rgba(0, 0, 0, 0.5)", flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                  <View style={{ flex: 1 }} />
                  <TouchableOpacity style={{ flex: 1, justifyContent: "center", alignItems: "center" }} onPress={this.capture.bind(this)}>
                    <View style={{ width: responsiveHeight(14), height: responsiveHeight(14), borderRadius: responsiveHeight(7), backgroundColor: this.state.isRecording ? "lightblue" : "white", justifyContent: "center", alignItems: "center" }}>
                      <Text style={{ fontFamily: "MyriadPro-Regular", fontSize: responsiveFontSize(2), color: "black" }}>
                        {this.state.isVideo ? this.state.isRecording ? "Stop" : "Rec" : "Capture"}
                      </Text>
                    </View>
                  </TouchableOpacity>
                  <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    {this.state.isRecording ? null : <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                        <Text style={{ fontFamily: "MyriadPro-Regular", fontSize: responsiveFontSize(2), color: "white" }}>
                          Camera - Video
                        </Text>
                        <Switch value={this.state.isVideo} onValueChange={() => this.setState(
                              {
                                captureMode:
                                  !this.state.isVideo == true
                                    ? Camera.constants.CaptureMode.video
                                    : Camera.constants.CaptureMode.still,
                                isVideo: !this.state.isVideo,
                                isLight: false
                              }
                            )} />
                      </View>}
                  </View>
                </View>
              </View>
            </Camera>}
        </View>

        {this.state.cameraPreview == true && this.state.pathCameraPreview != "" ? <View>
            <TouchableOpacity style={{ height: responsiveWidth(20), width: responsiveWidth(20), alignItems: "center", justifyContent: "center", position: "absolute", bottom: responsiveWidth(10), left: responsiveWidth(10) }} onPress={() => {
                this.setState({
                  cameraPreview: false,
                  pathCameraPreview: ""
                });
              }}>
              <Image style={{ resizeMode: "cover", width: responsiveWidth(7), height: responsiveWidth(7) }} source={require("../Images/New/return-button.png")} />
            </TouchableOpacity>
          </View> : <View />}

        {this.state.cameraPreview == true && this.state.pathCameraPreview != "" ? <View>
            <TouchableOpacity style={{ height: responsiveWidth(20), width: responsiveWidth(20), alignItems: "center", justifyContent: "center", position: "absolute", bottom: responsiveWidth(10), right: responsiveWidth(10) }} onPress={() => {
                this.setState({
                  cameraRender: false,
                  cameraPreview: false,
                  pathCameraPreview: ""
                });
                this.pictureTaken({
                  status: true,
                  assetExt: this.state.pathCameraPreview.split(".").pop(),
                  assetType: "image",
                  assetPath: this.state.pathCameraPreview
                });
              }}>
              <Image style={{ resizeMode: "cover", width: responsiveWidth(7), height: responsiveWidth(7) }} source={require("../Images/check.png")} />
            </TouchableOpacity>
          </View> : <View />}
      </Modal> : <View>
        {this.state.modalUserVisible ? <Modal transparent={true} animationType="fade" visible={this.JPEG} onRequestClose={() => this.setState(
                { modalUserVisible: false }
              )}>
            <TouchableWithoutFeedback onPress={() => this.setState({
                  modalUserVisible: false
                })}>
              <View behavior="padding" style={{ flex: 1, justifyContent: "center", backgroundColor: "rgba(0, 0, 0, 0.5)", alignItems: "center" }}>
                <View style={{ borderRadius: 10, backgroundColor: "#fff", height: Metrics.modalUserHeight, width: Metrics.modalUserWidth }}>
                  <View style={{ flex: 1, justifyContent: "space-between" }}>
                    <View style={{ alignItems: "center" }}>
                      <Text style={{ fontFamily: "MyriadPro-Regular", fontWeight: "bold", fontSize: responsiveFontSize(3), marginTop: responsiveHeight(2) }}>
                        {" "}
                        My Profile{" "}
                      </Text>
                      <Image source={Images.userprofile} style={{ height: responsiveHeight(20), width: responsiveHeight(20), marginTop: responsiveHeight(5) }} />
                      <View style={{ flexDirection: "row", marginTop: responsiveHeight(5) }}>
                        <View>
                          <Text style={{ fontFamily: "MyriadPro-Regular", marginBottom: 10, fontSize: responsiveFontSize(2.5) }}>
                            Name
                          </Text>
                          <Text style={{ fontFamily: "MyriadPro-Regular", marginBottom: 10, fontSize: responsiveFontSize(2.5) }}>
                            Position
                          </Text>
                          <Text style={{ fontFamily: "MyriadPro-Regular", marginBottom: 10, fontSize: responsiveFontSize(2.5) }}>
                            Plant
                          </Text>
                        </View>
                        <View>
                          <Text style={{ fontFamily: "MyriadPro-Regular", marginBottom: 10, fontSize: responsiveFontSize(2.5) }}>
                            : {this.state.fullName}
                          </Text>
                          <Text style={{ fontFamily: "MyriadPro-Regular", marginBottom: 10, fontSize: responsiveFontSize(2.5) }}>
                            : {this.state.userPosition}
                          </Text>
                          <Text style={{ fontFamily: "MyriadPro-Regular", marginBottom: 10, fontSize: responsiveFontSize(2.5) }}>
                            : {this.state.userPlant}
                          </Text>
                        </View>
                      </View>
                    </View>
                    <View style={{ alignItems: "center", justifyContent: "center" }}>
                      <TouchableOpacity style={{ backgroundColor: "#cecece", alignItems: "center", justifyContent: "center", marginBottom: responsiveHeight(2), height: responsiveHeight(7), width: Metrics.modalUserWidth - responsiveWidth(20) }} onPress={this._doLogout.bind(this)}>
                        <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center" }}>
                          <Image source={Images.power} style={{ width: responsiveHeight(5), height: responsiveHeight(5), marginRight: 10 }} />
                          <Text style={{ fontFamily: "MyriadPro-Regular", fontSize: newResponsiveFontSize(1.5), textAlign: "center" }}>
                            Log Out
                          </Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </Modal> : null}

        {this.state.modalSignOffAuditeeView ? <Modal transparent={true} animationType="fade" visible={this.state.modalSignOffAuditeeView} onRequestClose={() => this.setState(
                { modalSignOffAuditeeView: false }
              )}>
            <TouchableWithoutFeedback onPress={() => this.setState({
                  modalSignOffAuditeeView: false
                })}>
              <View behavior="padding" style={{ flex: 1, justifyContent: "center", backgroundColor: "rgba(0, 0, 0, 0.5)", alignItems: "center" }}>
                <View style={{ borderRadius: 30, backgroundColor: "#f2f2f2", width: Metrics.modalUserWidth }}>
                  <View style={{ marginTop: responsiveHeight(3) }}>
                    <Text style={{ fontFamily: "MyriadPro-Bold", fontSize: responsiveFontSize(3.5), marginLeft: responsiveWidth(4), marginTop: responsiveHeight(1) }}>
                      Auditee
                    </Text>
                    <View style={{ backgroundColor: "transparent", height: responsiveHeight(5), alignItems: "center" }}>
                      <ModalPicker data={this.state.arrAuditee} initValue={this.selectedAuditeeLabel} onChange={option => {
                          this.setState({
                            selectedAuditeeLabel: option.label,
                            selectedAuditee: option.key
                          });
                        }} style={{ flex: 4, width: responsiveWidth(80), backgroundColor: "transparent" }}>
                        <View style={{ flexDirection: "row" }}>
                          <Text style={{ fontSize: responsiveFontSize(2.5), height: responsiveHeight(5), width: responsiveWidth(70), backgroundColor: "white", paddingLeft: responsiveWidth(2), paddingTop: 5, fontFamily: "MyriadPro-Regular" }} editable={false}>
                            {this.selectedAuditeeLabel}
                          </Text>
                          <Icon size={responsiveWidth(6)} style={{ backgroundColor: "white", height: responsiveHeight(5), paddingRight: responsiveWidth(20), padding: responsiveWidth(1) }} name="chevron-down" />
                        </View>
                      </ModalPicker>
                    </View>

                    {
                      // <Input
                      // onChange={(event) => this.setState({ emailAuditee: event.nativeEvent.text })}
                      // keyboardType="email-address"
                      // style={{ fontFamily: 'MyriadPro-Regular', fontSize: responsiveFontSize(3), marginLeft: responsiveWidth(4), marginRight: responsiveWidth(4), paddingLeft: responsiveWidth(4), paddingRight: responsiveWidth(4), marginTop: responsiveHeight(1), height: responsiveHeight(8), backgroundColor: 'white' }} placeholder="email address" />
                    }

                    <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center", marginTop: responsiveHeight(3), marginBottom: responsiveHeight(3) }}>
                      <Button style={{ backgroundColor: "#DB2D42" }} onPress={() => this.finishOnPressAuditee()}>
                        <Text style={{ padding: responsiveWidth(5), fontSize: newResponsiveFontSize(1.5), color: "white", fontFamily: "MyriadPro-Regular" }}>
                          {this.state.finishText}
                        </Text>
                      </Button>
                    </View>
                  </View>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </Modal> : null}

        {this.state.modalSignOffView ? <Modal transparent={true} animationType="fade" visible={this.state.modalSignOffView} onRequestClose={() => this.setState(
                { modalSignOffView: false }
              )}>
            <TouchableWithoutFeedback onPress={() => this.setState({
                  modalSignOffView: false
                })}>
              <View behavior="padding" style={{ flex: 1, justifyContent: "center", backgroundColor: "rgba(0, 0, 0, 0.5)", alignItems: "center" }}>
                <View style={{ borderRadius: 30, backgroundColor: "#f2f2f2", width: Metrics.modalUserWidth }}>
                  <View style={{ marginTop: responsiveHeight(3) }}>
                    <Text style={{ fontFamily: "MyriadPro-Bold", fontSize: responsiveFontSize(3.5), marginLeft: responsiveWidth(4) }}>
                      Auditor
                    </Text>
                    <Input onChange={event => this.setState({
                          passwordAuditor: event.nativeEvent.text
                        })} style={{ fontFamily: "MyriadPro-Regular", fontSize: responsiveFontSize(3), marginLeft: responsiveWidth(4), marginRight: responsiveWidth(4), paddingLeft: responsiveWidth(4), paddingRight: responsiveWidth(4), height: responsiveHeight(8), backgroundColor: "white" }} placeholder="password" secureTextEntry={true} />

                    <Text style={{ fontFamily: "MyriadPro-Bold", fontSize: responsiveFontSize(3.5), marginLeft: responsiveWidth(4), marginTop: responsiveHeight(1) }}>
                      Auditee
                    </Text>
                    <Input onChange={event => this.setState({
                          usernameAuditee: event.nativeEvent.text
                        })} style={{ fontFamily: "MyriadPro-Regular", fontSize: responsiveFontSize(3), marginLeft: responsiveWidth(4), marginRight: responsiveWidth(4), paddingLeft: responsiveWidth(4), paddingRight: responsiveWidth(4), marginTop: responsiveHeight(1), height: responsiveHeight(8), backgroundColor: "white" }} placeholder="username" />
                    <Input onChange={event => this.setState({
                          passwordAuditee: event.nativeEvent.text
                        })} style={{ fontFamily: "MyriadPro-Regular", fontSize: responsiveFontSize(3), marginLeft: responsiveWidth(4), marginRight: responsiveWidth(4), paddingLeft: responsiveWidth(4), paddingRight: responsiveWidth(4), marginTop: responsiveHeight(1), height: responsiveHeight(8), backgroundColor: "white" }} placeholder="password" secureTextEntry={true} />

                    <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center", marginTop: responsiveHeight(3), marginBottom: responsiveHeight(3) }}>
                      <Button style={{ backgroundColor: "#DB2D42" }} onPress={() => this.finishOnPress()}>
                        <Text style={{ fontSize: newResponsiveFontSize(1.5), color: "white", fontFamily: "MyriadPro-Regular" }}>
                          {this.state.finishText}
                        </Text>
                      </Button>

                      {
                        // <Text
                        //   onPress={() => this.finishOnPress()}
                        //   style={{
                        //     fontFamily: 'MyriadPro-Regular',
                        //     fontSize: responsiveFontSize(3),
                        //     backgroundColor: 'red',
                        //     color: 'white',
                        //     paddingTop: responsiveHeight(2),
                        //     height: responsiveHeight(8),
                        //     width: responsiveWidth(25),
                        //     textAlign: 'center'
                        //   }}>{this.state.finishText}
                        // </Text>
                      }
                    </View>
                  </View>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </Modal> : null}

        {this.state.modalPDFPreview ? <Modal transparent={false} animationType="fade" visible={this.state.modalPDFPreview} onRequestClose={() => this.setState(
                { modalPDFPreview: false }
              )}>
            <View style={{ flex: 1, width: responsiveWidth(100) }}>
              {this.marginTopForIOS()}
              <View style={{ height: responsiveHeight(7), backgroundColor: "#c55a11", flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
                <Text style={{ flex: 1, color: "white", paddingLeft: responsiveWidth(1), fontWeight: "bold", fontFamily: "MyriadPro-Regular", fontSize: responsiveFontSize(3) }} onPress={() => this.setState(
                      { modalPDFPreview: false }
                    )}>
                  EXIT
                </Text>
                <Text style={{ flex: 3, textAlign: "center", color: "white", fontWeight: "bold", fontFamily: "MyriadPro-Regular", fontSize: responsiveFontSize(4) }}>
                  Preview PDF
                </Text>
                <Text style={{ flex: 1 }} />
              </View>

              {this.viewPDF()}
            </View>
          </Modal> : null}

        <Modal transparent={true} animationType="fade" visible={this.state.viewDeleteFindingList} onRequestClose={() => console.log("modal close")}>
          <View behavior="padding" style={{ flex: 1, justifyContent: "center", backgroundColor: "rgba(0, 0, 0, 0.5)", borderWidth: 1, alignItems: "center" }}>
            <View style={{ backgroundColor: "white", width: Metrics.modalUserWidth }}>
              <View style={{ flexDirection: "row", backgroundColor: "#C00", justifyContent: "center", padding: responsiveWidth(2) }}>
                <Text style={{ fontFamily: "MyriadPro-Regular", flex: 1, color: "white", fontSize: newResponsiveFontSize(2) }}>
                  {" "}
                  Delete{" "}
                </Text>
                <TouchableOpacity style={{ flex: 1, justifyContent: "center", alignItems: "flex-end" }} onPress={() => this.setState(
                      {
                        viewDeleteFindingList: false
                      }
                    )}>
                  <Image source={require("../Images/cancel-music.png")} style={{ height: responsiveHeight(3), width: responsiveHeight(3), resizeMode: "cover" }} />
                </TouchableOpacity>
              </View>
              <View style={{ padding: responsiveWidth(5) }}>
                <Text style={{ fontFamily: "MyriadPro-Regular", fontSize: newResponsiveFontSize(2), textAlign: "center" }}>
                  Are you sure want to delete this finding?{" "}
                </Text>
                <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: responsiveWidth(10), marginBottom: responsiveWidth(10) }}>
                  <Button onPress={() => this.setState({
                        viewDeleteFindingList: false
                      })} style={{ flex: 1, backgroundColor: "grey", padding: responsiveWidth(2), alignItems: "center", justifyContent: "center" }}>
                    <Text style={{ textAlign: "center", color: "white", fontSize: newResponsiveFontSize(1.5), fontFamily: "MyriadPro-Regular" }}>
                      No
                    </Text>
                  </Button>
                  <View style={{ flex: 0.3 }} />
                  <Button onPress={() => this._procDeleteFinding(this.state.indexListMediaToDelete)} style={{ flex: 1, backgroundColor: "grey", padding: responsiveWidth(2), alignItems: "center", justifyContent: "center" }}>
                    <Text style={{ textAlign: "center", color: "white", fontSize: newResponsiveFontSize(1.5), fontFamily: "MyriadPro-Regular" }}>
                      Yes
                    </Text>
                  </Button>
                </View>
              </View>
            </View>
          </View>
        </Modal>

        <Modal transparent={true} animationType="fade" visible={this.state.viewEditFindingList} onRequestClose={() => console.log("modal close")}>
          <View behavior="padding" style={{ flex: 1, justifyContent: "center", backgroundColor: "rgba(0, 0, 0, 0.5)", borderWidth: 1, alignItems: "center" }}>
            <View style={{ backgroundColor: "white", width: Metrics.modalUserWidth }}>
              <View style={{ flexDirection: "row", backgroundColor: "#C00", justifyContent: "center", padding: responsiveWidth(2) }}>
                <Text style={{ fontFamily: "MyriadPro-Regular", flex: 1, color: "white", fontSize: newResponsiveFontSize(2) }}>
                  {" "}
                  Edit{" "}
                </Text>
                <TouchableOpacity style={{ flex: 1, justifyContent: "center", alignItems: "flex-end" }} onPress={() => this.setState(
                      {
                        viewEditFindingList: false
                      }
                    )}>
                  <Image source={require("../Images/cancel-music.png")} style={{ height: responsiveHeight(3), width: responsiveHeight(3), resizeMode: "cover" }} />
                </TouchableOpacity>
              </View>

              <View style={{ padding: responsiveWidth(5), paddingBottom: responsiveWidth(0) }}>
                <Text style={{ fontWeight: "bold", fontFamily: "MyriadPro-Regular", fontSize: newResponsiveFontSize(1.5), textAlign: "left" }}>
                  CATEGORY{" "}
                </Text>
                <Text style={{ borderColor: "black", borderWidth: 1, padding: responsiveWidth(1), fontFamily: "MyriadPro-Regular", fontSize: newResponsiveFontSize(1.5), textAlign: "left" }}>
                  {this.state.editCategoryLabel}{" "}
                </Text>

                <Text style={{ marginTop: responsiveWidth(3), fontWeight: "bold", fontFamily: "MyriadPro-Regular", fontSize: newResponsiveFontSize(1.5), textAlign: "left" }}>
                  AREA{" "}
                </Text>
                <Text style={{ borderColor: "black", borderWidth: 1, padding: responsiveWidth(1), fontFamily: "MyriadPro-Regular", fontSize: newResponsiveFontSize(1.5), textAlign: "left" }}>
                  {this.state.editAreaLabel}{" "}
                </Text>

                <Text style={{ marginTop: responsiveWidth(3), fontWeight: "bold", fontFamily: "MyriadPro-Regular", fontSize: newResponsiveFontSize(1.5), textAlign: "left" }}>
                  COMMENT{" "}
                </Text>
                <TextInput value={this.state.editCommentLabel} multiline={true} onChangeText={text => this.setState(
                      { editCommentLabel: text }
                    )} style={{ marginLeft: responsiveWidth(1), marginRight: responsiveWidth(1), paddingLeft: responsiveWidth(1), paddingRight: responsiveWidth(1), fontSize: newResponsiveFontSize(1.5), fontFamily: "MyriadPro-Regular", borderWidth: 1, height: responsiveHeight(10) }} />
              </View>

              <View style={{ padding: responsiveWidth(5), paddingTop: 0, paddingBottom: responsiveWidth(1) }}>
                <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: responsiveWidth(10), marginBottom: responsiveWidth(10) }}>
                  <View style={{ flex: 1.3 }} />
                  <Button onPress={() => this._procEditFinding()} style={{ flex: 1, backgroundColor: "grey", padding: responsiveWidth(2), alignItems: "center", justifyContent: "center" }}>
                    <Text style={{ textAlign: "center", color: "white", fontSize: newResponsiveFontSize(1.5), fontFamily: "MyriadPro-Regular" }}>
                      Save
                    </Text>
                  </Button>
                </View>
              </View>
            </View>
          </View>
        </Modal>

        {this.state.modalFindingPicture ? <Modal transparent={true} animationType="fade" visible={this.state.modalFindingPicture} onRequestClose={() => this.setState(
                { modalFindingPicture: false }
              )}>
            <View style={{ flex: 1, backgroundColor: "white" }}>
              {this.marginTopForIOS()}

              <View style={{ height: Metrics.marginHeader, flexDirection: "row", justifyContent: "space-around", backgroundColor: "#c00", paddingLeft: responsiveWidth(3), paddingRight: responsiveWidth(3) }}>
                <TouchableOpacity onPress={() => this.setState({
                      modalFindingPicture: false
                    })} style={{ flex: 1, justifyContent: "center" }}>
                  <Icon name="chevron-left" size={responsiveWidth(6)} color="white" />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    console.log("index splice " + this.state.editedPictureIndex);
                    let attachments = this.state.selectedAttachment;
                    if (attachments.length > this.state.editedPictureIndex) {
                      attachments.splice(this.state.editedPictureIndex, 1);
                      this.setState({ selectedAttachment: attachments });
                    }
                  }} style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                  <Image style={{ resizeMode: "cover", width: responsiveWidth(7), height: responsiveWidth(7) }} source={require("../Images/New/rubbish-bin.png")} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.DoneOnPress()} style={{ flex: 1, justifyContent: "center" }}>
                  <Text style={{ textAlign: "right", fontFamily: "MyriadPro-Regular", fontWeight: "bold", fontSize: responsiveFontSize(3), color: "white" }}>
                    DONE
                  </Text>
                </TouchableOpacity>
              </View>
              <TouchableOpacity onPress={() => {
                  this.refs.timer.stopCounter();
                  this.setState(
                    {
                      modalFindingPicture: false,
                      editedPictureIndex:
                        this.state.editedPictureIndex >= 0
                          ? this.state.editedPictureIndex
                          : 0
                    },
                    function() {
                      this.setState({
                        cameraRender: true
                      });
                    }
                  );
                  //console.log("BigPicture = " + this.state.editedPictureIndex)
                }}>
                <View style={{ height: responsiveHeight(63) - Metrics.marginHeader - Metrics.marginIOS, width: responsiveWidth(100) }}>
                  {this.getBigImage()}
                </View>
              </TouchableOpacity>
              <View style={{ height: responsiveHeight(32) }}>
                <View style={{ flex: 1, marginTop: responsiveHeight(1) }}>
                  <View style={{ flex: 1, flexDirection: "row", justifyContent: "space-between", alignItems: "center", paddingLeft: responsiveWidth(1), paddingRight: responsiveWidth(1) }}>
                    <TouchableOpacity style={{ flex: 1 }} onPress={() => {
                        this._onPressAddPicture(0);
                      }}>
                      {this.getAttachedImage(0)}
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 1 }} onPress={() => {
                        this._onPressAddPicture(1);
                      }}>
                      {this.getAttachedImage(1)}
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 1 }} onPress={() => {
                        this._onPressAddPicture(2);
                      }}>
                      {this.getAttachedImage(2)}
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 1 }} onPress={() => {
                        this._onPressAddPicture(3);
                      }}>
                      {this.getAttachedImage(3)}
                    </TouchableOpacity>
                  </View>
                  <View style={{ flex: 1, marginTop: responsiveHeight(1), flexDirection: "row", justifyContent: "space-between", alignItems: "center", paddingLeft: responsiveWidth(1), paddingRight: responsiveWidth(1) }}>
                    <TouchableOpacity style={{ flex: 1 }} onPress={() => {
                        this._onPressAddPicture(4);
                      }}>
                      {this.getAttachedImage(4)}
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 1 }} onPress={() => {
                        this._onPressAddPicture(5);
                      }}>
                      {this.getAttachedImage(5)}
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 1 }} onPress={() => {
                        this._onPressAddPicture(6);
                      }}>
                      {this.getAttachedImage(6)}
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 1 }} onPress={() => {
                        this._onPressAddPicture(7);
                      }}>
                      {this.getAttachedImage(7)}
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              {
                // <View style={{ flex: 0.5, alignItems: 'center', flexDirection: 'row', backgroundColor: 'black' }}>
                //   <TouchableHighlight style={{ flex: 1, justifyContent: 'center', alignItems: 'center', borderRightColor: 'white', borderRightWidth: 1 }}
                //     onPress={() => this.setState({ modalFindingPicture: false })}>
                //     <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                //       <Text style={{ fontFamily: 'MyriadPro-Regular', fontSize: responsiveFontSize(3), textAlign: 'center', color: 'white' }}>Back</Text>
                //     </View>
                //   </TouchableHighlight>
                //   <TouchableHighlight style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.setState({ modalFindingPicture: false, savedAttachment: this.state.selectedAttachment })}>
                //     <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                //       <Text style={{ fontFamily: 'MyriadPro-Regular', fontSize: responsiveFontSize(3), textAlign: 'center', color: 'white' }}>Save</Text>
                //     </View>
                //   </TouchableHighlight>
                // </View>
              }
            </View>
          </Modal> : null}

        {
          // <View style={styles.header}>
          //   <Image source={require('../Images/Quinsys logo putih.png')} style={{ flex: 1, height: responsiveHeight(7), width: responsiveHeight(10), marginLeft: responsiveWidth(2) }}></Image>
          //   <Text style={{ fontFamily: 'MyriadPro-Regular', flex: 2, color: 'white', fontSize: 25, textAlign: 'center' }}>Audit Chamber</Text>
          //   <TouchableOpacity style={{ flex: 1 }} onPress={() => this.setState({ modalUserVisible: true })}>
          //     <View style={{ alignItems: 'flex-end' }}>
          //       <Image source={Images.iconuser} style={{ alignItems: 'flex-end', height: responsiveHeight(5), width: responsiveHeight(5), marginRight: responsiveWidth(5) }}></Image>
          //     </View>
          //   </TouchableOpacity>
          // </View>
        }

        {this.marginTopForIOS()}

        {this.state.isAuditHasBeenComplete == false ? <View style={[styles.header, { justifyContent: "center", alignItems: "center" }]}>
            {this.questionFooter()}
          </View> : null}

        <Spinner visible={this.state.loaderStatus} textContent={"Please Wait"} textStyle={{ color: "#fff" }} cancelable={false} />
        <View style={{ height: this.state.isAuditHasBeenComplete ? responsiveHeight(100) : responsiveHeight(100) - Metrics.marginHeader - heightStatusBar }}>
          {this.mainOuterContainer()}
        </View>
        <Toast ref={component => (this._toast = component)} marginTop={64}>
          Validating Data
        </Toast>
      </View>;
  }
}

var styles = StyleSheet.create({
  container: {
    marginTop: Metrics.marginTopContainer
  },
  header: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center",
    backgroundColor: "#c00",
    height: Metrics.marginHeader
  },
  footer: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "#cecece",
    height: Metrics.footerHeight,
    borderColor: "#c6c6c6",
    borderWidth: 1
  },
  mainContainer: {
    height: Metrics.mainContainerHeightQuestionScreen
    // height : 560.29,
  },
  footerText: {
    fontSize: 5,
    textAlign: "center"
  },
  footerIcon: {
    width: Metrics.footerIconWidth,
    height: Metrics.footerIconHeight
  },
  footerIconContainer: {
    width: Metrics.footerIconContainerWidth,
    height: Metrics.footerIconContainerHeight
  },
  addButton: {
    backgroundColor: "#fff",
    borderColor: "#fff",
    borderWidth: 2,
    height: Metrics.footerIconPlusHeight,
    width: Metrics.footerIconPlusWidth,
    borderRadius: Metrics.footerIconPlusHeight / 2,
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    bottom: responsiveHeight(1),
    right: Metrics.screenWidth / 2 - Metrics.footerIconPlusWidth / 2
  },
  modalPlusText: {
    fontSize: responsiveFontSize(2),
    width: responsiveWidth(70),
    backgroundColor: "white",
    height: responsiveHeight(4),
    alignItems: "center",
    justifyContent: "center",
    textAlign: "center",
    borderColor: "#cecece",
    borderWidth: 1,
    paddingTop: responsiveHeight(1.5)
  },
  groupQuestion: {
    flexDirection: "row",
    backgroundColor: "transparent",
    alignItems: "center",
    paddingLeft: responsiveWidth(2),
    paddingRight: responsiveWidth(2),
    // marginTop: responsiveHeight(1),
    marginBottom: responsiveHeight(1),
    borderBottomColor: "#AAAAAA",
    borderBottomWidth: 1
  }
});
