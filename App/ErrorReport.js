/**//*global __DEV__*/

import { Alert,Platform } from 'react-native';
import StackTrace from 'stacktrace-js';
const Fabric = require('react-native-fabric');
const { Crashlytics } = Fabric;
import RNRestart from 'react-native-restart';

//call this to start capturing any no-handled errors
exports.init = function(captrueOnDebugMode){
  if (__DEV__) {
    return;
  }
  
  const originalHandler = global.ErrorUtils.getGlobalHandler();
  function errorHandler(e) {
    exports.issue(e)
    if (originalHandler) {
      originalHandler(e);
    }
  }
  global.ErrorUtils.setGlobalHandler(errorHandler);
}

//user: {id: ,name: ,email: }
exports.setUser = function(user){
  const {id, name, email} = {id: 'anony', name: 'anony', email: 'anony', ...user};
  Crashlytics.setUserIdentifier(id+'');
  Crashlytics.setUserName(name+'');
  Crashlytics.setUserEmail(email+'');
}

exports.setAttrs = function(obj){
  for(let kk in obj){
    exports.setAttr(kk, obj[kk]);
  }
}

exports.setAttr = function(key, value){
  if(!key) return;
  if(typeof key !== 'string') key = key + '';
  let type = typeof value;
  if(type==='boolean') Crashlytics.setBool(key, value);
  else if(type==='number') Crashlytics.setNumber(key, value);
  else if(type==='string') Crashlytics.setString(key, value);
  else Crashlytics.setString(key, JSON.stringify(value));
}

//things that will be in issue's session logs
exports.log = function(value){
  if(!value) return;

  if(value instanceof Error){
    value = value.stack || value.message;
  }

  if(typeof value !== 'string') value += '';
  return Crashlytics.log(value);
}

//create a new issue. fileName will be the the error message as the `index.bundle.js` is meaningless
exports.issue = function(e){
  return StackTrace.fromError(e, {offline: true}).then((stack)=>{
    return stack.map(row=>{
      let {source, lineNumber} = row;
      if(!lineNumber){
        lineNumber = parseInt(source.split(':').slice(-2, -1)) || 0
      }
      return {fileName: e.message, lineNumber, functionName: source}
    })
  })
  .then((stack)=>{
    Crashlytics.recordCustomExceptionName(e.message, e.message, stack);

    Alert.alert(
        'An error occured',
        `We have reported this to our team. Please restart the app.`,
      [{
        text: 'Restart',
        onPress: () => {
          RNRestart.Restart();
        }
      }]
    );

  })
}

exports.crash = function(){
  return Crashlytics.crash();
}

exports.log = function(message){
// Record a non-fatal JS error only on Android
// Crashlytics.logException('');

// Record a non-fatal JS error only on iOS
// Crashlytics.recordError('something went wrong!')

  return (Platform.OS === 'ios') ? Crashlytics.recordError(JSON.stringify(message)) : Crashlytics.logException(JSON.stringify(message));


}