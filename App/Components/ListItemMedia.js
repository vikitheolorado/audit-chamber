// @flow

import React, { } from 'react'
import { Platform, Linking, View, Text, Alert, TouchableOpacity, TouchableHighlight, Image, Modal, StyleSheet, TouchableWithoutFeedback, Slider, PanResponder, ListView } from 'react-native'
import { Metrics, Images } from '../Themes';
import { ListItem, Button } from 'native-base';
import { responsiveHeight, responsiveWidth, responsiveFontSize, newResponsiveFontSize } from '../Themes/Responsive';
import Video from 'react-native-video';
import Icon from 'react-native-vector-icons/FontAwesome';
//import GestureRecognizer, { swipeDirections } from 'react-native-swipe-gestures';
//import GridView from "react-native-easy-grid-view";
import ImageOnPress from './ImageOnPress';
// import VideoShow from './VideoShow'
// import VideoPlayer from 'react-native-video-controls';
// import { Video } from 'react-native-media-kit';
import PropTypes from 'prop-types';
// import Popup from 'react-native-popup';
import style from '../Themes/Fonts';
import Constants from "../Data/Constants";


export default class ListItemMedia extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      areaLabel: this.props.data.area,
      namaLabel: this.props.data.nama,
      commentLabel: this.props.data.comment,
      blobData: this.props.data.blob,
      modalMediaView: false,
      modalTest: false,
      current: 0,

      rate: 1,
      volume: 1,
      muted: false,
      resizeMode: 'contain',
      duration: 0.0,
      currentTime: 0.0,
      paused: false,
      isBuffering: false,
      _deleteFinding: this.props._deleteFinding,
      _editFinding: this.props._editFinding,
    }
  }


  componentWillMount() {
    console.log("List media will mount")
    console.log(this.props.data)

  }

  static propTypes = {
    data: PropTypes.object.isRequired,
    isComplete: PropTypes.bool.isRequired,
  }

  showModal() {
    console.log('show')
    // console.log(this.props.data);
    this.setState({ modalMediaView: true })
  }

  onLoadStart = () => {
    console.log('On load start fired !');
    this.setState({ modalTest: true })
  }

  onLoad = (data) => {
    console.log('On load fired!');
    this.setState({ duration: data.duration, modalTest: false });
  }

  onProgress = (data) => {
    this.setState({ currentTime: data.currentTime });
  }

  onBuffer({ isBuffering }: { isBuffering: boolean }) {
    this.setState({ isBuffering });
  }

  _handleSeek(event) {
    const screenWidth = Metrics.width;
    const percent = (event.nativeEvent.pageX - responsiveWidth(20)) / responsiveWidth(60);
    this.player.seek(Math.floor(this.state.duration * percent))
  }

  getCurrentTimePercentage() {
    if (this.state.currentTime > 0) {
      return parseFloat(this.state.currentTime) / parseFloat(this.state.duration);
    } else {
      return 0;
    }
  }

  currentMedia() {
    console.log('Information - LisItemMedia - currentMedia')
    console.log(this.props.data)
    if (this.props.data.blob.length > 0) {
      console.log('Information - LisItemMedia - currentMedia - ')

      try {
        if (this.props.data.blob[this.state.current].type == 'image') {
          if (this.props.data.blob[this.state.current].source == null) {
            return (
              <Image source={Images.imagenotavailable} style={{ height: responsiveWidth(60), width: responsiveWidth(60) }} />
            )
          } else {
            return (
              <Image source={{ uri: this.props.data.blob[this.state.current].source }} style={{ height: responsiveWidth(60), width: responsiveWidth(60) }} />
            )
          }

        } else if (this.props.data.blob[this.state.current].type == 'video') {
          const flexCompleted = this.getCurrentTimePercentage() * 100;
          const flexRemaining = (1 - this.getCurrentTimePercentage()) * 100;

          return (
            <View>
              <Video
                ref={(ref) => {
                  this.player = ref
                }}
                source={{ uri: this.props.data.blob[this.state.current].source }}
                style={{ height: responsiveWidth(60), width: responsiveWidth(60) }}
                rate={1.0}
                paused={false}
                volume={1.0}
                muted={false}
                onLoadStart={this.onLoadStart}
                onLoad={this.onLoad}
                onProgress={this.onProgress}
                resizeMode='contain'
                repeat={true} />
              <View style={styles.controls}>
                <TouchableWithoutFeedback onPress={(evt) => this._handleSeek(evt)} >
                  <View style={styles.progress}>
                    <View style={[styles.innerProgressCompleted, { flex: flexCompleted }]} />
                    <View style={[styles.innerProgressRemaining, { flex: flexRemaining }]} />
                  </View>
                </TouchableWithoutFeedback>
              </View>
            </View>
          )
        } else {
          return (
            <View>
              <Text>
                Does not supported file or has been deleted
              </Text>
            </View>
          )
        }
      } catch (error) {
        return (
          <View>
            <Text>
              Does not supported file or has been deleted
            </Text>
          </View>
        )
      }
    }

  }

  back() {
    // Alert.alert('back')
    if (this.state.current === 0) {
    } else {
      this.setState({ current: this.state.current - 1 })
    }
  }

  next() {
    // Alert.alert('next')
    console.log('curr ' + this.state.current);
    console.log('length ' + this.props.data.blob.length);

    if (this.state.current === this.props.data.blob.length - 1) {
    } else {
      this.setState({ current: this.state.current + 1 })
    }
  }

  _generateImageShow(from, to) {
    //Alert.alert("generate Image")
    let sourceData = this.state.blobData;
    let result = sourceData.map(function (data, i) {
      if (i >= from && i <= to) {
        return (
          <Image source={{ uri: sourceData[i].source }} style={{ height: responsiveWidth(25), width: responsiveWidth(25), margin: responsiveWidth(1) }} />
        )
      }
      // if(i >= from && i <= to){
      //   return (
      //     <View />
      //     )
      // }
    })

    return result
  }

  editButton() {
    var arrayMedia = []

    arrayMedia = this.props.data.blob.map(function (media, j) {
      return {
        attachmentID: media.attachmentID,
        ext: media.source.split('.').pop(),
        type: media.type,
        url: media.source
      }
    })


    if (!this.props.isComplete) {
      return (
        <TouchableWithoutFeedback style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} onPress={() => { this.setState({ modalMediaView: false, current:0 }); console.log("edit on press"); console.log(this.props.data.auditFindingListID); this.props._editMedia(arrayMedia, this.props.data.auditFindingListID) }}>
          <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: responsiveHeight(1), marginBottom: responsiveHeight(1) }}>
            <Text style={[style.font.fontContentNormal, { backgroundColor: '#cecece', width: responsiveWidth(40), height: responsiveHeight(3), textAlign: 'center', alignSelf: 'center' }]}>Edit</Text>
          </View>
        </TouchableWithoutFeedback>
      )
    } else {
      return (
        null
      )
    }
  }

  // _renderCell(cell) {
  //   return <View onLayout={event => {
  //     var width = event.nativeEvent.layout.width;
  //     if (this.state.cellWidth != width) {
  //       this.setState({ cellWidth: width })
  //     }
  //     if (this.state.cellHeight != width) {
  //       this.setState({ cellHeight: width })
  //     }
  //   } }>
  //     {/*<View style={{ width: responsiveWidth(20), height: responsiveWidth(20), justifyContent: 'center', backgroundColor: cell.backgroundColor }}
  //       resizeMode={Image.resizeMode.stretch} source={cell.image}>
  //       <Text style={{ backgroundColor: '#0004', textAlign: 'center', color: '#fff', fontSize: 24 }}>{cell.text}</Text>
  //     </View>*/}
  //     <ImageOnPress data={cell.source} />

  //   </View>
  // }

  _onPressFinding() {
    if (this.props.isComplete) {
      //var url = 'http://google.com'
      //var url = 'http://ccaiprdcapcenter001.azurewebsites.net/Finding/Index/' + this.props.data.AuditFindingListId
      var url = Constants.LinkFinding + this.props.data.AuditFindingListId
      Linking.openURL(url).catch(err => console.error('An error occurred', err));
    }
  }

  waitingForAttachment() {
    if (this.props.data.blob.length == 0) {
      return (
        null
      )
    } else {
      // return (
      //   <TouchableHighlight onPress={this.showModal.bind(this)}>
      //     <Image source={{ uri: this.state.blobData[0].source }} style={{ height: responsiveWidth(15), width: responsiveWidth(15) }} />
      //   </TouchableHighlight>
      // )
      if(this.state.blobData[0].type == "video" && Platform.OS == 'ios'){
        return(
          <TouchableHighlight onPress={this.showModal.bind(this)}>
            <Image source={Images.iconVideo}  style={{ height: responsiveWidth(15), width: responsiveWidth(15)}}/>
         </TouchableHighlight>
        )
      }else{
        return(
          <TouchableHighlight onPress={this.showModal.bind(this)}>
            <Image source={{ uri: this.state.blobData[0].source }} style={{ height: responsiveWidth(15), width: responsiveWidth(15) }} />
          </TouchableHighlight>
        )
      } 
    }
  }

  showModalMediaNew() {
    return (
      this.state.modalMediaView ?
        <Modal
          transparent={true}
          animationType="fade"
          visible={this.state.modalMediaView}
          onRequestClose={() => this.setState({ modalMediaView: false, current: 0 })}>
          <TouchableWithoutFeedback onPress={() => this.setState({ modalMediaView: false, current: 0 })}>
            <View
              behavior='padding'
              style={{
                flex: 1,
                justifyContent: 'center',
                backgroundColor: 'rgba(0, 0, 0, 0.5)',
                borderWidth: 1,
                alignItems: 'center'
              }}>
              <TouchableWithoutFeedback>
                <View
                  style={{
                    borderRadius: 10,
                    backgroundColor: '#fff',
                    width: responsiveWidth(80),
                    borderColor: 'transparent',
                    borderWidth: 1,
                    padding: responsiveWidth(1)
                  }} >
                  {
                    // <View style={{ flexDirection: 'row' }}>
                    //   <Text style={{ flex: 1, fontFamily: 'MyriadPro-Regular', fontSize: responsiveFontSize(3.2), paddingLeft: responsiveWidth(1), paddingRight: responsiveWidth(1) }}>{this.props.data.comment}</Text>
                    // </View>
                  }
                  <View style={{ flexDirection: 'row', marginTop: responsiveHeight(1), marginBottom: responsiveHeight(1) }}>
                    <TouchableWithoutFeedback onPress={this.back.bind(this)}>
                      <View style={{ flex: 0.1, borderColor: 'white', borderWidth: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <Icon name="caret-left" size={40} style={{ color: '#cecece' }} />
                      </View>
                    </TouchableWithoutFeedback>

                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                      {/* <Image source={{uri:this.props.data.blob[0]}} style={{ height: responsiveWidth(60), width: responsiveWidth(60)}} /> */}
                      {this.currentMedia()}
                    </View>



                    <TouchableWithoutFeedback onPress={this.next.bind(this)}>
                      <View style={{ flex: 0.1, borderColor: 'white', borderWidth: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <Icon name="caret-right" size={40} style={{ color: '#cecece' }} />
                      </View>
                    </TouchableWithoutFeedback>
                  </View>
                  <View style={{ alignItems: 'center', justifyContent: "center" }}>
                    {this.editButton()}
                  </View>

                </View>
              </TouchableWithoutFeedback>
            </View>
          </TouchableWithoutFeedback>

        </Modal> : null
    )
  }

  render() {
    console.log(this.state.current)
    return (
      <ListItem style={{ backgroundColor: 'white' }} onPress={() => this._onPressFinding()}>
        {this.showModalMediaNew()}


        {/*<Modal
          transparent={true}
          animationType="fade"
          visible={this.state.modalTest}
          onRequestClose={() => this.setState({ modalTest: false })}>
          <TouchableWithoutFeedback onPress={() => this.setState({ modalTest: false, modalMediaView: true })}>
            <View
              behavior='padding'
              style={{
                flex: 1,
                justifyContent: 'center',
                backgroundColor: 'rgba(0, 0, 0, 0.5)',
                borderWidth: 1,
                alignItems: 'center'
              }}>
              <TouchableWithoutFeedback onPress={() => Alert.alert('bebek')}>
                <View
                  style={{
                    borderRadius: 10,
                    backgroundColor: '#fff',
                    width: responsiveWidth(80),
                     
                    borderColor: 'transparent',
                    borderWidth: 1,
                  }} >
                  <GridView dataSource={this.state.dataSource}
                    spacing={2}
                    style={{ padding: 10 }}
                    renderCell={this._renderCell.bind(this)}

                    />
                  <TouchableWithoutFeedback onPress={() => Alert.alert('delete')}>
                    <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: responsiveHeight(1), marginBottom: responsiveHeight(1) }}>
                      <Text style={{ fontFamily: 'MyriadPro-Regular', backgroundColor: '#cecece', width: responsiveWidth(40), height: responsiveHeight(3), textAlign: 'center' }}>Delete</Text>
                    </View>
                  </TouchableWithoutFeedback>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </TouchableWithoutFeedback>
        </Modal>*/}

        <Modal
          transparent={true}
          animationType="fade"
          visible={false}
          onRequestClose={() => this.setState({ modalTest: false })}>
          <TouchableWithoutFeedback onPress={() => this.setState({ modalTest: false, modalMediaView: true })}>
            <View
              behavior='padding'
              style={{
                flex: 1,
                justifyContent: 'center',
                backgroundColor: 'rgba(0, 0, 0, 0.5)',
                borderWidth: 1,
                alignItems: 'center'
              }}>
              <TouchableWithoutFeedback onPress={() => Alert.alert('bebek')}>
                <View
                  style={{
                    borderRadius: 10,
                    backgroundColor: '#fff',
                    width: responsiveWidth(80),

                    borderColor: 'transparent',
                    padding: responsiveWidth(3),
                  }} >


                  {

                    this.state.blobData.length > 4 ?
                      (
                        <View>
                          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            {this._generateImageShow(0, 2)}
                          </View>
                          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            {this._generateImageShow(3, 5)}
                          </View>
                        </View>
                      ) :
                      (
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                          {this._generateImageShow(0, 1)}
                        </View>
                      )


                    // > 4
                    // <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
                    //     <Image source={{ uri: this.state.blobData[0].source }} style={{ height: responsiveWidth(25), width: responsiveWidth(25), margin:responsiveWidth(1) }} />
                    //     <Image source={{ uri: this.state.blobData[0].source }} style={{ height: responsiveWidth(25), width: responsiveWidth(25), margin:responsiveWidth(1) }} />
                    //     <Image source={{ uri: this.state.blobData[0].source }} style={{ height: responsiveWidth(25), width: responsiveWidth(25), margin:responsiveWidth(1) }} />
                    // </View>
                    // <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
                    //     <Image source={{ uri: this.state.blobData[0].source }} style={{ height: responsiveWidth(25), width: responsiveWidth(25), margin:responsiveWidth(1) }} />
                    //     <Image source={{ uri: this.state.blobData[0].source }} style={{ height: responsiveWidth(25), width: responsiveWidth(25), margin:responsiveWidth(1) }} />
                    //     <Image source={{ uri: this.state.blobData[0].source }} style={{ height: responsiveWidth(25), width: responsiveWidth(25), margin:responsiveWidth(1) }} />
                    // </View>

                    // > 2
                    // <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
                    //     <Image source={{ uri: this.state.blobData[0].source }} style={{ height: responsiveWidth(25), width: responsiveWidth(25), margin:responsiveWidth(1) }} />
                    //     <Image source={{ uri: this.state.blobData[0].source }} style={{ height: responsiveWidth(25), width: responsiveWidth(25), margin:responsiveWidth(1) }} />
                    // </View>
                    // <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
                    //     <Image source={{ uri: this.state.blobData[0].source }} style={{ height: responsiveWidth(25), width: responsiveWidth(25), margin:responsiveWidth(1) }} />
                    //     <Image source={{ uri: this.state.blobData[0].source }} style={{ height: responsiveWidth(25), width: responsiveWidth(25), margin:responsiveWidth(1) }} />
                    // </View>

                    // > 0
                    // <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
                    //     <Image source={{ uri: this.state.blobData[0].source }} style={{ height: responsiveWidth(25), width: responsiveWidth(25), margin:responsiveWidth(1) }} />
                    //     <Image source={{ uri: this.state.blobData[0].source }} style={{ height: responsiveWidth(25), width: responsiveWidth(25), margin:responsiveWidth(1) }} />
                    // </View> 
                  }


                  <Button style={{ backgroundColor: 'grey', margin: responsiveWidth(2) }} >
                    <Text style={{ textAlign: 'center', width: responsiveWidth(70), color: 'white', padding: responsiveWidth(2), fontSize: newResponsiveFontSize(1.5), fontFamily: 'MyriadPro-Regular' }}>Save</Text>
                  </Button>

                </View>
              </TouchableWithoutFeedback>
            </View>
          </TouchableWithoutFeedback>
        </Modal>

        <View style={{ flex: 1, flexDirection: 'row' }}>
          {
            this.props.isComplete ? null :
              <TouchableOpacity style={{ flex: 0.3, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.state._deleteFinding(this.props.index)}>
                <Image style={{
                  resizeMode: 'cover', width: responsiveWidth(7), height: responsiveWidth(7)
                }}
                  source={require('../Images/New/cross.png')}
                />
              </TouchableOpacity>
          }

          {
            this.props.isComplete ?
              <View style={{ flex: 1.7, flexDirection: 'row' }}>
                <View style={{ flex: 1, }}>
                  <Text style={{ fontFamily: 'MyriadPro-Regular', fontWeight: 'bold' }}>{this.state.namaLabel}</Text>
                  <Text style={{ marginTop: responsiveHeight(1), fontFamily: 'MyriadPro-Regular' }}>{this.state.commentLabel}</Text>
                </View>
                <View style={{ flex: 0.7, }}>
                  <Text style={{ fontFamily: 'MyriadPro-Regular', fontWeight: 'bold' }}>Area</Text>
                  <Text style={{ marginTop: responsiveHeight(1), fontFamily: 'MyriadPro-Regular' }}>{this.state.areaLabel}</Text>
                </View>
              </View> :
              <TouchableOpacity style={{ flex: 1.7, flexDirection: 'row' }} onPress={() => this.state._editFinding(this.props.index, this.state.namaLabel, this.state.areaLabel, this.state.commentLabel)}>
                <View style={{ flex: 1, }}>
                  <Text style={{ fontFamily: 'MyriadPro-Regular', fontWeight: 'bold' }}>{this.state.namaLabel}</Text>
                  <Text style={{ marginTop: responsiveHeight(1), fontFamily: 'MyriadPro-Regular' }}>{this.state.commentLabel}</Text>
                </View>
                <View style={{ flex: 0.7, }}>
                  <Text style={{ fontFamily: 'MyriadPro-Regular', fontWeight: 'bold' }}>Area</Text>
                  <Text style={{ marginTop: responsiveHeight(1), fontFamily: 'MyriadPro-Regular' }}>{this.state.areaLabel}</Text>
                </View>
              </TouchableOpacity>

          }

        </View>
        <View style={{ flex: 0.2 }}>
          {/*<TouchableHighlight onPress={this.showModal.bind(this)}>
            <Image source={{ uri: this.state.blobData[0].source }} style={{ height: responsiveWidth(15), width: responsiveWidth(15) }} />
          </TouchableHighlight>*/}
          {this.waitingForAttachment()}
        </View>
      </ListItem>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
  },
  fullScreen: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  controls: {
    backgroundColor: 'transparent',
    borderRadius: 5,
    position: 'absolute',
    bottom: 20,
    left: 20,
    right: 20,
  },
  progress: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 3,
    overflow: 'hidden',
  },
  innerProgressCompleted: {
    height: 20,
    backgroundColor: '#cccccc',
  },
  innerProgressRemaining: {
    height: 20,
    backgroundColor: '#2C2C2C',
  },
  generalControls: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 4,
    overflow: 'hidden',
    paddingBottom: 10,
  },
  rateControl: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  volumeControl: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  resizeModeControl: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  controlOption: {
    alignSelf: 'center',
    fontSize: 11,
    color: 'white',
    paddingLeft: 2,
    paddingRight: 2,
    lineHeight: 12,
  },
  track: {
    alignSelf: 'stretch',
    justifyContent: 'center',
    backgroundColor: '#333',
    marginLeft: 28,
    marginRight: 28,
  },
  fill: {
    alignSelf: 'flex-start',
    height: 2,
    width: 1,
  },
  handle: {
    position: 'absolute',
    marginTop: -21,
    marginLeft: -24,
    padding: 16,
    paddingBottom: 4,
  },
  circle: {
    borderRadius: 20,
    height: 12,
    width: 12,
  },
});
