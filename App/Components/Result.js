import React, { Component } from 'react';
import { View, Text, TextInput, Image, TouchableOpacity, Alert, ScrollView } from 'react-native';
import { Picker, Item, Grid, Col } from 'native-base';
import ModalPicker from 'react-native-modal-picker';
import { Metrics, Images } from "../Themes";
import { responsiveWidth, responsiveHeight, responsiveFontSize } from '../Themes/Responsive'
import Icon from 'react-native-vector-icons/FontAwesome';
import axios from 'axios'
import Constants from '../Data/Constants';
import Toast from 'react-native-smart-toast'
import SpinnerFull from 'react-native-loading-spinner-overlay';
import style from '../Themes/Fonts';

export default class Result extends Component {
  constructor(props) {
    super(props);
    let year = []
    let now = 2017
    for (var i = 0; i < 10; i++) {
      // console.log((now + i).toString() + ' key' + i);
      year.push({ "label": (now + i).toString(), "key": 'key' + i })
    }

    this.state = {
      selectedPlant: '',
      selectedType: '',
      selectedMonth: '',
      selectedMonthKey: 0,
      selectedYear: '',

      selectedPlantResult: '',
      selectedPlantKeyResult: 0,
      selectedTypeResult: '',
      selectedMonthResult: '',
      selectedMonthKeyResult: 0,
      selectedYearResult: '',
      resultValue: [],

      type: [
        { "label": "GMP", "key": 'key0' },
        { "label": "G. Visit", "key": 'key1' },
      ],

      // plant: [
      //   { "label": "CIBITUNG", "key": '0' },
      //   { "label": "MEDAN", "key": '2' },
      //   { "label": "LAMPUNG", "key": '4' },
      //   { "label": "BANDUNG", "key": '5' },
      //   { "label": "SEMARANG", "key": '6' },
      //   { "label": "SURABAYA", "key": '7' },
      //   { "label": "BALINUSA", "key": '8' },
      //   { "label": "CIKEDOKAN", "key": '9' },
      // ],

      plant:[],

      month: [
        { "label": "Jan", "key": '1' },
        { "label": "Feb", "key": '2' },
        { "label": "Mar", "key": '3' },
        { "label": "Apr", "key": '4' },
        { "label": "May", "key": '5' },
        { "label": "Juni", "key": '6' },
        { "label": "July", "key": '7' },
        { "label": "Agst", "key": '8' },
        { "label": "Sept", "key": '9' },
        { "label": "Oct", "key": '10' },
        { "label": "Nov", "key": '11' },
        { "label": "Des", "key": '12' },
      ],

      year: year,

      isFillComplete: null,
      resultView: null,

      loaderStatus: false
    }
  }

  componentDidMount(){
    let arrPlant=[]
    Queries.getListPlantActive().map(function (data, index) {
      let temp = { "label": data.PlantDesc, "key": data.PlantID }
      arrPlant.push(temp)
    })

    this.setState({
      plant:arrPlant
    })
  }

  componentWillUnmount() {
    this.setState({ loaderStatus: false })
  }

  goPress() {
    let plant = this.state.selectedPlant;
    let year = this.state.selectedYear;
    // let type = this.state.selectedType;
    let month = this.state.selectedMonth;
    let type = 3;
    // let type = this.state.selectedType === 'GMP' ? 3 : 0;
    //let plant = Queries.getPlantIDByDesc(this.state.selectedPlant);

    if (plant === '' || year === '' || type === '' || month === '' || type != 3) {
      this.setState({ isFillComplete: false })
    } else {
      this.setState({ loaderStatus: true })

      //Alert.alert(type +" " + this.state.selectedPlantKeyResult + " " +  this.state.selectedMonthKey + " " + this.state.selectedYear)
      //Alert.alert(Constants.APILink + 'DoAudit/GetResultPage?AuditType=' + type + '&PlantID=' + this.state.selectedPlantKeyResult + '&Month=' + this.state.selectedMonthKey + '&Year=' +this.state.selectedYear)
      // console.log('CCAAPI')
      console.log(Constants.APILink + 'DoAudit/GetResultPage?AuditType=' + type + '&PlantID=' + this.state.selectedPlantKeyResult + '&Month=' + this.state.selectedMonthKey + '&Year=' + this.state.selectedYear + `apuytoken`)
      axios
        .get(
          Constants.APILink +
            "DoAudit/GetResultPage?AuditType=" +
            type +
            "&PlantID=" +
            this.state.selectedPlantKeyResult +
            "&Month=" +
            this.state.selectedMonthKey +
            "&Year=" +
            this.state.selectedYear,
          {
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization:
                "Bearer " + Queries.getPersistentData("token")
            },
            timeout: Constants.GetTimeout
          }
        )
        .then(function(response) {
            // Alert.alert("Connection Success!", "Data Success");
            // Alert.alert(response[0].length)
            console.log(response.data.length);
            // Alert.alert('good', JSON.stringify(response.data.length));
            //this.resultViewAPI(response);
            this.setState({
              isFillComplete: true,
              result: true,
              selectedPlantResult: plant,
              selectedTypeResult: type,
              selectedMonthResult: month,
              selectedMonthKeyResult: this.state.selectedMonthKey,
              selectedYearResult: year,
              resultValue: response.data
            });
            this.setState({ loaderStatus: false });
            if (response.data.length == 1) {
              this._toast.show({
                position: Toast.constants.gravity.top,
                children: "Nothing data to show"
              });
            }
          }.bind(this))
        .catch(function(error) {
             this.setState({ loaderStatus: false });
            // Alert.alert("Connection Failed!", "Catch!");
           
            console.log(error);
            if (error && error.response && error.response.status == "401") {
              console.log("Information - Result - GetResultPage - 401");
              Queries.sessionHabis(error.response.status);
            }
            else{
              Alert.alert(
                "Connection Failed!",
                "Unable to retrieve data. Please check your connection."
              );
            }
            // console.log(error);
            //Alert.alert(JSON.stringify(error))
          }.bind(this));
    }


  }

  onValuePickerPlantChange(value) {
    console.log(value);
    this.setState({
      selectedPlant: value
    });
  }

  resultView() {
    if (this.state.isFillComplete) {
      if (this.state.result) {
        let loopvalue4 = [
          { "line": '', "value1": 'W1', "value2": 'W2', "value3": 'W3', "value4": 'W4', "fm": 'FM' },
          { "line": 'L1', "value1": 100, "value2": 21, "value3": 12, "value4": 12, "fm": 12 },
          { "line": 'L2', "value1": 12, "value2": 12, "value3": 12, "value4": 12, "fm": 12 },
          { "line": 'L3', "value1": 12, "value2": 12, "value3": 12, "value4": 12, "fm": 12 },
          { "line": 'L4', "value1": 12, "value2": 12, "value3": 12, "value4": 12, "fm": 12 },
          { "line": 'L5', "value1": 12, "value2": 12, "value3": 12, "value4": 12, "fm": 12 },
          { "line": 'SYR', "value1": 12, "value2": 12, "value3": 12, "value4": 12, "fm": 12 },
          { "line": 'WTP', "value1": 12, "value2": 12, "value3": 12, "value4": 12, "fm": 12 },
          { "line": 'WWTP', "value1": 12, "value2": 12, "value3": 12, "value4": 12, "fm": 12 },
          { "line": 'ME', "value1": 12, "value2": 12, "value3": 12, "value4": 12, "fm": 12 },
          { "line": 'IM', "value1": 12, "value2": 12, "value3": 12, "value4": 12, "fm": 12 },
          { "line": 'PA', "value1": 12, "value2": 12, "value3": 12, "value4": 12, "fm": 12 },
          { "line": 'QA', "value1": 12, "value2": 12, "value3": 12, "value4": 12, "fm": 12 },
        ]

        let loopvalue5 = [
          { "line": '', "value1": 'W1', "value2": 'W2', "value3": 'W3', "value4": 'W4', "value5": 'W5', "fm": 'FM' },
          { "line": 'L1', "value1": 100, "value2": 21, "value3": 12, "value4": 12, "value5": 12, "fm": 12 },
          { "line": 'L2', "value1": 12, "value2": 12, "value3": 12, "value4": 12, "value5": 12, "fm": 12 },
          { "line": 'L3', "value1": 12, "value2": 12, "value3": 12, "value4": 12, "value5": 12, "fm": 12 },
          { "line": 'L4', "value1": 12, "value2": 12, "value3": 12, "value4": 12, "value5": 12, "fm": 12 },
          { "line": 'L5', "value1": 12, "value2": 12, "value3": 12, "value4": 12, "value5": 12, "fm": 12 },
          { "line": 'SYR', "value1": 12, "value2": 12, "value3": 12, "value4": 12, "value5": 12, "fm": 12 },
          { "line": 'WTP', "value1": 12, "value2": 12, "value3": 12, "value4": 12, "value5": 12, "fm": 12 },
          { "line": 'WWTP', "value1": 12, "value2": 12, "value3": 12, "value4": 12, "value5": 12, "fm": 12 },
          { "line": 'ME', "value1": 12, "value2": 12, "value3": 12, "value4": 12, "value5": 12, "fm": 12 },
          { "line": 'IM', "value1": 12, "value2": 12, "value3": 12, "value4": 12, "value5": 12, "fm": 12 },
          { "line": 'PA', "value1": 12, "value2": 12, "value3": 12, "value4": 12, "value5": 12, "fm": 12 },
          { "line": 'QA', "value1": 12, "value2": 12, "value3": 12, "value4": 12, "value5": 12, "fm": 12 },
        ]

        let color = 'red'
        //Alert.alert('boom', " " + this.state.resultValue.length);

        const smartTexts = this.state.resultValue.map((field, i) => {

          if (i % 2 === 0) {
            color = '#FFE8E6'
          } else {
            color = '#FED4C8'
          }

          if (i === 0) {
            color = '#FF7400'
          }

          let week = this.state.selectedMonthKeyResult % 3;
          if (week === 0) {
            return (
              //this.rowGridWeek4(field.line, field.value1, field.value2, field.value3, field.value4, field.fm, color)
              this.rowGridWeek5(field.Line, field.W1, field.W2, field.W3, field.W4, field.W5, field.FM, color)
            )
          } else {
            return (
              this.rowGridWeek4(field.Line, field.W1, field.W2, field.W3, field.W4, field.FM, color)
            )
          }
        });

        return (
          // <View>
          //   <Text>* Plese fill with appropriate data</Text>
          // </View>
          <ScrollView>
            <View style={{ flex: 1 }}>
              <View style={{ flex: 1, }}>
                {smartTexts}
              </View>
            </View>
          </ScrollView>
        )
      } else {
        return (
          <View>
            <Text style={style.font.fontContentNormal}>Result not found</Text>
          </View>
        )
      }
    } else {
      return (
        <Text style={style.font.fontContentNormal}>*Please fill appropriate data</Text>
      )
    }

  }

  rowGridWeek5(line, v1, v2, v3, v4, v5, fm, color) {
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginBottom: responsiveHeight(1) }}>
        <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: color, width: responsiveWidth(13), height: responsiveWidth(7) }}>
          <Text style={[style.font.fontContentSmallv1]}>{line}</Text>
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: color, width: responsiveWidth(13), height: responsiveWidth(7) }}>
          <Text style={[style.font.fontContentSmall,]}>{v1}</Text>
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: color, width: responsiveWidth(13), height: responsiveWidth(7) }}>
          <Text style={[style.font.fontContentSmall,]}>{v2}</Text>
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: color, width: responsiveWidth(13), height: responsiveWidth(7) }}>
          <Text style={[style.font.fontContentSmall,]}>{v3}</Text>
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: color, width: responsiveWidth(13), height: responsiveWidth(7) }}>
          <Text style={[style.font.fontContentSmall,]}>{v4}</Text>
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: color, width: responsiveWidth(13), height: responsiveWidth(7) }}>
          <Text style={[style.font.fontContentSmall,]}>{v5}</Text>
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: color, width: responsiveWidth(13), height: responsiveWidth(7) }}>
          <Text style={[style.font.fontContentSmall,]}>{fm}</Text>
        </View>
      </View>
    )
  }

  rowGridWeek4(line, v1, v2, v3, v4, fm, color) {
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginBottom: responsiveHeight(1) }}>
        <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: color, width: responsiveWidth(13), height: responsiveWidth(7) }}>
          <Text style={[style.font.fontContentSmallv1]}>{line}</Text>
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: color, width: responsiveWidth(13), height: responsiveWidth(7) }}>
          <Text style={[style.font.fontContentSmall,]}>{v1}</Text>
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: color, width: responsiveWidth(13), height: responsiveWidth(7) }}>
          <Text style={[style.font.fontContentSmall,]}>{v2}</Text>
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: color, width: responsiveWidth(13), height: responsiveWidth(7) }}>
          <Text style={[style.font.fontContentSmall,]}>{v3}</Text>
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: color, width: responsiveWidth(13), height: responsiveWidth(7) }}>
          <Text style={[style.font.fontContentSmall,]}>{v4}</Text>
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: color, width: responsiveWidth(13), height: responsiveWidth(7) }}>
          <Text style={[style.font.fontContentSmall,]}>{fm}</Text>
        </View>
      </View>
    )
  }


  render() {
    return (
      <View style={{ height: responsiveHeight(100) - Metrics.marginHeader - responsiveHeight(10), backgroundColor: '#F4F1E8' }}>
        <Toast
          ref={component => this._toast = component}
          marginTop={64}>
          Validating Data
                    </Toast>
        <View style={{ flexDirection: 'row', flex: 1 }}>
          <View style={{ flex: 1.5 }}>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={style.font.fontContentLarge}>
                Plant
                </Text>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={style.font.fontContentLarge}>
                Time
                </Text>
            </View>
          </View>
          <View style={{ flex: 4 }}>
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <ModalPicker
                data={this.state.plant}
                initValue={this.state.selectedPlant}
                onChange={(option) => { this.setState({ selectedPlant: option.label, selectedPlantKeyResult: option.key }) }}
                style={{ width: responsiveWidth(55)}}>
                <View style={{ flexDirection: 'row', backgroundColor: 'white', justifyContent: 'space-between' }}>
                  <Text
                    style={[style.font.fontContentNormalComboBox, {
                      height: responsiveHeight(5),
                      width: responsiveWidth(50),
                      backgroundColor: 'white',
                      paddingLeft: responsiveWidth(2),
                      paddingTop: responsiveHeight(0.5),
                    }]}
                    editable={false}>
                    {this.state.selectedPlant}</Text>
                  <Icon
                    size={responsiveWidth(6)}
                    style={{
                      backgroundColor: 'white',
                      height: responsiveHeight(5),
                      paddingTop: responsiveHeight(0.5),
                      paddingRight: responsiveWidth(1)
                    }}
                    name='caret-down' />

                </View>
              </ModalPicker>
            </View>
            <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'center' }}>
              <View style={{ flex: 1, justifyContent: 'center', }}>
                <ModalPicker
                  data={this.state.month}
                  initValue={this.state.selectedMonth}
                  onChange={(option) => { this.setState({ selectedMonth: option.label, selectedMonthKey: option.key }) }}
                  style={{ width: responsiveWidth(25), marginLeft: responsiveWidth(1.5) }}>
                  <View style={{ flexDirection: 'row', backgroundColor: 'white', justifyContent: 'space-between' }}>
                    <Text
                      style={[style.font.fontContentLarge, {
                        fontSize: responsiveFontSize(2.5),
                        backgroundColor: 'white',
                        paddingLeft: responsiveWidth(2),
                        paddingTop: responsiveHeight(0.5),
                        borderColor: 'transparent', borderWidth: 1
                      }]}
                      editable={false}>
                      {this.state.selectedMonth}</Text>
                    <Icon
                      size={responsiveWidth(6)}
                      style={{
                        backgroundColor: 'white',
                        height: responsiveHeight(5),
                        paddingTop: responsiveHeight(0.5),
                        paddingRight: responsiveWidth(1)
                      }}
                      name='caret-down' />
                  </View>
                </ModalPicker>
              </View>
              <View style={{ flex: 1, justifyContent: 'center' }}>
                <ModalPicker
                  data={this.state.year}
                  initValue={this.state.selectedYear}
                  onChange={(option) => { this.setState({ selectedYear: option.label }) }}
                  style={{ width: responsiveWidth(25), marginLeft: responsiveWidth(1.5) }}>
                  <View style={{ flexDirection: 'row', backgroundColor: 'white', justifyContent: 'space-between' }}>
                    <Text
                      style={[style.font.fontContentLarge, {
                        fontSize: responsiveFontSize(2.5),
                        backgroundColor: 'white',
                        paddingLeft: responsiveWidth(2),
                        paddingTop: responsiveHeight(0.5),
                        borderColor: 'transparent', borderWidth: 1
                      }]}
                      editable={false}>
                      {this.state.selectedYear}</Text>
                    <Icon
                      size={responsiveWidth(6)}
                      style={{
                        backgroundColor: 'white',
                        height: responsiveHeight(5),
                        paddingTop: responsiveHeight(0.5),
                        paddingRight: responsiveWidth(1)
                      }}
                      name='caret-down' />
                  </View>
                </ModalPicker>
              </View>
            </View>
          </View>
          <View style={{ flex: 1, padding: responsiveWidth(1), justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity onPress={this.goPress.bind(this)} style={{ flex: 1, width: responsiveWidth(10), backgroundColor: "#c00", alignItems: 'center', justifyContent: 'center' }}>
              <Text style={[style.font.fontContentLarge, { color: 'white' }]}>
                Go
              </Text>
              {/* <Image source={Images.go} style={{ height: responsiveWidth(20), width: responsiveWidth(20) }} /> */}
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ flex: 4 }}>
          {this.resultView()}
        </View>
        <SpinnerFull visible={this.state.loaderStatus} textContent={"Please Wait"} textStyle={[style.font.fontContentSmall, { color: '#fff' }]} cancelable={false} />
      </View>
    )

  }
}
