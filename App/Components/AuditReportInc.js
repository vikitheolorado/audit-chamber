import React, { Component } from 'react';
import {
  ScrollView,
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ListView
} from 'react-native';
import {Icon} from 'native-base';

import Accordion from 'react-native-accordion';

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
export default class AuditReportInc extends Component {
  constructor(){
    super();
    this.state = {
      questionData:
        [
          ["Ringkas", ["Quest 1", "Quest 2", "Quest 3", "Quest 4"], []],
          ["Rapi", ["Quest 1", "Quest 2", "Quest 3"], []],
          ["Resik", ["Quest 1", "Quest 2", "Quest 3", "Quest 4", "Quest 5"], []],
          ["Rawat", ["Quest 1", "Quest 2", "Quest 3", "Quest 4"], []],
          ["Rajin", ["Quest 1", "Quest 2"], []],
          ["Safety", ["Quest 1", "Quest 2", "Quest 3"], []],
        ],
      dataSource: ds.cloneWithRows([]),
    };
  };

  componentDidMount(){
    this.setState({
      dataSource: ds.cloneWithRows(this.state.questionData),
    });
  };

  _renderRow(rowData){
    var setIndicator = function(answerCount, questionCount){
      return {
        width: 16,
        height: 16,
        borderRadius: 8,
        margin: 5,
        backgroundColor: (answerCount >= questionCount ? "#00FF00" : "#FF0000")
      };
    };

    var header = (
      <View style={myStyles.groupQuestion}>
        <Text style={[myStyles.whiteText, {flex:8, fontFamily:'MyriadPro-Regular'}]}>{rowData[0]}</Text>
        <View style={setIndicator(rowData[2].length, rowData[1].length)} />
        <Text style={[myStyles.whiteText, {flex:1, fontFamily:'MyriadPro-Regular'}]}>{rowData[2].length}/{rowData[1].length}</Text>
        <Icon name='logo-windows' />
      </View>
    );
    
    var content = (
      <View style={{height: 200, backgroundColor: '#55CCFF'}}>
        <Text style={{fontFamily:'MyriadPro-Regular'}}>This content is hidden in the accordion</Text>
      </View>
    );

    return (
      <Accordion
        header={header}
        content={content}
        underlayColor= "#AABBCC" />
    );
  };

  _scoring(){
    let totalYes = 0;
    let totalQuestion = 0;
    
    this.state.questionData.forEach((section) => {
      section[2].forEach((answer) => {
        if(answer > -1){
          totalYes += answer;
        }
      });
      totalQuestion += section[1].length;
    });
    return totalYes * 100 / totalQuestion;
  };

  _totalQuestion(){
    let totalQuestion = 0;
    this.state.questionData.forEach((section) => {
      totalQuestion += section[1].length;
    });
    return totalQuestion;
  };

  _totalAnswer(){
    let totalAnswer = 0;
    this.state.questionData.forEach((section) => {
      section[2].forEach((answer) => {
        if(answer > -1){
          totalAnswer++;
        }
      });
    });
    return totalAnswer;
  };

  _setProgressText(){
    return {
      fontSize: 40,
      paddingRight: 10,
      textAlign: 'right',
      color: (this._totalAnswer() >= this._totalQuestion() ? "#00FF00" : "#FFAA00")
    };
  };

  render() {
    return (
      <View style={myStyles.container}>
        <ScrollView>
        <Text style={myStyles.location}>
          Cekidokan Plant - Line 4
        </Text>
        <Text style={myStyles.weekText}>
          Week 4 2017
        </Text>
        <View style={myStyles.textStatus}>
          <Text style={myStyles.leftText}>
            Current Score
          </Text>
          <Text style={myStyles.rightText}>
            Audit Completion
          </Text>
        </View>
        <View style={myStyles.textStatus}>
          <Text style={myStyles.scoreText}>
            {this._scoring()}%
          </Text>
          <Text style={this._setProgressText()}>
            {this._totalAnswer()}/{this._totalQuestion()}
          </Text>
        </View>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={this._renderRow}
          enableEmptySections={true} />
        </ScrollView>
      </View>
    );
  };
}

const myStyles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    backgroundColor: '#F5FCFF',
  },
  location: {
    fontSize: 20,
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    textAlign: 'left',
    color: '#444444', 
    fontFamily:'MyriadPro-Regular'
  },
  weekText: {
    fontSize: 20,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    marginBottom: 10,
    textAlign: 'left',
    color: '#000000', 
    fontFamily:'MyriadPro-Regular'
  },
  leftText: {
    fontSize: 15,
    marginTop: 10,
    marginLeft: 10,
    textAlign: 'left',
    color: '#000000', 
    fontFamily:'MyriadPro-Regular'
  },
  rightText: {
    fontSize: 15,
    marginTop: 10,
    marginRight: 10,
    textAlign: 'right',
    color: '#000000', 
    fontFamily:'MyriadPro-Regular'
  },
  textStatus: {
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
  scoreText: {
    fontSize: 40,
    paddingLeft: 10,
    textAlign: 'left',
    color: '#44FF44', 
    fontFamily:'MyriadPro-Regular'
  },
  groupQuestion: {
    flexDirection: 'row',
    backgroundColor: '#AAAAAA',
    marginTop: 2,
    marginBottom: 2,
    alignItems: 'center',
  },
  whiteText: {
    fontSize: 20,
    color: '#FFFFFF',
    margin: 5, 
    fontFamily:'MyriadPro-Regular'
  },
  arrowIcon: {
    width: 16,
    height: 16,
    margin: 5,
  },
});