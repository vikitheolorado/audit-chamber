// @flow

import React, { } from 'react'
import { View, Image, Text, TouchableWithoutFeedback } from 'react-native'
import { Metrics, Images } from '../Themes';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from '../Themes/Responsive';
import PropTypes from 'prop-types';

export default class ImageOnPress extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            onPress: false
        }
    }

    static propTypes = {
        data: PropTypes.string.isRequired,
    }

    imageToShow() {
        if (this.state.onPress) {
            return (
                <View>
                    <Image
                        source={this.props.data}
                        style={{
                            width: responsiveWidth(20),
                            height: responsiveWidth(20),
                        }}
                        />
                    <Image
                        source={Images.check}
                        style={{
                            width: responsiveWidth(20),
                            height: responsiveWidth(20),
                            position :'absolute',
                            top:0,
                            bottom:0,
                            left:0,
                            right:0,
                            backgroundColor:'rgba(0,0,0,0.5)'

                        }} />
                </View>

            )
        } else {
            return (
                <Image
                    source={this.props.data}
                    style={{
                        width: responsiveWidth(20),
                        height: responsiveWidth(20),
                        backgroundColor: 'red'
                    }}
                    />
            )

        }
    }

    render() {
        return (
            <TouchableWithoutFeedback onPress={() => this.setState({ onPress: !this.state.onPress })} >
                {this.imageToShow()}
            </TouchableWithoutFeedback>
        )
    }
} 