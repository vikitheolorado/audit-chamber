import React, { Component } from 'react';
import { Text } from 'native-base';
import { View } from 'react-native';
import {responsiveFontSize, responsiveHeight, responsiveWidth} from '../Themes/Responsive'
import style from '../Themes/Fonts';

export default class Timer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: this.props.startDate,
            endDate: this.props.endDate,
            isComplete: this.props.isComplete,
            secondFromStart: 0
        };

        this.intervalID = -1;
    }

    componentWillMount(){
        this.stopCounter();

        if(this.state.isComplete){
            this.setState({
                secondFromStart: Math.round((this.state.endDate - this.state.startDate) / 1000)
            });
        }else{
            this.startCounter();
        }
    }

    stopCounter = function(){
        if(this.intervalID != -1){
            clearInterval(this.intervalID);
            this.intervalID = -1;
        }
    }

    startCounter = function(){
        this.setState({
            secondFromStart: Math.round((new Date() - this.state.startDate) / 1000)
        });

        this.intervalID = setInterval(() => {
            this.setState({
                secondFromStart: this.state.secondFromStart + 1
            });
        }, 1000);
    }

    componentWillUnMount(){
        console.log('timer called')
        this.stopCounter();
    }

    render(){
        return(
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Text style={ [style.font.fontContentLarge ,{ color: 'white' }]}>{Math.floor(this.state.secondFromStart / 3600)}:{("0" + (Math.floor(this.state.secondFromStart / 60) % 60)).slice(-2)}:{("0" + (this.state.secondFromStart % 60)).slice(-2)}</Text>
            </View>
        );
    }
}