import React, { Component } from 'react';
import { ListItem, Text, CheckBox, Button, Segment, Separator } from 'native-base';
import { Image, View, Alert, TouchableOpacity,ScrollView } from 'react-native';
var dateFormat = require('dateformat');
import Moment from 'moment';
import { responsiveFontSize, responsiveHeight, responsiveWidth } from '../Themes/Responsive'
import { getInstance } from '../Data/DB/DBHelper';
import Queries from '../Data/Queries';
import { Metrics, Images } from '../Themes';
import Constants from '../Data/Constants';
import Axios from 'axios';
import Icon from 'react-native-vector-icons/FontAwesome';
import TimeAgo from 'react-native-timeago';
import Calendar from 'react-native-calendar';
import style from '../Themes/Fonts';
//var tempTime = "";
export default class Notification extends Component {
    constructor(props) {
        super(props);
        this.MsUser = Queries.getCurrentUser();
        this.MsPlant = getInstance().objects('MsPlant').filtered("PlantID = " + this.MsUser[0].PlantID)

        this.state = {

            readNotification: this.props.readNotification,
            moveToMyAudit: this.props.moveToMyAudit,
            listNotif: (<Text style={{ fontFamily: 'MyriadPro-Regular' }}> </Text>),

            fullName: this.MsUser[0].FullName,
            userName: this.MsUser[0].Username,
            userPosition: this.MsUser[0].Position,
            userPlant: this.MsPlant[0].PlantDesc,
        };
    }

    _onPressButton(notification) {
        if (notification.ReadStatus == 0) {

            // console.log('CCAAPI')
            console.log(Constants.APILink + 'notif/ReadNotification?notificationID=' + notification.NotificationID + `apuytoken`)
            // console.log("Constancs.APILink =" + Constants.APILink)
            // console.log("Notification" + notification.NotificationID)
            console.log("Queries" + Queries.getPersistentData('token'))

            Axios.get(Constants.APILink + 'notif/ReadNotification?notificationID=' + notification.NotificationID, {
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + Queries.getPersistentData('token')
                }
            })
                .then(res => {
                    this.state.readNotification();
                    Queries.ReadNotification(notification.NotificationID);

                    let auditStatus = Queries.getAuditStatus(notification.Data);
                    console.log('Audit Status ' + auditStatus);
                    if ((notification.Type == 3 || (notification.Type == 1 && notification.Message.substring(notification.Message.length - 31) == 'Click here to start your audit!')) && auditStatus == 0) {
                        console.log('Audit ID ' + notification.Data);
                        //this.state.moveToMyAudit('scheduleaudit', notification.Data);
                    }
                    this.loadNotificationData().then(function (result) {
                        this.setState({
                            listNotif: result
                        });
                    }.bind(this)).catch(() => { });
                })
                .catch(error => {
                    if (error && error.response && error.response.status == "401") {
                        console.log("Information - Notification - ReadNotification - 401")
                        Queries.sessionHabis(error.response.status);
                    }
                })
                .done();

        }
    }

    formatDate(date) {
        let monthNames = [
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"
        ];

        let day = date.getDate();
        let monthIndex = date.getMonth();
        let year = date.getFullYear();

        return day + ' ' + monthNames[monthIndex] + ' ' + year;
    }

    loadNotificationData() {
        return new Promise((resolve, reject) => {
            let notificationData = Queries.getNotifications(this.props.userName);
            var tempTime = '';
            var tempTimeYester = '';
            var getDateToday = dateFormat(new Date, "yyyy/mm/dd");

            var newdate = new Date();
            var setdateyesterday = newdate.setDate(newdate.getDate() - 1);
            var getDateYesterday = dateFormat(setdateyesterday, "yyyy/mm/dd");

            console.log("setdateyesterday " + setdateyesterday);
            console.log("getDateYesterday " + getDateYesterday);

            var rowNotif = notificationData.map(function (data, i) {
                console.log("Date = " + data.Date)
                var timeData = this.formatDate(data.Date);
                var tempImage = '';
                var getDateData = dateFormat(data.Date, "yyyy/mm/dd");

                console.log("getDateData " + getDateData);

                if (data.Type === 1 || data.Type === 2 || data.Type === 3) {
                    tempImage = <Image style={{ resizeMode: 'cover', width: responsiveWidth(20), height: responsiveWidth(20) }} source={require('../Images/warning.png')} />
                } else if (data.Type === 4) {
                    tempImage = <Image style={{ resizeMode: 'cover', width: responsiveWidth(20), height: responsiveWidth(20) }} source={require('../Images/smile.png')} />
                } else {
                    tempImage = <Image style={{ resizeMode: 'cover', width: responsiveWidth(20), height: responsiveWidth(20) }} source={require('../Images/sad.png')} />
                }
                if (getDateToday == getDateData) {
                    if (tempTime !== timeData) {
                        tempTime = timeData;
                        return (
                            <View>
                                <Text style={[style.font.fontContentLarge, { borderBottomWidth: 2, borderBottomColor: 'black' }]}>Recent</Text>

                                <TouchableOpacity style={{ paddingTop: responsiveHeight(1) }} onPress={function () { this._onPressButton(data) }.bind(this)}>
                                    <View style={{ flexDirection: 'row' }}>
                                        {tempImage}
                                        <View style={{ marginLeft: responsiveWidth(2), width: responsiveWidth(65) }}>
                                            {/* <Text style={{ fontSize: responsiveFontSize(1.5), fontFamily: 'MyriadPro-Regular' }}>From : {data.From}</Text> */}
                                            <Text>
                                                <Text style={[style.font.fontContentNormal, { fontWeight: (data.ReadStatus == 1 ? 'normal' : 'bold') }]}>Reminder : </Text>
                                                <Text style={[style.font.fontContentNormal,{textAlign:'center'}]}> {data.Message}</Text>
                                            </Text>
                                        {
                                            //<Text style={[style.font.fontContentNormal, { textAlign: 'right' }]}><TimeAgo time={data.Date} /> </Text>
                                        }
                                        <Text style={[style.font.fontContentNormal, { textAlign: 'right' }]}><TimeAgo time={dateFormat(data.Date, "dd mmmm yyyy HH:MM")} /> </Text>
                                        </View>
                                    </View>

                                </TouchableOpacity>
                            </View>
                        );

                    } else {
                        return (
                            <TouchableOpacity style={{ paddingTop: responsiveHeight(1) }} onPress={function () { this._onPressButton(data) }.bind(this)}  >
                                <View style={{ flexDirection: 'row' }}>
                                    {tempImage}
                                    <View style={{ marginLeft: responsiveWidth(2), width: responsiveWidth(65) }}>
                                        {/* <Text style={{ fontSize: responsiveFontSize(1.5), fontFamily: 'MyriadPro-Regular' }}>From : {data.From}</Text> */}
                                        <Text>
                                            <Text style={[style.font.fontContentNormal, { fontWeight: (data.ReadStatus == 1 ? 'normal' : 'bold') }]}> Reminder : </Text>
                                            <Text style={[style.font.fontContentNormal,{textAlign:'center'}]}> {data.Message}</Text>
                                        </Text>
                                        <Text style={[style.font.fontContentNormal, { textAlign: 'right' }]}><TimeAgo time={dateFormat(data.Date, "dd mmmm yyyy HH:MM")} /> </Text>
                                    </View>
                                </View>

                            </TouchableOpacity>
                        );

                    }
                }
                else if (getDateYesterday == getDateData) {
                    if (tempTimeYester !== timeData) {
                        tempTimeYester = timeData;
                        return (
                            <View>
                                <Text style={[style.font.fontContentLarge, { borderBottomWidth: 2, borderBottomColor: 'black' }]}>Older</Text>

                                <TouchableOpacity style={{ paddingTop: responsiveHeight(1) }} onPress={function () { this._onPressButton(data) }.bind(this)}>
                                    <View style={{ flexDirection: 'row' }}>
                                        {tempImage}
                                        <View style={{ marginLeft: responsiveWidth(2), width: responsiveWidth(65) }}>
                                            {/* <Text style={{ fontSize: responsiveFontSize(1.5), fontFamily: 'MyriadPro-Regular' }}>From : {data.From}</Text> */}
                                            <Text>
                                                <Text style={[style.font.fontContentNormal, { fontWeight: (data.ReadStatus == 1 ? 'normal' : 'bold') }]}>Reminder : </Text>
                                                <Text style={[style.font.fontContentNormal,{textAlign:'center'}]}> {data.Message}</Text>
                                            </Text>
                                            <Text style={[style.font.fontContentNormal, { textAlign: 'right' }]}><TimeAgo time={dateFormat(data.Date, "dd mmmm yyyy HH:MM")} /> </Text>
                                        </View>
                                    </View>

                                </TouchableOpacity>
                            </View>
                        );
                    } else {
                        return (
                            <TouchableOpacity style={{ paddingTop: responsiveHeight(1)}} onPress={function () { this._onPressButton(data) }.bind(this)}>
                                <View style={{ flexDirection: 'row' }}>
                                    {tempImage}
                                    <View style={{ marginLeft: responsiveWidth(2), width: responsiveWidth(65) }}>
                                        {/* <Text style={{ fontSize: responsiveFontSize(1.5), fontFamily: 'MyriadPro-Regular' }}>From : {data.From}</Text> */}
                                        <Text>
                                            <Text style={[style.font.fontContentNormal, { fontWeight: (data.ReadStatus == 1 ? 'normal' : 'bold') }]}>Reminder : </Text>
                                            <Text style={[style.font.fontContentNormal,{textAlign:'center'}]}> {data.Message}</Text>
                                        </Text>
                                        <Text style={[style.font.fontContentNormal, { textAlign: 'right' }]}><TimeAgo time={dateFormat(data.Date, "dd mmmm yyyy HH:MM")} /> </Text>
                                    </View>
                                </View>

                            </TouchableOpacity>
                        );

                    }
                }
            }.bind(this));
            resolve(rowNotif);
        });
    }

    componentDidMount() {
        console.log('notif in didmount ')
        this.loadNotificationData().then(function (result) {
            console.log('notif res didmount')
            console.log(result)
            this.setState({
                listNotif: result
            });
        }.bind(this)).catch((err) => {
            console.log('notif res didmount');
            console.log(err);
        });

    }

    render() {
        return (
            <View style={{ padding: responsiveWidth(5), backgroundColor: '#F4F1E8', height: responsiveHeight(100) - Metrics.marginHeader - responsiveHeight(10) }}>

                <View style={{ flexDirection: 'row', marginTop: responsiveHeight(1) }}>
                    <View>
                        <Image source={Images.userprofile} style={{ height: responsiveHeight(12), width: responsiveHeight(12), marginLeft: responsiveWidth(5), marginTop: responsiveHeight(-1) }} />
                    </View>
                    <View style={{ marginLeft: responsiveWidth(5), marginTop: responsiveHeight(0) }}>
                        <Text style={[style.font.fontContentNormal, { fontWeight: 'bold' }]}> {this.state.fullName}</Text>
                        <Text style={style.font.fontContentNormal} > <Icon name="suitcase" size={20} color="grey" /> {this.state.userPosition}</Text>
                        <Text style={style.font.fontContentNormal} > <Icon name="home" size={20} color="grey" /> {this.state.userPlant}</Text>
                    </View>
                </View>
                <ScrollView style={{ marginBottom: responsiveHeight(0), marginTop: responsiveHeight(2) }}>
                    {this.state.listNotif}
                </ScrollView>
            </View>
        );
    }
}