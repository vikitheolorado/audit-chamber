// @flow

import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, Modal } from 'react-native';
import style from '../Themes/Fonts';

export default class ModalProgress extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            modal:true
        }
    }


    componentWillMount() {
    }

    static propTypes = {
        currentValue: PropTypes.string,
        show:PropTypes.bool,
    }

    render() {
        return (
            <View style={{ backgroundColor: 'transparent' }}>
                <Modal
                    transparent={true}
                    animationType="fade"
                    visible={this.props.show}
                    onRequestClose={() => this.setState({ modal: this.props.show })}>
                    <View
                        behavior='padding'
                        style={{
                            flex: 1,
                            backgroundColor: 'rgba(0, 0, 0, 0.5)',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                        <Text style={[style.font.fontContentLarge, { color: 'white' }]}>{this.props.currentValue}</Text>
                    </View>
                </Modal>
            </View>
        )
    }
}
