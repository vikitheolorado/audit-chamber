//import RNHTMLtoPDF from 'react-native-html-to-pdf';

// var data_summary = []

// var summary = '';

// summary = ({
//     "name" : 'RINGKAS',
//     "id" : '1',
//     "summary_score" : '4/5 Score: 80%',
//     "result" : [{
//         "question" : 'Tidak ada peralatan/perlengkapan yang rusak/yang tidak digunakan di area kerja',
//         "response" : 'Yes',
//         "detail" : [
//             {
//                 comments :'ayam goreng',
//                 area : 1
//             },
//             {
//                 comments :'ayam goreng',
//                 area : 1
//             },
//             {
//                 comments :'ayam goreng',
//                 area : 1
//             }
//         ]
//     },
//     {
//         "question" : 'Tidak ada peralatan/material yang tidak terpakai yang disimpan di laci, area kerja, atau tempat penyimpanan lainnya',
//         "response" : 'No',
//         "detail" : [
//             {
//                 comments :'ayam goreng',
//                 area : 1
//             },
//             {
//                 comments :'ayam goreng',
//                 area : 1
//             }
//         ]
//     }]
// })

// data_summary.push(summary)

// let value =(
//       {
//           "file_name" : 'Quinsys Audit Chamber',
//           "path_image_logo" : "http://memory.foundation/wp-content/uploads/2013/08/youtube-logo-200px.png",
//           "audit_type" : 'Weekly',
//           "periode_type" : 'Week',
//           "conducted_date": '17-08-1945',
//           "prepared_by": 'User',
//           "plant_data": 'Plant 1',
//           "line_data": 'line 1',
//           "current_score:" : '78',
//           "last_score" : '72',
//           "current_count" : '30'+'/'+'35',
//           "last_count" : '28'+'/'+'35',
//           "auditor_name" : 'Bimo Haryowanto',
//           "auditor_signoff_date" : '22-01-1990',
//           "auditee_name" : 'Afrizal Munawas',
//           "auditee_signoff_date" : '24-01-1990',
//           "arr_summary" : JSON.stringify(data_summary)
//       })

export async function PDFAuditReport(value){
    console.log('masuk generate pdf')
    console.log(value)
    var string_result = "";

    //opening tag html & body, dan head
    var stringHead = '<!DOCTYPE html><html><head><style>body {margin: 15px;font-family: FrutigerLTStd-Roman;} .header {align-content: stretch;font-size: 16px;font-family: "Myriad Pro";font-weight: bold;} .content {font-size: 12px;font-family: "Myriad Pro";} .content-total {font-size: 32px;font-family: "Myriad Pro";font-weight:bold;} .content-total-over {font-size: 32px;font-family: "Myriad Pro";color:forestgreen;font-weight:bold;} .content-total-less {font-size: 32px;font-family: "Myriad Pro";color:red;font-weight:bold;} .content-response-over {font-size: 12px;font-family: "Myriad Pro";background-color:forestgreen;font-weight:bold;} .content-response-less {font-size: 12px;font-family: "Myriad Pro";background-color:red;font-weight:bold;} .content-border {font-size: 12px;font-family: "Myriad Pro";text-align:right;} .logo {text-align: left;margin-left: 10px;} .table-border-empty {border: none;border-width:1px;}</style><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/></head><body>';
    //closing tag
    var stringClosing = '</table></body></html>';

    //opening tag table, logo, dan judul report
    var stringTitle = '<table class="table-border-empty" style="width:100%; table-layout:fixed" cellspacing="0" cellpadding="2"><tr><td colspan="1" rowspan="2" style="border:none;border-width:1px;"><img class="logo" style="display: table-cell; width: 60%" src="'+value.path_image_logo+'" /></td><td colspan="5" rowspan="1" style="border:none;border-width:1px;" align="center"><div class="header">QUINSYS - AUDIT CHAMBER</div></td></tr><tr><td colspan="5" rowspan="1" style="border:none;border-width:1px;" align="center"><div class="header">GMP/6S "'+value.audit_type+'" Audit</div></td></tr><tr><td colspan="6" rowspan="1" style="border:none;border-width:1px;"></td></tr><tr><td width="15%" style="border:none;border-width:1px;"></td><td width="25%" style="border:none;border-width:1px;"></td><td width="10%" style="border:none;border-width:1px;"></td><td width="20%" style="border:none;border-width:1px;"></td><td width="15%" style="border:none;border-width:1px;" align="center"><div class="content">Score</div></td><td width="15%" style="border:none;border-width:1px;" align="center"><div class="content">Last '+value.periode_type+'</div></td></tr>'
    //define 'Conducted On', score saat ini, score audit sebelumnya
    var stringHeader = '<tr><td colspan="1" style="border:none;border-width:1px;"><div class="content">Conducted On</div></td><td colspan="2" style="border:solid;border-width:1px;"><div class="content-border">'+value.conducted_date+'</div></td><td colspan="1" style="border:none;border-width:1px;"><div class="content"></div></td><td colspan="1" rowspan="4" style="border:solid;border-right:none;border-width:1px;word-wrap:break-word" align="center" valign="middle"><div class="content-total-over">'+value.current_score+'</div></td><td colspan="1" rowspan="4" style="border:solid;border-width:1px;word-wrap:break-word" align="center" valign="middle"><div class="content-total">'+value.last_score+'</div></td></tr>'
    //define informasi header yang lain (Prepared By, Plant, Line)
    var stringHeader2= '<tr><td colspan="1" style="border:none;border-width:1px;"><div class="content">Prepared By</div></td><td colspan="2" style="border:solid;border-top:none;border-width:1px;"><div class="content-border">'+value.prepared_by+'</div></td></tr><tr><td colspan="1" style="border:none;border-width:1px;"><div class="content">Plant</div></td><td colspan="2" style="border:solid;border-top:none;border-width:1px;"><div class="content-border">'+value.plant_data+'</div></td></tr><tr><td colspan="1" style="border:none;border-width:1px;"><div class="content">Line</div></td><td colspan="2" style="border:solid;border-top:none;border-width:1px;"><div class="content-border">'+value.line_data+'</div></td></tr>'
    //informasi score dan judul table detail
    var stringHeader3= '<tr><td style="border:none;border-width:1px;"></td><td style="border:none;border-width:1px;"></td><td style="border:none;border-width:1px;"></td><td style="border:none;border-width:1px;"></td><td style="border:none;border-width:1px;" align="center"><div class="content">'+value.current_count+'</div></td><td style="border:none;border-width:1px;" align="center"><div class="content">'+value.last_count+'</div></td></tr><tr><td colspan="6" rowspan="1" style="border:none;border-width:1px;" align="center"><div class="content">Audit Details - '+value.current_count+'</div></td></tr><tr><td colspan="3" rowspan="1" style="border:solid;border-right:none;border-width:1px;background:#B0B0B0" align="center"><div class="content">Question</div></td><td colspan="1" rowspan="1" style="border:solid;border-right:none;border-width:1px;background:#B0B0B0" align="center"><div class="content">Response</div></td><td colspan="2" rowspan="1" style="border:solid;border-width:1px;background:#B0B0B0" align="center"><div class="content">Details</div></td></tr>'
    
    //table sign off
    var stringFooter='<tr><td colspan="6" rowspan="1" style="border:none;border-width:1px;"></td></tr><tr><td colspan="6" rowspan="1" style="border:none;border-width:1px;"></td></tr><tr><td colspan="3" rowspan="1" style="border:solid;border-width:1px;background:#B0B0B0" align="center"><div class="content">Sign Off</div></td></tr><tr><td colspan="1" rowspan="2" style="border:solid;border-top:none;border-right:none;border-width:1px;word-wrap:break-word" align="center"><div class="content">Auditor</div></td><td colspan="2" rowspan="1" style="border:solid;border-top:none;border-width:1px;word-wrap:break-word" align="center"><div class="content" align="left">'+value.auditor_name+'</div></td></tr><tr><td colspan="2" rowspan="1" style="border:solid;border-top:none;border-width:1px;word-wrap:break-word" align="center"><div class="content" align="left">'+value.auditor_signoff_date+'</div></td></tr><tr><td colspan="1" rowspan="2" style="border:solid;border-top:none;border-right:none;border-width:1px;word-wrap:break-word" align="center"><div class="content">Auditee</div></td><td colspan="2" rowspan="1" style="border:solid;border-top:none;border-width:1px;word-wrap:break-word" align="center"><div class="content" align="left">'+value.auditee_name+'</div></td></tr><tr><td colspan="2" rowspan="1" style="border:solid;border-top:none;border-width:1px;word-wrap:break-word" align="center"><div class="content" align="left">'+value.auditee_signoff_date+'</div></td></tr>'

    var stringDetails=''

    JSON.parse(value.arr_summary).map(function (summary, index) {
        ////insert row informasi kategori ('RINGKAS', 'RAPI', dll) dan total score
        stringDetails += '<tr><td colspan="2" rowspan="1" style="border-left:solid;border-top:none;border-right:none;border-bottom:none;border-width:1px;word-wrap:break-word" align="center"><div class="content">'+summary.name+'</div></td><td colspan="1" rowspan="1" style="border:none;border-width:1px;"></td><td colspan="3" rowspan="1" style="border-right:solid;border-top:none;border-left:none;border-bottom:none;;border-width:1px;word-wrap:break-word" align="center"><div class="content-border">'+summary.summary_score+'</div></td></tr>';
                
        var arr_length = summary.result.length
        if (arr_length > 0)
        {
            for (i=0; i < arr_length; i++)
            {
                ////untuk warna background
                var style = ''
                if (summary.result[i].response === 'Yes')
                {
                    style = 'content-response-over'
                }
                else
                {
                    style = 'content-response-less'
                }
                ////pertanyaan
                stringDetails+='<tr><td colspan="3" rowspan="1" style="border:solid;border-right:none;border-width:1px;word-wrap:break-word" align="center"><div align="left">'+summary.result[i].question+'</div></td>'
                ////response jawaban (yes/no)
                stringDetails+='<td colspan="1" rowspan="1" style="border:solid;border-right:none;border-width:1px;word-wrap:break-word" align="center"><div class="'+style+'">'+summary.result[i].response+'</div></td>'
                ////keterangan

                stringDetails += '<td colspan="2" rowspan="1" style="border:solid;border-width:1px;word-wrap:break-word" align="center"><div align="left">'

                summary.result[i].detail.map(function (result, key) {
                    console.log('apuy key ')
                     console.log(result)
                    stringDetails+= '<div>'+ result.comments + '(AREA : '+ result.area +')'+ '</div>'
                })

                stringDetails+='</div></td></tr>'
            }
        }
    })


    string_result = stringHead + stringTitle + stringHeader + stringHeader2 + stringHeader3 + stringDetails + stringFooter + stringClosing;
    console.log(stringHead + stringTitle + stringHeader + stringHeader2)
    console.log(stringHeader3)
    console.log(stringDetails)
    console.log(stringFooter + stringClosing)
    let options = {
        html: string_result,
        directory: 'docs',
        fileName: value.file_name,
        base64: true,
        padding: 24
    };

    try {
        const results = await RNHTMLtoPDF.convert(options).then((data) =>{
        console.log(data.filePath);
        console.log("PDF Created")
        // console.log(data)
        return data.filePath
        }); 
    } catch (err) {
        console.log("PDF Error")
        console.error(err)
        return null;
    }
}