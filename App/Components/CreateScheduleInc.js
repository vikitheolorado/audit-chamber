import React, { Component } from 'react';
import { Alert, Platform, View, Text, TextInput, ScrollView, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { Picker, Item, Button } from 'native-base';
import ModalPicker from 'react-native-modal-picker';
import { responsiveWidth, responsiveHeight, responsiveFontSize, newResponsiveFontSize } from '../Themes/Responsive'
import Icon from 'react-native-vector-icons/FontAwesome';
import update from 'react-addons-update';
import axios from 'axios';
import { getInstance } from '../Data/DB/DBHelper'
import Queries from '../Data/Queries'
import Constants from '../Data/Constants'
var dateFormat = require('dateformat');
import Toast from 'react-native-smart-toast'
import Spinner from 'react-native-loading-spinner-overlay';
import { Metrics, Images } from '../Themes';
import style from '../Themes/Fonts';
import { Actions } from 'react-native-router-flux';

export default class CreateScheduleInc extends Component {

    constructor(props) {
      super(props);

      this.MsUserLogin = Queries.getCurrentUser();
      // this.MsPlant = getInstance().objects('MsPlant').filtered("PlantID = " + this.MsUserLogin[0].PlantID)
      // this.MsGroupLine = getInstance().objects('MsGroupLine').filtered("PlantID = " + this.MsUserLogin[0].PlantID).sorted('GroupLineIDAbb');
      this.MsPlant = getInstance()
        .objects("MsPlant")
        .filtered("PlantID = " + this.MsUserLogin[0].PlantID + " AND active = true");
      this.MsGroupLine = getInstance()
        .objects("MsGroupLine")
        .filtered("PlantID = " + this.MsUserLogin[0].PlantID + " AND active = true")
        .sorted("GroupLineIDAbb");
      this.MsUser = getInstance()
        .objects("MsUser")
        .filtered("PlantID = " + this.MsUserLogin[0].PlantID + " AND active = true");
      // this.MsTemplate = getInstance().objects('MsTemplateHeader').filtered("TemplateHeaderId = " + this.MsUserLogin[0].TemplateHeaderId);
      // 2.0.6 additional filter template status 1
      this.MsTemplate = getInstance()
        .objects("MsTemplateHeader")
        .filtered("TemplateStatus = 1");

      //console.log("data ces")
      // 2.0.6, Jika tidak ada plant maka default value select
      this.selectedFromWeekLabel = "Select Week";
      this.selectedToWeekLabel = "Select Week";
      this.selectedPlantLabel = "Select the Plant";

      this.selectedTypeLabel = "GMP2";
      this.selectedTemplateLabel = "";
      (this.currentWeek = 0), 
      (this.state = { 
        selectedType: "3", 
        selectedPlant: "9", 
        selectedFromWeek: "", 
        selectedFromYear: "", 
        selectedToWeek: "", 
        selectedToYear: "", 
        selectedTemplate: "", 
        arrType: [{ label: "GMP2", key: "3" }], 
        arrPlant: [], 
        arrFromWeek: [], 
        arrToWeek: [], 
        arrFromYear: [], 
        arrToYear: [], 
        arrAuditor: [], 
        arrSelectedTemplate: [], 
        arrSelectedAuditor: [], 
        arrLine: [], 
        dataLine: "", isScheduleHasBeenCreated: false, 
        submitLabel: "Create", 
        loaderStatus: null, 
        _changeParam: this.props._changeParam, 
        currentWeek: 0, 
        currentYear: 0, 
        currentTextHeader: "start", 
        maxWeek: 0,
         maxYear: 0 });
    }

    componentWillUnmount() {
      console.log("changetimeout 0");
      this.setState({
        loaderStatus:false
      })
      //this.state._changeParam(0);
    }

    _updateListTemplate(template) {
      var arrTemplate = [];
      let tempFlag = 0;
      console.log("data Ms template ");
      console.log(this.MsTemplate);
      this.MsTemplate.map(function(data, index) {
        console.log("Template = " + template);
        console.log("Data Audit Type = " + data.AuditTypeId);
        if (template == data.AuditTypeId) {
          console.log("tempflag " + tempFlag);
          console.log(data);
          if (tempFlag == 0) {
            tempFlag = data.TemplateCode;
            //tempFlag++;
            console.log("masuk flag " + data.TemplateCode);
          }
          var tempTemplate = { label: data.TemplateCode, key: data.TemplateCode };
          arrTemplate.push(tempTemplate);
        }
      });

      console.log("template code " + tempFlag);

      this.setState({
        arrTemplate: arrTemplate,
        selectedTemplate: tempFlag
      });
    }

    fetchDataWeekAndYear(currWeek, currYear, maxWeek, maxYear) {
      console.log("fetchDataWeekAndYear curr week " + currWeek + " curr year " + currYear + " max week " + maxWeek + " max year " + maxYear);
      var arrFromWeek = this.state.arrFromWeek;
      var arrToWeek = this.state.arrToWeek;

      if (maxYear > currYear) {
        for (var i = currWeek; i <= 52; i++) {
          if (i > 9) {
            var tempWeek = { label: "W " + i, key: i };
            arrFromWeek.push(tempWeek);
            arrToWeek.push(tempWeek);
          } else {
            var tempWeek = { label: "W 0" + i, key: i };
            arrFromWeek.push(tempWeek);
            arrToWeek.push(tempWeek);
          }
        }
      } else if (maxYear == currYear) {
        for (var i = currWeek; i <= maxWeek; i++) {
          if (i > 9) {
            var tempWeek = { label: "W " + i, key: i };
            arrFromWeek.push(tempWeek);
            arrToWeek.push(tempWeek);
          } else {
            var tempWeek = { label: "W 0" + i, key: i };
            arrFromWeek.push(tempWeek);
            arrToWeek.push(tempWeek);
          }
        }
      }

      var arrFromYear = this.state.arrFromYear;
      var arrToYear = this.state.arrToYear;

      if (currYear == maxYear) {
        var tempYear = { label: maxYear, key: maxYear };
        arrFromYear.push(tempYear);
        arrToYear.push(tempYear);
      } else if (currYear < maxYear) {
        for (var i = currYear; i <= maxYear; i++) {
          var tempYear = { label: i, key: i };
          arrFromYear.push(tempYear);
          arrToYear.push(tempYear);
        }
      }

      this.setState({
        arrFromYear: arrFromYear,
        arrFromWeek: arrFromWeek
      });
    }

    filterFromWeekByYear(selectedYear) {
      this.setState(
        {
          arrFromWeek: [],
          selectedFromWeek: ""
        },
        () => {
          var arrFromWeek = [];

          if (this.state.maxYear > selectedYear) {
            console.log(
              "max year lbh besar dari selectedyear"
            );
            for (
              var i = this.state.currentWeek;
              i <= 52;
              i++
            ) {
              if (i > 9) {
                var tempWeek = {
                  label: "W " + i,
                  key: i
                };
                arrFromWeek.push(tempWeek);
              } else {
                var tempWeek = {
                  label: "W 0" + i,
                  key: i
                };
                arrFromWeek.push(tempWeek);
              }
            }
          } else if (
            this.state.maxYear == selectedYear
          ) {
            for (
              var i = 1;
              i <= this.state.maxWeek;
              i++
            ) {
              if (i > 9) {
                var tempWeek = {
                  label: "W " + i,
                  key: i
                };
                arrFromWeek.push(tempWeek);
              } else {
                var tempWeek = {
                  label: "W 0" + i,
                  key: i
                };
                arrFromWeek.push(tempWeek);
              }
            }
          }

          this.setState({
            arrFromWeek: arrFromWeek
          });
        }
      );
    }

    filterToWeekByYear(selectedYear) {
      this.setState(
        {
          arrToWeek: [],
          selectedToWeek: ""
        },
        () => {
          var arrToWeek = [];

          if (this.state.maxYear > selectedYear) {
            console.log(
              "max year lbh besar dari selectedyear"
            );
            for (
              var i = this.state.currentWeek;
              i <= 52;
              i++
            ) {
              if (i > 9) {
                var tempWeek = {
                  label: "W " + i,
                  key: i
                };
                arrToWeek.push(tempWeek);
              } else {
                var tempWeek = {
                  label: "W 0" + i,
                  key: i
                };
                arrToWeek.push(tempWeek);
              }
            }
          } else if (
            this.state.maxYear == selectedYear
          ) {
            for (
              var i = 1;
              i <= this.state.maxWeek;
              i++
            ) {
              if (i > 9) {
                var tempWeek = {
                  label: "W " + i,
                  key: i
                };
                arrToWeek.push(tempWeek);
              } else {
                var tempWeek = {
                  label: "W 0" + i,
                  key: i
                };
                arrToWeek.push(tempWeek);
              }
            }
          }

          this.setState({
                          arrToWeek: arrToWeek
                        });
        }
      );
    }

    componentWillMount() {
       this._updateListTemplate(this.state.selectedType);

       var arrAuditor = [];

       var tempAuditor = { label: "Select Auditor", key: "Select Auditor" };
       arrAuditor.push(tempAuditor);

       this.MsUser.map(function(data, index) {
         var tempAuditor = { label: data.Username, key: data.Username };
         arrAuditor.push(tempAuditor);
       });

       this.setState({ arrAuditor: arrAuditor });

       var arrPlant = [];
       this.MsPlant.map(function(data, index) {
           if (index == 0) {
             this.setState({ selectedPlant: data.PlantID });
             this.selectedPlant = data.PlantDesc;
             this.selectedPlantLabel = data.PlantDesc;
           }
           var tempPlant = { label: data.PlantDesc, key: data.PlantID };
           arrPlant.push(tempPlant);
         }.bind(this));

       this.setState({ arrPlant: arrPlant });

       var arrGroupLine = [];
       this.MsGroupLine.map(function(data, index) {
         var tempGroupLine = { id: data.GroupLineID, lineName: data.GroupLineIDAbb };
         arrGroupLine.push(tempGroupLine);
       });

       this.setState({ arrLine: arrGroupLine });

       var dataLine = arrGroupLine.map(function(data, i) {
           var arrSelectedAuditor = this.state.arrSelectedAuditor;
           var tempSelectedAuditor = {};
           tempSelectedAuditor = { label: "Select Auditor", key: "Select Auditor", line: data.id };
           arrSelectedAuditor.push(tempSelectedAuditor);

           this.setState({ arrSelectedAuditor: arrSelectedAuditor });
         }.bind(this)); 
      
    }

    _onChangePlant(param) {
      var arrGroupLine = [];
      Queries.getGroupLineByPlantId(param).map(
        function(data, index) {
          var tempGroupLine = {
            id: data.GroupLineID,
            lineName: data.GroupLineIDAbb
          };
          arrGroupLine.push(tempGroupLine);
        }
      );

      this.setState({ arrLine: arrGroupLine });

      ////console.log(data)
      var arrSelectedAuditor = [];
      var dataLine = arrGroupLine.map(function(data, i) {
          var tempSelectedAuditor = {};
          tempSelectedAuditor = { label: "Select Auditor", key: "Select Auditor", line: data.id };
          arrSelectedAuditor.push(tempSelectedAuditor);
        }.bind(this));

      this.setState({
        arrSelectedAuditor: arrSelectedAuditor
      });
      // //console.log("data selector")
      // //console.log(arrSelectedAuditor)
    }

    _onPreviewSchedule() {
      if (this.state.selectedFromWeek == "" || this.state.selectedFromYear == "" || this.state.selectedToWeek == "" || this.state.selectedToYear == "") {
        Alert.alert("Information", "Date schedule audit invalid, cannot create audit.",
                    [{text: "Ok"}], 
                    {cancelable:false} );
        return;
      } 

      if ((this.state.selectedToWeek + this.state.selectedToYear * 52) - (this.state.selectedFromWeek + this.state.selectedFromYear * 52) > 4) {
        Alert.alert(
          "Information",
          "Cannot create audit more than 5 periods.",
          [{ text: "Ok" }],
          { cancelable: false }
        );
        return;
      }

      console.log("Current Week " + this.state.currentWeek + " " + this.state.currentYear);
      console.log(this.state.selectedFromWeek + " " + this.state.selectedFromYear);
      console.log(this.state.selectedFromWeek + this.state.selectedFromYear * 52);
      console.log(this.state.currentWeek + this.state.currentYear * 52);
      console.log(this.currentWeek);

      var currentYear = dateFormat(new Date(), "yyyy");
      if (parseInt(this.state.selectedFromWeek) + this.state.selectedFromYear * 52 < this.currentWeek) {
        console.log("minggunya udah lewat bro");
        this._toast.show({
          position: Toast.constants.gravity.top,
          children:
            this.selectedFromWeekLabel +
            " was past."
        });

        this.setState({ // submitLabel: "Preview",
          loaderStatus: false 
        });
        return;
      } else if (parseInt(this.state.selectedToWeek) + this.state.selectedToYear * 52 < parseInt(this.state.selectedFromWeek) + this.state.selectedFromYear * 52) {
        console.log("minggunya akhirnya lebih kecil");
        this._toast.show({
          position: Toast.constants.gravity.top,
          children:
            "Start week should be more than end week."
        });

        this.setState({ // submitLabel: "Preview",
          loaderStatus: false });
        return;
      } else if (this.state.selectedFromYear < currentYear) {
        console.log("tahun sudah lewat");
        this._toast.show({
          position: Toast.constants.gravity.top,
          children:
            this.selectedFromYear + " was past."
        });

        this.setState({ // submitLabel: "Preview",
          loaderStatus: false });
        return;
      } else if (this.state.selectedToYear < this.state.selectedFromYear) {
        console.log("tahunnya akhirnya lebih kecil");
        this._toast.show({
          position: Toast.constants.gravity.top,
          children:
            "Start year should be more than end year."
        });

        this.setState({ // submitLabel: "Preview",
          loaderStatus: false });
        return;
      }
      var arrAuditDetail = [];
      this.state.arrSelectedAuditor.map(function(
        data,
        index
      ) {
        if (data.key !== "Select Auditor") {
          var tempGroupLine = {
            AuditorID: data.key,
            GroupLineId: data.line
          };
          arrAuditDetail.push(tempGroupLine);
        }
      });

      if (!arrAuditDetail.length) {
        console.log("belum pilih auditor");
        this._toast.show({
          position: Toast.constants.gravity.top,
          children:
            "You have not scheduled any auditor"
        });
        return;
      }

      //this.state._changeParam("preview");
      this.setState({
        submitLabel: "Preview",
        currentTextHeader: "preview"
      });
    }

    _backToCreate() {
      //this.state._changeParam(0);
      this.setState({
        submitLabel: "Create",
        currentTextHeader: "start"
      });
    }

    componentDidMount() {
      //this.props.onRef(this);

      this.setState({
        loaderStatus : true
      })
      
      var param = dateFormat(new Date(), "yyyy/mm/dd");

      axios
        .get(
          Constants.APILink + `Master/GetWeekPeriod?date=` + param,
          {
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",

              Authorization:
                "Bearer " + Queries.getPersistentData("token")
            },
            timeout: Constants.GetTimeout
          }
        )
        .then(res => {
          if (res.data.length == 0) {
            Alert.alert(
              "Information",
              "Current week hasn't been registered in database",
              [
                {
                  text: "Ok",
                  onPress: () => {
                    this.setState({
                      loaderStatus: false
                    });
                  }
                }
              ],
              { cancelable: false }
            );
            return;
          }
          this.fetchDataWeekAndYear(res.data[0].WeekToDate, res.data[0].YEAR, res.data[0].MaxWeek, res.data[0].MaxYear);

          this.currentWeek = res.data[0].WeekToDate + res.data[0].YEAR * 52;
          this.setState({
            currentWeek: res.data[0].WeekToDate,
            currentYear: res.data[0].YEAR,
            selectedFromWeek: res.data[0].WeekToDate,
            selectedToWeek: res.data[0].WeekToDate,
            selectedFromYear:
              res.data[0].YEAR > res.data[0].MaxYear
                ? ""
                : res.data[0].YEAR,
            selectedToYear:
              res.data[0].YEAR > res.data[0].MaxYear
                ? ""
                : res.data[0].YEAR,
            maxWeek: res.data[0].MaxWeek,
            maxYear: res.data[0].MaxYear,
            loaderStatus: false
          });

          (this.selectedFromWeekLabel = res.data[0].WeekToDate), (this.selectedToWeekLabel = res.data[0].WeekToDate);
        })
        .catch(function(error) {
            console.log("Create Schedule - GetWeekPeriod");
            console.log(error);
            if (error && error.response && error.response.status == "401") {
              console.log("Information - Result - GetResultPage - 401");
              Queries.sessionHabis(error.response.status);
            } else {
              Alert.alert(
                "Connection Failed!",
                "Unable to retrieve data. Please check your connection.",
                [
                  {
                    text: "Ok",
                    onPress: () => {
                      this.setState({
                        loaderStatus: false
                      });
                    }
                  }
                ],
                { cancelable: false }
              );
            }
          }.bind(this)); 
    }

    _onCreateSchedule() {
      // this.setState({
      //    submitLabel : "Create"
      // })
      this.setState({ //submitLabel: "On Process",
        loaderStatus: true });
      requestAnimationFrame(() => {
        try {
          var arrAuditDetail = [];
          this.state.arrSelectedAuditor.map(function(data, index) {
            if (data.key !== "Select Auditor") {
              var tempGroupLine = { AuditorID: data.key, GroupLineId: data.line };
              arrAuditDetail.push(tempGroupLine);
            }
          });
          var users = Queries.getCurrentUser();

          var dataPost = { UserName: users[0].Username, AuditTypeId: 3, TemplateCode: this.state.selectedTemplate, PlantId: this.state.selectedPlant, WeekToDateStart: this.state.selectedFromWeek, WeekToDateEnd: this.state.selectedToWeek, YearStart: this.state.selectedFromYear, YearEnd: this.state.selectedToYear, AuditDetail: arrAuditDetail }; //users[0].Username,

          var data = [];
          data = JSON.stringify(dataPost);

          //if (this.state.submitLabel == "Create") {
          axios
            .post(
              Constants.APILink +
                `Schedule/PostCreateAuditSchedule`,
              data,
              {
                headers: {
                  Accept: "application/json",
                  "Content-Type": "application/json",
                  Authorization:
                    "Bearer " +
                    Queries.getPersistentData("token")
                }
                
                //,timeout: Constants.GetTimeout * Math.ceil(this.state.arrSelectedAuditor.length * ((this.state.selectedToWeek + this.state.selectedToYear * 52) - (this.state.selectedFromWeek + this.state.selectedFromYear * 52)) / 45)
              }
            )
            .then(res => {
                
              console.log("berhasil kirim");
              console.log(res);
              if(res.data == '2'){
              Alert.alert(
                "Duplicate Schedule",
                "Your site already have audit schedules created in that period.",
                [{ text: "Ok", onPress:()=>{
                  this.setState({
                    loaderStatus: false
                  });
                } }],
                { cancelable: false }
              );
              return;
              }
              this.setState({
                isScheduleHasBeenCreated: true,
                currentTextHeader: "finish",
                loaderStatus: false 
              });
              //this.state._changeParam("finish"); 
            })
            .catch(function(error) { 
                console.log("error");
                console.log(error);
                if (error && error.response && error.response.status == "401") { 
                  console.log("Information - Result - GetResultPage - 401");
                  Queries.sessionHabis(error.response.status);
                } else {
                    // bedain result null dan timeout
                  Alert.alert("Connection Failed!", "Unable to retrieve data. Please check your connection.",
                      [{text: "Ok", onPress:()=>{
                        this.setState(
                          {
                            loaderStatus: false
                          }
                        );
                      }}], 
                      {cancelable:false} );
                }
                // console.log(error);
                //Alert.alert(JSON.stringify(error))
              }.bind(this));
          //} 
        } catch (error) {
          console.log('Try Catch error - post create schedule, then set loader false ')
          this.setState({
            loaderStatus:false
          })
        }
        
      });
    }

    mainContainerCreateSchedule() {
      if (!this.state.isScheduleHasBeenCreated) {
        console.log("ArrAuditor = " + this.state.arrAuditor.length);
        if (this.state.submitLabel == "Preview" || this.state.currentTextHeader == "preview") {
          if (this.state.arrAuditor.length > 0) {
            var dataLine = this.state.arrLine.map(function(data, i) {
                if (this.state.arrSelectedAuditor[i].label != "Select Auditor") {
                  return <View style={{ flexDirection: "row", marginTop: responsiveHeight(2) }}>
                      <Text
                        style={[
                          style.font
                            .fontContentLarge,
                          { flex: 1, paddingTop: 5 }
                        ]}
                      >
                        {data.lineName}
                      </Text>
                      <Text
                        style={[
                          style.font
                            .fontContentLarge,
                          { flex: 2, paddingTop: 5 }
                        ]}
                      >
                        {
                          this.state
                            .arrSelectedAuditor[i]
                            .label
                        }
                      </Text>
                    </View>;
                }
              }.bind(this));
          }
          return <View>
              <View style={{ marginBottom: responsiveHeight(17) }}>
                <ScrollView>
                  <View style={{ marginLeft: responsiveWidth(5) }}>
                    <Text style={[style.font.fontContentLarge, { marginTop: responsiveHeight(4) }]}>
                      Audit Detail :
                    </Text>
                    <View style={{ flexDirection: "row", marginTop: responsiveHeight(2) }}>
                      <Text style={[style.font.fontContentLarge, { flex: 1, paddingTop: 5 }]}>
                        Type
                      </Text>
                      <Text style={[style.font.fontContentLarge, { flex: 2, paddingTop: 5 }]}>
                        {this.selectedTypeLabel}
                      </Text>
                    </View>
                    <View style={{ flexDirection: "row", marginTop: responsiveHeight(2) }}>
                      <Text style={[style.font.fontContentLarge, { flex: 1, paddingTop: 5 }]}>
                        Template
                      </Text>
                      <Text style={[style.font.fontContentLarge, { flex: 2, paddingTop: 5 }]}>
                        {this.state.selectedTemplate}
                      </Text>
                    </View>
                    <View style={{ flexDirection: "row", marginTop: responsiveHeight(2) }}>
                      <Text style={[style.font.fontContentLarge, { flex: 1, paddingTop: 5 }]}>
                        Plant
                      </Text>
                      <Text style={[style.font.fontContentLarge, { flex: 2, paddingTop: 5 }]}>
                        {this.selectedPlantLabel}
                      </Text>
                    </View>
                    <View style={{ flexDirection: "row", marginTop: responsiveHeight(2) }}>
                      <Text style={[style.font.fontContentLarge, { flex: 1, paddingTop: 5 }]}>
                        From
                      </Text>
                      <Text style={[style.font.fontContentLarge, { flex: 2, paddingTop: 5 }]}>
                        {this.state.selectedFromYear + " W " + this.state.selectedFromWeek}
                      </Text>
                    </View>
                    <View style={{ flexDirection: "row", marginTop: responsiveHeight(2) }}>
                      <Text style={[style.font.fontContentLarge, { flex: 1, paddingTop: 5 }]}>
                        To
                      </Text>
                      <Text style={[style.font.fontContentLarge, { flex: 2, paddingTop: 5 }]}>
                        {this.state.selectedToYear + " W " + this.state.selectedToWeek}
                      </Text>
                    </View>
                    <View style={{ borderBottomWidth: 2, borderBottomColor: "#000", paddingTop: responsiveHeight(2), marginRight: responsiveWidth(-10) }} />
                    <Text style={[style.font.fontContentLarge, { marginTop: responsiveHeight(2) }]}>
                      Audit Team :
                    </Text>
                    {dataLine}
                  </View>
                </ScrollView>
              </View>

              <View style={{ flex: 4, backgroundColor: "white" }} />
            </View>;
        } else {
          if (this.state.arrAuditor.length > 0) {
            var dataLine = this.state.arrLine.map(function(data, i) {
                return <View style={{ flexDirection: "row", marginTop: responsiveHeight(2), marginLeft: responsiveWidth(0), marginRight: responsiveWidth(1) }}>
                    <Text
                      style={[
                        style.font.fontContentLarge,
                        { flex: 1, paddingTop: 5 }
                      ]}
                    >
                      {data.lineName}
                    </Text>

                    <ModalPicker data={this.state.arrAuditor} initValue={this.state.arrSelectedAuditor[i].label} onChange={option => {
                        //this.setState({ selectedPlant: option.label })
                        var arrSelectedAuditor = this.state.arrSelectedAuditor;

                        var tempSelectedAuditor = {};
                        tempSelectedAuditor[i] = { label: option.label, key: option.label, line: data.id };

                        arrSelectedAuditor = update(
                          arrSelectedAuditor,
                          {
                            $merge: tempSelectedAuditor
                          }
                        );

                        ////console.log(arrSelectedAuditor)

                        this.setState({
                          arrSelectedAuditor: arrSelectedAuditor
                        });
                      }} style={{ flex: 2 }}>
                      <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center", backgroundColor: "#F4f4f4" }}>
                        <Text style={[style.font.fontContentLarge, { height: responsiveHeight(4), flex: 9, backgroundColor: "#f4f4f4", paddingLeft: responsiveWidth(2), paddingTop: responsiveWidth(0.5) }]} editable={false}>
                          {this.state.arrSelectedAuditor[i].label}
                        </Text>
                        <Icon size={responsiveWidth(3)} style={{ backgroundColor: "#F4F4F4", alignItems: "center", justifyContent: "center", padding: responsiveWidth(1) } //paddingRight: responsiveWidth(10),
                          } name="caret-down" />
                      </View>
                    </ModalPicker>
                  </View>;
              }.bind(this));
          }
          return <View>
              <View style={{ marginBottom: responsiveHeight(17) }}>
                <ScrollView>
                  <View style={{ marginRight: responsiveWidth(5), marginLeft: responsiveWidth(5) }}>
                    <Text style={[style.font.fontContentLarge, { marginTop: responsiveHeight(4) }]}>
                      Select Audit Detail :
                    </Text>
                    <View style={{ flexDirection: "row", marginTop: responsiveHeight(2) }}>
                      <Text
                        style={[
                          style.font
                            .fontContentLarge,
                          { paddingTop: 5, flex: 1 }
                        ]}
                      >
                        Type
                      </Text>
                      <ModalPicker data={this.state.arrType} initValue={this.state.selectedType} onChange={option => {
                          this.selectedTypeLabel = option.label;
                          this._updateListTemplate(option.key);
                          this.setState({
                            selectedType: option.key
                          });
                        }} style={{ flex: 2 }}>
                        <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center", backgroundColor: "#f4f4f4" }}>
                          <Text style={[style.font.fontContentLarge, { height: responsiveHeight(4), flex: 9, backgroundColor: "#f4f4f4", paddingLeft: responsiveWidth(2) }]} editable={false}>
                            {this.selectedTypeLabel}
                          </Text>
                          <Icon size={responsiveWidth(3)} style={{ backgroundColor: "#F4F4F4", alignItems: "center", justifyContent: "center", padding: responsiveWidth(1) } //paddingRight: responsiveWidth(10),
                            } name="caret-down" />
                        </View>
                      </ModalPicker>
                    </View>
                    <View style={{ flexDirection: "row", marginTop: responsiveHeight(2) }}>
                      <Text
                        style={[
                          style.font
                            .fontContentLarge,
                          { flex: 1, paddingTop: 5 }
                        ]}
                      >
                        Template
                      </Text>
                      <ModalPicker data={this.state.arrTemplate} initValue={this.state.selectedTemplate} onChange={option => {
                          this.selectedTemplateLabel = option.label;
                          this.setState({
                            selectedTemplate:
                              option.key
                          });
                        }} style={{ flex: 2 }}>
                        <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center", backgroundColor: "#f4f4f4" }}>
                          <Text style={[style.font.fontContentLarge, { height: responsiveHeight(4), flex: 9, backgroundColor: "#f4f4f4", paddingLeft: responsiveWidth(2), paddingTop: responsiveWidth(0.5) }]} editable={false}>
                            {this.state.selectedTemplate}
                          </Text>
                          <Icon size={responsiveWidth(3)} style={{ backgroundColor: "#F4F4F4", alignItems: "center", justifyContent: "center", padding: responsiveWidth(1) } //paddingRight: responsiveWidth(10),
                            } name="caret-down" />
                        </View>
                      </ModalPicker>
                    </View>
                    <View style={{ flexDirection: "row", marginTop: responsiveHeight(2) }}>
                      <Text
                        style={[
                          style.font
                            .fontContentLarge,
                          { flex: 1, paddingTop: 5 }
                        ]}
                      >
                        Plant
                      </Text>
                      <ModalPicker data={this.state.arrPlant} initValue={this.state.selectedPlant} onChange={option => {
                          this.selectedPlantLabel = option.label;
                          this.setState({
                            selectedPlant:
                              option.key
                          });
                          this._onChangePlant(option.key);
                        }} style={{ flex: 2 }}>
                        <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center", backgroundColor: "#f4f4f4" }}>
                          <Text style={[style.font.fontContentLarge, { height: responsiveHeight(4), flex: 9, backgroundColor: "#f4f4f4", paddingLeft: responsiveWidth(2), paddingTop: responsiveWidth(0.5) }]} editable={false}>
                            {this.selectedPlantLabel}
                          </Text>
                          <Icon size={responsiveWidth(3)} style={{ backgroundColor: "#F4F4F4", alignItems: "center", justifyContent: "center", padding: responsiveWidth(1) } //paddingRight: responsiveWidth(10),
                            } name="caret-down" />
                        </View>
                      </ModalPicker>
                    </View>
                    <View style={{ flexDirection: "row", marginTop: responsiveHeight(2) }}>
                      <Text
                        style={[
                          style.font
                            .fontContentLarge,
                          { flex: 1, paddingTop: 5 }
                        ]}
                      >
                        From
                      </Text>
                      <View style={{ flex: 2, flexDirection: "row" }}>
                        <ModalPicker data={this.state.arrFromYear} initValue={this.state.selectedFromYear} onChange={option => {
                            this.filterFromWeekByYear(option.label);
                            this.setState({
                              selectedFromYear:
                                option.label
                            });
                          }} style={{ flex: 2 }}>
                          <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center", backgroundColor: "#f4f4f4" }}>
                            <Text style={[style.font.fontContentLarge, { height: responsiveHeight(4), flex: 2.5, backgroundColor: "#f4f4f4", paddingLeft: responsiveWidth(4), paddingTop: responsiveWidth(0.5) }]} editable={false}>
                              {this.state.selectedFromYear}
                            </Text>
                            <Icon size={responsiveWidth(3)} style={{ backgroundColor: "#F4F4F4", alignItems: "center", justifyContent: "center", padding: responsiveWidth(1) } //paddingRight: responsiveWidth(10),
                              } name="caret-down" />
                          </View>
                        </ModalPicker>
                        <View style={{ flex: 0.5 }} />
                        <ModalPicker data={this.state.arrFromWeek} initValue={this.state.selectedFromWeek} onChange={option => {
                            this.selectedFromWeekLabel = option.label;
                            this.setState({
                              selectedFromWeek:
                                option.key
                            });
                          }} style={{ flex: 2 }}>
                          <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center", backgroundColor: "#f4f4f4" }}>
                            <Text style={[style.font.fontContentLarge, { height: responsiveHeight(4), flex: 2.5, backgroundColor: "#f4f4f4", paddingLeft: responsiveWidth(4), paddingTop: responsiveWidth(0.5) }]} editable={false}>
                              {parseInt(this.state.selectedFromWeek) < 10 ? "W 0" + this.state.selectedFromWeek : "W " + this.state.selectedFromWeek}
                            </Text>
                            <Icon size={responsiveWidth(3)} style={{ backgroundColor: "#F4F4F4", alignItems: "center", justifyContent: "center", padding: responsiveWidth(1) } //paddingRight: responsiveWidth(10),
                              } name="caret-down" />
                          </View>
                        </ModalPicker>
                      </View>
                    </View>
                    <View style={{ flexDirection: "row", marginTop: responsiveHeight(2) }}>
                      <Text
                        style={[
                          style.font
                            .fontContentLarge,
                          { flex: 1, paddingTop: 5 }
                        ]}
                      >
                        To
                      </Text>
                      <View style={{ flex: 2, flexDirection: "row" }}>
                        <ModalPicker data={this.state.arrToYear} initValue={this.state.selectedToYear} onChange={option => {
                            this.filterToWeekByYear(option.label);
                            this.setState({
                              selectedToYear:
                                option.label
                            });
                          }} style={{ flex: 2 }}>
                          <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center", backgroundColor: "#f4f4f4" }}>
                            <Text style={[style.font.fontContentLarge, { height: responsiveHeight(4), flex: 2.5, backgroundColor: "#f4f4f4", paddingLeft: responsiveWidth(4), paddingTop: responsiveWidth(0.5) }]} editable={false}>
                              {this.state.selectedToYear}
                            </Text>
                            <Icon size={responsiveWidth(3)} style={{ backgroundColor: "#F4F4F4", alignItems: "center", justifyContent: "center", padding: responsiveWidth(1) } //paddingRight: responsiveWidth(10),
                              } name="caret-down" />
                          </View>
                        </ModalPicker>

                        <View style={{ flex: 0.5 }} />

                        <ModalPicker data={this.state.arrToWeek} initValue={this.state.selectedToWeek} onChange={option => {
                            this.selectedToWeekLabel = option.label;
                            this.setState({
                              selectedToWeek:
                                option.key
                            });
                          }} style={{ flex: 2 }}>
                          <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center", backgroundColor: "#f4f4f4" }}>
                            <Text style={[style.font.fontContentLarge, { height: responsiveHeight(4), flex: 2.5, backgroundColor: "#f4f4f4", paddingLeft: responsiveWidth(4), paddingTop: responsiveWidth(0.5) }]} editable={false}>
                              {parseInt(this.state.selectedToWeek) < 10 ? "W 0" + this.state.selectedToWeek : "W " + this.state.selectedToWeek}
                            </Text>
                            <Icon size={responsiveWidth(3)} style={{ backgroundColor: "#F4F4F4", alignItems: "center", justifyContent: "center", padding: responsiveWidth(1) } //paddingRight: responsiveWidth(10),
                              } name="caret-down" />
                          </View>
                        </ModalPicker>
                      </View>
                    </View>
                    <View style={{ borderBottomWidth: 2, borderBottomColor: "#000", paddingTop: responsiveHeight(2), marginRight: responsiveWidth(-10) }} />
                    <Text style={[style.font.fontContentLarge, { marginTop: responsiveHeight(2) }]}>
                      Select Audit Team :
                    </Text>
                    {dataLine}
                  </View>
                </ScrollView>
              </View>

              <View style={{ flex: 4, backgroundColor: "white" }} />
            </View>;
        }
      } else {
        return <View style={{ backgroundColor: "#c00", flex: 1, alignItems: "center", justifyContent: "center" }}>
            <TouchableOpacity onPress={this.BackButtonOnPress.bind(this)}>
              <View style={{ alignItems: "center", justifyContent: "center", flex: 1, width: responsiveWidth(100) }}>
                <View style={{ alignItems: "center", justifyContent: "center", marginLeft: responsiveWidth(10), marginRight: responsiveWidth(10) }}>
                  <Image style={{ resizeMode: "cover", width: responsiveWidth(15), height: responsiveWidth(15) }} source={require("../Images/check.png")} />
                  <Text style={{ color: "#fff", fontSize: responsiveFontSize(5), textAlign: "center", fontFamily: "MyriadPro-Regular" }}>
                    Schedule has been created!
                  </Text>

                  <Text style={[style.font.fontContentSmall, { color: "#fff", textAlign: "center", marginTop: responsiveHeight(5) }]}>
                    Tap to dismiss
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>;
      }
    }

    BackButtonOnPress() {
      console.log("Create Schedule Inc On Press");
      //this.props.CloseModal(2);
      Actions.HomeScreen({ type: 'replace' })
    }

    Header() {
      if (this.state.currentTextHeader == "start") {
        return <View style={[styles.header, { justifyContent: "space-between", paddingLeft: responsiveWidth(1), paddingRight: responsiveWidth(1) }]}>
            <TouchableOpacity style={{ alignItems: "center", justifyContent: "space-between" }} onPress={this.BackButtonOnPress.bind(this)}>
              <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center", height: responsiveHeight(10), width: responsiveWidth(10) }}>
                <Icon size={responsiveWidth(3)} style={{ alignItems: "center", justifyContent: "center", color: "white", padding: responsiveWidth(1) }} name="chevron-left" />
                <Image source={Images.detective} style={{ flex: 1, height: responsiveHeight(6), width: responsiveHeight(6) }} />
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={{ alignItems: "center", justifyContent: "center", height: responsiveHeight(10) }} onPress={() => this._onPreviewSchedule()}>
              <Text style={{ fontFamily: "MyriadPro-Regular", color: "white", fontSize: responsiveFontSize(2.5) }}>
                Create Schedule Audit
              </Text>
            </TouchableOpacity>
          </View>;
      } else if (this.state.currentTextHeader == "preview") {
        return <View style={[styles.header, { justifyContent: "space-between", paddingLeft: responsiveWidth(1), paddingRight: responsiveWidth(1) }]}>
            <TouchableOpacity style={{ alignItems: "center", justifyContent: "space-between" }} onPress={this._backToCreate.bind(this)}>
              <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center", height: responsiveHeight(10), width: responsiveWidth(10) }}>
                <Icon size={responsiveWidth(3)} style={{ alignItems: "center", justifyContent: "center", color: "white", padding: responsiveWidth(1) }} name="chevron-left" />
                <Image source={Images.detective} style={{ flex: 1, height: responsiveHeight(6), width: responsiveHeight(6) }} />
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={{ alignItems: "flex-end", justifyContent: "center", height: responsiveHeight(10), width:responsiveWidth(30) }} onPress={() => this._onCreateSchedule()}>
              <Text
                style={[
                  style.font.fontContentLarge,
                  { color: "white" }
                ]}
              >
                Finish
              </Text>
            </TouchableOpacity>
          </View>;
      } else if (this.state.currentTextHeader == "finish") {
        console.log("blablabla");
        return null;
      }
    }

    marginTopForIOS() {
      if (Platform.OS === "ios") {
        return <View style={{ height: 20, backgroundColor: "#c00" }} />;
      }
    }

    render() {
      return <View style={{ backgroundColor: "white", height: responsiveHeight(100) }}>
          {this.marginTopForIOS()}

          {this.Header()}

          <View style={{ flex: 1 }}>
            {this.mainContainerCreateSchedule()}
          </View>
          <Spinner visible={this.state.loaderStatus} textContent={"Please Wait"} textStyle={{ color: "#fff" }} cancelable={false} />

          <Toast ref={component => (this._toast = component)} marginTop={64}>
            Validating Data
          </Toast>
        </View>;
    }
  }

            

var styles = StyleSheet.create({
    container: {
        marginTop: Metrics.marginTopContainer,
        backgroundColor: "#fff"
    },
    header: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: '#c00',
        height: Metrics.marginHeader
    },
})