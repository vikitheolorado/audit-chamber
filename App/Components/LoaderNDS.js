// @flow

import React, { } from 'react'
import { View,Modal, ActivityIndicator, Text} from 'react-native'
import PropTypes from 'prop-types';

export default class LoaderNDS extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      textContent: this.props.text,
      visible : this.props.visible
    }
  }

  static propTypes = {
    text: PropTypes.string.isRequired,
    visible: PropTypes.bool.isRequired,
  }

  componentWillReceiveProps(nextProps) {
      console.log('loadernds')
    console.log(nextProps) 
    this.setState({ visible : nextProps.visible, text : nextProps.text }); 
  }

  render() {
      console.log('render ' + this.state.visible)
    return (
        <Modal
            transparent={true}
            animationType={'none'}
            visible={this.state.visible}>
            <View>
                <ActivityIndicator size="small" color="#00ff00" />
                <Text>{this.state.text}</Text>
            </View>
      </Modal>
    )
  }
}
