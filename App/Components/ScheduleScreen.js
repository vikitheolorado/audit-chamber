import React, { Component } from 'react';

import {
    StyleSheet,
    Text,
    Alert,
    View,
    ScrollView,
    TouchableHighlight,
    Dimensions,
    Platform
} from 'react-native';

import { Button, Spinner } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { responsiveFontSize, responsiveHeight, responsiveWidth, newResponsiveFontSize } from '../Themes/Responsive'
import { range } from 'lodash';
import Calendar from 'react-native-calendar';
import moment from 'moment';
import Queries from '../Data/Queries';
import Constants from '../Data/Constants';
import axios from 'axios';
import style from '../Themes/Fonts';

var dateFormat = require('dateformat');

const DEVICE_WIDTH = Dimensions
    .get('window')
    .width;

const customStyle = {
    hasEventCircle: {
        backgroundColor: '#c00000'
    },
    weekRow: {
        backgroundColor: 'white'
    },
    dayButton: {
        backgroundColor: '#F5F5F5',
        margin: 1,
        borderColor: "#c6c6c6",
        borderTopColor: "#c6c6c6",
        alignItems: "flex-start",
        justifyContent: 'flex-start',
        borderWidth: 1,
        marginTop: 1,
        width: (DEVICE_WIDTH - 15) / 7,
        height: responsiveHeight(6),
        padding: 0
    },
    dayButtonFiller: {
        margin: 1,
        borderWidth: 1,
        borderColor: "#c6c6c6",
        alignItems: "flex-start",
        borderTopColor: "#c6c6c6",
        width: (DEVICE_WIDTH - 15) / 7,
        height: responsiveHeight(6)
    },
    currentDayText: {
        padding: responsiveWidth(3),
        fontSize: newResponsiveFontSize(1), 
        textAlign: 'center',
        fontFamily: 'MyriadPro-Regular',
    },
    currentDayCircle: {
        height: responsiveHeight(6),
        color: "black"
    },
    controlButtonText: {
        color: "#c00",
        fontWeight: "bold",
        fontSize: newResponsiveFontSize(2),
        textAlign: 'center',
        fontFamily: 'MyriadPro-Regular'
    },
    title: {
        color: "#c00",
        fontWeight: "bold",
        textAlign: 'center',
        fontSize: newResponsiveFontSize(1.7),
        fontFamily: 'MyriadPro-Regular'
    },
    day: {
        fontSize: newResponsiveFontSize(1), 
        textAlign: 'center',
        fontFamily: 'MyriadPro-Regular'
    },
}
const customDayHeadings = [
    'S',
    'M',
    'T',
    'W',
    'T',
    'F',
    'S'
];
const customMonthNames = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
];
const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    notes: {
        marginTop: responsiveHeight(1),
        paddingLeft: responsiveWidth(5),
        borderColor: '#F5F5F5',
        borderBottomWidth: 1,
        padding: responsiveWidth(1)
    },
    notes_notes: {
    },
    notes_text: {
        fontSize: newResponsiveFontSize(1.5),
        fontFamily: 'MyriadPro-Regular'
    },
    small_text: {
        fontSize: newResponsiveFontSize(1.2)
    },
})
export default class ScheduleScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            events: [],
            selectedDate: moment().format(),
            eventsinfo: [],
            eventsnonsched: [],
            eventssched: [],
            AuditSchedule: [],
            viewCalendar: <Spinner />,
            loaderStatus: true,
            colorBorder : false
        }
        this.flagDelete = 0;
    }
    ScheduleFiltering(perioddate) {
        var CurrentUser = Queries.getCurrentUser()[0].Username;
        this.setState({ eventsinfo: [] });
        var info = [];
        var AuditSchedule = Queries.getAuditSchedule(CurrentUser);
        AuditSchedule.map(function (data, i) {
            if (new Date(dateFormat(data.PeriodStart, "yyyy/mm/dd")) <= new Date(dateFormat(perioddate, "yyyy/mm/dd")) && new Date(dateFormat(data.PeriodEnd, "yyyy/mm/dd")) >= new Date(dateFormat(perioddate, "yyyy/mm/dd"))) {
                info.push(data)
            }
        });
        this.setState({ eventsinfo: info });
    }
    PopulateData(AuditSchedule) {
        var NonSchedule = [];
        var Schedule = [];
        var info = [];

        AuditSchedule.map(function (data, i) {
            if (data.AuditTypeId == 2) {
                NonSchedule.push({
                    date: new Date(data.PeriodStart),
                    hasEventCircleII: {
                        backgroundColor: '#ffc000',
                        flex: 1
                    }
                });
            }
        })

        AuditSchedule.map(function (data, i) {
            if (data.AuditTypeId == 3) {
                for (var d = new Date(data.PeriodStart); d <= new Date(data.PeriodEnd); d.setDate(d.getDate() + 1)) {
                    Schedule.push(new Date(d));
                }
            }
        })

        //console.log('Hasil Akhir')
        this.setState({ eventssched: Schedule, eventsnonsched: NonSchedule })
        // console.log(info); console.log('Hasil Paling Akhir')
        this.ScheduleFiltering(new Date())

        this.setState({
            viewCalendar: (
                <Calendar
                    eventDates={this.state.eventssched}
                    events={this.state.eventsnonsched}
                    weekStart={6}
                    dayHeadings={customDayHeadings}
                    monthNames={customMonthNames}
                    //scrollEnabled
                    showControls
                    onDateSelect={(date) => this.ScheduleFiltering(date)}
                    titleFormat={'MMMM YYYY'}
                    customStyle={customStyle}
                     />
            ),
            colorBorder: true
        })
    }
    componentWillMount() {
        var CurrentUser = Queries.getCurrentUser()[0].Username;

        console.log('CCAAPI')
        console.log(Queries.getPersistentData('token'))
        console.log(Constants.APILink + 'Schedule/GetAllSchedule?AuditorId=' + CurrentUser + `apuytoken`)
        axios
            .get(Constants.APILink + 'Schedule/GetAllSchedule?AuditorId=' + CurrentUser, {
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + Queries.getPersistentData('token'),
                },
                timeout: Constants.GetTimeout
            })
            .then(function (response) {
                Queries.deleteAuditSchedule(CurrentUser); 
                response
                    .data
                    .map(function (data, index) {
                        //console.log('Save Audit Begin')
                        Queries.saveAuditSchedule(data.AuditScheduleHeaderId, data.AuditScheduleHeaderId, data.PlantDesc, data.GroupLineDesc, data.AuditorId, data.AuditorName, data.WeekToDateStart, data.WeekToDateEnd, data.YearStart, data.YearEnd, data.PeriodStart, data.PeriodEnd, data.AuditTypeID);
                        //console.log('Save Audit End')
                    })

                var AuditSchedule = Queries.getAuditSchedule(CurrentUser); 
                this.PopulateData(AuditSchedule)
            }.bind(this))
            .catch(function(error) {
                this.setState({ loaderStatus: false });
                // Alert.alert("Connection Failed!", "Catch!");
            
                console.log(error);
                if (error && error.response && error.response.status == "401") {
                console.log("Information - Result - GetResultPage - 401");
                Queries.sessionHabis(error.response.status);
                }
                else{
                    var AuditSchedule = Queries.getAuditSchedule(CurrentUser);
                        // console.log('Dari data Local')
                        // console.log(AuditSchedule)
                        // console.log('Dari data Local Close')                
                        this.PopulateData(AuditSchedule)
                Alert.alert(
                    "Connection Failed!",
                    "Unable to retrieve data. Please check your connection."
                );

                }
                // console.log(error);
                //Alert.alert(JSON.stringify(error))
            }.bind(this));
           
    }
    render() {
        var row = this
            .state
            .eventsinfo
            .map(function (data, i) {

                return (
                    <ScrollView style={styles.notes}>
                        <View style={styles.notes_notes}>
                            <Text style={style.font.fontContentLarge}>Plant : {data.PlantDesc}
                                - {data.GroupLineDesc}</Text>
                            <Text style={style.font.fontContentNormal}>Auditor : {data.AuditorName}</Text>
                            <Text style={ style.font.fontContentNormal }>Week {data.WeekToDateStart}, {data.YearStart}</Text>
                        </View>
                        {/*<View style={[styles.notes_selected_date]}>
                            <Text style={styles.small_text}>{data.WeekToDateStart}</Text>
                        </View>*/}
                    </ScrollView>
                )
            });
        return (
            <View style={{paddingLeft:responsiveWidth(0),paddingRight:responsiveWidth(0)}}>
                {this.state.viewCalendar}
                <View style={{backgroundColor: this.state.colorBorder == false ? "transparent" : "grey", height:responsiveWidth(1),marginTop: responsiveWidth(1)}} />
                <ScrollView style={{ marginTop: responsiveWidth(1), borderWidth: 2, borderColor: "transparent", height: (width < height ? height : width) - ((Platform.OS === 'ios') ? 20 : 0) - responsiveHeight(7) - responsiveHeight(5) - 370 - responsiveHeight(2) }}>
                    {row}
                </ScrollView>
            </View>
        );
    }
}
