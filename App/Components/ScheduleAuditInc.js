import React, { Component } from 'react';
import { Container, Content, Body, ListItem, Text, CheckBox, Button, Segment, Input, Item, Left, Thumbnail, Spinner } from 'native-base';
import { Platform, Image, View, Dimensions, StatusBar, Alert, TouchableOpacity, StyleSheet, ScrollView, InteractionManager } from 'react-native';
const { width, height } = Dimensions.get('window');
import { responsiveHeight, responsiveWidth, responsiveFontSize, newResponsiveFontSize } from '../Themes/Responsive';
import Queries from '../Data/Queries'
import { getInstance } from '../Data/DB/DBHelper'
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button'
import DB from '../Data/DB/Db';
import axios from 'axios';
import Constants from '../Data/Constants'
import { Actions } from 'react-native-router-flux';
import Axios from 'axios';
import SpinnerFull from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-smart-toast'
import { Metrics, Images } from '../Themes';
var dateFormat = require('dateformat');
import Icon from 'react-native-vector-icons/FontAwesome';
import style from '../Themes/Fonts';
import LoaderNDS from './LoaderNDS';

export default class ScheduleAuditInc extends Component {

    constructor(props) {
        super(props);
        this.users = Queries.getCurrentUser();
        //console.log("data sch")
        //console.log(this.props.PK_AuditScheduleDetailID)
        this.state = {
            rowData: null,
            arrDataSubmit: '',
            indexAuditSelected: this.props.PK_AuditScheduleDetailID !== -1 ? this.props.PK_AuditScheduleDetailID : 0,
            loaderStatus: null,
            resCount: '',
            dummy:null
        };
        this.onSelect = this.onSelect.bind(this)
    }

    onSelect(index, value) {
      console.log("on select")
      console.log(this.state.indexAuditSelected)
      console.log(index)

        //var number = 0;
        //var tempAuditScheduleDetail = getInstance().objects('AuditScheduleDetail').filtered('AuditorID = "' + this.users[0].Username + '" AND ScheduleStatus = "0"')


        var tempAuditScheduleDetail = []

        getInstance().objects('AuditScheduleDetail').filtered('AuditorID = "' + this.users[0].Username + '" AND ScheduleStatus = "0"').map(function (data, i) {

            //Check overdue
            var tempAuditScheduleHeader = getInstance().objects('AuditScheduleHeader').filtered('ScheduleCode = "' + data.ScheduleCode + '"');

            if ((this.currentWeek >= (parseInt(tempAuditScheduleHeader[0].WeekToDateStart) + (parseInt(tempAuditScheduleHeader[0].YearStart) * 52))) && (this.currentWeek <= (parseInt(tempAuditScheduleHeader[0].WeekToDateEnd) + (parseInt(tempAuditScheduleHeader[0].YearEnd) * 52)))) {

                tempAuditScheduleDetail.push(data)
            }

        }.bind(this))

        var rowRadio = tempAuditScheduleDetail.map(function (data, i) {
            console.log("data.line radio") 
            //if(data.ScheduleStatus === '1'){
            var tempAuditScheduleHeader = getInstance().objects('AuditScheduleHeader').filtered('ScheduleCode = "' + data.ScheduleCode + '"');
            var tempMsGroupLine = getInstance().objects('MsGroupLine').filtered('GroupLineID = "' + data.GroupLineID + '"');

            if (index === i) {
                //number++;

                //Data untuk onpress
                var arrDataSubmit = {
                    plantid: tempAuditScheduleHeader[0].PlantID,
                    grouplinedesc: tempMsGroupLine[0].GroupLineIDDesc,
                    week: tempAuditScheduleHeader[0].WeekToDateStart,
                    year: tempAuditScheduleHeader[0].YearStart,
                    auditScheduleDetailID: data.PK_AuditScheduleDetailID,
                    auditType: tempAuditScheduleHeader[0].AuditType,
                    templateCode: tempAuditScheduleHeader[0].TemplateCode,
                    groupLineID: data.GroupLineID,
                    scheduleCode: data.ScheduleCode
                }

                this.setState({
                    arrDataSubmit: arrDataSubmit
                })


                return (
                    <RadioButton value={'item1'} style={{ padding: 20, borderColor: 'grey', borderBottomWidth: 1 }}>
                        <View style={{ flexDirection: 'row', marginLeft: 5 }}>
                            <Text style={[style.font.fontContentLarge, { width: responsiveWidth(45), color: 'white' }]}>{tempMsGroupLine[0].GroupLineIDDesc}</Text>
                            <Text style={[style.font.fontContentLarge, { textAlign: 'right', color: 'white' }]}>{tempAuditScheduleHeader[0].PlantDesc}</Text>
                        </View>
                    </RadioButton>
                );
            }
            else {
                return (
                    <RadioButton value={'item1'} style={{ padding: 20, borderColor: 'grey', borderBottomWidth: 1 }}>
                        <View style={{ flexDirection: 'row', marginLeft: 5 }}>
                            <Text style={[style.font.fontContentLarge, { width: responsiveWidth(45), color: 'black' }]}>{tempMsGroupLine[0].GroupLineIDDesc}</Text>
                            <Text style={[style.font.fontContentLarge, { textAlign: 'right', color: 'black' }]}>{tempAuditScheduleHeader[0].PlantDesc}</Text>
                        </View>
                    </RadioButton>
                );
            }
            //}
        }.bind(this)
        )

        var fullRadio = (<RadioGroup
            onSelect={(index, value) => { this.onSelect(index, value); console.log(index + " ### " + value) }}
            color='black'
            highlightColor='#c00'
            style={{ borderColor: 'grey', borderTopWidth: 1 }}
        >

            {rowRadio}

        </RadioGroup>)

        this.setState({
            rowData: fullRadio
        })
    }

    componentDidMount() {  
      console.log("Schedule Audit Didmount")
      console.log('here 21 march 2018 changes')
      
      this.setState({
        loaderStatus : true
      }, function(){
        console.log("done set to true")
      //perubahan di constructor rowdata: <spinner /> dan loaderStatus: false
      var param = dateFormat(new Date(), "yyyy/mm/dd");
      //console.log(param)
      console.log("CCAAPI");
      console.log(Constants.APILink + `Master/GetWeekPeriod?date=` + param + `apuytoken`);
      axios
        .get(
          Constants.APILink +
            `Master/GetWeekPeriod?date=` +
            param,
          {
            headers: {
              Accept:
                "application/json",
              "Content-Type":
                "application/json",
              Authorization:
                "Bearer " +
                Queries.getPersistentData(
                  "token"
                )
            },
            timeout: Constants.GetTimeout
          }
        )
        .then(res => {
          if (res.data.length == 0) { 
            Alert.alert(
              "Information",
              "Current week hasn't been registered in database",
              [
                {
                  text: "Ok", onPress : ()=>{
                      this.setState({ loaderStatus: false });    
                  }
                }
              ],
              {
                cancelable: false
              }
            )
          }
 
          this.currentWeek = res.data[0].WeekToDate + res.data[0].YEAR * 52;

          console.log("CCAAPI");
          console.log(Constants.APILink + `Schedule/GetAllAuditScheduleDetailByScheduleStatus?IntScheduleStatus=0` + `apuytoken`);
          axios
            //.get(`http://ccaaudit.azurewebsites.net/api/Schedule/GetAllAuditScheduleDetail`, {
            .get(
              Constants.APILink +
                `Schedule/GetAllAuditScheduleDetailByScheduleStatus?IntScheduleStatus=0`,
              {
                headers: {
                  Accept:
                    "application/json",
                  "Content-Type":
                    "application/json",
                  Authorization:
                    "Bearer " +
                    Queries.getPersistentData(
                      "token"
                    )
                },
                timeout: Constants.GetTimeout
              }
            )
            .then(res => {
              console.log("berhasil GetAllAuditScheduleDetailByScheduleStatus"); 
              console.log(res); 

              //setTimeout(()=>{
                // this.setState({ loaderStatus: false },()=>{
                //   console.log("done set to false")
                // });
              //}, 10000)

              DB.write(() => {
                getInstance().delete(getInstance().objects("AuditScheduleHeader"));
                getInstance().delete(getInstance().objects("AuditScheduleDetail"));
              });

              res.data.map(
                function(
                  data,
                  index
                ) { 
                  DB.write(() => {
                    DB.create(
                      "AuditScheduleHeader",
                      {
                        ScheduleCode:
                          data.AuditScheduleCode,
                        PlantID:
                          data.PlantId,
                        PlantDesc:
                          data.PlantDesc,
                        PeriodStart:
                          data.PeriodStart,
                        PeriodEnd:
                          data.PeriodEnd,
                        WeekToDateStart:
                          data.WeekToDateStart,
                        WeekToDateEnd:
                          data.WeekToDateEnd,
                        YearStart:
                          data.YearStart,
                        YearEnd:
                          data.YearEnd,
                        AuditType:
                          data.AuditType,
                        TemplateCode:
                          data.TemplateCode
                      },
                      true
                    );
                  });

                  DB.write(() => {
                    data.ObjTrAuditScheduleDetail.map(
                      function(
                        data,
                        index
                      ) { 
                        DB.create(
                          "AuditScheduleDetail",
                          {
                            PK_AuditScheduleDetailID: parseInt(
                              data.AuditScheduleDetailId
                            ),
                            ScheduleCode:
                              data.AuditScheduleCode,
                            AuditorID:
                              data.AuditorId,
                            GroupLineID:
                              data.GroupLineId,
                            CompleteTask:
                              data.CompletedTask,
                            TotalTask:
                              data.TotalTask,
                            AuditeeSignStatus:
                              data.AuditeeSignStatus,
                            LastUpdateDate:
                              data.LastUpdateDate,
                            ScheduleStatus:
                              data.ScheduleStatus,
                            StartDateTime:
                              data.StartAuditDate,
                            EndDateTime:
                              data.EndAuditDate,
                            TotalFindings:
                              data.TotalFindings
                          },
                          true
                        );
                      }
                    );
                  });
                }
              );

              var rowRadio;
              var fullRadio;

              //var tempAuditScheduleDetail = getInstance().objects('AuditScheduleDetail').filtered('AuditorID = "' + this.users[0].Username + '" AND ScheduleStatus = "0"')

              var tempAuditScheduleDetail = [];
              console.log("masuk 13 " + this.users[0].Username);
              console.log(getInstance().objects("AuditScheduleDetail"));
              getInstance()
                .objects("AuditScheduleDetail")
                .filtered('AuditorID = "' + this.users[0].Username + '" AND ScheduleStatus = "0"')
                .map(function(data, i) {
                    console.log("masuk 14");
                    console.log(data);
                    //Check overdue
                    var tempAuditScheduleHeader = getInstance()
                      .objects("AuditScheduleHeader")
                      .filtered('ScheduleCode = "' + data.ScheduleCode + '"');

                      console.log(tempAuditScheduleHeader[0].WeekToDateStart)
                      console.log(tempAuditScheduleHeader[0].WeekToDateEnd)

                      console.log(tempAuditScheduleHeader[0].YearStart)
                      console.log(tempAuditScheduleHeader[0].YearEnd)

                      console.log(this.currentWeek)
                      console.log(parseInt(tempAuditScheduleHeader[0].WeekToDateStart) + parseInt(tempAuditScheduleHeader[0].YearStart)* 52)
                      console.log(parseInt(tempAuditScheduleHeader[0].WeekToDateEnd) + parseInt(tempAuditScheduleHeader[0].YearEnd) * 52)

                    if (this.currentWeek >= parseInt(tempAuditScheduleHeader[0].WeekToDateStart) + parseInt(tempAuditScheduleHeader[0].YearStart) * 52 && this.currentWeek <= parseInt(tempAuditScheduleHeader[0].WeekToDateEnd) + parseInt(tempAuditScheduleHeader[0].YearEnd) * 52) {
                      tempAuditScheduleDetail.push(data);
                    }
                  }.bind(this));

              var number = 0;
              if (tempAuditScheduleDetail.length > 0) {
                var rowRadio = tempAuditScheduleDetail.map(function(data, i) {
                    //console.log(data.line)
                    //if(data.ScheduleStatus === '1'){
                    var tempAuditScheduleHeader = getInstance()
                      .objects("AuditScheduleHeader")
                      .filtered('ScheduleCode = "' + data.ScheduleCode + '"');
                    var tempMsGroupLine = getInstance()
                      .objects("MsGroupLine")
                      .filtered('GroupLineID = "' + data.GroupLineID + '"');

                    console.log("indexSelected" + this.state.indexAuditSelected)
                    if (this.state.indexAuditSelected == data.PK_AuditScheduleDetailID && this.state.indexAuditSelected !== 0) {
                      //console.log("masuk pilihan " + this.state.indexAuditSelected)
                      number = i;

                      //Data untuk onpress
                      var arrDataSubmit = { plantid: tempAuditScheduleHeader[0].PlantID, grouplinedesc: tempMsGroupLine[0].GroupLineIDDesc, week: tempAuditScheduleHeader[0].WeekToDateStart, year: tempAuditScheduleHeader[0].YearStart, auditScheduleDetailID: data.PK_AuditScheduleDetailID, auditType: tempAuditScheduleHeader[0].AuditType, templateCode: tempAuditScheduleHeader[0].TemplateCode, groupLineID: data.GroupLineID, scheduleCode: data.ScheduleCode };

                      this.setState(
                        {
                          arrDataSubmit: arrDataSubmit
                        }
                      );

                      return <RadioButton value={"item1"} style={{ padding: 20, borderColor: "grey", borderBottomWidth: 1}}>
                          <View style={{ flexDirection: "row", marginLeft: 5 }}>
                            <Text style={[style.font.fontContentLarge, { width: responsiveWidth(45) }]}>
                              {tempMsGroupLine[0].GroupLineIDDesc}
                            </Text>
                            <Text style={[style.font.fontContentLarge, { textAlign: "right" }]}>
                              {tempAuditScheduleHeader[0].PlantDesc}
                            </Text>
                          </View>
                        </RadioButton>;
                    } else {
                      //console.log("gak masuk pilihan " + this.state.indexAuditSelected)
                      //number++;
                      return <RadioButton value={"item1"} style={{ padding: 20, borderColor: "grey", borderBottomWidth: 1 }}>
                          <View style={{ flexDirection: "row", marginLeft: 5 }}>
                            <Text style={[style.font.fontContentLarge, { width: responsiveWidth(45) }]}>
                              {tempMsGroupLine[0].GroupLineIDDesc} 
                            </Text>
                            <Text style={[style.font.fontContentLarge, { textAlign: "right" }]}>
                              {tempAuditScheduleHeader[0].PlantDesc}
                            </Text>
                          </View>
                        </RadioButton>;
                    }
                    //}
                  }.bind(this));

                var indexRadioStart = null; 
                if (this.state.indexAuditSelected !== 0 && this.state.indexAuditSelected) {
                  indexRadioStart = number;
                } 

                fullRadio = <RadioGroup 
                onSelect={(index, value) => {
                  this.onSelect(index, value);
                  console.log(index + " ### " + value);
                }} 
                color="#c00" 
                highlightColor="#fff" 
                selectedIndex={indexRadioStart} 
                style={{ borderColor: "grey", borderTopWidth: 1 }}>
                    {rowRadio}
                  </RadioGroup>;
                
              } else {
                fullRadio = <Text />;
              }

              this.setState({
                rowData: fullRadio,
                resCount: res.data.length,
                loaderStatus: false,
              }); 

              // if(rowRadio.length > 0){
              //   console.log('row radio > 1')
              //   onSelect(0,'')
              // }

            })
            .catch(function(error) { 
              // this.setState({ 
              //   loaderStatus: false 
              // }, () => {
              //      console.log(error);
              //      if (error && error.response && error.response.status == "401") {
              //        console.log("Information - Result - GetResultPage - 401");
              //        Queries.sessionHabis(error.response.status);
              //      } else {
              //        Alert.alert("Connection Failed!", "Unable to retrieve data. Please check your connection.");
              //        fullRadio = <Text />;
              //        this.setState({ rowData: fullRadio });
              //      } 
              //    })
              console.log("error getallscheduleaudit")
              if (error && error.response && error.response.status == "401") {
                console.log("Information - Result - GetResultPage - 401");
                Queries.sessionHabis(error.response.status);
              } else {
                Alert.alert(
                  "Connection Failed!", 
                  "Unable to retrieve data. Please check your connection.",
                  [{
                    text:"Ok", onPress: ()=>{
                      this.setState({ loaderStatus: false },()=>{
                        fullRadio = <Text />;
                       this.setState({ rowData: fullRadio });
                      });
                    } 
                  }],
                  { cancelable:false }
                );
                
              } 
          }.bind(this));
          //console.log("data current week" + this.currentWeek) 
          
        })
        .catch(function(error) {

          console.log("error in get weekperiod");
          console.log(error);
          if (error && error.response && error.response.status == "401") {
            console.log("Information - Result - GetResultPage - 401");
            Queries.sessionHabis(error.response.status);
          } else {
            console.log("error in get weekperiod - connection failed");
            Alert.alert("Connection Failed!", "Unable to retrieve data. Please check your connection.",[{text:"Ok", onPress: ()=>{
              this.setState({ loaderStatus: false });
            } }],
            {cancelable:false}
            );
          }  
        }.bind(this));
      }.bind(this));


    }

    _onPressButton(e) {

        if (this.state.arrDataSubmit.length == 0) {
            this._toast.show({
                position: Toast.constants.gravity.top,
                children: 'Please choose a schedule'
            })
            return
        }

        this.setState({
            loaderStatus: true
        })

        requestAnimationFrame(() => {
            //console.log("masuk onpress")
            var param = this.state.arrDataSubmit
            //console.log(param.length)
            //console.log(param)

            var tempParam;
            if (param.auditType === 'GMP1') {
                tempParam = 2
            }
            else {
                tempParam = 3
            }
            //console.log(param);
            console.log('CCAAPI')
            console.log(Constants.APILink + 'DoAudit/PostUpdateAuditScheduleDetail?AuditScheduleDetailID=' + param.auditScheduleDetailID + '&status=1' + `apuytoken`)
            Axios.get(
              Constants.APILink +
                "DoAudit/PostUpdateAuditScheduleDetail?AuditScheduleDetailID=" +
                param.auditScheduleDetailID +
                "&status=1",
              {
                headers: {
                  Accept: "application/json",
                  "Content-Type": "application/json",
                  Authorization:
                    "Bearer " + Queries.getPersistentData("token")
                },
                timeout: Constants.GetTimeout
              }
            )
              .then(res => {
                //console.log("berhasil13")
                //console.log(res)
                this.setState({ loaderStatus: false },()=>{
                  Actions.QuestionScreen({
                    plantid: param.plantid,
                    grouplinedesc: param.grouplinedesc,
                    week: param.week,
                    year: param.year,
                    auditScheduleDetailID:
                      param.auditScheduleDetailID,
                    auditType: tempParam,
                    templateCode: param.templateCode,
                    groupLineID: param.groupLineID,
                    isComplete: false,
                    scheduleCode: param.scheduleCode
                  });
                });
                
              })
              .catch(function(error) { 
                    console.log(error);
                    if (error && error.response && error.response.status == "401") {
                      console.log("Information - Result - GetResultPage - 401");
                      Queries.sessionHabis(error.response.status);
                    } else {
                      Alert.alert("Connection Failed!", "Unable to retrieve data. Please check your connection.",[{text: "Ok", onPress:()=> {this.loaderStatus= false; this.setState({ loaderStatus : false })} }], {cancelable:false});
                    } 
                }.bind(this));
        })
    }

    BackButtonOnPress() {
        console.log("Schedule Audit Inc On Press")
        //this.props.CloseModal(1)
        Actions.HomeScreen({ type: 'replace' })
    }


    marginTopForIOS() {
        if (Platform.OS === 'ios') {
            return (
                <View style={{ height: 20, backgroundColor: '#c00' }}>
                </View>
            )
        }
    }

    showExistingSchedule() {
        console.log('showExistingSchedule')
        console.log(this.state.resCount)
        if (this.state.resCount === 0) {
            console.log('tidak ada schedule')
            return (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={style.font.fontContentNormal}> There is no schedule to conduct</Text>
                </View>

            )
        } else {
            console.log('ada schedule')
            return (
                <ScrollView>
                    {this.state.rowData}
                </ScrollView>
            )
        }
    }

    componentWillUnmount(){
      console.log("Schedule Audit WillUnmount")
      this.setState({
        loaderStatus:false
      },()=>{
        this.setState({
          loaderStatus:false
        })
        console.log("done set loader status to false")
        
      })
    }


    render() {

        return (
            <View style={{ backgroundColor: 'white', height: responsiveHeight(100) }}>
                <SpinnerFull visible={this.state.loaderStatus == null ? false : this.state.loaderStatus } textContent={"Please Wait"} textStyle={{ color: '#fff' }} cancelable={false} />
                {/*<LoaderNDS visible={this.state.loaderStatus == null ? false : this.state.loaderStatus } text={"Please Wait"}/>*/}
                {this.marginTopForIOS()}
                <View style={styles.header}>
                    <TouchableOpacity style={{ flex: 1, alignItems: 'center' }} onPress={this.BackButtonOnPress.bind(this)}>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: responsiveHeight(10), width: responsiveWidth(10) }}>
                            <Icon
                                size={responsiveWidth(3)}
                                style={{
                                    alignItems: 'center', justifyContent: 'center',
                                    color: 'white',
                                    padding: responsiveWidth(1)
                                }}
                                name='chevron-left' />
                            <Image source={Images.detective} style={{ flex: 1, height: responsiveHeight(6), width: responsiveHeight(6) }} />
                        </View>
                    </TouchableOpacity>
                    <View style={{ flex: 4, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', height: responsiveHeight(10) }}>
                        <Text style={[style.font.fontTitleLarge, { color: 'white', textAlign: 'center' }]}>Audit Chamber</Text>
                    </View>
                    <View style={{ flex: 1 }}>
                    </View>
                </View>
                <View style={{ height: responsiveHeight(75) }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', padding: responsiveWidth(5) }}>
                        <Text style={[style.font.fontContentLarge, {}]}>
                            Please choose your schedule :
                            </Text>
                    </View>

                    {this.showExistingSchedule()}

                </View>
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity style={{ backgroundColor: '#c00', height: responsiveHeight(5), width: responsiveWidth(20), alignItems: 'center', justifyContent: 'center' }} onPress={this._onPressButton.bind(this)}>
                        <Text style={[style.font.fontContentLarge, { color: 'white' }]}>Conduct</Text>
                    </TouchableOpacity>
                </View>
                <Toast
                    ref={component => this._toast = component}
                    marginTop={64}>
                    Validating Data
                    </Toast>
            </View>
        );
    }
}


var styles = StyleSheet.create({
    container: {
        marginTop: Metrics.marginTopContainer,
        backgroundColor: "#fff"
    },
    header: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: '#c00',
        height: Metrics.marginHeader
    },
})
