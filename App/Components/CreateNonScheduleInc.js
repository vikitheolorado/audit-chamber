import React, { Component } from 'react';
import { Platform, TouchableOpacity, View, Text, TextInput, ScrollView, Image, Alert, StyleSheet } from 'react-native';
import { Picker, Item, Button } from 'native-base';
import ModalPicker from 'react-native-modal-picker';
import { responsiveWidth, responsiveHeight, responsiveFontSize, newResponsiveFontSize } from '../Themes/Responsive'
import Icon from 'react-native-vector-icons/FontAwesome';
import update from 'react-addons-update';
import { getInstance } from '../Data/DB/DBHelper';
var dateFormat = require('dateformat');
import Moment from 'moment';
import Queries from '../Data/Queries'
import axios from 'axios';
import Constants from '../Data/Constants'
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-smart-toast'
import { Metrics, Images } from '../Themes';
import style from '../Themes/Fonts';
import { Actions } from 'react-native-router-flux';

export default class CreateNonScheduleInc extends Component {
    constructor(props) {
        super(props);
        //this.MsPlant = getInstance().objects('MsPlant');
        //this.MsGroupLine = getInstance().objects('MsGroupLine');

        this.MsUserLogin = Queries.getCurrentUser();
        this.MsPlant = getInstance().objects('MsPlant').filtered("PlantID = " + this.MsUserLogin[0].PlantID + " AND active = true")
        this.MsGroupLine = getInstance().objects('MsGroupLine').filtered("PlantID = " + this.MsUserLogin[0].PlantID + " AND active = true");
        //2.0.6
        this.MsTemplate = getInstance().objects('MsTemplateHeader').filtered("AuditTypeId = 2 AND TemplateStatus = 1");

        this.selectedPlantLabel = ''
        this.selectedAreaLabel = ''

        this.state = {
            selectedArea: '',
            selectedPlant: '',
            selectedTemplate: '',
            arrArea: [],
            arrPlant: [],
            arrTemplate: [],
            currentDate: '',
            isScheduleHasBeenCreated: false,
            submitLabel: "Create",
            loaderStatus: false,

        }
    }

    _onCreateSchedule() {
        // console.log(this.state.selectedType)
        // console.log(this.state.selectedPlant)
        // console.log(this.state.selectedFromWeek)
        // console.log(this.state.selectedToWeek)
        // console.log(this.state.selectedFromYear)
        // console.log(this.state.selectedToYear)
        // console.log(this.state.arrSelectedAuditor)

        this.setState({
            loaderStatus: true
        })

        requestAnimationFrame(() => {
            if (this.state.submitLabel == "Create") {

                var users = Queries.getCurrentUser();
                console.log(users)
                if (users.length) {
                    var dataPost = {
                        "UserName": users[0].Username,
                        "AuditTypeId": "2",
                        "PlantId": this.state.selectedPlant,
                        "PeriodStart": dateFormat(new Date(), "yyyy-mm-dd 00:00:00"),
                        "PeriodEnd": dateFormat(new Date(), "yyyy-mm-dd 23:59:59"),
                        "TemplateCode": this.state.selectedTemplate,
                        "AuditDetail": [
                            {
                                "AuditorID": users[0].Username,
                                "GroupLineId": this.state.selectedArea
                            }
                        ]
                    }

                    var data = [];
                    data = JSON.stringify(dataPost); 
                }

                console.log("data create non schedule")
                console.log(data)

                console.log('CCAAPI')
                console.log(Constants.APILink + `Schedule/PostCreateAuditNonSchedule`, data + 'apuytoken')
                axios
                    .post(Constants.APILink + `Schedule/PostCreateAuditNonSchedule`, data, {
                        headers: {
                            "Accept": "application/json",
                            "Content-Type": "application/json",
                            "Authorization": "Bearer " + Queries.getPersistentData('token'),
                        },
                        timeout: Constants.GetTimeout
                    })
                    .then(res => {
                        console.log(res) 
                        if (res.data == 0) {
                            this.setState({ isScheduleHasBeenCreated: true })
                            this.setState({
                                submitLabel: "Create",
                                loaderStatus: false
                            })
                        }
                        else {
                            Alert.alert(
                                "Connection Failed!",
                                "Unable to retrieve data. Please check your connection.",
                                [{text:"Ok", onPress:()=>{
                                    this.setState({
                                        submitLabel:"Create",
                                        loaderStatus:false
                                    })
                                }}],
                            {cancelable:false}
                            ); 
                            
                        }
                    })
                    .catch(function(error) { 
                        console.log(error);
                        if (error && error.response && error.response.status == "401") {
                            console.log("Information - Result - GetResultPage - 401");
                            Queries.sessionHabis(error.response.status);
                        }
                        else{
                            Alert.alert(
                                "Connection Failed!",
                                "Unable to retrieve data. Please check your connection.",
                                [{text:"Ok", onPress:()=>{
                                    this.setState({
                                        submitLabel:"Create",
                                        loaderStatus:false
                                    })
                                }}],
                            {cancelable:false}
                            ); 
                        } 
                    }.bind(this));
                    
            }
        })

    }


    componentWillMount() {
        //console.log(this.MsPlant)
        var arrPlant = [];
        this.MsPlant.map(function (data, index) {
            if (index === 0) {
                this.selectedPlantLabel = data.PlantDesc;
                this.setState({
                    selectedPlant: data.PlantID
                })
            }
            var tempPlant = { "label": data.PlantDesc, "key": data.PlantID }
            arrPlant.push(tempPlant)
        }.bind(this))

        var arrArea = [];
        this.MsGroupLine.map(function (data, index) {
            console.log(data)
            if (index === 0) {
                this.selectedAreaLabel = data.GroupLineIDDesc;
                this.setState({
                    selectedArea: data.GroupLineID
                })
            }
            var tempArea = { "label": data.GroupLineIDDesc, "key": data.GroupLineID }
            arrArea.push(tempArea)
        }.bind(this))

        var arrTemplate = [];
        this.MsTemplate.map(function (data, index) {
            if (index === 0) {
                this.setState({
                    selectedTemplate: data.TemplateCode
                })
            }
            var tempTemplate = { "label": data.TemplateCode, "key": data.TemplateCode }
            arrTemplate.push(tempTemplate)
        }.bind(this))

        var timeData = dateFormat(new Date(), "dd/mm/yyyy");

        this.setState({
            arrPlant: arrPlant,
            arrArea: arrArea,
            arrTemplate: arrTemplate,
            currentDate: timeData
        })


    }

    mainContainerCreateSchedule() {
        if (!this.state.isScheduleHasBeenCreated) {
            return (
                <View style={{ backgroundColor: 'white', flex: 1 }}>
                    <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: responsiveHeight(5) }}>
                        <View style={{ alignItems: 'center', justifyContent: 'center', marginLeft: responsiveWidth(10), marginRight: responsiveWidth(10) }}>
                            <Text style={[style.font.fontContentLarge]}>You are create a non schedule audit. Please fill below information to proceed!</Text>

                            <View style={{ flexDirection: 'row', marginTop: responsiveHeight(1) }}>
                                <Text style={[style.font.fontContentLarge, { flex: 1.5, paddingTop: responsiveHeight(1) }]}>
                                    Plant
                                </Text>
                                <ModalPicker
                                    data={this.state.arrPlant}
                                    initValue={this.state.selectedPlantLabel}
                                    onChange={(option) => { selectedPlantLabel = option.label; this.setState({ selectedPlant: option.key }) }}
                                    style={{ flex: 4, width: responsiveWidth(60) }}>
                                    <View style={{ height: responsiveHeight(5),flexDirection: 'row', alignItems: 'center', backgroundColor: '#F4F4F4' }}>
                                        <Text
                                            style={[style.font.fontContentLarge, {
                                                width: responsiveWidth(50),
                                                backgroundColor: '#F4F4F4',
                                                paddingLeft: responsiveWidth(2),
                                                paddingTop: 5,
                                            }]}
                                            editable={false}
                                        >
                                            {this.selectedPlantLabel}
                                        </Text>
                                        <Icon
                                            size={responsiveWidth(3)}
                                            style={{
                                                backgroundColor: '#F4F4F4',
                                                alignItems: 'center', justifyContent: 'center',
                                                //paddingRight: responsiveWidth(10),
                                                padding: responsiveWidth(1)
                                            }}
                                            name='caret-down' />
                                    </View>
                                </ModalPicker>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: responsiveHeight(2) }}>
                                <Text style={[style.font.fontContentLarge, { flex: 1.5, paddingTop: responsiveHeight(1) }]}>
                                    Template
                                </Text>
                                <ModalPicker
                                    data={this.state.arrTemplate}
                                    initValue={this.state.selectedTemplate}
                                    onChange={(option) => { this.setState({ selectedTemplate: option.key }) }}
                                    style={{ flex: 4, width: responsiveWidth(60) }}>
                                    <View style={{ height: responsiveHeight(5),flexDirection: 'row', alignItems: 'center', backgroundColor: '#F4F4F4' }}>
                                        <Text
                                            style={[style.font.fontContentLarge, {
                                                width: responsiveWidth(50),
                                                backgroundColor: '#F4F4F4',
                                                paddingLeft: responsiveWidth(2),
                                                paddingTop: 5,
                                            }]}
                                            editable={false}
                                        >
                                            {this.state.selectedTemplate}
                                        </Text>
                                        <Icon
                                            size={responsiveWidth(3)}
                                            style={{
                                                backgroundColor: '#F4F4F4',
                                                alignItems: 'center', justifyContent: 'center',
                                                //paddingRight: responsiveWidth(10),
                                                padding: responsiveWidth(1)
                                            }}
                                            name='caret-down' />
                                    </View>
                                </ModalPicker>
                            </View>
                            <View style={{ alignItems:'center', justifyContent:'center', flexDirection: 'row', marginTop: responsiveHeight(2)}}>
                                <Text style={[style.font.fontContentLarge, { flex: 1.5, paddingTop: responsiveHeight(1) }]}>
                                    Area
                                </Text>
                                <ModalPicker
                                    data={this.state.arrArea}
                                    initValue={this.state.selectedAreaLabel}
                                    onChange={(option) => { this.selectedAreaLabel = option.label; this.setState({ selectedArea: option.key }) }}
                                    style={{ flex: 4, width: responsiveWidth(60) }}>
                                    <View style={{ height: responsiveHeight(5), flexDirection: 'row', alignItems: 'center', backgroundColor: '#F4F4F4' }}>
                                        <Text
                                            style={[style.font.fontContentLarge, {
                                                width: responsiveWidth(50),
                                                backgroundColor: '#F4F4F4',
                                                paddingLeft: responsiveWidth(2),
                                            }]}
                                            editable={false}
                                        >
                                            {this.selectedAreaLabel}
                                        </Text>
                                        <Icon
                                            size={responsiveWidth(3)}
                                            style={{
                                                backgroundColor: '#F4F4F4',
                                                alignItems: 'center', justifyContent: 'center',
                                                //paddingRight: responsiveWidth(10),
                                                padding: responsiveWidth(1)
                                            }}
                                            name='caret-down' />
                                    </View>
                                </ModalPicker>
                            </View>

                            <View style={{ flexDirection: 'row', marginTop: responsiveHeight(2) }}>
                                <Text style={[style.font.fontContentLarge, { flex: 1.5, paddingTop: responsiveHeight(1) }]}>
                                    Date
                            </Text>
                                <Text style={{ flex: 4, fontSize: responsiveFontSize(2.5), paddingTop: responsiveWidth(1.5), color: 'black', width: responsiveWidth(60), fontFamily: 'MyriadPro-Regular' }}>{this.state.currentDate}</Text>
                            </View>

                            <View style={{ flexDirection: 'row', marginTop: responsiveHeight(2) }}>
                                <View style={{ flex: 1.5 }} />

                                <Button style={{ flex: 4, backgroundColor: '#c00' }} onPress={this._onCreateSchedule.bind(this)}>
                                    <Text style={[style.font.fontContentLarge, { flex: 1, textAlign: "center", color: 'white' }]}>{this.state.submitLabel}</Text>
                                </Button>
                            </View>


                        </View>
                    </View>


                </View>
            )
        } else {
            return (
                <View style={{ backgroundColor: '#c00', flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={this.BackButtonOnPress.bind(this)}>

                        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, width: responsiveWidth(100) }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center', marginLeft: responsiveWidth(10), marginRight: responsiveWidth(10) }}>
                                <Image style={{ resizeMode: 'cover', width: responsiveWidth(15), height: responsiveWidth(15) }} source={require('../Images/check.png')} />
                                <Text style={{ color: '#fff', fontSize: responsiveFontSize(5), textAlign: 'center', fontFamily: 'MyriadPro-Regular' }}>Schedule has been created!</Text>

                                <Text style={[style.font.fontContentSmall, { color: '#fff', textAlign: 'center', marginTop: responsiveHeight(5) }]}>Tap to dismiss</Text>
                            </View>
                        </View>
                    </TouchableOpacity>

                </View>




            )
        }

    }

    BackButtonOnPress() {
        console.log("Create Non Schedule Inc On Press")
        //this.props.CloseModal(3)
        Actions.HomeScreen({ type: 'replace' })
    }

    marginTopForIOS() {
        if (Platform.OS === 'ios') {
            return (
                <View style={{ height: 20, backgroundColor: '#c00' }}>
                </View>
            )
        }
    }

    header() {
        if (!this.state.isScheduleHasBeenCreated) {
        return (
            <View style={styles.header}>
                <TouchableOpacity style={{ flex: 1, alignItems: 'center' }} onPress={this.BackButtonOnPress.bind(this)}>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: responsiveHeight(10), width: responsiveWidth(10) }}>
                        <Icon
                            size={responsiveWidth(3)}
                            style={{
                                alignItems: 'center', justifyContent: 'center',
                                color: 'white',
                                padding: responsiveWidth(1)
                            }}
                            name='chevron-left' />
                        <Image source={Images.detective} style={{ flex: 1, height: responsiveHeight(6), width: responsiveHeight(6) }} />
                    </View>
                </TouchableOpacity>
                <View style={{ flex: 4, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', height: responsiveHeight(10) }}>
                    <Text style={[style.font.fontTitleLarge, { flex: 2, color: 'white', textAlign: 'center' }]}>Audit Chamber</Text>
                </View>
                <View style={{ flex: 1 }} />
            </View>
        )
    }else
        return(
            null
        )
    }

    render() {
        console.log("Create non schedule")
        return (
            <View style={{ backgroundColor: 'white', height: responsiveHeight(100) }}>
                <Spinner visible={this.state.loaderStatus} textContent={"Please Wait"} textStyle={{ color: '#fff' }} cancelable={false} />
                {this.marginTopForIOS()}

                {this.header()}

                <View style={{ flex: 1 }}>
                    {this.mainContainerCreateSchedule()}
                </View>

                <Toast
                    ref={component => this._toast = component}
                    marginTop={64}>
                    Validating Data
                </Toast>
            </View>
        )

    }
}



var styles = StyleSheet.create({
    container: {
        marginTop: Metrics.marginTopContainer,
        backgroundColor: "#fff"
    },
    header: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: '#c00',
        height: Metrics.marginHeader
    },
})
