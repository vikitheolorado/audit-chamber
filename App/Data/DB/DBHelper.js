import DB from './Db';

export function getInstance() {
    let dbVersion = DB.objects('Version');
    let currentVersion = '0.2';
    if (dbVersion.length == 0) {
        initInstance();

        DB.write(() => {
            let ver = DB.create('Version', { dbVersion: currentVersion });
        });
    } else if (dbVersion[0].dbVersion != currentVersion) {
        DB.write(() => {
            let cat = DB.create('TemplateQuestion', {
                templateCode: "ID-TMP-GMP-001",
                categoryID: 4,
                questionFill: "Karyawan menggunakan APD selama bekerja",
                questionOrder: 5,
                weight: 5.0,
                isRequired: true,
                templateQuestionID: 22
            });
        }, true);

        DB.write(() => {
            let cat = DB.create('TemplateQuestion', {
                templateCode: "ID-TMP-GMP-002",
                categoryID: 4,
                questionFill: "Karyawan menggunakan APD selama bekerja",
                questionOrder: 5,
                weight: 5.0,
                isRequired: true,
                templateQuestionID: 53
            });
        }, true);

    } else {

    }

    return DB;
}

function initInstance() {
    DB.write(() => {
        let cat = DB.create('TemplateCategory', {
            templateCode: "ID-TMP-GMP-001",
            auditType: 2,
            templateStatus: "RELEASE",
            releasedDate: new Date(),
            categoryID: 1,
            categoryDesc: "Ringkas",
            evidenceID: 1,
            evidenceType: "TYPE 01",
            categoryOrder: 1
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateCategory', {
            templateCode: "ID-TMP-GMP-001",
            auditType: 2,
            templateStatus: "RELEASE",
            releasedDate: new Date(),
            categoryID: 2,
            categoryDesc: "Rapi",
            evidenceID: 1,
            evidenceType: "TYPE 01",
            categoryOrder: 2
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateCategory', {
            templateCode: "ID-TMP-GMP-001",
            auditType: 2,
            templateStatus: "RELEASE",
            releasedDate: new Date(),
            categoryID: 3,
            categoryDesc: "Resik",
            evidenceID: 1,
            evidenceType: "TYPE 01",
            categoryOrder: 3
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateCategory', {
            templateCode: "ID-TMP-GMP-001",
            auditType: 2,
            templateStatus: "RELEASE",
            releasedDate: new Date(),
            categoryID: 4,
            categoryDesc: "Safety",
            evidenceID: 1,
            evidenceType: "TYPE 01",
            categoryOrder: 4
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateCategory', {
            templateCode: "ID-TMP-GMP-001",
            auditType: 2,
            templateStatus: "RELEASE",
            releasedDate: new Date(),
            categoryID: 5,
            categoryDesc: "Rawat",
            evidenceID: 1,
            evidenceType: "TYPE 01",
            categoryOrder: 5
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateCategory', {
            templateCode: "ID-TMP-GMP-001",
            auditType: 2,
            templateStatus: "RELEASE",
            releasedDate: new Date(),
            categoryID: 6,
            categoryDesc: "Rajin",
            evidenceID: 1,
            evidenceType: "TYPE 01",
            categoryOrder: 6
        });
    });

    DB.write(() => {
        let cat = DB.create('TemplateCategory', {
            templateCode: "ID-TMP-GMP-002",
            auditType: 3,
            templateStatus: "RELEASE",
            releasedDate: new Date(),
            categoryID: 1,
            categoryDesc: "Ringkas",
            evidenceID: 1,
            evidenceType: "TYPE 01",
            categoryOrder: 1
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateCategory', {
            templateCode: "ID-TMP-GMP-002",
            auditType: 3,
            templateStatus: "RELEASE",
            releasedDate: new Date(),
            categoryID: 2,
            categoryDesc: "Rapi",
            evidenceID: 1,
            evidenceType: "TYPE 01",
            categoryOrder: 2
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateCategory', {
            templateCode: "ID-TMP-GMP-002",
            auditType: 3,
            templateStatus: "RELEASE",
            releasedDate: new Date(),
            categoryID: 3,
            categoryDesc: "Resik",
            evidenceID: 1,
            evidenceType: "TYPE 01",
            categoryOrder: 3
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateCategory', {
            templateCode: "ID-TMP-GMP-002",
            auditType: 3,
            templateStatus: "RELEASE",
            releasedDate: new Date(),
            categoryID: 4,
            categoryDesc: "Safety",
            evidenceID: 1,
            evidenceType: "TYPE 01",
            categoryOrder: 4
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateCategory', {
            templateCode: "ID-TMP-GMP-002",
            auditType: 3,
            templateStatus: "RELEASE",
            releasedDate: new Date(),
            categoryID: 5,
            categoryDesc: "Rawat",
            evidenceID: 1,
            evidenceType: "TYPE 01",
            categoryOrder: 5
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateCategory', {
            templateCode: "ID-TMP-GMP-002",
            auditType: 3,
            templateStatus: "RELEASE",
            releasedDate: new Date(),
            categoryID: 6,
            categoryDesc: "Rajin",
            evidenceID: 1,
            evidenceType: "TYPE 01",
            categoryOrder: 6
        });
    });

    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 1,
            questionFill: "Tidak ada peralatan/perlengkapan/material yang rusak/tidak digunakan di area kerja",
            questionOrder: 1,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 1
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 1,
            questionFill: "Tidak ada peralatan/material yang tidak terpakai yang disimpan di laci, area kerja, atau tempat penyimpanan lainnya",
            questionOrder: 2,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 2
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 1,
            questionFill: "Tidak ada barang/aksesoris pribadi selama di area kerja kecuali APD standar yang digunakan secara baik dan benar",
            questionOrder: 3,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 3
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 1,
            questionFill: "Hanya karyawan yang berwenang/yang diberi otoritas saja yang berada di area",
            questionOrder: 4,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 4
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 1,
            questionFill: "Sampah dipisah menurut jenisnya, organik/non organik/B3",
            questionOrder: 5,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 5
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 2,
            questionFill: "Peralatan dan material terletak pada tempat yang spesifik dan sesuai",
            questionOrder: 1,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 6
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 2,
            questionFill: "Peralatan, pipa, tangki, lemari, area kerja, dan area penyimpanan, rak, peralatan, sudah terlabel (ada identifikasi) dan tertata dengan rapi dan teratur",
            questionOrder: 2,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 7
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 2,
            questionFill: "Tempat sampah sudah terpasang label",
            questionOrder: 3,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 8
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 2,
            questionFill: "Area pejalan kaki, tempat kerja/lantai, dan peralatan teridentifikasi( terdapat marking jelas, terbagi zona secara jelas )",
            questionOrder: 4,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 9
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 3,
            questionFill: "Lingkungan, rak penyimpanan, lemari, area kerja, dan area penyimpanan bersih (bebas dari jamur, kotoran, debu, tetesan/genangan), termasuk jalur crown/closure, jalur perform/air conveyour, jalur can lid dan can body",
            questionOrder: 1,
            weight: 5.5,
            isRequired: true,
            templateQuestionID: 10
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 3,
            questionFill: "Tempat sampah dilengkapi dengan plastik dan dibuang secara berkala",
            questionOrder: 2,
            weight: 5.0,
            isRequired: false,
            templateQuestionID: 11
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 3,
            questionFill: "Alat kebersihan yang diperlukan tersedia lengkap & berfungsi baik",
            questionOrder: 3,
            weight: 5.0,
            isRequired: false,
            templateQuestionID: 12
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 3,
            questionFill: "Tidak ada akumulasi mikrobiologi ditandai bau busuk/fermentasi",
            questionOrder: 4,
            weight: 5.0,
            isRequired: true,
            templateQuestionID: 13
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 3,
            questionFill: "Lampu penerangan berfungsi dan cover lampu terpasang dalam keadaan bersih",
            questionOrder: 5,
            weight: 5.0,
            isRequired: false,
            templateQuestionID: 14
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 3,
            questionFill: "Tersedia sarana mencuci tangan lengkap & tata caranya",
            questionOrder: 6,
            weight: 5.0,
            isRequired: false,
            templateQuestionID: 15
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 3,
            questionFill: "Seluruh pegawai memiliki kebersihan diri (personal hygiene) yang baik",
            questionOrder: 7,
            weight: 5.0,
            isRequired: true,
            templateQuestionID: 16
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 3,
            questionFill: "Tidak ada indikasi adanya infestasi atau aktifitas pest dan insect",
            questionOrder: 8,
            weight: 5.0,
            isRequired: true,
            templateQuestionID: 17
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 4,
            questionFill: "Pengamanan mesin (safety shut down), tanda peringatan, dan rak digunakan sesuai dengan load ratingnya",
            questionOrder: 1,
            weight: 5.0,
            isRequired: true,
            templateQuestionID: 18
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 4,
            questionFill: "Tanda peringatan sudah terpasang dengan baik (denah ruangan, denah alur evakuasi, kriteria chemical, dll). Semua personel mengetahui lokasi penyimpanan P3K, APAR & eye shower",
            questionOrder: 2,
            weight: 5.0,
            isRequired: false,
            templateQuestionID: 19
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 4,
            questionFill: "Tersedia lampu emergency di area dan berfungsi dengan baik",
            questionOrder: 3,
            weight: 5.0,
            isRequired: false,
            templateQuestionID: 20
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 4,
            questionFill: "MSDS tersedia di tiap area penempatan bahan kimia/pelumas/bahan baku/finish product dan selalu update",
            questionOrder: 4,
            weight: 5.0,
            isRequired: false,
            templateQuestionID: 21
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 4,
            questionFill: "Karyawan menggunakan APD selama bekerja",
            questionOrder: 5,
            weight: 5.0,
            isRequired: true,
            templateQuestionID: 22
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 4,
            questionFill: "Prosedur Log Out Tag Out (LOTO)/signage terpasang saat pelaksanaan perbaikan",
            questionOrder: 6,
            weight: 5.5,
            isRequired: true,
            templateQuestionID: 23
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 4,
            questionFill: "Peralatan keamanan (APAR, hydrant, APD & P3K) sudah tersimpan dengan baik, sesuai jenisnya dan dapat digunakan",
            questionOrder: 7,
            weight: 5.0,
            isRequired: false,
            templateQuestionID: 24
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 5,
            questionFill: "Prosedur dan Work Instruction (WI) kerja terdapat di area kerja dan terupdate",
            questionOrder: 1,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 25
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 5,
            questionFill: "Karyawan memahami definisi, jenis & sifat B3 & LB3",
            questionOrder: 2,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 26
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 5,
            questionFill: "Tersedia titik penempatan B3 & LB3 lengkap dengan sarana pendukung (spill kit, bunding, label LB3 & simbol LB3)",
            questionOrder: 3,
            weight: 1.5,
            isRequired: true,
            templateQuestionID: 27
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 5,
            questionFill: "Tidak ada kebocoran pelumas, steam, udara bertekanan, chemical, bahan baku, dan produk jadi",
            questionOrder: 4,
            weight: 1.5,
            isRequired: true,
            templateQuestionID: 28
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 5,
            questionFill: "Visual management untuk memastikan semua tugas terlaksana dengan benar",
            questionOrder: 5,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 29
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 6,
            questionFill: "Semua personel terlibat dan turut serta dalam kegiatan GMP & 5R-1S",
            questionOrder: 1,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 30
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-001",
            categoryID: 6,
            questionFill: "Karyawan melakukan pencatatan WO/checklist/QMP yang berlaku di area masing-masing",
            questionOrder: 2,
            weight: 1.5,
            isRequired: true,
            templateQuestionID: 31
        });
    });

    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 1,
            questionFill: "Tidak ada peralatan/perlengkapan/material yang rusak/tidak digunakan di area kerja",
            questionOrder: 1,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 32
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 1,
            questionFill: "Tidak ada peralatan/material yang tidak terpakai yang disimpan di laci, area kerja, atau tempat penyimpanan lainnya",
            questionOrder: 2,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 33
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 1,
            questionFill: "Tidak ada barang/aksesoris pribadi selama di area kerja kecuali APD standar yang digunakan secara baik dan benar",
            questionOrder: 3,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 34
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 1,
            questionFill: "Hanya karyawan yang berwenang/yang diberi otoritas saja yang berada di area",
            questionOrder: 4,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 35
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 1,
            questionFill: "Sampah dipisah menurut jenisnya, organik/non organik/B3",
            questionOrder: 5,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 36
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 2,
            questionFill: "Peralatan dan material terletak pada tempat yang spesifik dan sesuai",
            questionOrder: 1,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 37
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 2,
            questionFill: "Peralatan, pipa, tangki, lemari, area kerja, dan area penyimpanan, rak, peralatan, sudah terlabel (ada identifikasi) dan tertata dengan rapi dan teratur",
            questionOrder: 2,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 38
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 2,
            questionFill: "Tempat sampah sudah terpasang label",
            questionOrder: 3,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 39
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 2,
            questionFill: "Area pejalan kaki, tempat kerja/lantai, dan peralatan teridentifikasi( terdapat marking jelas, terbagi zona secara jelas )",
            questionOrder: 4,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 40
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 3,
            questionFill: "Lingkungan, rak penyimpanan, lemari, area kerja, dan area penyimpanan bersih (bebas dari jamur, kotoran, debu, tetesan/genangan), termasuk jalur crown/closure, jalur perform/air conveyour, jalur can lid dan can body",
            questionOrder: 1,
            weight: 5.5,
            isRequired: true,
            templateQuestionID: 41
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 3,
            questionFill: "Tempat sampah dilengkapi dengan plastik dan dibuang secara berkala",
            questionOrder: 2,
            weight: 5.0,
            isRequired: false,
            templateQuestionID: 42
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 3,
            questionFill: "Alat kebersihan yang diperlukan tersedia lengkap & berfungsi baik",
            questionOrder: 3,
            weight: 5.0,
            isRequired: false,
            templateQuestionID: 43
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 3,
            questionFill: "Tidak ada akumulasi mikrobiologi ditandai bau busuk/fermentasi",
            questionOrder: 4,
            weight: 5.0,
            isRequired: true,
            templateQuestionID: 44
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 3,
            questionFill: "Lampu penerangan berfungsi dan cover lampu terpasang dalam keadaan bersih",
            questionOrder: 5,
            weight: 5.0,
            isRequired: false,
            templateQuestionID: 45
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 3,
            questionFill: "Tersedia sarana mencuci tangan lengkap & tata caranya",
            questionOrder: 6,
            weight: 5.0,
            isRequired: false,
            templateQuestionID: 46
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 3,
            questionFill: "Seluruh pegawai memiliki kebersihan diri (personal hygiene) yang baik",
            questionOrder: 7,
            weight: 5.0,
            isRequired: true,
            templateQuestionID: 47
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 3,
            questionFill: "Tidak ada indikasi adanya infestasi atau aktifitas pest dan insect",
            questionOrder: 8,
            weight: 5.0,
            isRequired: true,
            templateQuestionID: 48
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 4,
            questionFill: "Pengamanan mesin (safety shut down), tanda peringatan, dan rak digunakan sesuai dengan load ratingnya",
            questionOrder: 1,
            weight: 5.0,
            isRequired: true,
            templateQuestionID: 49
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 4,
            questionFill: "Tanda peringatan sudah terpasang dengan baik (denah ruangan, denah alur evakuasi, kriteria chemical, dll). Semua personel mengetahui lokasi penyimpanan P3K, APAR & eye shower",
            questionOrder: 2,
            weight: 5.0,
            isRequired: false,
            templateQuestionID: 50
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 4,
            questionFill: "Tersedia lampu emergency di area dan berfungsi dengan baik",
            questionOrder: 3,
            weight: 5.0,
            isRequired: false,
            templateQuestionID: 51
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 4,
            questionFill: "MSDS tersedia di tiap area penempatan bahan kimia/pelumas/bahan baku/finish product dan selalu update",
            questionOrder: 4,
            weight: 5.0,
            isRequired: false,
            templateQuestionID: 52
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 4,
            questionFill: "Karyawan menggunakan APD selama bekerja",
            questionOrder: 5,
            weight: 5.0,
            isRequired: true,
            templateQuestionID: 53
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 4,
            questionFill: "Prosedur Log Out Tag Out (LOTO)/signage terpasang saat pelaksanaan perbaikan",
            questionOrder: 6,
            weight: 5.5,
            isRequired: true,
            templateQuestionID: 54
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 4,
            questionFill: "Peralatan keamanan (APAR, hydrant, APD & P3K) sudah tersimpan dengan baik, sesuai jenisnya dan dapat digunakan",
            questionOrder: 7,
            weight: 5.0,
            isRequired: false,
            templateQuestionID: 55
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 5,
            questionFill: "Prosedur dan Work Instruction (WI) kerja terdapat di area kerja dan terupdate",
            questionOrder: 1,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 56
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 5,
            questionFill: "Karyawan memahami definisi, jenis & sifat B3 & LB3",
            questionOrder: 2,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 57
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 5,
            questionFill: "Tersedia titik penempatan B3 & LB3 lengkap dengan sarana pendukung (spill kit, bunding, label LB3 & simbol LB3)",
            questionOrder: 3,
            weight: 1.5,
            isRequired: true,
            templateQuestionID: 58
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 5,
            questionFill: "Tidak ada kebocoran pelumas, steam, udara bertekanan, chemical, bahan baku, dan produk jadi",
            questionOrder: 4,
            weight: 1.5,
            isRequired: true,
            templateQuestionID: 59
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 5,
            questionFill: "Visual management untuk memastikan semua tugas terlaksana dengan benar",
            questionOrder: 5,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 60
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 6,
            questionFill: "Semua personel terlibat dan turut serta dalam kegiatan GMP & 5R-1S",
            questionOrder: 1,
            weight: 1.5,
            isRequired: false,
            templateQuestionID: 61
        });
    });
    DB.write(() => {
        let cat = DB.create('TemplateQuestion', {
            templateCode: "ID-TMP-GMP-002",
            categoryID: 6,
            questionFill: "Karyawan melakukan pencatatan WO/checklist/QMP yang berlaku di area masing-masing",
            questionOrder: 2,
            weight: 1.5,
            isRequired: true,
            templateQuestionID: 62
        });
    });

    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 1, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 1, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 1, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 2, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 2, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 2, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 3, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 3, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 3, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 4, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 4, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 4, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 5, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 5, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 5, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 6, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 6, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 6, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 7, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 7, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 7, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 8, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 8, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 8, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 9, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 9, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 9, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 10, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 10, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 10, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 11, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 11, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 11, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 12, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 12, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 12, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 13, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 13, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 13, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 14, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 14, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 14, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 15, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 15, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 15, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 16, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 16, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 16, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 17, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 17, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 17, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 18, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 18, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 18, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 19, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 19, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 19, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 20, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 20, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 20, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 21, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 21, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 21, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 22, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 22, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 22, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 23, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 23, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 23, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 24, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 24, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 24, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 25, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 25, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 25, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 26, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 26, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 26, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 27, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 27, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 27, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 28, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 28, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 28, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 29, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 29, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 29, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 30, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 30, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 30, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 31, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 31, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 31, answerType: 'N/A' }) });

    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 32, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 32, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 32, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 33, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 33, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 33, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 34, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 34, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 34, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 35, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 35, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 35, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 36, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 36, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 36, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 37, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 37, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 37, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 38, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 38, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 38, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 39, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 39, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 39, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 40, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 40, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 40, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 41, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 41, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 41, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 42, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 42, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 42, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 43, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 43, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 43, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 44, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 44, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 44, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 45, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 45, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 45, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 46, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 46, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 46, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 47, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 47, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 47, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 48, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 48, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 48, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 49, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 49, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 49, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 50, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 50, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 50, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 51, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 51, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 51, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 52, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 52, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 52, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 53, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 53, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 53, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 54, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 54, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 54, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 55, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 55, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 55, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 56, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 56, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 56, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 57, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 57, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 57, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 58, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 58, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 58, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 59, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 59, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 59, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 60, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 60, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 60, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 61, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 61, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 61, answerType: 'N/A' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 1, templateQuestionID: 62, answerType: 'YES' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 2, templateQuestionID: 62, answerType: 'NO' }) });
    DB.write(() => { let ans = DB.create('TemplateAnswer', { answerID: 3, templateQuestionID: 62, answerType: 'N/A' }) });

    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 1, lineID: 791, areaID: 3, areaDesc: 'Blowing'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 1, lineID: 791, areaID: 9, areaDesc: 'Filling'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 1, lineID: 791, areaID: 14, areaDesc: 'Labeller'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 1, lineID: 791, areaID: 20, areaDesc: 'Packer'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 1, lineID: 791, areaID: 21, areaDesc: 'Palletizer'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 2, lineID: 792, areaID: 2, areaDesc: 'Blowfil'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 2, lineID: 792, areaID: 14, areaDesc: 'Labeller'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 2, lineID: 792, areaID: 20, areaDesc: 'Packer'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 2, lineID: 792, areaID: 21, areaDesc: 'Palletizer'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 3, lineID: 793, areaID: 2, areaDesc: 'Blowfil'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 3, lineID: 793, areaID: 14, areaDesc: 'Labeller'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 3, lineID: 793, areaID: 20, areaDesc: 'Packer'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 3, lineID: 793, areaID: 21, areaDesc: 'Palletizer'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 4, lineID: 794, areaID: 3, areaDesc: 'Blowing'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 4, lineID: 794, areaID: 9, areaDesc: 'Filling'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 4, lineID: 794, areaID: 14, areaDesc: 'Labeller'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 4, lineID: 794, areaID: 20, areaDesc: 'Packer'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 4, lineID: 794, areaID: 21, areaDesc: 'Palletizer'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 5, lineID: 795, areaID: 8, areaDesc: 'Filler'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 5, lineID: 795, areaID: 20, areaDesc: 'Packer'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 5, lineID: 795, areaID: 21, areaDesc: 'Palletizer'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 6, lineID: 291, areaID: 7, areaDesc: 'Extract Room'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 6, lineID: 291, areaID: 11, areaDesc: 'Finish Syrup'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 6, lineID: 291, areaID: 18, areaDesc: 'Office'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 6, lineID: 291, areaID: 22, areaDesc: 'Simple Syrup'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 7, lineID: 292, areaID: 6, areaDesc: 'External Area'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 7, lineID: 292, areaID: 13, areaDesc: 'Internal Area'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 7, lineID: 292, areaID: 18, areaDesc: 'Office'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 8, lineID: 293, areaID: 1, areaDesc: 'Aerator'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 8, lineID: 293, areaID: 5, areaDesc: 'Effluent'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 8, lineID: 293, areaID: 12, areaDesc: 'Influent'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 8, lineID: 293, areaID: 18, areaDesc: 'Office'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 9, lineID: 294, areaID: 17, areaDesc: 'Maintenance Office'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 9, lineID: 294, areaID: 23, areaDesc: 'Spare Part Room'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 9, lineID: 294, areaID: 25, areaDesc: 'Utility'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 9, lineID: 294, areaID: 26, areaDesc: 'Utility Office'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 9, lineID: 294, areaID: 27, areaDesc: 'Workshop'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 10, lineID: 295, areaID: 10, areaDesc: 'Finish Good Racking'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 10, lineID: 295, areaID: 15, areaDesc: 'Loading Dock'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 10, lineID: 295, areaID: 24, areaDesc: 'Staging Area'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 11, lineID: 296, areaID: 4, areaDesc: 'Canteen'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 11, lineID: 296, areaID: 18, areaDesc: 'Office'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 11, lineID: 296, areaID: 19, areaDesc: 'Other'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 12, lineID: 297, areaID: 16, areaDesc: 'Main Lab'}) });
    // DB.write(() => { let area = DB.create('MsArea', {groupLineID: 12, lineID: 297, areaID: 18, areaDesc: 'Office'}) });

    // DB.write(() =>{
    //     DB.create('MsUser', {UserID:1, Username:'IDRAHARJODE', FullName: 'Deni Kukuh Raharjo', Email: 'DENI.RAHARJO@CCAMATIL.COM', Roles: 2, isLogin: 0,PlantID:9, Position:'GMP Auditor'});
    //     DB.create('MsUser', {UserID:2, Username:'IDSUBAGYOWA', FullName: 'Wahyu Subagyo', Email: 'WAHYU.SUBAGYO@CCAMATIL.COM', Roles: 2, isLogin: 0,PlantID:9, Position:'GMP Auditor'});
    //     DB.create('MsUser', {UserID:3, Username:'IDDARYANTLI', FullName: 'Linda Christina Daryanto', Email: 'LINDACH.DARYANTO@CCAMATIL.COM', Roles: 2, isLogin: 0,PlantID:9, Position:'GMP Auditor'});
    //     DB.create('MsUser', {UserID:4, Username:'IDSILALAHSU', FullName: 'Sujono Silalahi', Email: 'SUJONO.SILALAHI@CCAMATIL.COM', Roles: 2, isLogin: 0,PlantID:9, Position:'GMP Auditor'});
    //     DB.create('MsUser', {UserID:5, Username:'IDRISMAWAZZ', FullName: 'Rismawati', Email: 'RISMAWATI@CCAMATIL.COM', Roles: 2, isLogin: 0,PlantID:9, Position:'GMP Auditor'});
    //     DB.create('MsUser', {UserID:6, Username:'IDYUNANTOBR', FullName: 'Bawa Rizki', Email: 'BAWA.RISKI.YUNANTO@CCAMATIL.COM', Roles: 1, isLogin: 0,PlantID:9, Position:'Manager'});
    //     DB.create('MsUser', {UserID:7, Username:'IDMANAGER', FullName: 'Dimas', Email: 'Dimas.Pr.Donosaputra@ccamatil.com', Roles: 1, isLogin: 0,PlantID:9, Position:'Manager'});
    //     DB.create('MsUser', {UserID:8, Username:'IDAUDITOR', FullName: 'Dimas', Email: 'Dimas.Pr.Donosaputra@ccamatil.com', Roles: 2, isLogin: 0,PlantID:9, Position:'GMP Auditor'});
    // });

    // DB.write(() =>{
    //     DB.create('MsUserAuditee', {UserID:1, Username:'IDKRISWIDIG', FullName:'I Gde Kris', Email: 'GDE.KRISWIDANA@CCAMATIL.COM', PlantID:9, GroupLineID:2});
    //     DB.create('MsUserAuditee', {UserID:2, Username:'IDROMBEHE', FullName:'Hendra Rombe', Email: 'HENDRA.ROMBE@CCAMATIL.COM', PlantID:9, GroupLineID:2});
    //     DB.create('MsUserAuditee', {UserID:3, Username:'IDSUBROTOAN', FullName:'Anggi', Email: '', PlantID:9, GroupLineID:2});
    //     DB.create('MsUserAuditee', {UserID:4, Username:'IDARDIPURBO', FullName:'Bonifatius', Email: 'BONIFATIUS.ARDIPURO@CCAMATIL.COM', PlantID:9, GroupLineID:3});
    //     DB.create('MsUserAuditee', {UserID:5, Username:'IDAWANGGA', FullName:'Awangga', Email: '', PlantID:9, GroupLineID:3});
    //     DB.create('MsUserAuditee', {UserID:6, Username:'IDBAYU', FullName:'Bayu', Email: '', PlantID:9, GroupLineID:3});
    //     DB.create('MsUserAuditee', {UserID:7, Username:'IDSAPUTRADI', FullName:'Dian Saputro', Email: 'DIAN.SAPUTRA@CCAMATIL.COM', PlantID:9, GroupLineID:4});
    //     DB.create('MsUserAuditee', {UserID:8, Username:'IDMURHANDZZ', FullName:'Murhandoko', Email: 'MURHANDOKO@CCAMATIL.COM', PlantID:9, GroupLineID:4});
    //     DB.create('MsUserAuditee', {UserID:9, Username:'IDLISTYANLE', FullName:'Leonardus ', Email: 'LEONARDUS.LISTYANA@CCAMATIL.COM', PlantID:9, GroupLineID:5});
    //     DB.create('MsUserAuditee', {UserID:10, Username:'IDPUTRADE', FullName:'Delant', Email: 'DELLANT.PUTRA@CCAMATIL.COM', PlantID:9, GroupLineID:5});
    //     DB.create('MsUserAuditee', {UserID:11, Username:'IDSODIQHI', FullName:'Himawan', Email: 'HIMAWAN.SODIQ@CCAMATIL.COM', PlantID:9, GroupLineID:6});
    //     DB.create('MsUserAuditee', {UserID:12, Username:'IDNORMANSZZ', FullName:'NOrmansyah', Email: 'M.NORMANSYAH@CCAMATIL.COM', PlantID:9, GroupLineID:6});
    //     DB.create('MsUserAuditee', {UserID:13, Username:'IDSUDIYONZZ', FullName:'SudiyoNO', Email: 'SUDIYONO@CCAMATIL.COM', PlantID:9, GroupLineID:6});
    //     DB.create('MsUserAuditee', {UserID:14, Username:'IDSODIQHI', FullName:'Himawan', Email: 'HIMAWAN.SODIQ@CCAMATIL.COM', PlantID:9, GroupLineID:7});
    //     DB.create('MsUserAuditee', {UserID:15, Username:'IDNORMANSZZ', FullName:'NOrmansyah', Email: 'M.NORMANSYAH@CCAMATIL.COM', PlantID:9, GroupLineID:7});
    //     DB.create('MsUserAuditee', {UserID:16, Username:'IDSUDIYONZZ', FullName:'SudiyoNO', Email: 'SUDIYONO@CCAMATIL.COM', PlantID:9, GroupLineID:7});
    //     DB.create('MsUserAuditee', {UserID:17, Username:'IDSODIQHI', FullName:'Himawan', Email: 'HIMAWAN.SODIQ@CCAMATIL.COM', PlantID:9, GroupLineID:8});
    //     DB.create('MsUserAuditee', {UserID:18, Username:'IDNORMANSZZ', FullName:'NOrmansyah', Email: 'M.NORMANSYAH@CCAMATIL.COM', PlantID:9, GroupLineID:8});
    //     DB.create('MsUserAuditee', {UserID:19, Username:'IDSUDIYONZZ', FullName:'SudiyoNO', Email: 'SUDIYONO@CCAMATIL.COM', PlantID:9, GroupLineID:8});
    //     DB.create('MsUserAuditee', {UserID:20, Username:'IDSURYAWAZZ', FullName:'Suryawan', Email: 'SURYAWAN@CCAMATIL.COM', PlantID:9, GroupLineID:9});
    //     DB.create('MsUserAuditee', {UserID:21, Username:'IDYULISTIHE', FullName:'Hendri YulistioNO', Email: 'HENRI.YULISTIONO@CCAMATIL.COM', PlantID:9, GroupLineID:9});
    //     DB.create('MsUserAuditee', {UserID:22, Username:'IDUTOMOIN', FullName:'Indro', Email: 'INDRO.UTOMO@CCAMATIL.COM', PlantID:9, GroupLineID:9});
    //     DB.create('MsUserAuditee', {UserID:23, Username:'IDEFENDYWE', FullName:'Wendy', Email: 'WENDY.EFENDY@CCAMATIL.COM', PlantID:9, GroupLineID:9});
    //     DB.create('MsUserAuditee', {UserID:24, Username:'IDLATIFHE', FullName:'Heri Latif', Email: 'HERI.LATIF@CCAMATIL.COM', PlantID:9, GroupLineID:10});
    //     DB.create('MsUserAuditee', {UserID:25, Username:'IDSUSANTIZZ', FullName:'Susanti', Email: 'SUSANTI@CCAMATIL.COM', PlantID:9, GroupLineID:10});
    //     DB.create('MsUserAuditee', {UserID:26, Username:'IDKAMIDIH', FullName:'Ihsanudin', Email: 'IHSANUDIN.KAMID@CCAMATIL.COM', PlantID:9, GroupLineID:10});
    //     DB.create('MsUserAuditee', {UserID:27, Username:'IDYUNANTOBR', FullName:'Bawa Riski', Email: 'BAWA.RISKI.YUNANTO@CCAMATIL.COM', PlantID:9, GroupLineID:12});
    //     DB.create('MsUserAuditee', {UserID:28, Username:'IDDANIAN', FullName:'Anita', Email: 'ANITAKURNIATI.DANI@CCAMATIL.COM', PlantID:9, GroupLineID:12});
    //     DB.create('MsUserAuditee', {UserID:29, Username:'IDRAHARJODE', FullName:'Deni Kukuh Raharjo', Email: 'DENI.RAHARJO@CCAMATIL.COM', PlantID:9, GroupLineID:11});
    //     DB.create('MsUserAuditee', {UserID:30, Username:'DJUARTAAD', FullName:'Ade Juarta', Email: 'ADE.JUARTA@CCAMATIL.COM', PlantID:9, GroupLineID:11});

    //     DB.create('MsUserAuditee', {UserID:31, Username:'IDTEST', FullName:'Test', Email: 'test@CCAMATIL.COM', PlantID:9, GroupLineID:1});

    // });

    // DB.write(() => {
    //    DB.create('MsPlant', {PlantID: 1, PlantDesc:'Cibitung'});
    //    DB.create('MsPlant', {PlantID: 2, PlantDesc:'Medan'});
    //    DB.create('MsPlant', {PlantID: 4, PlantDesc:'Lampung'});
    //    DB.create('MsPlant', {PlantID: 5, PlantDesc:'Bandung'});
    //    DB.create('MsPlant', {PlantID: 6, PlantDesc:'Semarang'});
    //    DB.create('MsPlant', {PlantID: 7, PlantDesc:'Surabaya'});
    //    DB.create('MsPlant', {PlantID: 8, PlantDesc:'Balinusa'});
    //    DB.create('MsPlant', {PlantID: 9, PlantDesc:'Cikedokan'});

    // });

    // // DB.write(() => {
    // //     DB.create('AuditScheduleHeader', {ScheduleCode: 'GMP1', PeriodStart:'2017-03-13', PeriodEnd: '2017-03-16', WeekToDateStart: 3, WeekToDateEnd: 3, YearStart: 2017, YearEnd: 2017, AuditType: 'GMP', TemplateCode:'CD-Test1'});
    // //     DB.create('AuditScheduleHeader', {ScheduleCode: 'GMP2', PeriodStart:'2017-03-13', PeriodEnd: '2017-03-16', WeekToDateStart: 4, WeekToDateEnd: 4, YearStart: 2017, YearEnd: 2017, AuditType: 'GMP', TemplateCode:'CD-Test2'});
    // // }); 

    // // DB.write(() => {
    // //     DB.create('AuditScheduleDetail', {PK_AuditScheduleDetailID : 1, ScheduleCode: 'ID-SCD-GMP1-002', AuditorID:'Adhitya', GroupLineID: 1, CompleteTask: 3, TotalTask: 10, AuditeeSignStatus:0, LastUpdateDate: '2017-03-13 11:37:37.807'});
    // //     DB.create('AuditScheduleDetail', {PK_AuditScheduleDetailID : 2, ScheduleCode: 'ID-SCD-GMP1-002', AuditorID:'Adhitya', GroupLineID: 2, CompleteTask: 3, TotalTask: 20, AuditeeSignStatus:0, LastUpdateDate: '2017-03-13 11:37:37.807'});
    // //     DB.create('AuditScheduleDetail', {PK_AuditScheduleDetailID : 3, ScheduleCode: 'ID-SCD-GMP1-002', AuditorID:'Adhitya', GroupLineID: 2, CompleteTask: 3, TotalTask: 20, AuditeeSignStatus:1, LastUpdateDate: '2017-03-14 11:37:37.807'});
    // //     DB.create('AuditScheduleDetail', {PK_AuditScheduleDetailID : 4, ScheduleCode: 'ID-SCD-GMP2-004', AuditorID:'Adhitya', GroupLineID: 3, CompleteTask: 3, TotalTask: 20, AuditeeSignStatus:1, LastUpdateDate: '2017-03-14 13:37:37.807'});
    // //     DB.create('AuditScheduleDetail', {PK_AuditScheduleDetailID : 5, ScheduleCode: 'ID-SCD-GMP2-004', AuditorID:'Adhitya', GroupLineID: 2, CompleteTask: 3, TotalTask: 20, AuditeeSignStatus:0, LastUpdateDate: '2017-03-15 11:37:37.807'});
    // //     DB.create('AuditScheduleDetail', {PK_AuditScheduleDetailID : 6, ScheduleCode: 'ID-SCD-GMP2-004', AuditorID:'Adhitya', GroupLineID: 2, CompleteTask: 3, TotalTask: 20, AuditeeSignStatus:0, LastUpdateDate: '2017-03-15 11:37:37.807'});
    // //     DB.create('AuditScheduleDetail', {PK_AuditScheduleDetailID : 7, ScheduleCode: 'ID-SCD-GMP2-004', AuditorID:'Adhitya', GroupLineID: 2, CompleteTask: 3, TotalTask: 20, AuditeeSignStatus:0, LastUpdateDate: '2017-03-15 11:37:37.807'});
    // // }); 

    // DB.write(() => {
    //     DB.create('MsGroupLine', {GroupLineID:1, GroupLineIDDesc:'Line 1', PlantID:9, GroupLineIDAbb: 'L1'});
    //     DB.create('MsGroupLine', {GroupLineID:2, GroupLineIDDesc:'Line 2', PlantID:9, GroupLineIDAbb: 'L2'});
    //     DB.create('MsGroupLine', {GroupLineID:3, GroupLineIDDesc:'Line 3', PlantID:9, GroupLineIDAbb: 'L3'});
    //     DB.create('MsGroupLine', {GroupLineID:4, GroupLineIDDesc:'Line 4', PlantID:9, GroupLineIDAbb: 'L4'});
    //     DB.create('MsGroupLine', {GroupLineID:5, GroupLineIDDesc:'Line 5', PlantID:9, GroupLineIDAbb: 'L5'});
    //     DB.create('MsGroupLine', {GroupLineID:6, GroupLineIDDesc:'Syrup Making', PlantID:9, GroupLineIDAbb: 'SYR'});
    //     DB.create('MsGroupLine', {GroupLineID:7, GroupLineIDDesc:'Water Treatment', PlantID:9, GroupLineIDAbb: 'WTP'});
    //     DB.create('MsGroupLine', {GroupLineID:8, GroupLineIDDesc:'Waste Water', PlantID:9, GroupLineIDAbb: 'WWTP'});
    //     DB.create('MsGroupLine', {GroupLineID:9, GroupLineIDDesc:'Maintenance & Utility', PlantID:9, GroupLineIDAbb: 'ME'});
    //     DB.create('MsGroupLine', {GroupLineID:10, GroupLineIDDesc:'Inventory Area', PlantID:9, GroupLineIDAbb: 'IM'});
    //     DB.create('MsGroupLine', {GroupLineID:11, GroupLineIDDesc:'Public Area', PlantID:9, GroupLineIDAbb: 'PA'});
    //     DB.create('MsGroupLine', {GroupLineID:12, GroupLineIDDesc:'Quality Area', PlantID:9, GroupLineIDAbb: 'QA'});
    //     DB.create('MsGroupLine', {GroupLineID:13, GroupLineIDDesc:'Line 1', PlantID:1, GroupLineIDAbb: 'L1'});
    //     DB.create('MsGroupLine', {GroupLineID:14, GroupLineIDDesc:'Line 2', PlantID:1, GroupLineIDAbb: 'L2'});
    //     DB.create('MsGroupLine', {GroupLineID:15, GroupLineIDDesc:'Line 3', PlantID:1, GroupLineIDAbb: 'L3'});
    // });
}