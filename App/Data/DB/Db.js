import Realm from 'realm';
import Version from '../Models/Version';
import PersistentData from '../Models/PersistentData';
import TemplateCategory from '../Models/TemplateCategory';
import TemplateQuestion from '../Models/TemplateQuestion';
import TemplateAnswer from '../Models/TemplateAnswer';
import TrxAuditAnswer from '../Models/TrxAuditAnswer';
import MsArea from '../Models/MsArea';
import AuditFindingList from '../Models/AuditFindingList';
import MsUser from '../Models/MsUser';
import AuditScheduleHeader from '../Models/AuditScheduleHeader';
import AuditScheduleDetail from '../Models/AuditScheduleDetail';
import MsLine from '../Models/MsLine';
import MsGroupLine from '../Models/MsGroupLine';
import MsPlant from '../Models/MsPlant';
import Attachment from '../Models/Attachment';
import MsUserAuditee from '../Models/MsUserAuditee';
import Notification from '../Models/Notification';
import AuditSchedule from '../Models/AuditSchedule';
import MsTemplateHeader from '../Models/MsTemplateHeader';

modelSchema = [
    Version,
    PersistentData,
    TemplateCategory,
    TemplateQuestion,
    TemplateAnswer,
    TrxAuditAnswer,
    MsArea,
    AuditFindingList,
    MsUser, AuditScheduleHeader, AuditScheduleDetail, MsLine, MsGroupLine, MsPlant,
    Attachment,
    Notification,
    MsUserAuditee,
    AuditSchedule,
    MsTemplateHeader
];
export default new Realm({ schemaVersion:3, schema: modelSchema});