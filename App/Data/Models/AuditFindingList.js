import Realm from 'realm';
export default class AuditFindingList extends Realm.Object {}
AuditFindingList.schema = {
    name: "AuditFindingList",
    primaryKey: "auditFindingListID",
    properties:{
        auditFindingListID: "int",
        trxAuditAnswerID: "int",
        comments: "string",
        area: "int",
        createdDate: "date",
    },
};