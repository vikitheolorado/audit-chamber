import Realm from 'realm';
export default class Attachment extends Realm.Object {}
Attachment.schema = {
    name: "Attachment",
    primaryKey: "attachmentID",
    properties:{
        attachmentID: "int",
        auditFindingListID: "int",
        url: "string",
        extension: "string",
        type: "string",
        createdBy: "string",
        createdDate: "date",
    },
};