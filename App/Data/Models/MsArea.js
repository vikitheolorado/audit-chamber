import Realm from 'realm';
export default class MsArea extends Realm.Object {}
MsArea.schema = {
    name: "MsArea",
    properties:{
        groupLineID: "int",
        areaID: "int",
        areaDesc: "string",
        active :"bool"
    },
};