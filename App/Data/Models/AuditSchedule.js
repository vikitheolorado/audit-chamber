import Realm from 'realm';
export default class AuditSchedule extends Realm.Object {}
AuditSchedule.schema = {
    name: 'AuditSchedule',
    properties: {
		AuditScheduleHeaderId: 'int',
        AuditScheduleDetailId:'int',
        PlantDesc: {type: 'string', optional: true}, 
        GroupLineDesc: {type: 'string', optional: true},
        AuditorId: {type: 'string', optional: true},  
        AuditorName: {type: 'string', optional: true}, 
        WeekToDateStart: {type: 'int', optional: true}, 
        WeekToDateEnd: {type: 'int', optional: true}, 
        YearStart: {type: 'int', optional: true}, 
        YearEnd: {type: 'int', optional: true},
        PeriodStart: {type: 'date', optional: true}, 
        PeriodEnd: {type: 'date', optional: true}, 
        AuditTypeId: {type: 'int', optional: true}
    }
    };