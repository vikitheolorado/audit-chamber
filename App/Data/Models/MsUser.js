import Realm from 'realm';
export default class MsUser extends Realm.Object {}
MsUser.schema = {
    name: 'MsUser',
    primaryKey: 'UserID',
    properties: {
        UserID:    'int',    // primary key
        Username: {type: 'string', optional: true},
        FullName: {type: 'string', optional: true},
        Email: {type: 'string', optional: true},
        Position: {type: 'string', optional: true},
        Roles: {type: 'int', optional: true},
        GroulLineID: {type: 'int', optional: true},
        PlantID: {type: 'int', optional: true},
        isLogin: {type: 'int', optional: true},//sementara digunakan untuk menandakan user yang sedang login
        active :"bool"
    }
};