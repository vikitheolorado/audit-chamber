import Realm from 'realm';
export default class MsTemplateHeader extends Realm.Object {}
MsTemplateHeader.schema = {
    name: 'MsTemplateHeader',
    properties: {
		TemplateHeaderId: 'int',
        AuditTypeId:'int',
        TemplateCode: {type: 'string', optional: true}, 
        TemplateStatus:'int'
    }
    };