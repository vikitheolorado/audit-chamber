import Realm from 'realm';
export default class MsPlant extends Realm.Object {}
MsPlant.schema = {
    name: 'MsPlant',
    primaryKey: 'PlantID',
    properties: {
		PlantID: 'int',
        PlantDesc: {type: 'string', optional: true}, 
        UTC: {type: 'string', optional: true},
        active :"bool"
    }
    };