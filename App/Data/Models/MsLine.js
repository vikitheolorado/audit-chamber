import Realm from 'realm';
export default class MsLine extends Realm.Object {}
MsLine.schema = {
    name: 'MsLine',
    primaryKey: 'PK_LineID',
    properties: {
        PK_LineID: 'int',
		LineID: 'int',
        LineIDDesc: {type: 'string', optional: true}, 
        GroupLineID: {type: 'int', optional: true},
        active :"bool"
    }
    };