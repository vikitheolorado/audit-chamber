import Realm from 'realm';
export default class TemplateQuestion extends Realm.Object {}
TemplateQuestion.schema = {
    name: "TemplateQuestion",
    properties:{
    templateCode: "string",
    categoryID: "int",
    questionFill: "string",
    questionOrder: "int",
    weight: "float",
    isRequired: "bool",
    templateQuestionID: "int",
    },
};