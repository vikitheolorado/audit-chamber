import Realm from 'realm';
export default class MsUserAuditee extends Realm.Object {}
MsUserAuditee.schema = {
    name: 'MsUserAuditee',
    primaryKey: 'PK_MsUserAuditee_ID',
    properties: {
        PK_MsUserAuditee_ID: 'int',    // primary key
        UserID:    'int',
        Username: {type: 'string', optional: true},
        FullName: {type: 'string', optional: true},
        Email: {type: 'string', optional: true},
        Position: {type: 'string', optional: true},
        Roles: {type: 'int', optional: true},
        GroupLineID: {type: 'int', optional: true},
        PlantID: {type: 'int', optional: true},
        active :"bool"
    }
};