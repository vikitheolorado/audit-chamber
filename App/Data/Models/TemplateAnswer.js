import Realm from 'realm';
export default class TemplateAnswer extends Realm.Object {}
TemplateAnswer.schema = {
    name: "TemplateAnswer",
    properties:{
        answerID: "int",
        templateQuestionID: "int",
        answerType: "string",
    },
};