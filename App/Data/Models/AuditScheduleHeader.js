import Realm from 'realm';
export default class AuditScheduleHeader extends Realm.Object {}
AuditScheduleHeader.schema = {
    name: 'AuditScheduleHeader',
    primaryKey: 'ScheduleCode',
    properties: {
        ScheduleCode:    'string',    // primary key
        PlantID: {type: 'string', optional: true},
        PlantDesc: {type: 'string', optional: true},
        PeriodStart: {type: 'string', optional: true},
        PeriodEnd: {type: 'string', optional: true},
        WeekToDateStart: {type: 'string', optional: true},
        WeekToDateEnd: {type: 'string', optional: true},
        YearStart: {type: 'string', optional: true},
		YearEnd: {type: 'string', optional: true},
		AuditType: {type: 'string', optional: true},
		TemplateCode: {type: 'string', optional: true}
    }
    // properties: {
    //     ScheduleCode:    'string',    // primary key
    //     PlantID: {type: 'int', optional: true},
    //     PeriodStart: {type: 'string', optional: true},
    //     PeriodEnd: {type: 'string', optional: true},
    //     WeekToDateStart: {type: 'int', optional: true},
    //     WeekToDateEnd: {type: 'int', optional: true},
    //     YearStart: {type: 'int', optional: true},
	// 	YearEnd: {type: 'int', optional: true},
	// 	AuditType: {type: 'string', optional: true},
	// 	TemplateCode: {type: 'string', optional: true}
    // }
    };