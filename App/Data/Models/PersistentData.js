import Realm from 'realm';
export default class PersistentData extends Realm.Object {}
PersistentData.schema = {
    name: 'PersistentData',
    properties: {
        Key: 'string',
        Value: 'string',
    }
};