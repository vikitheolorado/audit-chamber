import Realm from 'realm';
export default class Notification extends Realm.Object {}
Notification.schema = {
    name: 'Notification',
    primaryKey: 'NotificationID',
    properties: {
        NotificationID: 'int',
        Type: 'int',
        From: 'string',
        To: 'string',
        Message: 'string',
        Data: 'string',
        Date: 'date',
        ReadStatus: 'int'
    }
};