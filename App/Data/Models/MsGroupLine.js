import Realm from 'realm';
export default class MsGroupLine extends Realm.Object {}
MsGroupLine.schema = {
    name: 'MsGroupLine',
    primaryKey: 'GroupLineID',
    properties: {
		GroupLineID: 'int',
        GroupLineIDDesc: {type: 'string', optional: true}, 
        GroupLineIDAbb: {type: 'string', optional: true}, 
        PlantID: {type: 'int', optional: true},
        active :"bool"
    }
    };