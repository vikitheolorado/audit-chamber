import Realm from 'realm';
export default class TrxAuditAnswer extends Realm.Object {}
TrxAuditAnswer.schema = {
    name: "TrxAuditAnswer",
    primaryKey: "trxAuditAnswerID",
    properties:{
        trxAuditAnswerID: "int",
        auditScheduleDetailID: "int",
        templateQuestionID: "int",
        totalFindings: "int",
        answerID: "int",
        answerDate: "date"
    },
};