import Realm from 'realm';
export default class Version extends Realm.Object {}
Version.schema = {
    name: "Version",
    properties: {
        dbVersion: "string",
    },
};