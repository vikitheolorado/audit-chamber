import Realm from 'realm';
export default class AuditScheduleDetail extends Realm.Object {}
AuditScheduleDetail.schema = {
    name: 'AuditScheduleDetail',
    primaryKey: 'PK_AuditScheduleDetailID',
    properties: {
		PK_AuditScheduleDetailID: 'int',
        ScheduleCode: {type: 'string', optional: true}, 
        AuditorID: {type: 'string', optional: true},
        GroupLineID: {type: 'string', optional: true},
        AuditeeID: {type: 'string', optional: true},
        AuditScore: {type: 'string', optional: true},
        CompleteTask: {type: 'string', optional: true},
        TotalTask: {type: 'string', optional: true},
		AuditeeSignDate: {type: 'string', optional: true},
		AuditeeSignStatus: {type: 'string', optional: true},
		LastUpdateDate: {type: 'string', optional: true},
		StartDateTime: {type: 'string', optional: true},
		EndDateTime: {type: 'string', optional: true},
        ScheduleStatus: {type: 'string', optional: true},
        TotalFindings: {type: 'int', optional: true}
    }
    // properties: {
	// 	PK_AuditScheduleDetailID: 'int',
    //     ScheduleCode: {type: 'string', optional: true}, 
    //     AuditorID: {type: 'string', optional: true},
    //     GroupLineID: {type: 'int', optional: true},
    //     AuditeeID: {type: 'string', optional: true},
    //     AuditScore: {type: 'int', optional: true},
    //     CompleteTask: {type: 'int', optional: true},
    //     TotalTask: {type: 'int', optional: true},
	// 	AuditeeSignDate: {type: 'string', optional: true},
	// 	AuditeeSignStatus: {type: 'int', optional: true},
	// 	LastUpdateDate: {type: 'string', optional: true},
	// 	StartDateTime: {type: 'string', optional: true},
	// 	EndDateTime: {type: 'string', optional: true}
    // }
    };