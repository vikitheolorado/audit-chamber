import Realm from 'realm';
export default class TemplateCategory extends Realm.Object {}
TemplateCategory.schema = {
    name: "TemplateCategory",
    properties: {
        templateCode: "string",
        auditType: "int",
        templateStatus: "string",
        releasedDate: "date",
        categoryID: "int",
        categoryDesc: "string",
        evidenceID: "int",
        evidenceType: "string",
        categoryOrder: "int",
    },
};