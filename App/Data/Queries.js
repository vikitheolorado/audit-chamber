import { getInstance } from './DB/DBHelper';
import { Alert } from 'react-native';
import { Actions } from 'react-native-router-flux'

function getAllPersistentData() {
    let db = getInstance();
    return db.objects('PersistentData');
}

function getPersistentData(key) {
    let db = getInstance();
    let res = db.objects('PersistentData').filtered('Key = "' + key + '"');
    return (res.length > 0 ? res[0].Value : null);
}

function setPersistentData(key, value) {
    let db = getInstance();
    let data = db.objects('PersistentData').filtered('Key = "' + key + '"');
    if (data.length > 0) {
        db.write(() => {
            data[0].Value = value;
        });
    } else {
        db.write(() => {
            db.create('PersistentData', { Key: key, Value: value });
        });
    }
}

function getArrayQuestion(auditType, templateCode, auditScheduleDetailID) {
    let result = [];

    let db = getInstance();

    let sections = db.objects('TemplateCategory').filtered('auditType = ' + auditType + ' AND templateCode = "' + templateCode + '"').sorted('categoryOrder');
    sections.forEach((item, index) => {
        result[index] = {
            categoryID: item.categoryID,
            categoryDesc: item.categoryDesc,
            questions: [],
        };

        let questions = db.objects('TemplateQuestion').filtered('templateCode = "' + templateCode + '" AND categoryID = ' + item.categoryID).sorted('questionOrder');
        questions.forEach((value, key) => {
            result[index].questions[key] = {
                questionID: value.templateQuestionID,
                questionDesc: value.questionFill,
                weight: value.weight,
                isRequired: value.isRequired,
                options: [],
                answer: {
                    trxAuditAnswerID: -1,
                    answerID: __DEV__ ? 1 : -1,
                    answerDate: new Date(),
                    findings: [],
                },
            };

            let options = db.objects('TemplateAnswer').filtered('templateQuestionID = ' + value.templateQuestionID).sorted('answerID');
            options.forEach((val, field) => {
                result[index].questions[key].options[field] = {
                    optionID: val.answerID,
                    optionDesc: val.answerType,
                };
            });

            let answer = db.objects('TrxAuditAnswer').filtered('auditScheduleDetailID = ' + auditScheduleDetailID + ' AND templateQuestionID = ' + value.templateQuestionID).sorted('templateQuestionID');
            if (answer.length == 1) {
                result[index].questions[key].answer.trxAuditAnswerID = answer[0].trxAuditAnswerID;
                result[index].questions[key].answer.answerID = answer[0].answerID;
                result[index].questions[key].answer.answerDate = answer[0].answerDate;
                let findings = db.objects('AuditFindingList').filtered('trxAuditAnswerID = ' + answer[0].trxAuditAnswerID);
                findings.forEach((rec, i) => {
                    result[index].questions[key].answer.findings[i] = {
                        auditFindingListID: rec.auditFindingListID,
                        comments: rec.comments,
                        area: rec.area,
                        attachment: [],
                        findingDate: rec.createdDate
                    };

                    let attachments = db.objects('Attachment').filtered('auditFindingListID = ' + rec.auditFindingListID);
                    attachments.forEach((content, inc) => {
                        result[index].questions[key].answer.findings[i].attachment[inc] = {
                            attachmentID: content.attachmentID,
                            ext: content.extension,
                            type: content.type,
                            url: content.url
                        };
                    });
                });
            }
        });
    });
    return result;
}

function getNotifications(userID) {
    let db = getInstance();
    let notifications = db.objects('Notification').filtered('To = "' + userID + '"').sorted('Date', true);
    return notifications;
}

function getArrayArea(groupLineID) {
    let db = getInstance();
    let areas = db.objects('MsArea').filtered('groupLineID = ' + groupLineID).sorted('areaID');
    return areas;
}

function getArrayAreaActive(groupLineID) {
    let db = getInstance();
    let areas = db.objects('MsArea').filtered('groupLineID = ' + groupLineID + " AND active = true").sorted('areaID');
    return areas;
}

function getPlantDescByID(plantID) {
    let db = getInstance();
    let plant = db.objects('MsPlant').filtered('PlantID = ' + plantID);
    return (plant.length == 1 ? plant[0].PlantDesc : null);
}

function dateToStringFormat(dateObject) {
    return dateObject.getUTCFullYear().toString() + '-' + ('0' + (dateObject.getUTCMonth() + 1)).slice(-2) + '-' + ('0' + dateObject.getUTCDate()).slice(-2) + ' ' + ('0' + dateObject.getUTCHours()).slice(-2) + ':' + ('0' + dateObject.getUTCMinutes()).slice(-2) + ':' + ('0' + dateObject.getUTCSeconds()).slice(-2);
}

function dateToStringFormatLocal(dateObject) {
    return dateObject.getFullYear().toString() + '-' + ('0' + (dateObject.getMonth() + 1)).slice(-2) + '-' + ('0' + dateObject.getDate()).slice(-2) + ' ' + ('0' + dateObject.getHours()).slice(-2) + ':' + ('0' + dateObject.getMinutes()).slice(-2) + ':' + ('0' + dateObject.getSeconds()).slice(-2);
}

function stringToDateFormat(dateString) {
    //Alert.alert(dateString.substring(0, 4) + ' ' + dateString.substring(5, 7) + ' ' + dateString.substring(8, 10) + ' ' + dateString.substring(11, 13) + ' ' + dateString.substring(14, 16) + ' ' + dateString.substring(17));
    return new Date(Date.UTC(dateString.substring(0, 4), (parseInt(dateString.substring(5, 7)) - 1), dateString.substring(8, 10), dateString.substring(11, 13), dateString.substring(14, 16), dateString.substring(17)));
}

function getStartDate(auditScheduleDetailID) {
    let db = getInstance();
    let audit = db.objects('AuditScheduleDetail').filtered('PK_AuditScheduleDetailID = ' + auditScheduleDetailID);
    if (audit.length > 0) {
        if (audit[0].StartDateTime == null || audit[0].StartDateTime == '') {
            let currentDate = new Date();
            db.write(() => {
                audit[0].StartDateTime = dateToStringFormat(currentDate);
            });
            return currentDate;
        } else {
            return stringToDateFormat(audit[0].StartDateTime);
        }
    }
    return new Date();
}

function getEndDate(auditScheduleDetailID) {
    let db = getInstance();
    let audit = db.objects('AuditScheduleDetail').filtered('PK_AuditScheduleDetailID = ' + auditScheduleDetailID);
    if (audit.length > 0) {
        if (audit[0].EndDateTime == null || audit[0].EndDateTime == '') {
            let currentDate = new Date();
            return currentDate;
        } else {
            return stringToDateFormat(audit[0].EndDateTime);
        }
    }
    return new Date();
}

function saveAnswer(auditScheduleDetailID, templateQuestionID, answerID, totalFindings, answerDate) {
    let db = getInstance();
    let answers = db.objects('TrxAuditAnswer').filtered('auditScheduleDetailID = ' + auditScheduleDetailID + ' AND templateQuestionID = ' + templateQuestionID);
    if (answers.length > 0) {
        let answer = answers[0];

        db.write(() => {
            answer.answerID = answerID;
            answer.totalFindings = totalFindings;
            answer.answerDate = answerDate;
        });

        return answer.trxAuditAnswerID;
    } else {
        let lastAnswers = db.objects('TrxAuditAnswer').sorted('trxAuditAnswerID', true).slice(0, 1);
        let lastAnswer = 0;
        if (lastAnswers.length > 0) {
            lastAnswer = lastAnswers[0].trxAuditAnswerID;
        }

        db.write(() => {
            let finding = db.create('TrxAuditAnswer', {
                trxAuditAnswerID: lastAnswer + 1,
                auditScheduleDetailID: auditScheduleDetailID,
                templateQuestionID: templateQuestionID,
                totalFindings: totalFindings,
                answerID: answerID,
                answerDate: answerDate
            });
        });
        return lastAnswer + 1;
    }
}

function getAreaDescByID(areaID) {
    let db = getInstance();
    let area = db.objects('MsArea').filtered('areaID = ' + areaID);

    return (area.length > 0 ? area[0].areaDesc : null);
}

function isAuditeeRegistered(plantID, groupLineID, emailAuditee) {

    let db = getInstance();
    let userAuditee = db.objects('MsUserAuditee').filtered('PlantID = ' + plantID + ' AND GroupLineID = ' + groupLineID + ' AND Email ==[c] "' + emailAuditee + '"');

    return (userAuditee.length > 0 ? userAuditee[0].Username : null);
}



//Adhit
function getDataAnswer(auditFindingListID) {
    try {
        let db = getInstance();
        let tempObj = db.objects('AuditFindingList').filtered('auditFindingListID = ' + auditFindingListID);
        return tempObj[0]
    }
    catch (err) {
        console.log("Queries - getDataAnswer")
        console.log(err)
    }
}


function editComment(auditFindingListID, comments) {
    try {
        let db = getInstance();

        let tempFinding = db.objects('AuditFindingList').filtered('auditFindingListID = ' + auditFindingListID);

        console.log(tempFinding)
        console.log("auditFindingListID " + auditFindingListID)
        console.log(db.objects('AuditFindingList'))

        db.write(() => {
            db.create('AuditFindingList', { auditFindingListID: auditFindingListID, comments: comments, trxAuditAnswerID: tempFinding[0].trxAuditAnswerID, area: tempFinding[0].area, createdDate: tempFinding[0].createdDate }, true);
        })
        console.log("editComment Berhasil")

        console.log(db.objects("AuditFindingList"));

        console.log("diatas comment yang terbaru");
    }
    catch (ex) {
        console.log("editComment Gagal")
        console.log(ex)
    }

}

function deleteFindingList(auditFindingListID) {
    try {
        let db = getInstance();

        let dataReturn = 0;

        //Cek dulu apa finding yang akan dihapus adalah finding terakhir di pertanyaannya?
        //Jika iya, set auditanswerid jadi -1 (belum dijawab)

        let tempObj = db.objects('AuditFindingList').filtered('auditFindingListID = ' + auditFindingListID);

        if (db.objects('AuditFindingList').filtered('trxAuditAnswerID = ' + tempObj[0].trxAuditAnswerID).length == 1) {
            console.log("jumlah data 1")
            let tempAnswer = db.objects('TrxAuditAnswer').filtered('trxAuditAnswerID = ' + tempObj[0].trxAuditAnswerID);
            console.log(tempAnswer)
            db.write(() => {
                db.create('TrxAuditAnswer', {
                    trxAuditAnswerID: tempAnswer[0].trxAuditAnswerID,
                    auditScheduleDetailID: tempAnswer[0].auditScheduleDetailID,
                    templateQuestionID: tempAnswer[0].templateQuestionID,
                    totalFindings: (tempAnswer[0].totalFindings - 1),
                    answerID: -1,
                    answerDate: tempAnswer[0].answerDate
                }, true);
            })

            dataReturn = -1
            console.log("selesai update 1")
            console.log(db.objects('TrxAuditAnswer').filtered('trxAuditAnswerID = ' + tempObj[0].trxAuditAnswerID))
        }
        else {
            console.log("jumlah data > 1")
            let tempAnswer = db.objects('TrxAuditAnswer').filtered('trxAuditAnswerID = ' + tempObj[0].trxAuditAnswerID);
            console.log(tempAnswer)
            db.write(() => {
                db.create('TrxAuditAnswer', {
                    trxAuditAnswerID: tempAnswer[0].trxAuditAnswerID,
                    auditScheduleDetailID: tempAnswer[0].auditScheduleDetailID,
                    templateQuestionID: tempAnswer[0].templateQuestionID,
                    totalFindings: (tempAnswer[0].totalFindings - 1),
                    answerID: tempAnswer[0].answerID,
                    answerDate: tempAnswer[0].answerDate
                }, true);
            })

            dataReturn = tempAnswer[0].answerID
            console.log("selesai update > 1")
            console.log(db.objects('TrxAuditAnswer').filtered('trxAuditAnswerID = ' + tempObj[0].trxAuditAnswerID))
        }


        db.write(() => {
            getInstance().delete(getInstance().objects('Attachment').filtered('auditFindingListID = ' + auditFindingListID));
            getInstance().delete(getInstance().objects('AuditFindingList').filtered('auditFindingListID = ' + auditFindingListID));
        })
        console.log("deleteFindingList Berhasil")
        return dataReturn
    }
    catch (ex) {
        console.log("deleteFindingList Gagal")
        console.log(ex)
    }
}

function getListPlantActive(){
    let db = getInstance();
    let listplant = db.objects('MsPlant').filtered('active = true');

    return listplant;
}

function getListAuditee(plantID, groupLineID) {
    let db = getInstance();
    let userAuditee = db.objects('MsUserAuditee').filtered('PlantID = ' + plantID + ' AND GroupLineID = ' + groupLineID);

    return userAuditee;
}

function getListAuditeeActive(plantID, groupLineID) {
    let db = getInstance();
    let userAuditee = db.objects('MsUserAuditee').filtered('PlantID = ' + plantID + ' AND GroupLineID = ' + groupLineID + ' AND active = true');

    return userAuditee;
}

function getCurrentUser() {
    let db = getInstance();
    let users = db.objects('MsUser').filtered('isLogin = 1');
    return users;
}

function getGroupLineByPlantId(param) {
    let db = getInstance();
    let groupline = db.objects('MsGroupLine').filtered("active = true AND PlantID = " + param);
    return groupline;
}

function setAllUserNotLogin() {
    let db = getInstance();
    let users = db.objects('MsUser');
    db.write(() => {
        users.map(function (data, index) {
            db.create('MsUser', { UserID: data.UserID, isLogin: 0 }, true);
        })
    });
}
function saveAuditFindingList(trxAuditAnswerID, comments, area, createdDate) {
    if (trxAuditAnswerID != -1) {
        let db = getInstance();
        let lastFindings = db.objects('AuditFindingList').sorted('auditFindingListID', true).slice(0, 1);
        let lastFinding = 0;
        if (lastFindings.length > 0) {
            lastFinding = lastFindings[0].auditFindingListID;
        }
        console.log(JSON.stringify(
            {
                auditFindingListID: lastFinding + 1,
                trxAuditAnswerID: trxAuditAnswerID,
                comments: comments,
                area: area,
                createdDate: createdDate
            }
        ))
        db.write(() => {
            let finding = db.create('AuditFindingList', {
                auditFindingListID: lastFinding + 1,
                trxAuditAnswerID: trxAuditAnswerID,
                comments: comments,
                area: area,
                createdDate: createdDate
            });
        });

        return lastFinding + 1;
    }

    return -1;
}

function updateAttachment(attachmentID, auditFindingListID, url, ext, type, createdBy, createdDate) {
    if (attachmentID != -1) {
        let db = getInstance();
        let lastAttachments = db.objects('Attachment').filtered('attachmentID = ' + attachmentID).slice(0, 1);

        if (lastAttachments.length > 0) {
            let lastAttachment = lastAttachments[0];

            db.write(() => {
                lastAttachment.url = url;
                lastAttachment.extension = ext;
                lastAttachment.type = type;
                lastAttachment.createdBy = createdBy;
                lastAttachment.createdDate = createdDate;
            });
        }
    } else {
        return saveAttachment(auditFindingListID, url, ext, type, createdBy, createdDate);
    }
}

function saveAttachment(auditFindingListID, url, ext, type, createdBy, createdDate) {
    if (auditFindingListID != -1) {
        let db = getInstance();
        let lastAttachments = db.objects('Attachment').sorted('attachmentID', true).slice(0, 1);
        let lastAttachment = 0;
        if (lastAttachments.length > 0) {
            lastAttachment = lastAttachments[0].attachmentID;
        }
        db.write(() => {
            let attachment = db.create('Attachment', {
                attachmentID: lastAttachment + 1,
                auditFindingListID: auditFindingListID,
                url: url,
                extension: ext,
                type: type,
                createdBy: createdBy,
                createdDate: createdDate
            });
        });

        return lastAttachment + 1;
    }

    return -1;
}

function saveProgress(auditScheduleDetailID, finalScore, completeTask, totalTask) {
    let db = getInstance();
    let audit = db.objects('AuditScheduleDetail').filtered('PK_AuditScheduleDetailID = ' + auditScheduleDetailID);
    if (audit != null) {
        if (audit.length > 0) {
            let newAudit = audit[0];
            db.write(() => {
                newAudit.AuditScore = finalScore.toString();
                newAudit.CompleteTask = completeTask.toString();
                newAudit.TotalTask = totalTask.toString();
                newAudit.LastUpdateDate = dateToStringFormat(new Date());
            });
        }
    }
}

function syncNotifications(serverNotifications) {
    let db = getInstance();

    db.write(() => {
      serverNotifications.forEach((value, key) => {
        db.create("Notification", { NotificationID: value.NotificationID, Type: value.Type, From: value.FromNotif, To: value.ToNotif, Message: value.Message, Data: value.Data, Date: new Date(value.CreatedDate), ReadStatus: value.StatusRead == true ? 1 : 0 }, true);
      });
    });

    
    // serverNotifications.forEach((value, key) => {
    //     console.log("date after convert " + new Date(value.CreatedDate));
        //let localNotification = db.objects('Notification').filtered('NotificationID = ' + value.NotificationID);
        // if (localNotification.length == 0) {
        //     db.write(() => {
        //         let notification = db.create('Notification', {
        //             NotificationID: value.NotificationID,
        //             Type: value.Type,
        //             From: value.FromNotif,
        //             To: value.ToNotif,
        //             Message: value.Message,
        //             Data: value.Data,
        //             Date: new Date(value.CreatedDate),
        //             ReadStatus: (value.StatusRead == true ? 1 : 0)
        //         });
        //     });
        // } 
    // });
}

function getPlantIDByDesc(desc) {
    let db = getInstance();
    let plant = db.objects('MsPlant').filtered('PlantDesc = "' + desc + '"');
    return (plant.length == 1 ? plant[0].PlantID : null);
}

function ReadNotification(notificationID) {
    let db = getInstance();
    let notification = db.objects('Notification').filtered('NotificationID = ' + notificationID);
    if (notification.length > 0) {
        db.write(() => {
            notification[0].ReadStatus = 1;
        });
    }
}

function getAuditSchedule(AuditorId) {
    let db = getInstance();
    let AuditSchedule = db.objects('AuditSchedule').filtered('AuditorId = "' + AuditorId + '"');
    return AuditSchedule;
}

function saveAuditSchedule(AuditScheduleHeaderId, AuditScheduleDetailId, PlantDesc, GroupLineDesc, AuditorId, AuditorName,
    WeekToDateStart, WeekToDateEnd, YearStart, YearEnd, PeriodStart, PeriodEnd, AuditTypeId) {
    if (AuditScheduleDetailId != -1) {
        let db = getInstance();
        db.write(() => {
            let attachment = db.create('AuditSchedule', {
                AuditScheduleHeaderId: AuditScheduleHeaderId,
                AuditScheduleDetailId: AuditScheduleDetailId,
                PlantDesc: PlantDesc,
                GroupLineDesc: GroupLineDesc,
                AuditorId: AuditorId,
                AuditorName: AuditorName,
                WeekToDateStart: WeekToDateStart,
                WeekToDateEnd: WeekToDateEnd,
                YearStart: YearStart,
                YearEnd: YearEnd,
                PeriodStart: new Date(PeriodStart),
                PeriodEnd: new Date(PeriodEnd),
                AuditTypeId: AuditTypeId
            });
        });
        return 1;
    }

    return 1;
}

function deleteAuditSchedule(AuditorId) {
    let db = getInstance();
    let AuditSchedule = db.objects('AuditSchedule');
    db.write(() => {
        db.delete(AuditSchedule);
    });
    return (AuditSchedule);
}

function getCurrentUser() {
    let db = getInstance();
    let users = db.objects('MsUser').filtered('isLogin = 1');
    return users;
}

function getAuditStatus(auditScheduleDetailID) {
    let db = getInstance();
    let audit = db.objects('AuditScheduleDetail').filtered('PK_AuditScheduleDetailID = ' + auditScheduleDetailID);
    if (audit == null || audit.length <= 0) {
        return 0;
    }
    return parseInt(audit[0].ScheduleStatus);
}

function getAllAttachmentByFindingListId(id) {
    let db = getInstance();
    let allAttachment = db.objects('Attachment').filtered('auditFindingListID = ' + id);

    return allAttachment;
}

function deleteAttachmentById(id) {
    let db = getInstance();
    db.write(() => {
        getInstance().delete(getInstance().objects('Attachment').filtered('attachmentID = ' + id));
    })
}

function deleteAuditAnswerByAuditScheduleDetailID(id) {
    let db = getInstance();
    db.write(() => {
        getInstance().delete(getInstance().objects('TrxAuditAnswer').filtered('auditScheduleDetailID = ' + id));
    })
}

function deleteFindingListByAuditAnswerID(id) {
    let db = getInstance();
    db.write(() => {
        getInstance().delete(getInstance().objects('AuditFindingList').filtered('trxAuditAnswerID = ' + id));
    })
}

function deleteMediaByFindingListID(id) {
    let db = getInstance();
    db.write(() => {
        getInstance().delete(getInstance().objects('Attachment').filtered('auditFindingListID = ' + id));
    })
}

function sessionHabis(status) {
    if (status == '401') {
        //console.log("Information - CreateScheduleInc - PostCreateAuditSchedule - 401")
        Queries.setPersistentData('token', '');
        Queries.setAllUserNotLogin();

        Alert.alert('Information',
            `Your session has been expired, please login`,
            [

                {
                    text: 'OK',
                    onPress: () => {
                        Actions.Login({ type: 'replace' });
                    },
                    style: 'default'
                }
            ],
            { cancelable: false }
        )

    }
}

Queries = {
    getArrayQuestion: getArrayQuestion,
    getArrayArea: getArrayArea,
    saveAnswer: saveAnswer,
    saveAuditFindingList: saveAuditFindingList,
    getPlantDescByID: getPlantDescByID,
    getAllPersistentData: getAllPersistentData,
    getPersistentData: getPersistentData,
    setPersistentData: setPersistentData,
    getNotifications: getNotifications,
    saveAttachment: saveAttachment,
    getStartDate: getStartDate,
    getEndDate: getEndDate,
    syncNotifications: syncNotifications,
    getPlantIDByDesc: getPlantIDByDesc,
    ReadNotification: ReadNotification,
    getAreaDescByID: getAreaDescByID,
    isAuditeeRegistered: isAuditeeRegistered,
    getAuditSchedule: getAuditSchedule,
    saveAuditSchedule: saveAuditSchedule,
    deleteAuditSchedule: deleteAuditSchedule,
    getCurrentUser: getCurrentUser,
    setAllUserNotLogin: setAllUserNotLogin,
    getAuditStatus: getAuditStatus,
    saveProgress: saveProgress,
    dateToStringFormat: dateToStringFormat,
    dateToStringFormatLocal: dateToStringFormatLocal,
    stringToDateFormat: stringToDateFormat,
    getGroupLineByPlantId: getGroupLineByPlantId,
    sessionHabis: sessionHabis,
    getListAuditee: getListAuditee,
    deleteFindingList: deleteFindingList,
    editComment: editComment,
    getDataAnswer: getDataAnswer,
    updateAttachment: updateAttachment,
    getAllAttachmentByFindingListId: getAllAttachmentByFindingListId,
    deleteAttachmentById: deleteAttachmentById,
    deleteAuditAnswerByAuditScheduleDetailID : deleteAuditAnswerByAuditScheduleDetailID,
    deleteFindingListByAuditAnswerID : deleteFindingListByAuditAnswerID,
    deleteMediaByFindingListID : deleteMediaByFindingListID,
    getArrayAreaActive : getArrayAreaActive,
    getListAuditeeActive : getListAuditeeActive,
    getListPlantActive : getListPlantActive
};

export default Queries;