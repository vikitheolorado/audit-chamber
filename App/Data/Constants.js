this.Environment = 'DEV';
//this.Environment = "PROD";

Constants = {
    //APILink: 'http://ccaaudit.azurewebsites.net/api/' azure nawadata
    //APILink: 'https://ccaidprdauditchamber001.azurewebsites.net/api/',
    //APILink: 'https://ccaiddevauditchamber001.azurewebsites.net/api/', //dev
    //APILink: 'https://ac-prod.coca-colaamatil.co.id/api/', //prod
    //LinkFinding: 'https://ccaiddevauditchamber001.azurewebsites.net/capcenter/Finding/Index/', //Dev
    //LinkFinding: 'https://ccaidprdcapcenter001.azurewebsites.net/Finding/Index/', //Prod
    LinkFinding: this.Environment == 'DEV' ? "https://ccaiddevauditchamber001.azurewebsites.net/capcenter/Finding/Index/" : "https://ccaidprdcapcenter001.azurewebsites.net/Finding/Index/",
    Environment : this.Environment,
    APILink:  "http://10.5.50.12/ccaapi/api/",
    //APILink:  "http://192.168.8.114/ccaapi/api/",
    //APILink:  "http://192.168.0.120/ccaapi/api/",
    //APILink: __DEV__ ? 'http://10.5.50.12/ccaapi/api/' : (this.Environment == 'DEV' ? "https://ccaiddevauditchamber001.azurewebsites.net/api/" : "https://ccaidprdauditchamber001.azurewebsites.net/api/"),
    //APILink: this.Environment == 'DEV' ? "https://ccaiddevauditchamber001.azurewebsites.net/api/" : "https://ccaidprdauditchamber001.azurewebsites.net/api/",
    PDFPath:'/storage/emulated/0/Documents',
    MediaPath:'/storage/emulated/0/.NDSCCAAC',
    Timeout : 160000,
    GetTimeout : (60 * 1000) 
};

export default Constants;