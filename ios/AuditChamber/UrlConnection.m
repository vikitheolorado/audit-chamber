//
//  UrlConnection.m
//  AuditChamberV2
//
//  Created by Dev NDS on 11/8/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UrlConnection.h"
#import <React/RCTLog.h>

static UrlConnection *sharedInstance = nil;
static sqlite3 *database = nil;
static sqlite3_stmt *statement = nil;

dispatch_source_t CreateDispatchTimer(double interval, dispatch_queue_t queue, dispatch_block_t block)
{
  dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
  if (timer)
  {
    dispatch_source_set_timer(timer, dispatch_time(DISPATCH_TIME_NOW, interval * NSEC_PER_SEC), interval * NSEC_PER_SEC, (1ull * NSEC_PER_SEC) / 10);
    dispatch_source_set_event_handler(timer, block);
    dispatch_resume(timer);
  }
  return timer;
}

dispatch_source_t _timer;

@implementation UrlConnection

+ (void)connect:(NSArray *)name Token:(NSString *)token UrlAPI:(NSString *)urlapi withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion
{
  
  
   //Create Variable
    NSLog(@"Service: Masuk Pertama Kali");
    NSLog(@"Cek Token : %@", token);
  
    NSString *folderPath = nil;
    NSString *docsDir;
    NSArray *dirPaths;
    NSString *databasePath;
  
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
  
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString:[docsDir stringByAppendingPathComponent: @"coba7.db"]]; //Percobaan Menambahkan Field Flag
  
    NSFileManager *filemgr = [NSFileManager defaultManager];
    // Path Database
    const char *dbpath = [databasePath UTF8String];
    char *errMsg = NULL;
    // NSString *lagingapain = @"";
    NSString *proses    = @"Service: Proses";
    NSString *gagal     = @"Gagal, Errornya dimari -> ";
    NSString *berhasil  = @"Berhasil";
    NSString *gagalbuka = @"Buka Database Gagal";
  
    if ([filemgr fileExistsAtPath: databasePath ] == NO) { //Mengecek Database ada atau tidak
      if (sqlite3_open(dbpath, &database) == SQLITE_OK) { // Membuka File Database
        NSString *lagingapain = @"Buat Tabel";
        const char *sql_stmt = "create table if not exists media (ID INTEGER PRIMARY KEY AUTOINCREMENT, AuditScheduleDetailID integer,AuditAnswerId integer, LocalFindingID integer, LocalMediaID integer, Files blob, ExtFiles text, Flag integer DEFAULT 0)";
        if (sqlite3_exec(database, sql_stmt, NULL, NULL, NULL) != SQLITE_OK) {
          return NSLog(@"%@ %@ %@ %s", proses, lagingapain, gagal, errMsg);
        }
        NSLog(@"%@ %@ %@", proses, lagingapain, berhasil);
        sqlite3_close(database);
      } else { NSLog(@"%@ %@", proses, gagalbuka);}
    }
  
  @try {
    if (sqlite3_open(dbpath, &database) == SQLITE_OK) { // Membuka File Database
      NSString *Bearer = [NSString stringWithFormat:@"Bearer %@", token];

      NSString *lagingapain = @"Simpan Data";
      for(NSArray *itemjson in name) { // Loop data media
        NSError* error = nil;
        NSDictionary *JSON =
        [NSJSONSerialization JSONObjectWithData: [(NSString *) itemjson dataUsingEncoding:NSUTF8StringEncoding]
                                        options: NSJSONReadingMutableContainers error: &error];

        if (!JSON){ NSLog(@"%@ %@ %@ %@", proses, lagingapain, gagal, [error localizedDescription]); }
        else {
          NSString *Value1 = [JSON objectForKey:@"AuditScheduleDetailID"];
          NSString *Value2 = [JSON objectForKey:@"AuditAnswerId"];
          NSString *Value3 = [JSON objectForKey:@"LocalFindingID"];
          NSString *Value4 = [JSON objectForKey:@"LocalMediaID"];
          NSString *Value5 = [JSON objectForKey:@"Files"];
          NSString *Value6 = [JSON objectForKey:@"ExtFiles"];
          
          NSString *insertSQL =
          [NSString stringWithFormat:@"insert into media (AuditScheduleDetailID,AuditAnswerId,LocalFindingID,LocalMediaID,Files,ExtFiles) values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")", Value1, Value2, Value3, Value4, Value5, Value6];

          const char *insert_stmt = [insertSQL UTF8String];
          sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
          //          sqlite3_prepare_v2(
          if (sqlite3_step(statement) == SQLITE_DONE) { NSLog(@"%@ %@ %@ %@", proses, lagingapain, Value1, berhasil);}
          else{ NSLog(@"%@ %@ %@ %@ %@", proses, lagingapain, Value1, gagal, errMsg); }
        }
      }
      sqlite3_close(database);
    }
    NSString *strcallback =[NSString stringWithFormat:@"Ok"];
      completion(YES,nil,strcallback);
  } @catch (NSException *exception) {
    NSString *strcallback =[NSString stringWithFormat:@"No"];
      completion(NO,nil,strcallback);
  }
  
  dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
  double secondsToFire = 60.000f;
  sqlite3_open(dbpath, &database);
  _timer = CreateDispatchTimer(secondsToFire, queue, ^{
      // if (sqlite3_open(dbpath, &database) == SQLITE_OK) { // Membuka File Database
    
    NSLog(@"Cek Token 1 : %@", token);
    
        NSString *Bearer = [NSString stringWithFormat:@"Bearer %@", token];
        NSString *lagingapain = @"Ambil Data";

        sqlite3_stmt *compiledStatement;
        const char *sql_stmt = "select * from media WHERE Flag = 0";
        if(sqlite3_prepare_v2(database, sql_stmt, -1, &compiledStatement, NULL) == SQLITE_OK) {
          NSLog(@"%@ %@ %@ %s", proses, lagingapain, gagal, sqlite3_errmsg(database));
          NSLog(@"%@ %@ %@", proses, lagingapain, berhasil);

          NSMutableArray *arrDataRow;
          NSString *error;

          while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
            NSString *lagingapain = @"Ngirim Data API Media";
            arrDataRow = [[NSMutableArray alloc] init];
            int totalColumns = sqlite3_column_count(compiledStatement);
            NSLog(@"%@ Isi Data Ke -> %d",proses, sqlite3_column_int(compiledStatement, 0));

            for (int i=0; i<totalColumns; i++){
              char *dbDataAsChars = (char *)sqlite3_column_text(compiledStatement, i);
              if (dbDataAsChars != NULL) {
                  [arrDataRow addObject:[NSString  stringWithUTF8String:dbDataAsChars]];
              }
            }
            
            NSLog(@"check ext files %@", arrDataRow[6] );
            NSString *base64;
            
            if ([arrDataRow[6]  isEqual: @"mov"]){
              NSLog(@"Compress video ");
              NSURL *url = [[NSURL alloc] initWithString:arrDataRow[5]];
              NSString *path = [url path];
              NSData *nsdata = [[NSFileManager defaultManager] contentsAtPath:path];
              // Get NSString from NSData object in Base64
              base64 = [nsdata base64EncodedStringWithOptions:0];
              NSLog(@"base64 video %@", base64);
            }else if ([arrDataRow[6]  isEqual: @"jpg"]){
              NSLog(@"Compress image 1");
              NSURL *url = [[NSURL alloc] initWithString:arrDataRow[5]];
              NSString *path = [url path];
              NSData *nsdata = [[NSFileManager defaultManager] contentsAtPath:path];
              // Get NSString from NSData object in Base64
              base64 = [nsdata base64EncodedStringWithOptions:0];
              
              NSLog(@"base64 image %@", base64);
            }
            
            
              NSDictionary *firstJsonDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                  arrDataRow[1], @"AuditScheduleDetailID",
                                  arrDataRow[2], @"AuditAnswerId",
                                  arrDataRow[3], @"LocalFindingID",
                                  arrDataRow[4], @"LocalMediaID",
                                  base64, @"Files",
                                  arrDataRow[6], @"ExtFiles",
                                  nil];
            
            

              NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:firstJsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
              NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];

              NSString *post = [NSString stringWithFormat:jsonString]; // <--here put the request parameters you used to get the response
              NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
              NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
              NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
              NSString *UrlPostMedia = [NSString stringWithFormat:@"%@doaudit/postauditmedia", urlapi];
              NSLog(@"%@ %@", proses, UrlPostMedia);
              [request setURL:[NSURL URLWithString:UrlPostMedia]];
            
              [request setHTTPMethod:@"POST"];
              [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
              [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
              [request setValue:Bearer forHTTPHeaderField:@"Authorization"];
              [request setHTTPBody:postData];

              NSURLResponse *response;
              NSError *err;
              NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
              NSString *str=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];

              NSLog(@"%@ Melihat ID yang ke %@", proses, arrDataRow[0]);
              NSLog(@"%@ %@ Feedbacknya \"%@\"", proses, lagingapain, str);
              NSString *nol = @"0";

              if([str isEqualToString:nol]){

                NSLog(@"%@ %@ %@", proses, lagingapain, berhasil);

                NSString *lagingapain = @"Update Data";

                sqlite3_stmt *UpdateStatement;
                const char *update_stmt = [[NSString stringWithFormat:@"update media set Flag = 1 where ID = %@", arrDataRow[0]] UTF8String];

                sqlite3_prepare_v2(database, update_stmt, -1, &UpdateStatement, NULL);
                sqlite3_step(UpdateStatement);
              }else{ NSLog(@"%@ %@ %@ %@", proses, lagingapain, gagal, @"Karena Return Bukan 0");}
            //     //NSString *dict6  = [cleanJsonToObject:responseData];

          }
          // /*

          NSString *lagingapain = @"Compare Data";

          sqlite3_stmt *CountSelect;
          const char *CountSelectSQL = "SELECT m.AuditScheduleDetailID, COUNT(*) AS JumlahTotal, SUM( CASE WHEN Flag = 1 THEN 1 ELSE 0 END ) JumlahFlag FROM media AS m GROUP BY m.AuditScheduleDetailID ";
          sqlite3_prepare_v2(database, CountSelectSQL, -1, &CountSelect, NULL);


          while(sqlite3_step(CountSelect) == SQLITE_ROW) {
            arrDataRow = [[NSMutableArray alloc] init];
            int totalColumns = sqlite3_column_count(CountSelect);
            NSLog(@"%@ Baca Data AuditScheduleDetailID -> %d",proses, sqlite3_column_int(CountSelect, 0));
            for (int i=0; i<totalColumns; i++){
              char *dbDataAsChars = (char *)sqlite3_column_text(CountSelect, i);
              if (dbDataAsChars != NULL) {
                [arrDataRow addObject:[NSString  stringWithUTF8String:dbDataAsChars]];
              }
            }
            NSLog(@"%@ Baca Data Jumlah Total -> %d",proses, sqlite3_column_int(CountSelect, 1));
            NSLog(@"%@ Baca Data Jumlah Flag -> %d",proses, sqlite3_column_int(CountSelect, 2));
            if(sqlite3_column_int(CountSelect, 1) == sqlite3_column_int(CountSelect, 2)){
              NSLog(@"%@ Compare Count %d dan %d", proses, sqlite3_column_int(CountSelect, 1), sqlite3_column_int(CountSelect, 2));

              NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
              NSString *get = [NSString stringWithFormat:@"%@doaudit/postcompleteauditmedia?auditid=%d", urlapi, sqlite3_column_int(CountSelect, 0)];
              NSLog(@"%@ %@", proses, get);
              [request setURL:[NSURL URLWithString:get]];
              [request setHTTPMethod:@"GET"];
              [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
              [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
              [request setValue:Bearer forHTTPHeaderField:@"Authorization"];
              NSURLResponse *response;
              NSError *err;
              NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
              NSString *str=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];

              NSLog(@"%@ %@ Feedbacknya \"%@\"", proses, lagingapain, str);
              NSString *nol = @"0";

              if([str isEqualToString:nol]){
                NSString *lagingapain = @"Hapus Data";
                sqlite3_stmt *DeleteData;
                NSString *deleteSQL = [NSString stringWithFormat:@"delete from media where AuditScheduleDetailID = %d AND Flag = 1", sqlite3_column_int(CountSelect, 0)];
                const char *Delete_stmt = [deleteSQL UTF8String];
                sqlite3_prepare_v2(database, Delete_stmt, -1, &DeleteData, NULL);
                sqlite3_step(DeleteData);
                
                NSLog(@"%@ %@ %d %@", proses, lagingapain, sqlite3_column_int(CountSelect, 0), berhasil);
              }
            }else{ NSLog(@"%@ %@ %@ Data Tidak Sama", proses, lagingapain, gagal); }
          }

        }

        NSLog(@"Service : Melewati Pemanggilan Data");
      
//
       // return 0;
//        }
    
  });
  
  
}

- (void)cancelTimer
{
  if (_timer) {
    dispatch_source_cancel(_timer);
    _timer = nil;
  }
}
 @end


