//
//  Service.m
//  AuditChamberV2
//
//  Created by Dev NDS on 11/3/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Service.h"
#import "UrlConnection.h"
//#import "GantiYangLain.h"
#import <React/RCTLog.h>

static Service *sharedInstance = nil;
static sqlite3 *database = nil;
static sqlite3_stmt *statement = nil;

@implementation Service

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(addEvent:(NSArray *)name Token:(NSString *)token UrlAPI:(NSString *)urlapi callback:(RCTResponseSenderBlock)callback)
{
  
  [UrlConnection connect:name Token:token UrlAPI:urlapi withCompletion:^(BOOL success, NSError *error, id responce){
    if(success){
      NSLog(@"Coba Kirim antar function %@",responce); // here you get response once method camplet
      callback(@[responce]);
    }else{
      callback(@[responce]);
    }
  }];
  
//  callback(testreturn);
  
//  NSLog(@"Service: Percobaan Membuat Callback Dari URLConnection -> %@");
  
  [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
    NSLog(@"Background handler called. Not running background tasks anymore.");
    [[UIApplication sharedApplication] endBackgroundTask:self.backgroundTask];
    self.backgroundTask = UIBackgroundTaskInvalid;
  }];
}

@end


