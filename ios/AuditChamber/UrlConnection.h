//
//  UrlConnection.h
//  AuditChamberV2
//
//  Created by Dev NDS on 11/8/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#ifndef UrlConnection_h
#define UrlConnection_h

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface UrlConnection : NSObject {
  NSString *databasePath;
}
//@property (nonatomic) UIBackgroundTaskIdentifier backgroundTask;
@property (nonatomic, strong) NSMutableArray *arrColumnNames;
@property (nonatomic) int affectedRows;
@property (nonatomic) long long lastInsertedRowID;
@property (nonatomic, assign) int uniqueId;

+ (void)connect:(NSArray *)name Token:(NSString *)token UrlAPI:(NSString *)urlapi withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion;
- (void)cancelTimer;
@end

#endif /* UrlConnection_h */


