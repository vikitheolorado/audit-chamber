
//  Service.h
//  AuditChamberV2
//
//  Created by Dev NDS on 11/3/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#ifndef Service_h
#define Service_h
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <sqlite3.h>

@interface Service : NSObject <RCTBridgeModule> {
  NSString *databasePath;
}
//
//@property (nonatomic, strong) NSDecimalNumber *previous;
//@property (nonatomic, strong) NSDecimalNumber *current;
//@property (nonatomic) NSUInteger position;
//@property (nonatomic, strong) NSTimer *updateTimer;
@property (nonatomic) UIBackgroundTaskIdentifier backgroundTask;
@property (nonatomic, strong) NSMutableArray *arrColumnNames;
@property (nonatomic) int affectedRows;
@property (nonatomic) long long lastInsertedRowID;
@property (nonatomic, assign) int uniqueId;


@end

#endif /* Service_h */

